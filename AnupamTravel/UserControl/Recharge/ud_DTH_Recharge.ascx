﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ud_DTH_Recharge.ascx.cs" Inherits="DMT_Manager_User_Control_ud_DTH_Recharge" %>


<br />

<div class="row">
    <div class="col-md-2">
        <div class="theme-payment-page-form-item form-group  form-validation">
            <i class="fa fa-angle-down"></i>
            <select id="ddlA2ZDTHOprator" class="form-control"></select>
        </div>
    </div>
    <div class="col-md-2  hidden dthsecation">
        <div class="theme-payment-page-form-item form-group form-validation">
            <input type="text" class="form-control" id="txtA2ZDTHNumber" placeholder="DTH Number" minlength="15" maxlength="15" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
        </div>
    </div>
    <div class="col-md-2  hidden dthsecation">
        <div class="theme-payment-page-form-item form-group form-validation">
            <input type="text" class="form-control" id="txtA2ZDTHAmount" placeholder="Enter Amount" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
        </div>
    </div>
    <div class="col-md-2">
        <div class="theme-payment-page-form-item form-group">
            <span class="btn btn-primary btn-block" id="btnA2ZDTHRecharge" onclick="A2ZDTHRechargeEvent();">Recharge</span>
        </div>
    </div>
</div>

<br />
<div class="row" id="A2Zdthnotesmsg">
    <div class="col-md-12">
        <p class="text-danger">Notes:</p>
    </div>
    <div class="col-md-12">
        <p style="color: #a5a4a4;">
            <i class="fa fa-star text-danger" style="font-size: 8px;"></i>
            Please enter your DTH number from 15 to 15 digit.<br>
        </p>
    </div>
    <div class="col-md-12">
        <p style="color: #a5a4a4;"></p>
    </div>
</div>
<br />
<br />

<button type="button" class="btn btn-info btn-lg hidden dtha2zmodelclickclass" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#A2ZDTHModelSection"></button>
<div class="modal fade" id="A2ZDTHModelSection" role="dialog">
    <div class="modal-dialog" style="margin: 7% auto!important;">
        <div class="modal-content" id="modela2zdthheadbody"></div>
    </div>
</div>

<script src="../../Utilities/js/A2ZRecharge.js"></script>