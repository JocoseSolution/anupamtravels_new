﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="A2ZRecharge.ascx.vb" Inherits="UserControl_Recharge_A2ZRecharge" %>
<style>
    .text-warning {
        color: #ff9900;
    }

    input[type="radio"], input[type="checkbox"] {
        margin: 5px;
        -ms-transform: scale(1.5);
        -webkit-transform: scale(1.5);
        transform: scale(1.8);
        cursor:pointer;
    }
</style>

<br />
<div class="row">
    <div class="col-sm-6">
        <div class="theme-search-area-options theme-search-area-options-white theme-search-area-options-dot-primary-inverse clearfix">
            <span class="custom-control custom-radio custom-control-inline">
                <label class="mail active" for="rdoPrepaid" style="color: #fff;">
                    <input type="radio" id="rdoA2ZPrepaid" name="mobiletype" class="custom-control-input" value="A2ZPrepaid" checked="checked" />
                    Prepaid</label>
            </span>
            <span class="custom-control custom-radio custom-control-inline" style="margin-left: 15px;">
                <label class="mail" for="rdoPostpaid" style="color: #fff;">
                    <input type="radio" id="rdoA2ZPostpaid" name="mobiletype" class="custom-control-input" value="A2ZPostpaid" />
                    Postpaid</label>
            </span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <div class="theme-payment-page-form-item form-group form-validation">
            <input type="text" class="form-control" id="txtA2ZMobileNumber" placeholder="Mobile Number" maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
        </div>
    </div>
    <div class="col-md-2">
        <div class="theme-payment-page-form-item form-group form-validation">
            <i class="fa fa-angle-down"></i>
            <%--<select id="ddlA2ZMobileOprator" class="form-control" onchange="ShowA2ZGetRechargeOffer()">--%>
            <select id="ddlA2ZMobileOprator" class="form-control">
                <option value="0">Select Operator</option>
            </select>
        </div>
    </div>
    <div class="col-md-1" id="divA2ZGetRechargeOffer" style="display: none;">
        <span class="btn btn-primary" id="btnA2ZGetRechargeOffer" onclick="return GetA2ZGetRechargeOffer();">Get Offer</span>
    </div>
    <div class="col-md-2" id="A2ZAmountSection">
        <div class="theme-payment-page-form-item form-group form-validation">
            <input type="text" class="form-control" id="txtA2ZAmount" placeholder="Amount" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
        </div>
    </div>
    <div class="col-md-2">
        <span class="btn btn-primary" id="btnA2ZMobileRecharge" onclick="A2ZRechargeEvent();">Recharge</span>
    </div>
</div>

<br />
<div class="row" id="A2Zmobilenotesmsg">
    <div class="col-md-12">
        <p class="text-danger">Notes:</p>
    </div>
    <div class="col-md-12">
        <p style="color: #a5a4a4;">
            <i class="fa fa-star text-danger" style="font-size: 8px;"></i>
            Please enter your Mobile number from 10 to 10 digit.<br>
        </p>
    </div>
    <div class="col-md-12">
        <p style="color: #a5a4a4;"></p>
    </div>
</div>
<br />
<br />


<button type="button" class="btn btn-info btn-lg a2zmobilemodelclickclass" style="display: none;" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#A2ZMobileModelSection"></button>
<div class="modal fade" id="A2ZMobileModelSection" role="dialog">
    <div class="modal-dialog modal-lg" style="margin: 4% auto!important;">
        <div class="modal-content" id="a2zmodelmobileheadbody"></div>
    </div>
</div>

<script src="../../Utilities/js/A2ZRecharge.js"></script>
