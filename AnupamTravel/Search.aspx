﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search"
    MasterPageFile="~/MasterForHome.master" %>

<%--<%@ Register Src="~/UserControl/DashBoard.ascx" TagPrefix="uc1" TagName="DashBoard" %>--%>
<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>
<%@ Register Src="~/UserControl/FltSearchFixDep.ascx" TagName="IBESearchDep" TagPrefix="SearchDep" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />
    <style type="text/css">
    .wrapper {
        text-align: center;
    }
    .wrapper ul {
        display: inline-block;
        margin: 0;
        padding: 0;
        /* For IE, the outcast */
        zoom:1;
        *display: inline;
    }
    .wrapper li {
        float: left;
        padding: 2px 5px;
        /*border: 1px solid black;*/
    }
</style>
    <style type="text/css">
        .r-c
        {
            display: inline-block;
            color: #ff7e00;
            text-align: center;
            font-size: 15px;
            cursor: pointer;
        }
        
        .r-c1
        {
            margin-top: 4px;
            font-size: 12px;
            color: #080808;
            opacity: .5;
            display: block;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }
        
        
        #bgimg
        {
            background-image: url('Advance_CSS/Images/pexels-tobias-bjørkli-2104152.jpg');
            background-repeat: no-repeat;
            background-position: center center;
        }
    </style>
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.minus').click(function () {
                var $input = $(this).parent().find('input');
                var $inputid = $input.attr('id');
                var count = parseInt($input.val()) - 1;
                if ($inputid != "Adult") {
                    count = count <= 0 ? 0 : count;
                }
                else {
                    count = count < 1 ? 1 : count;
                }
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        /* Vanilla JS */

        var rightJS = {
            init: function () {
                rightJS.Tags = document.querySelectorAll('.rightJS');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.overflow = 'hidden';
                }
                rightJS.Tags = document.querySelectorAll('.rightJS div');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.position = 'relative';
                    rightJS.Tags[i].style.right = '-' + rightJS.Tags[i].parentElement.offsetWidth + 'px';
                }
                rightJS.loop();
            },
            loop: function () {
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    var x = parseFloat(rightJS.Tags[i].style.right);
                    x++;
                    var W = rightJS.Tags[i].parentElement.offsetWidth;
                    var w = rightJS.Tags[i].offsetWidth;
                    if ((x / 100) * W > w) x = -W;
                    if (rightJS.Tags[i].parentElement.parentElement.querySelector(':hover') !== rightJS.Tags[i].parentElement) rightJS.Tags[i].style.right = x + 'px';
                }
                requestAnimationFrame(this.loop.bind(this));
            }
        };
        window.addEventListener('load', rightJS.init);

        /* JQUERY */

        $(function () {
            var rightJQ = {
                init: function () {
                    $('.rightJQ').css({
                        overflow: 'hidden'
                    });
                    $('.rightJQ').on('mouseover', function () {
                        $('div', this).stop();
                    });
                    $('.rightJQ').on('mouseout', function () {
                        $('div', this).animate({
                            right: '100%'
                        }, 15000, 'linear');
                    });
                    rightJQ.loop();
                },
                loop: function () {
                    $('.rightJQ div').css({
                        position: 'relative',
                        right: '-100%'
                    }).animate({
                        right: '100%'
                    }, 15000, 'linear', rightJQ.loop);
                }
            };
            rightJQ.init();
        });

    </script>

       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #fff!important">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%If NotificationContentCount > 0 Then%>
                    <div class="slideshow-container notification noteexist">
                        <%=NotificationContent %>
                        <%--<a class="prev" onclick="plusSlides(-1)" style="background: rgb(238 49 87);">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)" style="background: rgb(238 49 87);">&#10095;</a>--%>
                    </div>
                    <%Else %>
                    <div class="slideshow-container notification"></div>
                    <%End If%>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-hero-area theme-hero-area-primary">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg ws-action " id="bgimg" data-parallax="true">
            </div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half">
            </div>
            <div class="theme-hero-area-inner-shadow theme-hero-area-inner-shadow-light">
            </div>
        </div>
        <div class="theme-hero-area-body">
            <div class="_pt-250 _pb-200 _pv-mob-50">
                <div class="container">
                    <div class="theme-search-area-tabs">
                        <div class="theme-search-area-tabs-header _c-w _ta-mob-c" style="text-align: center;">
                            <h1 class="theme-search-area-tabs-title">
                                Your Journey Begins</h1>
                            <p class="theme-search-area-tabs-subtitle">
                                Compare hundreds travel websites at once</p>
                        </div>
                        <div class="wrapper">
                            <ul class="nav nav-tabs" style="border-bottom: none !important;">
                                <li class="active"><a class="r-c actiontype" id="plane" data-valtext="mobile" href="#tab1default"
                                    data-toggle="tab"><i class="icofont-airplane icofont-2x"></i><span class="r-c1">Flight</span></a>
                                </li>
                                <li><a class="r-c actiontype" data-valtext="dth" id="bus" href="#tab2default" data-toggle="tab">
                                    <i class="icofont-bus-alt-1 icofont-2x"></i><span class="r-c1">Bus</span></a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                                <Search:IBESearch ID="IBESearch2" runat="server" />

                                <SearchDep:IBESearchDep runat="server" ID="FixDep" />
                            </div>
                            <div class="tab-pane fade" id="tab2default">
                                <Searchsss:BusSearch ID="Bus2" runat="server" />
                            </div>
                        </div>
                        <div class="tabbable" style="margin-top: 0px; position: relative;">
                            <%-- <ul class="nav nav-tabs nav-line nav-white nav-lg nav-mob-inline" role="tablist">

                                <li role="presentation" class="active">
                                    <a aria-controls="SearchAreaTabs-3" role="tab" data-toggle="tab" href="#SearchAreaTabs-3">Flights</a>
                                </li>

                                  <li role="presentation">
                                    <a aria-controls="SearchAreaTabs-3" role="tab" data-toggle="tab" href="#SearchAreaTabs-3">Bus</a>
                                </li>

                            </ul>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--old Design-->
    <div class="container" style="display: none;">
        <div class="row">
            <div class="col-sm-7 content">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="flight">
                        <Search:IBESearch ID="IBESearch1" runat="server" />
                    </div>
                    <div role="tabpanel" class="tab-pane" id="hotel" style="height: 395px;">
                        <Search:HotelSearch runat="server" ID="HotelSearch2" />
                    </div>
                    <div role="tabpanel" class="tab-pane" id="train" style="height: 395px;">
                        <%--<Searchsss:BusSearch ID="Bus2" runat="server" />--%>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="settings">
                        Dashboard</div>
                </div>
            </div>
        </div>
    </div>
    <nav style="display: none;">
	<ul>

 <%--       <li><button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
  Popover on top
</button></li>--%>

     <%--   <li>
            <a tabindex="0"
   href="#" 
   role="button" 
   data-html="true" 
   data-toggle="popover" 
   data-placement="bottom" 
   data-trigger="focus" 
   title="<b>Example popover</b> - title" 
   data-content="<div><a href='/link'><b>Geoff</b> - content</a></div><div><b>Hilary</b> - content</div>">RSS <i class="fa fa-rss" aria-hidden="true"></i></a>
        </li>--%>

		<li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="Last Booking" data-content="DEL → BOM | 17 Mar, 2020 | SG-456"><i class="fa fa-plane" aria-hidden="true"></i>  Last Searching</a></li>
        <li><a href="Report/TicketReport.aspx" > <i class="fa fa-book" ></i>  Last Booking</a></li>
		<li><a href="Report/Agent/TravelCalender.aspx" style="width: 160px;"><i class="fa fa-calendar" aria-hidden="true"></i>  Travel Calendar</a></li>
		<li><a href="Report/Accounts/BankDetails.aspx"><i class="fa fa-bank" aria-hidden="true"></i>  Bank Details</a></li>
		<li><a href="Report/Accounts/LedgerSingleOrderID.aspx"><i class="fa fa-file" aria-hidden="true"></i>  Ledger Details</a></li>
		<li><a href="Report/Accounts/AgentPanel.aspx"><i class="fa fa-inr" aria-hidden="true"></i>  Upload Amount</a></li>
		<asp:HiddenField ID="hdlastsearch" runat="server" />
	</ul>
</nav>
    <div class="booking-remarks" id="divStockistList" style="display: none;">
        <img id="imgCloseCHSch" src="Images/closebox.png" alt="" style="float: right; z-index: 9999;
            cursor: pointer; margin-top: -27px; margin-right: -20px;" title="Close" />
    </div>
    <div class="row" style="display: none;">
        <div id="toPopup" class="tbltbl large-12 medium-12 small-12">
            <div class="close">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
            <div id="popup_content">
                <!--your content start-->
                <table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif;
                    font-size: 12px; font-weight: normal; font-style: normal; color: #000000">
                    <tr>
                        <td>
                            <b>PNR :</b> <span id="PNR"></span>
                            <input id="txtPNRNO" name="txtPNRNO" type="hidden" />
                        </td>
                        <td id="TktNoInfo" style="display: none;">
                            <b>Ticket No:</b> <span id="TktNo"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: none;" id="PaxnameInfoResu">
                            <b>PAX NAME :</b> <span id="Paxname"></span>
                        </td>
                        <td style="display: none;" id="PaxnameInfoRefnd">
                            <div id="Refunddtldata" class="large-12 medium-12 small-12">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b><span id="RemarksTypetext"></span>Remark </b>
                            <input id="RemarksType" name="RemarksType" type="hidden" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea id="txtRemark" name="txtRemark" cols="56" rows="1" style="border: thin solid #808080"></textarea>
                        </td>
                    </tr>
                    <tr id="trCancelledBy" visible="false">
                        <td>
                            <b>Cancelled By:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="DrpCancelledBy" CssClass="drop" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width: 20%; margin-left: 40%;">
                                <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="buttonfltbk rgt w20" />
                                <input id="txtPaxid" name="txtPaxid" type="hidden" />
                                <input id="txtPaxType" name="txtPaxType" type="hidden" />
                                <input id="txtSectorid" name="txtSectorid" type="hidden" />
                                <input id="txtOrderid" name="txtOrderid" type="hidden" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--your content end-->
        </div>
        <div class="loader">
        </div>
        <div id="backgroundPopup">
        </div>
        <div id="HourDeparturePopup">
            <div class="close11">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrocol-md-4 col-sm-6 col-xs-5 nopadw">
            </span></span>
            <div class="HourDeparturepopup_content">
                <div class="large-12 medium-12 small-12">
                    <div class="clear">
                    </div>
                    <div class="large-12 medium-12 small-12 text-center ">
                        Click “OK” to proceed for offline request.</div>
                    <div class="clear">
                    </div>
                    <div class="large-4 medium-4 small-4">
                    </div>
                    <div style="margin-left: 100px;" class="rgt w100 buttonfltbkss">
                        OK</div>
                    <input id="txtPaxid_4HourDeparture" name="txtPaxid_4HourDeparture" type="hidden" />
                </div>
            </div>
        </div>
        <div id="htlRfndPopup">
            <div class="refundbox">
                <div style="font-weight: bold; font-size: 16px; text-align: center; width: 100%;">
                    <div id="RemarkTitle">
                    </div>
                    <div style="float: right; width: 20px; height: 20px; margin: -20px -13px 0 0;">
                        <a href="javascript:ShowHide('hide');">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>
                </div>
                <div class="large-12 medium-12 small-12">
                    <div class="laege-1 medium-1 small-1 columns bld">
                        Hotel Name:</div>
                    <div class="laege-5 medium-5 small-5 columns" id="HotelName">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="laege-1 medium-1 small-1 columns bld">
                        Net Cost:
                    </div>
                    <div class="laege-1 medium-1 small-1 columns" id="amt">
                    </div>
                    <div class="large-1 medium-1 small-1  medium-push-1 columns bld">
                        No. of Room:
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="room">
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">
                        No. of Night:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="night">
                    </div>
                    <div class="laege-1 medium-1 small-1 columns bld">
                        No. of Adult:</div>
                    <div class="laege-1 medium-1 small-1 columns" id="adt">
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">
                        No. of Child:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns end" id="chd">
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="policy" class="large-12 medium-12 small-12">
                </div>
                <div class="clear">
                </div>
                <div class="large-12 medium-12 small-12">
                    <div style="font-weight: bold;">
                        Cancellation Remark:
                    </div>
                    <div>
                        <textarea id="txtRemarkss" cols="40" rows="2" name="txtRemarkss"></textarea>
                    </div>
                    <div style="float: right; padding-left: 40px;">
                        <asp:Button ID="btn_Refund" runat="server" Text="Hotel Cancelltion" CssClass="button"
                            ToolTip="Auto Cancel of Hotel" OnClientClick="return RemarkValidation('cancellation')" />
                    </div>
                    <div class="clear1">
                    </div>
                </div>
                <div>
                    <input id="StartDate" type="hidden" name="StartDate" />
                    <input id="EndDate" type="hidden" name="EndDate" />
                    <input id="Parcial" type="hidden" name="Parcial" value="false" />
                    <input id="OrderIDS" type="hidden" name="OrderIDS" />
                </div>
                <div style="visibility: hidden;">
                    <div style="font-weight: bold;">
                        Full Cancellation
                        <input id="ChkFullCan" type="radio" name="Can" checked="checked" />
                    </div>
                    <div style="font-weight: bold; padding-left: 20px;">
                        Partial Cancellation
                        <input id="ChkParcialCan" type="radio" name="Can" />
                    </div>
                    <%-- <tr><td colspan="3" class="PrcialRegardCancelMsg"></td></tr>--%>
                </div>
            </div>
        </div>
    </div>
	 <!-- <div class="modal fade" id="myModal" role="dialog" style='z-index: 9999999;'>
        <div class="modal-dialog" style="width: 730px;">

            <!-- Modal content-->
            <div class="modal-content" style="display:none;">
                <div class="modal-header" style="background: #fff  !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <img src="Advance_CSS/notification/image/holiimage.jpg" class="img-responsive" style="height: 467px;" />
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        $(window).load(function () {
            $("#myModal").modal('show');
        });
    </script> 
    <script type="text/javascript">
        $(document).ready(function () {

            $("#change").on('click', function () {
                var pickup = $('#txt-pickup').val();
                $('#txt-pickup').val($('#txt-destination').val());
                $('#txt-destination').val(pickup);


            });

            $("#plane").click(function () {
                $("#bgimg").css("background-image", "url(Advance_CSS/Images/pexels-tobias-bjørkli-2104152.jpg)");

            });

            $("#bus").click(function () {
                $("#bgimg").css("background-image", "url(Advance_CSS/Images/bus.jpg)");
            });

        });
    </script>
    <%-- <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />

    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>--%>
    <%--    <script type="text/javascript">
        $("#rdbMultiCity").click(function () {

            $("#multisit").removeClass("searchengine searchbg").addClass("searchenginess searchbg ");
            $(".toptxt").hide();

        });

        $(".toptxt").show();

        //$('.carousel').carousel({
        //    interval: 1000
        //})

        if ('<%=Request("Htl")%>' == 'H') {
            $("#img2-").show();
            $("#img1").hide();
        }
        else {
            $("#img1").show();
            $("#img2").hide();
        }


    </script>--%>
    <%--<script type="text/javascript">

        $(document).ready(function () {


            $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });




            $("#CB_GroupSearch").click(function () {
                if ($(this).is(":checked")) {
                    // $("#box").hide();
                    $(".Traveller").hide();
                    $("#rdbRoundTrip").attr("checked", true);
                    $("#rdbOneWay").attr("checked", false);

                } else {
                    // $("#box").show();
                    $(".Traveller").show();
                }
            });
        });
    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>
    <script type="text/javascript">

        $('.btn').on('click', function () {
            var $this = $(this);
            $this.button('loading');
            setTimeout(function () {
                $this.button('reset');
            }, 8000);
        });
    </script>
    <script type="text/javascript">
        $(function () {
            debugger;
            // Enables popover
            $("[data-toggle=popover]").popover();
            container: 'body'
            $("lsearch").prepend($("hdlastsearch").val);
        });
    </script>
   <script type="text/javascript">

       $(window).load(function () {
           if ($(".notification").hasClass("noteexist")) {
               $("#exampleModal").modal('show');
           }
       });

       //$(window).load(function () {
       //    if (localStorage.getItem('isnotification') != 'notyshown') {
       //        $('#exampleModal').modal('show');
       //        localStorage.setItem('isnotification', 'notyshown');
       //    }
       //});
   </script>
</asp:Content>
