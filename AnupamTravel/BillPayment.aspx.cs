﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using SWIFTMoneyBillPayments;
using Newtonsoft.Json;

public partial class BillPayment : System.Web.UI.Page
{
    private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();
            Session["Validate"] = UserId;
        }
        else
        {
            Response.Redirect("/");
        }
    }

    #region [Web Method Calling Section]
    [WebMethod]
    public static string BindOpratorByType(string servicetype, string selectname)
    {
        string result = string.Empty;

        try
        {
            result = SMBPApiService.GetMobileOprator(servicetype, selectname);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> BindInputByLabelDel(string nametype, string servicetype, string spkey, string fetchid, string isbillfetch)
    {
        List<string> resultStr = new List<string>();

        try
        {
            StringBuilder result = new StringBuilder();
            StringBuilder labels = new StringBuilder();

            labels.Append("<div class='col-md-12'><p class='text-danger'>Notes:</p></div>");

            DataTable dtLabel = SMBPApiService.GetInputByLabelDel(fetchid);

            if (isbillfetch.ToLower().Trim() == "false" && nametype != "mobile")
            {
                DataRow row = dtLabel.NewRow();
                row["BillUpdation"] = "";
                row["Index"] = "1";
                row["Labels"] = "Amount";
                row["FieldMinLen"] = "1";
                row["FieldMaxLen"] = "10";
                dtLabel.Rows.Add(row);
            }

            List<string> indexcount = new List<string>();

            string BillUpdation = string.Empty;
            string Index = string.Empty;
            string Labels = string.Empty;
            string FieldMinLen = string.Empty;
            string FieldMaxLen = string.Empty;

            labels.Append("<div class='col-md-12'><p style='color: #a5a4a4;'>");
            for (int i = 0; i < dtLabel.Rows.Count; i++)
            {
                if (!indexcount.Contains(dtLabel.Rows[i]["Index"].ToString()))
                {
                    BillUpdation = dtLabel.Rows[i]["BillUpdation"].ToString();
                    Index = dtLabel.Rows[i]["Index"].ToString();
                    Labels = dtLabel.Rows[i]["Labels"].ToString().Replace(":", "").Trim();
                    FieldMinLen = dtLabel.Rows[i]["FieldMinLen"].ToString();
                    FieldMaxLen = dtLabel.Rows[i]["FieldMaxLen"].ToString();

                    string labelname = Regex.Replace(Labels, @"\s", "");
                    string strinputname = servicetype + labelname;
                    string dateclass = Labels.ToLower().Contains("date") == true ? "commonDate" : "";
                    string dateplaceholder = Labels.ToLower().Contains("date") == true ? Labels + " (dd/mm/yyyy) " : Labels;

                    string putmaxlen = !string.IsNullOrEmpty(FieldMaxLen) && FieldMaxLen != "0" ? "maxlength='" + FieldMaxLen + "'" : string.Empty; ;

                    result.Append("<div class='col-md-2 " + nametype + "dynamicinput' data-" + nametype + "id='txt" + strinputname + "' data-labels='" + labelname + "'><div class='theme-payment-page-form-item form-group form-validation'><input type='text' class='form-control " + dateclass + "' id='txt" + strinputname + "' minlength='" + FieldMinLen + "' " + putmaxlen + " placeholder='" + dateplaceholder + "'/></div></div>");

                    labels.Append(GetLabelMinMaxNotes(Labels, FieldMinLen, FieldMaxLen) + "<br/>");

                    indexcount.Add(dtLabel.Rows[i]["Index"].ToString());
                }
            }

            labels.Append("</p></div>");

            labels.Append("<div class='col-md-12'><p style='color: #a5a4a4;'>" + GetLabelUpdationNotes(BillUpdation, string.Empty) + "</p></div>");

            resultStr.Add(result.ToString());
            resultStr.Add(labels.ToString());
        }
        catch (Exception)
        {

            throw;
        }

        return resultStr;
    }

    private static string GetLabelMinMaxNotes(string labelname, string minlen, string maxlen)
    {
        return "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Please enter your " + labelname + " from " + minlen + " to " + maxlen + " digit.";
    }

    private static string GetLabelUpdationNotes(string billupdation, string servicetype)
    {
        var result = "";

        if (billupdation == "T+0")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take one working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+1")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take two working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+2")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take three working days to consider bill paid in their accounts.";
        }
        else if (billupdation == "T+3")
        {
            result = "<i class='fa fa-star text-danger' style='font-size: 8px;'></i> Your service provider will take four working days to consider bill paid in their accounts.";
        }
        return result;
    }

    #region [Mobile Section] 
    [WebMethod]
    public static List<string> MobileRecharge(string mobile, string opratorname, string oprator, string circle, string amount, string isbillfetch, string rchtype, string fetchid)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                HttpContext.Current.Session["CustomerName"] = null;
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (rchtype.ToLower().Trim() == "prepaid")
                    {
                        string heading = string.Empty;
                        if (circle != "0")
                        {
                            heading = "prepaid Mobile Payment";
                        }
                        else
                        {
                            heading = "Postpaid Bill Payment";
                        }

                        result = OperatorPaymentEvent(heading, mobile, opratorname, oprator, circle, amount, null, fetchid);
                    }
                    else
                    {
                        string billFetch = BillFetch(UserId, mobile, oprator, circle);

                        result = BillFetchMessage("Postpaid Mobile Bill Detail", billFetch, "PostPaidMobile");
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BillFetch(string agentId, string number, string spKey, string circleID, List<string> optional = null)
    {
        HttpContext.Current.Session["CustomerName"] = null;
        return SMBPApiService.BBPSBillFetch(agentId, number, spKey, circleID, optional);
    }
    #endregion

    #region [DTH Section]
    [WebMethod]
    public static List<string> DTHPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("DTH Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Electricity Section]
    [WebMethod]
    public static List<string> ElectricityBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Electricity Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> ElectricityPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Electricity Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Landline Section]
    [WebMethod]
    public static List<string> LandlineBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Landline Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> LandlinePayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Landline Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Insurance Section]
    [WebMethod]
    public static List<string> InsuranceBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Insurance Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> InsurancePayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Insurance Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Gas Section]
    [WebMethod]
    public static List<string> GasBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Gas Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> GasPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Gas Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Broadband Section]
    [WebMethod]
    public static List<string> InternetBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Broadband Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> BroadbandPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Broadband Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [Water Section]
    [WebMethod]
    public static List<string> WaterBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Water Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> WaterPayment(string spkeytext, string spkey, List<string> optionsvalue, string fetchid)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Water Bill Payment", number, spkeytext, spkey, "0", amount, option, fetchid);
    }
    #endregion

    #region [FasTag Section]
    [WebMethod]
    public static List<string> FasTagBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("FasTag Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> FasTagPayment(string spkeytext, string spkey, List<string> optionsvalue)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("FasTag Bill Payment", number, spkeytext, spkey, "0", amount, option);
    }
    #endregion

    #region [EMI Section]
    [WebMethod]
    public static List<string> EMIBillFeatch(string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        return GetBillFeatch("Loan RePayment Bill Detail", spkey, optionsname, optionsvalue, actiontype);
    }

    [WebMethod]
    public static List<string> EMIPayment(string spkeytext, string spkey, List<string> optionsvalue)
    {
        string amount = string.Empty;
        string number = string.Empty;
        List<string> option = new List<string>();

        if (optionsvalue != null && optionsvalue.Count > 0)
        {
            int loopCounting = 0;
            foreach (var item in optionsvalue)
            {
                if (loopCounting == 0)
                {
                    number = item;
                }
                else
                {
                    if (loopCounting == (optionsvalue.Count - 1))
                    {
                        amount = item;
                    }
                    else
                    {
                        option.Add(item);
                    }
                }

                loopCounting = loopCounting + 1;
            }
        }

        return OperatorPaymentEvent("Loan RePayment Bill Payment", number, spkeytext, spkey, "0", amount, option);
    }
    #endregion

    private static List<string> BillFetchMessage(string heading, string fetchRespo, string actiontype)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(fetchRespo))
        {
            dynamic rech = JObject.Parse(fetchRespo);

            string responseCode = rech.ResponseCode;
            string responseMessage = rech.ResponseMessage;
            string dueamount = rech.dueamount;
            string duedate = rech.duedate != null ? rech.duedate : "NA";
            string customername = rech.customername != "NA" ? rech.customername : "- - -";
            string billnumber = rech.billnumber != "NA" ? rech.billnumber : "- - -";
            string billdate = rech.billdate != "NA" ? rech.billdate : "- - -";

            string acceptPartPay = rech.acceptPartPay;
            string BBPSCharges = rech.BBPSCharges;
            string BillUpdate = rech.BillUpdate;
            string RequestID = rech.RequestID;
            string ClientRefId = rech.ClientRefId;

            if (duedate.ToLower() != "na" && duedate.ToLower() != "null")
            {
                DateTime currDate = new DateTime();
                currDate = Convert.ToDateTime(duedate);
                duedate = currDate.ToString("dd MMM yyyy");
            }
            else
            {
                duedate = "- - -";
            }

            string finalamt = (Convert.ToDecimal(!string.IsNullOrEmpty(dueamount) ? dueamount : "0") + Convert.ToDecimal(!string.IsNullOrEmpty(BBPSCharges) ? BBPSCharges : "0")).ToString();

            result.Add(responseCode);
            if (responseCode == "000")
            {
                HttpContext.Current.Session["CustomerName"] = customername;

                string partAmtPay = string.Empty;
                if (!string.IsNullOrEmpty(acceptPartPay) && acceptPartPay.Trim().ToLower() == "y")
                {
                    partAmtPay = "<br/><input id='txtPart" + actiontype + "Amount' type='text' value='" + dueamount + "' class='form-control' style='height:40px;width:160px;text-align:center;'/>";
                }

                string respo = "<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;" + heading + "</h5>"
                     + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                     + "<div class='modal-body'><div class='row'>"
                     + "<div class='col-sm-5'><span style='font-size: 15px;'>Request ID</span><p>" + RequestID + "</p></div>"
                     + "<div class='col-sm-7 text-right'><span>Customer Name</span><p>" + customername + "</p></div>"
                     + "</div><hr />"
                     + "<div class='row'>"
                    + "<div class='col-sm-6'><span style='font-size: 15px;'>Due Amount : ₹ " + dueamount + "</span><br /><span style='color: #8d8d8d;'>Charges : ₹ " + (!string.IsNullOrEmpty(BBPSCharges) ? BBPSCharges : "0") + "</span>" + partAmtPay + "</div>"
                     + "<div class='col-sm-6 text-right'><s" +
                     "pan>Payment Due Date</span><p>" + duedate + "</p></div>"
                     + "</div><hr />"
                     + "<div class='row'>"
                     + "<div class='col-sm-4'><span style='font-size: 15px;'>Bill Number</span><p>" + billnumber + "</p></div>"
                     + "<div class='col-sm-8 text-right'><span>Billing Date</span><p>" + billdate + "</p></div>"
                     + "</div></div>";

                if (finalamt != "0")
                {
                    respo += "<div class='modal-footer'><button type='button' id='btn" + actiontype + "Payment' class='btn btn-success' onclick='" + actiontype + "PaymentSubmit();'>Bill Pay</button></div>"
                        + "<input type='hidden' id='hdnTotal" + actiontype + "PaidAmt' value='" + finalamt + "'/>";
                }
                result.Add(respo);
            }
            else
            {
                result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "</h5>"
                                              + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                              + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                                              + "<h5 class='text-danger'>" + responseMessage + "</h4>"
                                              + "</div></div>");
            }
        }
        else
        {
            result.Add("notavl");
            result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;Failed</h5>"
                              + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                              + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                              + "<h5>Error occurred, Please try again after sometime.</h4>"
                              + "</div></div>");
        }

        return result;
    }

    private static List<string> OperatorPaymentEvent(string heading, string number, string spkeytext, string spKey, string circleID, string amount, List<string> optionsvalue = null, string fetchId = null)
    {
        List<string> result = new List<string>();

        try
        {
            List<string> ledResult = Ledgerandcreditlimit_Transaction(amount, number, spkeytext, "Debit", "DR", "DEBIT NOTE");
            if (ledResult != null && ledResult.Count > 0)
            {
                if (ledResult[0] == "000")
                {
                    string clientRefId = ledResult[1];

                    string paymentRespo = DoOpratorPayment(clientRefId, spKey, circleID, number, amount, optionsvalue, fetchId);
                    //string paymentRespo = "{\"ResponseCode\":\"000\",\"ResponseMessage\":\"Transaction Successful\",\"TransactionId\":\"100062347\",\"AvailableBalance\":\"10487.54\",\"ClientRefId\":\"B2BAPI7F22A7EC60\",\"OperatorTransactionId\":\"150224477514A12C55D5\"}";

                    if (!string.IsNullOrEmpty(paymentRespo))
                    {
                        dynamic rech = JObject.Parse(paymentRespo);
                        string responseCode = rech.ResponseCode;
                        if (responseCode != "000" && responseCode != "999")
                        {
                            List<string> rufLedResult = Ledgerandcreditlimit_Transaction(amount, number, spkeytext, "Credit", "CR", "CREDIT NOTE");
                        }
                    }

                    result = DoOpratorPaymentMessage(heading, paymentRespo, amount, clientRefId, spkeytext);
                }
                else
                {
                    result = LedgerMessage(ledResult);

                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string DoOpratorPayment(string clientRefId, string spKey, string circleID, string number, string amount, List<string> optionsvalue, string fetchId = null)
    {
        string result = string.Empty;

        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            UserId = HttpContext.Current.Session["Validate"].ToString();
            string custName = HttpContext.Current.Session["CustomerName"] != null ? HttpContext.Current.Session["CustomerName"].ToString() : "- - -";

            result = SMBPApiService.BBPSPayment(UserId, clientRefId, number, spKey, circleID, amount, optionsvalue, custName, fetchId);
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    private static List<string> LedgerMessage(List<string> respoList)
    {
        List<string> result = new List<string>();

        result.Add(respoList[0]);
        result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title' style='color: #a94442;'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occured!</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                     + "<h5>" + respoList[1] + "</h5>"
                    + "</div></div>");

        return result;
    }

    private static List<string> DoOpratorPaymentMessage(string heading, string reqstr, string amount, string clientRefId, string spkeytext)
    {
        List<string> result = new List<string>();

        if (reqstr != null && !string.IsNullOrEmpty(reqstr))
        {
            dynamic rech = JObject.Parse(reqstr);

            string responseCode = rech.ResponseCode;
            string responseMessage = rech.ResponseMessage;
            string transactionId = rech.TransactionId;
            string availableBalance = rech.AvailableBalance;
            string resClientRefId = rech.ClientRefId;
            string operatorTransactionId = rech.OperatorTransactionId;

            DateTime currDate = DateTime.Now;
            string strCurrDate = currDate.ToString("dd MMM yyyy, h:mm tt");

            result.Add(responseCode);

            if (responseCode == "000")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-success' id='rechheading'><i class='fa fa-check-circle'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else if (responseCode == "999")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-warning' id='rechheading'><i class='fa fa-clock'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "-" + responseMessage + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + transactionId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + transactionId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
        }

        return result;
    }

    private static List<string> GetBillFeatch(string modelheading, string spkey, List<string> optionsname, List<string> optionsvalue, string actiontype)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string number = string.Empty;
                    List<string> optional = new List<string>();

                    if (optionsvalue != null)
                    {
                        int counting = 1;
                        foreach (var item in optionsvalue)
                        {
                            if (counting == 1)
                            {
                                number = item;
                            }
                            else
                            {
                                optional.Add(item);
                            }
                            counting = counting + 1;
                        }
                    }

                    string billFetch = BillFetch(UserId, number, spkey, "0", optional);
                    //string billFetch = "{\"ResponseCode\":\"000\",\"ResponseMessage\":\"Transaction Successful\",\"dueamount\":420.0,\"duedate\":\"2020-08-08\",\"customername\":\"KRISHAN\",\"billnumber\":\"152686866\",\"billdate\":\"01 Jan 0001\",\"acceptPartPay\":\"N\",\"BBPSCharges\":\"\",\"BillUpdate\":\"T+1\",\"RequestID\":\"3320190805276240\",\"ClientRefId\":\"D2D3EF958EB04FE\"}";

                    result = BillFetchMessage(modelheading, billFetch, actiontype);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetBillTransactionHistory(string fromdate, string todate, string clintrefid, string transtype, string status, string defaultdate)
    {
        List<string> result = new List<string>();
        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            UserId = HttpContext.Current.Session["Validate"].ToString();
            if (!string.IsNullOrEmpty(UserId))
            {
                if (defaultdate == "today")
                {
                    DateTime currDate = DateTime.Now;
                    fromdate = currDate.ToString("dd/MM/yyyy");
                }

                DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, status);

                result.Add("success");
                result.Add(BindBillTransDetails(dtTrans));
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    private static string BindBillTransDetails(DataTable dtTrans)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string refund_html = "- - -";
                string status = dtTrans.Rows[i]["Status"].ToString();
                string alterstatus = status;

                bool isRefund = dtTrans.Rows[i]["Refund"].ToString().ToLower() == "true" ? true : false;

                if (status.ToLower().Trim() == "transaction under process" || status.ToLower().Trim().Contains("processed"))
                {
                    status = "<td class='text-warning'>UNDER PROCESS</td>";
                    alterstatus = "UNDER PROCESS";
                }
                else if (status.ToLower().Trim() == "transaction successful" || status.ToLower().Trim().Contains("success"))
                {
                    status = "<td class='text-success'>SUCCESS</td>";
                    alterstatus = "SUCCESS";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>DUPLICATE</td>";
                    alterstatus = "DUPLICATE";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-paymentid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundBillFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else if (status.ToLower().Trim() == "transaction failed" || status.ToLower().Trim() == "failed from simulator" || status.ToLower().Trim().Contains("invalid") || status.ToLower().Trim().Contains("failure") || status.ToLower().Trim().Contains("failed"))
                {
                    status = "<td class='text-danger'>FAILED</td>";
                    alterstatus = "FAILED";
                    if (isRefund)
                    {
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }
                    else
                    {
                        refund_html = "<span class='btn btn-primary' style='height:auto!important;line-height: 10px!important;padding: 5px;' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "' data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundBillFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    }
                }
                else if (status.ToLower().Trim() == "pending")
                {
                    status = "<td class='text-danger'>PENDING</td>";
                    alterstatus = "PENDING";
                }
                else
                {
                    status = "<td>--</td>";
                    alterstatus = "FAILED";
                    //if (isRefund)
                    //{
                    //    refund_html = "<span style='color: blue;font-weight: bold;'>REFUND</span>";
                    //}
                    //else
                    //{
                    //    refund_html = "<span class='btn btn-primary' style='height:auto!important;line-height: 10px!important;padding: 5px;' data-reportid='" + dtTrans.Rows[i]["RefundId"].ToString() + "'  data-amount='" + dtTrans.Rows[i]["Amount"].ToString() + "' data-transid='" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' id='btnBillRefund_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' onclick='RefundFaildAmount(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ");' style='padding: 0rem 0.3rem;font-size: 12px;'>Refund</span>";
                    //}
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TransactionId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ClientRefId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["Number"].ToString() + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["AgentId"].ToString() + "</td>");

                //string serviceType = (dtTrans.Rows[i]["ServiceType"].ToString() == "A2ZPREPAID" ? "PREPAID" : (dtTrans.Rows[i]["ServiceType"].ToString() == "A2ZPOSTPAID" ? "POSTPAID" : dtTrans.Rows[i]["ServiceType"].ToString()));

                sbTrans.Append("<td>" + dtTrans.Rows[i]["ServiceType"].ToString().Trim().Replace("A2Z", "") + "</td>");
                sbTrans.Append(status);
                if (alterstatus.ToLower() == "under process" || alterstatus.ToLower() == "pending")
                {
                    sbTrans.Append("<td class='text-center'><span class='btn btn-sm checkstatus' style='padding: 2px;color:#ff414d;' id='btnCallBackCheckStatus_" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + "' data-transid='" + dtTrans.Rows[i]["TransactionId"].ToString() + "' data-status='" + alterstatus + "' data-refid='" + dtTrans.Rows[i]["ClientRefId"].ToString() + "' data-trnsdate='" + DateDDMMYYYY(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "' onclick='A2ZCallBackCheckStatus(" + dtTrans.Rows[i]["BBPSPaymentId"].ToString() + ")'>Check Status</span></td>");
                }
                else
                {
                    sbTrans.Append("<td class='text-center'>- - -</td>");
                }
                sbTrans.Append("<td class='text-center'>" + refund_html + "</td>");
                string url = "/utilities/printbilltrans.aspx?transid=" + dtTrans.Rows[i]["TransactionId"].ToString() + "&agentid=" + dtTrans.Rows[i]["AgentId"].ToString() + "&trackid=" + dtTrans.Rows[i]["ClientRefId"].ToString();

                sbTrans.Append("<td class='text-center'><a style='color:#ff8d3c;font-weight: bold; ' href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            sbTrans.Append("<tr>");
            sbTrans.Append("<td colspan='11' class='text-danger text-center'>Record not found !</td>");
            sbTrans.Append("</tr>");
        }

        return sbTrans.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    public static string DateDDMMYYYY(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd/MM/yyyy");
        }

        return string.Empty;
    }

    [WebMethod]
    public static List<string> ProcessToCallBackCheckStatus(string transid, string refid, string status, string billid, string fromdate, string todate, string clintrefid, string transtype, string inputstatus)
    {
        List<string> result = new List<string>();

        try
        {
            string callBack = BBPSPaymentEnquiry(transid, refid, status);

            if (!string.IsNullOrEmpty(callBack))
            {
                dynamic dyResult = JObject.Parse(callBack);

                string responseCode = dyResult.ResponseCode;
                string responseMessage = dyResult.ResponseMessage;
                string transactionId = dyResult.TransactionId;
                string transactionStatus = dyResult.TransactionStatus;
                string clientRefId = dyResult.ClientRefId;
                string operatorTransactionId = dyResult.OperatorTransactionId;

                if (responseCode == "000")
                {
                    if (transactionStatus.ToLower() == "success")
                    {
                        result.Add(responseCode);
                        result.Add(responseMessage);
                        result.Add(transactionStatus);

                        UpdateBSSPPaymentByStatus(billid, "Transaction Successful");

                        DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, inputstatus);
                        result.Add(BindBillTransDetails(dtTrans));
                    }
                    else
                    {
                        if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
                        {
                            UserId = HttpContext.Current.Session["Validate"].ToString();
                            if (!string.IsNullOrEmpty(UserId))
                            {
                                string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                                string[] strDate = fromdate.Split('/');
                                string fromdatenews = strDate[2] + "-" + strDate[1] + "-" + strDate[0];

                                SqlTransactionDom STDom = new SqlTransactionDom();
                                DataTable dtLed = STDom.GetLedgerDetails("AGENT", UserId, fromdatenews, "", "", "", "Own", "", refid, "").Tables[0];

                                string debitAmt = string.Empty;
                                if (dtLed.Rows.Count > 0)
                                {
                                    debitAmt = !string.IsNullOrEmpty(dtLed.Rows[0]["Debit"].ToString()) ? dtLed.Rows[0]["Debit"].ToString() : string.Empty;
                                    agencyName = !string.IsNullOrEmpty(dtLed.Rows[0]["AgencyName"].ToString()) ? dtLed.Rows[0]["AgencyName"].ToString() : string.Empty;
                                }

                                if (!string.IsNullOrEmpty(debitAmt))
                                {
                                    List<string> isLedger = LedgerDebitCreditSection(debitAmt, UserId, agencyName, "Credit", "Transaction Failed", "Recharge Refund", "cr", "Transaction Failed Refund", "CREDIT NOTE");
                                    if (isLedger.Count > 0)
                                    {
                                        result.Add(responseCode);
                                        result.Add(responseMessage);
                                        result.Add(transactionStatus);

                                        UpdateBSSPPaymentByStatus(billid, "Transaction Failed");

                                        DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, inputstatus);
                                        result.Add(BindBillTransDetails(dtTrans));
                                    }
                                }
                            }
                        }
                    }
                }
                else if (responseCode == "999")
                {
                    result.Add(responseCode);
                    result.Add(responseMessage);
                    result.Add(transactionStatus);
                }
                else
                {
                    result.Add("Error Occurred !");
                    result.Add(responseMessage);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BBPSPaymentEnquiry(string transid, string refid, string status)
    {
        string result = string.Empty;

        if (!string.IsNullOrEmpty(transid) && !string.IsNullOrEmpty(refid) && !string.IsNullOrEmpty(status))
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    result = SMBPApiService.BBPSPaymentEnquiry(UserId, refid, transid, status);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }

        return result;
    }

    private static bool UpdateBSSPPaymentByStatus(string billid, string transStatus)
    {
        return SMBPApiService.UpdateBSSPPaymentByStatus(billid, transStatus);
    }
    #endregion

    #region [Payment Section]
    private static List<string> Ledgerandcreditlimit_Transaction(string amount, string number, string spkeytext, string transType, string transShortType, string transNotes)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(amount))
                    {
                        if (Convert.ToDouble(amount) > 0)
                        {
                            string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
                            DataTable dtAgency = SMBPApiService.GetAgencyDetailById(UserId);

                            if (dtAgency != null && dtAgency.Rows.Count > 0)
                            {
                                agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                                agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                                if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                                {
                                    string narration = amount + "_Recharge_To_" + number + "_by_" + spkeytext;
                                    List<string> isLedger = LedgerDebitCreditSection(amount, UserId, agencyName, transType, "Recharge To " + number + "", "Recharge", transShortType, narration, transNotes);
                                    if (isLedger.Count > 0)
                                    {
                                        string avlBal = isLedger[1];
                                        string trackid = isLedger[0];

                                        result.Add("000");
                                        result.Add(trackid);
                                    }
                                    else
                                    {
                                        result.Add("500");
                                        result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                    }
                                }
                                else
                                {
                                    result.Add("500");
                                    result.Add("INSUFFICIENT CREDIT LIMIT IN YOUR WALLET !");
                                }
                            }
                            else
                            {
                                result.Add("500");
                                result.Add("AGENCY DOES NOT EXIST !");
                            }
                        }
                        else
                        {
                            result.Add("500");
                            result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                        }
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }

        return result;
    }

    private static List<string> LedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype)
    {
        List<string> result = new List<string>();

        try
        {
            double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
            double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

            if (Uploadtype.Trim().ToLower() == "cr")
            {
                Remark = "Utility Refund";
                shortRemark = "Utility Refund";
            }

            result = LedgerService.LedgerDebitCreditUtility(Convert.ToDouble(amount), "", agentid, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion

    public static string GetLocalIPAddress()
    {
        string result = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                result = ip.ToString();
            }
        }

        return result;
    }

    private void InsertBBPSBillerList(string agentId)
    {
        //bool issuccess = SMBPApiService.BBPSBillerList(agentId);
    }

    #region [A2Z Api Section]
    [WebMethod]
    public static string BindA2ZOpratorByType(string servicetype, string selectname)
    {
        string result = string.Empty;

        try
        {
            result = A2ZService.GetA2ZOpratorList(servicetype, selectname);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static string GetA2ZGetRechargeOffer(string mobileno, string providerKey, string providerKeyName)
    {
        string result = string.Empty;
        string msg = string.Empty;

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                string response = A2ZService.GetRechargeOffer(mobileno, Convert.ToInt32(providerKey), providerKeyName, UserId, ref msg);

                if (!string.IsNullOrEmpty(msg))
                {
                    if (!string.IsNullOrEmpty(response))
                    {
                        result = "<div class='modal-content'><div class='modal-header' style='background:#fff!important;'>"
                                    + "<h5 class='modal-title' style='color: #000;'>" + providerKeyName + " : Recharge Offer</h5>"
                                    + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>";
                        RootOffer offer = JsonConvert.DeserializeObject<RootOffer>(response);
                        if (offer.status == 1)
                        {
                            foreach (var item in offer.records)
                            {
                                if (item.desc == "Plan Not Available")
                                {
                                    result = result + "<div class='row' style='text-align: center;color: red;'><p>" + item.desc + "!</p></div>"
                                + "</div></div>";
                                    break;
                                }
                                else
                                {
                                    result = result + "<div class='row' style='border-bottom: 1px dotted #ccc;margin-bottom: 10px;'>"
                                        + "<div class='col-sm-10'><p>" + item.desc + "</p></div>"
                                        //+ "<div class='col-sm-2' style='text-align: right;'><p>₹ " + item.rs + "</p></div></div>";
                                        + "<div class='col-sm-2' style='text-align: right;'><span class='btn btn-primary' style='height: 30px;line-height: 19px;' onclick='return SelectMobilePlan(" + item.rs + ");'>₹ " + item.rs + "</span></div></div>";
                                }
                            }
                            result = result + "</div></div>";
                        }
                        else
                        {
                            result = result + "<div class='row' style='text-align: center;color: red;'><p>No recharge offer available!</p></div>"
                                + "</div></div>";
                        }
                    }
                }
                else
                {
                    result = "<div class='modal-content'><div class='modal-header' style='background:#fff!important;'>"
                                   + "<h5 class='modal-title' style='color: #000;'>" + providerKeyName + " : Recharge Offer</h5>"
                                   + "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                   + "<div class='modal-body' style='padding: 50px;text-align: center;'>" + response + "</div>";
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> A2ZMobileRecharge(string mobile, string providerKey, string providerKeyName, string servicetype, string amount, string serviceid, string rchtype, string spkey)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (rchtype.ToLower().Trim() == "a2zprepaid")
                    {
                        string heading = "Prepaid Mobile Payment";
                        result = A2ZOperatorPaymentEvent(heading, mobile, providerKey, providerKeyName, servicetype, amount, serviceid, spkey);
                    }
                    else
                    {
                        string heading = "Postpaid Mobile Payment";
                        result = A2ZOperatorPaymentEvent(heading, mobile, providerKey, providerKeyName, servicetype, amount, serviceid, spkey);
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> A2ZGetAirtelPostPaidBill(string mobile, string providerKey, string providerKeyName, string servicetype, string amount, string serviceid, string rchtype, string spkey)
    {
        List<string> result = new List<string>();
        try
        {
            string msg = string.Empty;
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                string response = A2ZService.GetAirtelPostPaidBill(mobile, providerKey, UserId, ref msg);
                result = A2ZGetAirtelPostPaidBillMessage(mobile, providerKey, response);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> A2ZPostPaidMobileRecharge(string mobile, string providerKey, string providerKeyName, string servicetype, string amount, string serviceid, string rchtype, string spkey)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (providerKeyName.ToLower() == "airtel postpaid")
                    {
                        result = A2ZPostPaidPaymentEvent("Postpaid Mobile Payment", mobile, providerKey, providerKeyName, servicetype, amount, serviceid, spkey);
                    }
                    else
                    {
                        result = A2ZOperatorPaymentEvent("Postpaid Mobile Payment", mobile, providerKey, providerKeyName, servicetype, amount, serviceid, spkey);
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> A2ZProcessToCallBackCheckStatus(string transid, string refid, string status, string billid, string fromdate, string todate, string clintrefid, string transtype, string inputstatus)
    {
        List<string> result = new List<string>();

        try
        {
            string callBack = A2ZPaymentEnquiry(transid, refid, status);

            if (!string.IsNullOrEmpty(callBack))
            {
                dynamic dyResult = JObject.Parse(callBack);
                string respoStatus = dyResult.status;
                string respoMessage = dyResult.message;
                string bankRefNo = dyResult.bankRefNo;
                string respoDescription = dyResult.description;
                string clientRefId = dyResult.clientId;
                string transactionId = dyResult.txnId;

                if (respoMessage.ToString().ToLower().Trim().Contains("success"))
                {
                    bool isUpdated = A2ZService.A2ZUpdatePaymentByStatus(billid, respoMessage);
                    DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, inputstatus);
                    result.Add(BindBillTransDetails(dtTrans));
                }
                else if (respoMessage.ToString().ToLower().Trim().Contains("failed"))
                {
                    if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
                    {
                        UserId = HttpContext.Current.Session["Validate"].ToString();
                        if (!string.IsNullOrEmpty(UserId))
                        {
                            string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                            string[] strDate = fromdate.Split('/');
                            string fromdatenews = strDate[2] + "-" + strDate[1] + "-" + strDate[0];

                            SqlTransactionDom STDom = new SqlTransactionDom();
                            DataTable dtLed = STDom.GetLedgerDetails("AGENT", UserId, fromdatenews, "", "", "", "Own", "", refid, "").Tables[0];

                            string debitAmt = string.Empty;
                            if (dtLed.Rows.Count > 0)
                            {
                                debitAmt = !string.IsNullOrEmpty(dtLed.Rows[0]["Debit"].ToString()) ? dtLed.Rows[0]["Debit"].ToString() : string.Empty;
                            }

                            if (!string.IsNullOrEmpty(debitAmt))
                            {
                                List<string> isLedger = LedgerDebitCreditSection(debitAmt, UserId, agencyName, "Credit", "Transaction Failed", "Recharge Refund", "cr", "Transaction Failed Refund", "CREDIT NOTE");
                                if (isLedger.Count > 0)
                                {
                                    result.Add(respoMessage);
                                    result.Add("Transaction Failed");
                                    //result.Add(respoStatus);

                                    bool isUpdated = A2ZService.A2ZUpdatePaymentByStatus(billid, respoMessage);

                                    DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(UserId, fromdate, todate, clintrefid, transtype, inputstatus);
                                    result.Add(BindBillTransDetails(dtTrans));
                                }
                            }
                        }
                    }
                }
                else if (respoMessage.ToString().ToLower().Trim().Contains("pending") || respoMessage.ToString().ToLower().Trim().Contains("error"))
                {
                    result.Add(respoMessage);
                    result.Add(respoDescription);
                }
                else
                {
                    result.Add("Error");
                    result.Add(respoDescription);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string A2ZPaymentEnquiry(string transid, string refid, string status)
    {
        string result = string.Empty;

        if (!string.IsNullOrEmpty(transid) && !string.IsNullOrEmpty(refid) && !string.IsNullOrEmpty(status))
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    result = A2ZService.A2ZPaymentEnquiry(UserId, refid, transid, status);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }

        return result;
    }

    private static List<string> A2ZPostPaidPaymentEvent(string heading, string mobile, string providerKey, string providerKeyName, string servicetype, string amount, string serviceid, string spkey)
    {
        List<string> result = new List<string>();
        string msg = string.Empty;

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                List<string> ledResult = Ledgerandcreditlimit_Transaction(amount, mobile, providerKeyName, "Debit", "DR", "DEBIT NOTE");
                if (ledResult != null && ledResult.Count > 0)
                {
                    if (ledResult[0] == "000")
                    {
                        string clientRefId = ledResult[1];

                        string paymentRespo = A2ZService.PostPaidRechargePayment(providerKey, Convert.ToDouble(amount), mobile, UserId, clientRefId, spkey, ref msg);

                        if (!string.IsNullOrEmpty(paymentRespo))
                        {
                            dynamic rech = JObject.Parse(paymentRespo);
                            string status = rech.status;
                            string message = rech.message;

                            if (status.Trim().ToLower() == "failure")
                            {
                                List<string> rufLedResult = Ledgerandcreditlimit_Transaction(amount, mobile, providerKeyName, "Credit", "CR", "CREDIT NOTE");
                            }
                        }

                        result = A2ZDoOpratorPaymentMessage(heading, paymentRespo, amount, clientRefId, providerKeyName);
                    }
                    else
                    {
                        result = LedgerMessage(ledResult);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static List<string> A2ZOperatorPaymentEvent(string heading, string mobile, string providerKey, string providerKeyName, string servicetype, string amount, string serviceid, string spkey)
    {
        List<string> result = new List<string>();
        string msg = string.Empty;

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                List<string> ledResult = A2ZLedgerandcreditlimit_Transaction(amount, mobile, providerKey, spkey, "Debit", "DR", "DEBIT NOTE");
                if (ledResult != null && ledResult.Count > 0)
                {
                    if (ledResult[0] == "000")
                    {
                        string clientRefId = ledResult[1];

                        string paymentRespo = A2ZService.RechargePayment(providerKey, Convert.ToDouble(amount), mobile, UserId, clientRefId, spkey, ref msg);

                        if (!string.IsNullOrEmpty(paymentRespo))
                        {
                            dynamic rech = JObject.Parse(paymentRespo);
                            string status = rech.status;
                            string message = rech.message;

                            if (status.Trim().ToLower() == "failure")
                            {
                                List<string> rufLedResult = A2ZLedgerandcreditlimit_Transaction(amount, mobile, providerKey, spkey, "Credit", "CR", "CREDIT NOTE");
                            }
                        }
                        //else
                        //{
                        //    List<string> rufLedResult = Ledgerandcreditlimit_Transaction(amount, mobile, providerKeyName, "Credit", "CR", "CREDIT NOTE");
                        //}

                        result = A2ZDoOpratorPaymentMessage(heading, paymentRespo, amount, clientRefId, providerKeyName);
                    }
                    else
                    {
                        result = LedgerMessage(ledResult);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static List<string> A2ZDoOpratorPaymentMessage(string heading, string reqstr, string amount, string clientRefId, string spkeytext)
    {
        List<string> result = new List<string>();
        DateTime currDate = DateTime.Now;
        string strCurrDate = currDate.ToString("dd MMM yyyy, h:mm tt");

        if (reqstr != null && !string.IsNullOrEmpty(reqstr))
        {
            dynamic rech = JObject.Parse(reqstr);

            string status = rech.status;
            string message = rech.message;
            string txnId = rech.txnId;
            string operatorId = rech.operatorId;
            string clientId = rech.clientId;
            string statusId = rech.statusId;
            string availableBalance = rech.availableBalance;

            result.Add(status);

            if (status.Trim().ToLower() == "success")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-success' id='rechheading'><i class='fa fa-check-circle'></i>&nbsp;" + heading + "-" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    //+ "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + txnId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else if (status.Trim().ToLower() == "pending")
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-warning' id='rechheading'><i class='fa fa-clock'></i>&nbsp;" + heading + "-" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + txnId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "-" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><p>To</p><span>" + spkeytext + "</span></div>"
                    + "<div class='col-sm-6 text-right'><p>&nbsp;</p><span>Order ID: " + txnId + "</span></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><p>Wallet Txn ID: " + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
        }
        else
        {
            result.Add("failure");
            result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + heading + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'>"
                    + "<div class='row' style='text-align: center;'><h5>An error occurred please try again later or contact to administrator</h5></div>"
                    + "</div>"
                    + "</div>");
        }

        return result;
    }

    private static List<string> A2ZGetAirtelPostPaidBillMessage(string mobile, string provider, string respo)
    {
        List<string> result = new List<string>();
        try
        {
            if (!string.IsNullOrEmpty(respo))
            {
                dynamic rech = JObject.Parse(respo);

                string status = rech.status;
                string message = rech.message;
                string Billamount = rech.billInfo.Billamount;
                string Netamount = rech.billInfo.Netamount;
                string Billdate = rech.billInfo.Billdate;
                string Duedate = rech.billInfo.Duedate;

                if (status == "1")
                {
                    result.Add("success");

                    result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;Postpaid Mobile Bill Detail</h5>"
                         + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                         + "<div class='modal-body'>"
                         + "<div class='row'>"
                        + "<div class='col-sm-6'><span style='font-size: 15px;'>Due Amount : ₹ " + Netamount + "</span><br /><input id='txtPartPostPaidMobileAmount' type='text' value='" + Netamount + "' class='form-control' style='height:40px;width:160px;text-align:center;'></div>"
                         + "<div class='col-sm-6 text-right'><p>Billing Date : " + Billdate + "</p><p>Payment Due Date : " + Duedate + "</p></div>"
                         + "</div></div>"
                         + "<div class='modal-footer'><button type='button' id='btnPostPaidBillPayment' class='btn btn-success' onclick='A2ZPostPaidBillPaymentSubmit();'>Bill Pay</button></div>"
                         + "<input type='hidden' id='hdnPostPaidMobileDetails' data-mobile='" + mobile + "' data-provider='" + provider + "'/>");
                }
                else
                {
                    result.Add("failed");

                    result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;Postpaid Mobile Bill Detail</h5>"
                         + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                         + "<div class='modal-body'>"
                         + "<div class='row'>"
                        + "<div class='col-sm-12'><span style='font-size: 15px;'>" + message + "</div>"
                         + "</div></div>");
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("success");

            result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;Postpaid Mobile Bill Detail</h5>"
                 + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                 + "<div class='modal-body'>"
                 + "<div class='row'>"
                + "<div class='col-sm-12'><span style='font-size: 15px;'>Some error occured! Please try again later or call to admin.</div>"
                 + "</div></div>");
            ex.ToString();
        }
        return result;
    }

    [WebMethod]
    public static List<string> A2ZDTHRecharge(string providerKey, string providerKeyName, string servicetype, string amount, string spkey, string dthno)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    result = A2ZOperatorPaymentEvent("DTH Payment", dthno, providerKey, providerKeyName, servicetype, amount, string.Empty, spkey);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> FetchAllBillDetails(string number, string provider, string providername, string actiontype, bool istxtshow = false)
    {
        List<string> result = new List<string>();
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string agentId = HttpContext.Current.Session["Validate"].ToString();
                result = A2ZService.FetchAllBillDetails(number, provider, agentId, actiontype);
                //result.Add("fetch");
                //result.Add("{'status':0,'message':'Invalid inputs entered or no bill due. ','billInfo':[],'serviceName':'INSURANCE','isAmountEditable':0}");
                if (result != null && result.Count > 0)
                {
                    result = A2ZBillFetchMessage("BILL DETAIL", providername, result[1], actiontype, istxtshow);
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("Error Occured!");
            result.Add(ex.Message);
        }
        return result;
    }

    private static List<string> A2ZBillFetchMessage(string heading, string providerName, string fetchRespo, string actiontype, bool istxtshow)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(fetchRespo))
            {
                dynamic rech = JObject.Parse(fetchRespo);

                string status = rech.status;
                string message = rech.message;
                string serviceName = rech.serviceName;
                string isAmountEditable = rech.isAmountEditable;

                string CustomerName = string.Empty; string BillNumber = string.Empty; string Billdate = string.Empty;
                string Billamount = string.Empty; string Duedate = string.Empty; string context = string.Empty;

                string tempBillInfo = rech.billInfo.ToString();

                if (!string.IsNullOrEmpty(tempBillInfo) && tempBillInfo != "[]")
                {
                    CustomerName = rech.billInfo.CustomerName;
                    BillNumber = rech.billInfo.BillNumber;
                    Billdate = rech.billInfo.Billdate;
                    Billamount = rech.billInfo.Billamount;
                    Duedate = rech.billInfo.Duedate;
                    context = rech.billInfo.context;
                }

                if (!string.IsNullOrEmpty(CustomerName))
                {
                    HttpContext.Current.Session["CustomerName"] = CustomerName;
                }

                if (status == "1")
                {
                    result.Add("success");

                    string partAmtPay = "";
                    if (istxtshow)
                    {
                        partAmtPay = "<br/><div class='row'> <div class='col-sm-6' style='margin-top: 10px;font-size: 15px;'>Part Pay Amount </div> <div class='col-sm-6'><input id='txtPart" + actiontype + "Amount' type='text' value='" + Billamount + "' class='form-control' style='height:40px;width:160px;text-align:center;'/></div> </div>";
                    }

                    string respo = "<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-success'><i class='fa fa-file-word' aria-hidden='true'></i>&nbsp;" + serviceName + " " + heading + "</h5>"
                         + "<button type='button' class='close closepopup text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                         + "<div class='modal-body'><div class='row'>"
                         + "<div class='col-sm-5'><span style='font-size: 15px;'>Customer Name</span><p>" + CustomerName + "</p></div>"
                         + "<div class='col-sm-7 text-right'><span>Provider Name</span><p>" + providerName + "</p></div>"
                         + "</div><hr />"
                         + "<div class='row'>"
                         //+ "<div class='col-sm-6'><span style='font-size: 15px;'>Bill Amount : ₹ " + Billamount + "</span><br /><span style='color: #8d8d8d;'>Charges : ₹ " + (!string.IsNullOrEmpty(BBPSCharges) ? BBPSCharges : "0") + "</span>" + partAmtPay + "</div>"
                         + "<div class='col-sm-6'><span style='font-size: 15px;'>Bill Amount : ₹ " + Billamount + "</span><br />" + partAmtPay + "</div>"
                         + "<div class='col-sm-6 text-right'><span>Payment Due Date</span><p>" + Duedate + "</p></div>"
                         + "</div><hr />"
                         + "<div class='row'>"
                         + "<div class='col-sm-4'><span style='font-size: 15px;'>Consumer Number</span><p>" + BillNumber + "</p></div>"
                         + "<div class='col-sm-8 text-right'><span>Billing Date</span><p>" + Billdate + "</p></div>"
                         + "</div></div>";

                    respo += "<div class='modal-footer'><button type='button' id='btn" + actiontype + "Payment' class='btn btn-success' onclick='" + actiontype + "PaymentSubmit();'>Bill Pay</button></div>"
                           + "<input type='hidden' id='hdnTotal" + actiontype + "PaidAmt' value='" + Billamount + "' data-customername='" + CustomerName + "'/>";
                    result.Add(respo);
                }
                else
                {
                    result.Add(status);
                    result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + serviceName + " " + heading + "</h5>"
                                                  + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                                  + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                                                  + "<h5 class='text-danger'>" + message + "</h4>"
                                                  + "</div></div>");
                }
            }
            else
            {
                result.Add("notavl");
                result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;Failed</h5>"
                                  + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                  + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                                  + "<h5>Error occurred, Please try again after sometime.</h4>"
                                  + "</div></div>");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add("<div class='modal-header' style='background:none!important;'><h5 class='modal-title text-danger'><i class='fa fa-exclamation-triangle'></i>&nbsp;Error Occured!</h5>"
                                  + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                                  + " <div class='modal-body text-danger text-center' style='padding: 40px!important;'><div class='row'>"
                                  + "<h5>" + ex.Message + "</h4>"
                                  + "</div></div>");
        }

        return result;
    }

    [WebMethod]
    public static List<string> PayFetchedBillPayment(string number, string provider, string amount, string providername, string spkey, string actiontype, string customername)
    {
        List<string> result = new List<string>();
        string msg = string.Empty;
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string agentId = HttpContext.Current.Session["Validate"].ToString();
                List<string> ledResult = A2ZLedgerandcreditlimit_Transaction(amount, number, provider, spkey, "Debit", "DR", "DEBIT NOTE");
                if (ledResult != null && ledResult.Count > 0)
                {
                    if (ledResult[0] == "000")
                    {
                        string clientRefId = ledResult[1];
                        string paymentRespo = A2ZService.PayFetchedBillPayment(provider, Convert.ToDouble(amount), number, agentId, clientRefId, spkey, customername, actiontype, ref msg);
                        //string paymentRespo = "{'txnId':8200526,'txnTime':'03-05-2021 01:00:39','operator_ref':'','status':'SUCCESSFULLY SUBMITTED','message':'Transaction Submitted Successfully Thanks','billerName':null,'providerName':'West Bengal State Electricity (WBSEDCL)','amount':2267,'statusId':24}";
                        if (!string.IsNullOrEmpty(paymentRespo))
                        {
                            dynamic rech = JObject.Parse(paymentRespo);
                            string status = rech.status;
                            string message = rech.message;

                            if (status.Trim().ToLower().Contains("failure"))
                            {
                                List<string> rufLedResult = A2ZLedgerandcreditlimit_Transaction(amount, number, provider, spkey, "Credit", "CR", "CREDIT NOTE");
                            }
                        }

                        result = A2ZBillPaymentMessage(paymentRespo, clientRefId);
                    }
                    else
                    {
                        result = LedgerMessage(ledResult);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("Error Occured!");
            result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;Error Occured!</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'>"
                    + "<div class='row' style='text-align: center;'><h5>" + ex.Message + "</h5></div>"
                    + "</div>"
                    + "</div>");
        }
        return result;
    }

    private static List<string> A2ZBillPaymentMessage(string payResponse, string clientRefId)
    {
        List<string> result = new List<string>();
        DateTime currDate = DateTime.Now;
        string strCurrDate = currDate.ToString("dd MMM yyyy, h:mm tt");

        if (payResponse != null && !string.IsNullOrEmpty(payResponse))
        {
            dynamic rech = JObject.Parse(payResponse);

            string status = rech.status;
            string message = rech.message;
            string txnId = rech.txnId;
            string txnTime = rech.txnTime;
            string operator_ref = rech.operator_ref;
            string billerName = rech.billerName;
            string providerName = rech.providerName;
            string amount = rech.amount;
            string statusId = rech.statusId;

            result.Add(status);

            if (status.Trim().ToLower().Contains("success"))
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-success' id='rechheading'><i class='fa fa-check-circle'></i>&nbsp;" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    //+ "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span style='font-size: 15px;'>Transaction Date</span><p>" + strCurrDate + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><span style='font-size: 15px;'>Provider Name</span><p>" + providerName + "</p></div>"
                    + "<div class='col-sm-6 text-right'><span style='font-size: 15px;'>Transaction Id</span><p>" + txnId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><span style='font-size: 15px;'>Wallet Txn. ID</span><p>" + clientRefId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else if (status.Trim().ToLower().Contains("pending"))
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-warning' id='rechheading'><i class='fa fa-clock'></i>&nbsp;" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                    + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    //+ "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span style='font-size: 15px;'>Transaction Date</span><p>" + strCurrDate + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><span style='font-size: 15px;'>Provider Name</span><p>" + providerName + "</p></div>"
                    + "<div class='col-sm-6 text-right'><span style='font-size: 15px;'>Transaction Id</span><p>" + txnId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                   + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><span style='font-size: 15px;'>Wallet Txn. ID</span><p>" + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
            else
            {
                result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;" + message + "</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'><div class='row'>"
                     + "<div class='col-sm-4'><span style='font-size: 15px;'>Money Paid</span><p>₹ " + amount + "</p></div>"
                    //+ "<div class='col-sm-8 text-right'><span>" + strCurrDate + "</span><p>Closing Balance: ₹ " + availableBalance + "</p></div>"
                    + "<div class='col-sm-8 text-right'><span style='font-size: 15px;'>Transaction Date</span><p>" + strCurrDate + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-6'><span style='font-size: 15px;'>Provider Name</span><p>" + providerName + "</p></div>"
                    + "<div class='col-sm-6 text-right'><span style='font-size: 15px;'>Transaction Id</span><p>" + txnId + "</p></div>"
                    + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-4'><p>From Your Wallet</p></div><div class='col-sm-8 text-right'><span style='font-size: 15px;'>Wallet Txn. ID</span><p>" + clientRefId + "</p></div>"
                     + "</div><hr />"
                    + "<div class='row'>"
                    + "<div class='col-sm-12 text-right'><a href='/utilities/printbilltrans.aspx?transid=" + txnId + "&agentid=" + HttpContext.Current.Session["Validate"].ToString() + "&trackid=" + clientRefId + "' target='_blank' class='btn btn-success btn-sm'>"
                    + "<i class='fa fa-print'></i>&nbsp; Print</a>"
                    + "</div></div>"
                    + "</div>");
            }
        }
        else
        {
            result.Add("failure");
            result.Add("<div class='modal-header' style='background: none!important;'><h5 class='modal-title text-danger' id='rechheading'><i class='fa fa-exclamation-triangle'></i>&nbsp;Error Occured!</h5>"
                    + "<button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                    + "<div class='modal-body' id='rechbody'>"
                    + "<div class='row' style='text-align: center;'><h5>An error occurred please try again later or contact to administrator</h5></div>"
                    + "</div>"
                    + "</div>");
        }

        return result;
    }

    #region [Payment Section]
    private static List<string> A2ZLedgerandcreditlimit_Transaction(string amount, string number, string provider, string spkey, string transType, string transShortType, string transNotes)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(amount))
                    {
                        if (Convert.ToDouble(amount) > 0)
                        {
                            string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
                            DataTable dtAgency = A2ZService.GetAgencyDetailById(UserId);

                            if (dtAgency != null && dtAgency.Rows.Count > 0)
                            {
                                agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                                agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                                if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                                {
                                    string narration = amount + "_Recharge_To_" + number + "_by_" + provider;
                                    List<string> isLedger = A2ZLedgerDebitCreditSection(amount, spkey, UserId, agencyName, transType, "Recharge To " + number + "", "Recharge", transShortType, narration, transNotes);
                                    if (isLedger.Count > 0)
                                    {
                                        string avlBal = isLedger[1];
                                        string trackid = isLedger[0];

                                        result.Add("000");
                                        result.Add(trackid);
                                    }
                                    else
                                    {
                                        result.Add("500");
                                        result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                    }
                                }
                                else
                                {
                                    result.Add("500");
                                    result.Add("INSUFFICIENT CREDIT LIMIT IN YOUR WALLET !");
                                }
                            }
                            else
                            {
                                result.Add("500");
                                result.Add("AGENCY DOES NOT EXIST !");
                            }
                        }
                        else
                        {
                            result.Add("500");
                            result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                        }
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx", false);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }

        return result;
    }

    private static List<string> A2ZLedgerDebitCreditSection(string amount, string spkey, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype)
    {
        List<string> result = new List<string>();

        try
        {
            double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
            double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

            if (Uploadtype.Trim().ToLower() == "cr")
            {
                Remark = "Utility Refund";
                shortRemark = "Utility Refund";
            }

            result = LedgerService.LedgerDebitCreditUtility(Convert.ToDouble(amount), spkey, agentid, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion
    #endregion
}