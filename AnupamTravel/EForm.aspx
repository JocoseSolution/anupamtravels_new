﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EForm.aspx.cs" Inherits="EForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />
    <link href="Advance_CSS/css/font-awesome.css" rel="stylesheet" />
    <%-- <link href="Advance_CSS/css/Enquirysheet.css" rel="stylesheet" />--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
        function ValidateEnquiry() {
            if ($("[id$=txtEYourName]").val() == "") {
                alert("Your Name field should not be blank or empty!");

                return false;
            }
            else if ($("[id$=txtEMobileNumber]").val() == "") {
                alert("Your Mobile No. field should not be blank or empty!");
                return false;
            }
            else if ($("[id$=txtEEmailId]").val() == "") {
                alert("Your Email ID field should not be blank or empty!");
                return false;
            }
            else if ($("[id$=txtEMessage]").val() == "") {
                alert("Enter Message field should not be blank or empty!");
                return false;
            }
            alert("Thank You For contacting. We will get back to you soon");
            return true;
        }
    </script>
	<style>
	  .boxs{
	      width: 100%;
    border-radius: 32px;
    padding: 2px 15px;
	  }
	</style>
</head>
<body>

    <form id="form1" runat="server">
        <nav class="navbar navbar-default navbar-inverse navbar-theme navbar-theme-abs navbar-theme-transparent navbar-theme-border" id="main-nav">
            <div class="container">
                <div class="navbar-inner nav">
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" data-target="#navbar-main" data-toggle="collapse" type="button" area-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="search.aspx">
                            <img src="Advance_CSS/Icons/logo(ft).png" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-main">
                        <ul class="nav navbar-nav">

                        <li>
                            <a href="../EForm.aspx" role="button" aria-haspopup="true" aria-expanded="false">Send Enquiry</a>

                        </li>
                        <li>
                            <a href="about-us.html" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About</a>

                        </li>
                        <li>
                            <a href="contact.html" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact</a>

                        </li>


                        </ul>

                    </div>
                </div>
            </div>
        </nav>
        <div class="theme-hero-area" style="margin-top: 62px;">
            <div class="theme-hero-area-bg-wrap">
                <div class="theme-hero-area-bg" style="background-image: url(../Images/gallery/news.jpg);"></div>
                <div class="theme-hero-area-mask theme-hero-area-mask-strong"></div>
            </div>

            <div class="theme-hero-area-body">
                <div class="theme-page-section _pt-100 theme-page-section-xl">

                    <div class="container">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="theme-login theme-login-white">
                                        <div class="theme-login-header">
                                            <h1 class="theme-login-title">Send Enquiry Now</h1>
                                        </div>
                                        <div class="theme-login-box">
                                            <div class="theme-login-box-inner">

                                                <div class="form-group theme-login-form-group">

                                                    <asp:TextBox runat="server" CssClass="form-control-custom" placeholder="Your Name" ID="txtEYourName"></asp:TextBox>
                                                </div>

                                                <div class="form-group theme-login-form-group">

                                                    <asp:TextBox runat="server" class="form-control-custom" placeholder="Your Mobile No." ID="txtEMobileNumber"></asp:TextBox>
                                                </div>
                                                <div class="form-group theme-login-form-group">
                                                    <asp:TextBox runat="server" class="form-control-custom" placeholder="Your Email" ID="txtEEmailId"></asp:TextBox>
                                                </div>
                                                <div class="form-group theme-login-form-group">
                                                    <asp:TextBox runat="server" class="form-control-custom-txtarea boxs" placeholder="Write Your Message Here.." ID="txtEMessage" TextMode="MultiLine" Rows="3"></asp:TextBox><br />
                                                </div>
                                                <asp:Button ID="btnSendEnquiry" runat="server" Text="Send" class="btn btn-uc btn-dark btn-block btn-lg-custom" OnClick="btnSendEnquiry_Click" OnClientClick="return ValidateEnquiry()" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="_pv-100 _pv-mob-30">
                                        <div class="theme-hero-text theme-hero-text-white">
                                            <div class="theme-hero-text-header">
                                                <h2 class="theme-hero-text-title">Anupam</h2>
                                                <h3 style="font-style: italic">Tour & Travles</h3>
                                                <p class="theme-hero-text-subtitle">We are proud to offer excellent quality and value for money in our tours, which give you the chance to experience your chosen destination in an authentic and exciting way.</p>
                                            </div>
                                            <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" target="_blank" href="https://anupamtravelonline.com/">Sign Up Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="theme-page-section _pt-desk-30 theme-page-section-xxl">
            <div class="container">
                <div class="row row-col-mob-gap" data-gutter="60">

                    <div class="col-md-3 ">
                        <div class="feature">
                            <i class="feature-icon feature-icon-gray feature-icon-box feature-icon-round fa fa-globe" style="font-size: 60px"></i>
                            <div class="feature-caption _op-07">
                                <h5 class="feature-title">Explore the World</h5>
                                <p class="feature-subtitle">Start to discrover. We will help you to visit any place you can imagine</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="feature">
                            <i class="feature-icon feature-icon-gray feature-icon-box feature-icon-round fa fa-gift" style="font-size: 60px"></i>
                            <div class="feature-caption _op-07">
                                <h5 class="feature-title">Gifts &amp; Rewards</h5>
                                <p class="feature-subtitle">Get even more from our service. Spend less and travel more</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="feature">
                            <i class="feature-icon feature-icon-gray feature-icon-box feature-icon-round fa fa-credit-card" style="font-size: 48px"></i>
                            <div class="feature-caption _op-07">
                                <h5 class="feature-title">Best prices</h5>
                                <p class="feature-subtitle">We are comparing hundreds travel websites to find best price for you</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="feature">
                            <i class="feature-icon feature-icon-gray feature-icon-box feature-icon-round fa fa-comments-o" style="font-size: 58px"></i>
                            <div class="feature-caption _op-07">
                                <h5 class="feature-title">27/7 Support</h5>
                                <p class="feature-subtitle">Contact us anytime, anywhere. We will resolve any issues ASAP</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="theme-page-section theme-page-section-xxl">
            <div class="container">
                <div class="theme-page-section-header">
                    <h5 class="theme-page-section-title">Top Destinations</h5>
                    <p class="theme-page-section-subtitle">The most searched countries in March</p>
                </div>
                <div class="row row-col-gap" data-gutter="10">
                    <div class="col-md-4 ">
                        <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/taj-mahal-india-tourist-2574056_550x360.jpg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">India</h5>
                                <p class="banner-subtitle">Incredeble !india</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 ">
                        <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/agriculture-asia-cat-china-cloud-1807570_860x360.jpg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">China</h5>
                                <p class="banner-subtitle">China like never before</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/ojofv8dzd_w_800x800.jpg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">Morocco</h5>
                                <p class="banner-subtitle">Much mor</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/7bjmdicvloe_800x800.jpeg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">Canada</h5>
                                <p class="banner-subtitle">Keep exploring</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/beach-coast-daylight-landscape-461969_400x360.jpeg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">Portugal</h5>
                                <p class="banner-subtitle">Europe's west coast</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                            <div class="banner-bg" style="background-image: url(Advance_CSS/img/agriculture-animals-asia-beautiful-1793409_400x360.jpg);"></div>
                            <div class="banner-mask"></div>
                            <a class="banner-link" href="#"></a>
                            <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                <h5 class="banner-title">Malasia</h5>
                                <p class="banner-subtitle">Truly asia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="theme-hero-area">
            <div class="theme-hero-area-bg-wrap">
                <div class="theme-hero-area-bg ws-action" style="background-image: url(&quot;Advance_CSS/img/adult-book-business-cactus-297755_1500x800.jpg&quot;); transform: translate3d(0px, -110px, 0px); height: 660px;" data-parallax="true"></div>
                <div class="theme-hero-area-mask"></div>
            </div>
            <div class="theme-hero-area-body">
                <div class="theme-page-section theme-page-section-xxl">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="theme-hero-text theme-hero-text-white theme-hero-text-center">
                                    <div class="theme-hero-text-header">
                                        <h2 class="theme-hero-text-title">Get best travel deals</h2>
                                        <p class="theme-hero-text-subtitle">Subscribe now and unlock our secret deals. Save up to 70% by getting access to our special offers for hotels, flights, cars, vacation rentals and travel experiences.</p>
                                    </div>
                                    <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" target="_blank" href="https://anupamtravelonline.com/">Sign Up Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="theme-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="theme-copyright-text">
                            Copyright © 2019
             
                        <a href="#">Anupam Travels</a>. All rights reserved.
           
                        </p>
                    </div>
                    <div class="col-md-6">
                        <ul class="theme-copyright-social">
                            <li>
                                <a class="fa fa-facebook" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-google" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-youtube-play" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-instagram" href="#"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
