﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Enquiry_Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anupam Travel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" />
    <link rel="stylesheet" href="/Enquiry/assets/css/animate.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/datepicker.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/OverlayScrollbars.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/fontawesome.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/icofont.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/magnific-popup.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/slick.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/slick-theme.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/slider-range.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/select2.min.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/tippy.css" />
    <link rel="stylesheet" href="/Enquiry/assets/css/app.css" />

    <script>
        function ValidateEnquiry() {
            if ($("[id$=txtEYourName]").val() == "") {
                alert("Your Name field should not be blank or empty!");

                return false;
            }
            else if ($("[id$=txtEMobileNumber]").val() == "") {
                alert("Your Mobile No. field should not be blank or empty!");
                return false;
            }
            else if ($("[id$=txtEEmailId]").val() == "") {
                alert("Your Email ID field should not be blank or empty!");
                return false;
            }
            else if ($("[id$=txtEMessage]").val() == "") {
                alert("Enter Message field should not be blank or empty!");
                return false;
            }
            return true;
        }
    </script>
    <style>
        .rt-lgoinmodal .modal-content {
            background: #ffffff59 !important;
            -webkit-box-shadow: 0 10px 45px rgba(0,0,0,0.15), 0 2px 4px rgba(0,0,0,0.1) !important;
            box-shadow: 0 10px 45px rgba(0,0,0,0.15), 0 2px 4px rgba(0,0,0,0.1) !important;
            border-radius: 15px !important;
            overflow: hidden !important;
        }


        @media only screen and (max-width:1050px) {
            /* For tablets: */
            .modal-dialog modal-dialog-centered rt-lgoinmodal {
                margin-top: -90px;
            }

            .wow fade-in-bottom {
                margin-top: 120px;
            }
            .col-lg-7
            {
                                margin-top: 53px;

            }
        }

        @media only screen and (max-width:500px) {
            /* For mobile phones: */
            .modal-dialog modal-dialog-centered rt-lgoinmodal {
                margin-top: -56px;
            }

            .svcPage-area {
                margin-top: 310px;
            }
        }
    </style>

</head>
<body>

    <%--<a href="#" data-toggle="modal" data-target="#rtmodal-1" style="position: fixed; z-index: 99999; right: 15px; top: 80%;">
        <img src="assets/enquiry.png" style="width: 150px;" /></a>--%>

    <header class="rt-site-header  rt-fixed-top white-menu">

        <div class="main-header rt-sticky">
            <nav class="navbar">
                <div class="container">
                    <img src="assets/images/icons-image/1200px-IRCTC_Logo.svg.png" alt="icon-image" draggable="false" style="width: 100px; height: 80px">
                    <%--                                        <a href="Home.aspx" class="brand-logo"><img src="/Advance_CSS/Icons/logo(ft).png" alt="" style="width: 168px;"></a>--%>
                    <a href="Home.aspx" class="brand-logo" style="position: absolute; left: 45%;">
                        <img src="/Advance_CSS/Icons/logo(ft).png" alt="" style="width: 168px;"></a>
                    <a href="Home.aspx" class="sticky-logo">
                        <img src="/Advance_CSS/Icons/logo(ft).png" alt="" style="width: 168px; position: absolute; left: 40%; margin-top: -37px">
                    </a>

                    <div class="ml-auto d-flex align-items-center">


                        <div class="main-menu">
                            <ul>
                                <li class="current-menu-item"><a href="login.aspx">Login</a></li>
                                <li class="current-menu-item"><a href="../regs_new.aspx">Register</a></li>
                            </ul>
                        </div>
                        <!-- end main menu -->
                        <div class="rt-nav-tolls d-flex align-items-center">
                            <span class="d-md-inline d-none">
                                <a href="#contactUS" id="hulk" class="rt-btn rt-gradient2 rt-rounded text-uppercase rt-Bshadow-1">Contact
                                    Us
                                </a>
                            </span>
                        </div>

                    </div>
                </div>
            </nav>
        </div>
        <!-- /.bootom-header -->

    </header>

    <!-- Modal -->
    <%--    <div class="modal fade" id="rtmodal-1" tabindex="-1" role="dialog" aria-labelledby="rtmodal-1"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered rt-lgoinmodal " role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="rt-modal-headr rt-mb-20 one">
                        
                        <h4>Quick Enquiry</h4>
                       
                    </div>
                    <div class="rt-modal-input one">
                       <form action="#" runat="server" class="rt-form rt-line-form">
                            <asp:TextBox runat="server" type="text" placeholder="Your Name" ID="txtEYourName" class="form-control rt-mb-30"></asp:TextBox>
                            <asp:TextBox runat="server" type="text" placeholder="Your Mobile No" ID="txtEMobileNumber" class="form-control rt-mb-30"></asp:TextBox>
                            <asp:TextBox runat="server" type="email" placeholder="Your Email" ID="txtEEmailId" class="form-control rt-mb-30"></asp:TextBox>
                            <asp:TextBox runat="server" placeholder="Message" ID="txtEMessage" class="form-control rt-mb-30"></asp:TextBox>
                            <asp:Button runat="server" type="submit" ID="btnSendEnquiry" OnClick="btnSendEnquiry_Click1" text="SUBMIT NOW" class="rt-btn rt-gradient pill text-uppercase rt-mb-30" />
                        </form>
                     
                    </div><!-- /.rt-modal-input -->
                    
                </div>
            </div>
        </div>
    </div>--%>




    <section class="rt-banner-area">
        <div class="single-rt-banner rt-banner-height" style="background-image: url(assets/images/backgrounds/train-LGT.gif); background-size: contain; background-position: right;">
            <div class="container">
                <div class="row  rt-banner-height align-items-center">
                    <div class="col-lg-7">
                        <%--                    <div class="col-sm-6 col-md-6 col-lg-7">--%>
                        <div class="rt-banner-content">

                            <h2 class="wow fade-in-bottom" data-wow-duration="1s" data-wow-delay="0.5s">Become an
                                <br />
                                Authorized IRCTC Travel Agent!
                            </h2>
                            <p class="wow fade-in-top">
                                Get an authorized licence from IRCTC and become a booking agent.<br>
                                Enjoy a number of other benefits and start your own business today.
                            </p>




                            <ul class="nav serachnavs wow fade-in-bottom" id="rtMultiTab" role="tablist" data-wow-duration="1.5s" data-wow-delay="1.5s">

                                <li class="nav-item">
                                    <a class="nav-link inactive" id="four-tab" data-target="#rt-item_b_four" data-secondary="#rt-item_a_four"
                                        data-toggle="tab" href="#four" role="tab" aria-controls="four-tab" aria-selected="false">
                                        <i class="icofont-train-line"></i>
                                        <span>Trains</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="first-tab" data-target="#rt-item_b_first" data-secondary="#rt-item_a_first"
                                        data-toggle="tab" href="#first" role="tab" aria-controls="first-tab" aria-selected="false">
                                        <i class="icofont-airplane"></i>
                                        <span>Flights</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link inactive" id="second-tab" data-target="#rt-item_b_second" data-secondary="#rt-item_a_second"
                                        data-toggle="tab" href="#second" role="tab" aria-controls="second-tab" aria-selected="true">

                                        <i class="icofont-hotel"></i>
                                        <span>Hotels</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link inactive" id="third-tab" data-target="#rt-item_b_thrid" data-secondary="#rt-item_a_third"
                                        data-toggle="tab" href="#third" role="tab" aria-controls="third-tab" aria-selected="false">
                                        <i class="icofont-bus"></i>
                                        <span>Bus</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link inactive" id="four-tab" data-target="#rt-item_b_four" data-secondary="#rt-item_a_four"
                                        data-toggle="tab" href="#four" role="tab" aria-controls="four-tab" aria-selected="false">
                                        <i class="icofont-phone"></i>
                                        <span>Bill</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link inactive" id="four-tab" data-target="#rt-item_b_four" data-secondary="#rt-item_a_four"
                                        data-toggle="tab" href="#four" role="tab" aria-controls="four-tab" aria-selected="false">
                                        <i class="icofont-money"></i><span>DMT</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- end banner content -->
                    </div>
                    <!-- end column -->
                    <%--                    <div class="col-sm-9 col-md-6 col-lg-5">--%>
                    <div class="col-lg-5">


                        <div class="modal-dialog modal-dialog-centered rt-lgoinmodal " role="document" style="margin-top: 50px">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="rt-modal-headr rt-mb-20 one">

                                        <h4 style="margin-top: 12px">Quick Enquiry</h4>

                                    </div>
                                    <div class="rt-modal-input one">
                                        <form action="#" runat="server" class="rt-form rt-line-form">
                                            <asp:TextBox runat="server" type="text" placeholder="Your Name" ID="txtEYourName" class="form-control rt-mb-30"></asp:TextBox>
                                            <asp:TextBox runat="server" type="text" placeholder="Your Mobile No" ID="txtEMobileNumber" class="form-control rt-mb-30"></asp:TextBox>
                                            <asp:TextBox runat="server" type="email" placeholder="Your Email" ID="txtEEmailId" class="form-control rt-mb-30"></asp:TextBox>
                                            <asp:TextBox runat="server" placeholder="Message" ID="txtEMessage" class="form-control rt-mb-30"></asp:TextBox>
                                            <asp:Button runat="server" type="submit" ID="btnSendEnquiry" OnClientClick="return ValidateEnquiry()" OnClick="btnSendEnquiry_Click1" Text="SUBMIT NOW" class="rt-btn rt-gradient pill text-uppercase rt-mb-30" Style="width: 100%; margin-top: 10px" />
                                        </form>

                                    </div>
                                    <!-- /.rt-modal-input -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->

        </div>
        <!-- end single rt banner -->
    </section>


    <%--    <section class="deal-area   rtbgprefix-full bg-hide-md" style="background-image: url(assets/images/backgrounds/bgshapes_1.png)">
        <div class="container-fluid p-0">
            <div class="deal-carosel-active owl-carousel">
                <div class="row single-deal-carosel align-items-center">
                    <div class="col-lg-5">
                        <!--<div class="deal-bg" style="background-image: url(assets/images/all-img/deal-bg.jpeg)">
                            <div class="inner-content">
                                <h4 data-animation="fadeInDown" data-duration=".2s" data-delay=".4s">14 Day Classic Tour of Thailand & Beaches</h4>
                                <p data-animation="fade-in-bottom" data-duration=".2s" data-delay=".4s">Grab a promo code for extra savings up to 75% on discounted hotels!</p>
                            </div>
                        </div>-->
                        <img src="assets/images/counter-icons/Travel-Vectors.png" />
                    </div>
                    <div class="col-lg-7">
                        <div class="rt-section-title-wrapper text-white" data-animation="fadeIn" data-duration=".4s">
                            <h2 class="rt-section-title">
                                <span></span>
                                About Us
                            </h2>
                            <p>Anupam Travels.com is the leading B2B portal for integrated travel and travel related services in the country. Anupam Travels offers the travel agents a wide spectrum of solutions that include Ticket Bookings, Hotel Bookings, Transfers and Sightseeing. Our principal motto is to provide nonpareil rates and quality Services to the agents all over India.</p>
                            <div class="section-title-spacer"></div>
                            <div class="deal-bottom-content">

                                <div class="section-title-spacer"></div>
                                <h4>Our team works 24X7 to give you unexcelled and heavenly experiences</h4>
                                <p>
                                    Anupam Travels.com is a new-fangled traveling portal managed by a team of immensely experienced people in the B2B Market.
                                </p>
                                <div class="rt-button-group">
                                    
                                    <a href="#" class="rt-btn rt-outline-gradientL rt-rounded">Help me to get agent ID</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.deal-carosel-active -->
        </div><!-- /.container -->
    </section>--%>



    <section class="svcPage-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 text-center mx-auto">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            <span>Our Exclusive Offers</span>
                            Our Services
                        </h2>
                        <!-- /.rt-section-title -->
                        <p>
                            IRCTC Railway Ticketing Agency Provider and also Get Exclusive Access to Our Range of Products and Services
                        </p>
                    </div>
                    <!-- /.rt-section-title-wrapper- -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="section-title-spacer"></div>
            <!-- /.section-title-spacer -->
            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-8.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Rail Booking</h5>
                            <p>
                                Anupam Travel is offering travel agents across India an opportunity to become an IRCTC-authorized travel agent and get their own IRCTC approved rail license.
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="0.5s" style="visibility: visible; animation-duration: 0.5s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-5.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Air Ticketing</h5>
                            <p>
                                Book Cheap Air Flight Tickets Online for India and Worldwide Destinations.
                                Also Book LCC and GDS Airplane Ticket at the Lowest Prices at Anupam Travels.
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-6.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Hotel Bookings</h5>
                            <p>
                                Booking a hotel on Anupam Travels · Whether you're travelling for business or pleasure,
                                Anupam Travels makes it a simple and delightful experience to book hotels
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-7.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Bus Booking</h5>
                            <p>
                                Make Online Bus Ticket Bookings across India with Anupam Travels and get great discounts.
                                Book Volvo, luxury, semi deluxe, Volvo A/c Sleeper and other buses.
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>
                <!-- /.col-lg-4 -->
                <!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="2.5s" style="visibility: visible; animation-duration: 2.5s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-9.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Bill Payment</h5>
                            <p>
                                Make Bills anytime anywhere with Desktop and Mobile seamless sync.
                                Simple GST Billing & Stock Management software for your business. Mobile & Desktop Sync.
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rt-single-icon-box icon-center text-center justify-content-center wow fade-in-bottom animated" data-wow-duration="3s" style="visibility: visible; animation-duration: 3s; animation-name: fade-in-bottom;">
                        <div class="icon-thumb">
                            <img src="assets/images/icons-image/box-icon-10.png" alt="box-icon" draggable="false">
                        </div>
                        <!-- /.icon-thumb -->
                        <div class="iconbox-content">
                            <h5>Money Transfer</h5>
                            <p>
                                Our secure, immediate and easy domestic service allows you to send money to any bank account in India.
                                We provides the best money transfer services in India.
                            </p>

                        </div>
                        <!-- /.iconbox-content -->
                    </div>
                    <!-- /.rt-single-icon-box -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <section class="content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mx-auto text-center">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            <span>Why Should you Join</span>
                            Affiliate Program
                        </h2>
                        <p>
                            Joining our affiliate program provides you with countless opportunities to combine your
                            love for travel and your ability to earn additional revenue.
                        </p>
                    </div>
                    <!-- /.rt-section-title-wrapper -->
                </div>
                <!-- /.col-lg-8 -->
            </div>
            <!-- ./row -->
            <div class="section-title-spacer"></div>
            <!-- /.section-title-spacer -->
            <div class="row align-items-center">
                <div class="col-lg-5 mb-0 mb-md-4">
                    <div class="icon-thumb rt-mb-30 wow fadeInDown animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInDown;">
                        <%--                        <img src="assets/images/icons-image/1200px-IRCTC_Logo.svg.png" alt="icon-image" draggable="false" style="width: 100px;">--%>
                    </div>
                    <!-- /.icon-thumb -->
                    <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInDown animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInDown;">Benefits of becoming an IRCTC Authorised Agent</h4>
                    <div class="text-424 wow fadeInUp animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                        <ul class="rt-list f-size-14">

                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Unlimited number of tickets can be booked</li>

                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>arn huge commissions on every booking</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Accessible and dynamic reporting</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Book all type of Tickets in bulk (Tatkal, Waiting List, RAC also)</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Earn regular Income of up to INR 1,00,000/- per month & more</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Book Tatkal tickets after 15 minutes of general public opening time</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>No requirement of Trade License to become an IRCTC authorised agent</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Faster & hassle-free ticket booking system as the amount will get directly deducted from the wallet</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>No bank account required. Money can be directly deposited in Principal agent bank account</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Your agency details will get printed in the IRCTC ticket</li>
                            <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>One single platform to freely book other travel products along with rail tickets such as International & Domestic flights, bus booking, tours/holiday packages, money transfer and much more</li>
                        </ul>
                    </div>
                    <!-- /.text-424 -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6 offset-lg-1">
                    <img src="assets/images/backgrounds/irctc.png" alt="work image" draggable="false" class="wow zoomIn animated" data-wow-duration="2.5s" style="visibility: visible; animation-duration: 2.5s; animation-name: zoomIn;">
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
            <div class="section-title-spacer"></div>
            <!-- /.section-title-spacer -->
            <div class="row align-items-center">
                <div class="col-lg-6 mb-0 mb-md-4">

                    <img src="assets/images/all-img/work-img-7.png" alt="work image" draggable="false" class="wow zoomIn animated" data-wow-duration="2.5s" style="visibility: visible; animation-duration: 2.5s; animation-name: zoomIn;">
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-5 offset-lg-1">
                    <div class="icon-thumb rt-mb-30 wow fadeInUp animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                        <img src="assets/images/all-img/iconx-4.png" alt="icon-image" draggable="false">
                    </div>
                    <!-- /.icon-thumb -->
                    <h4 class="f-size-24 rt-semiblod rt-mb-20 wow fadeInUp animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">Why partner with Anupam Travels?</h4>
                    <div class="text-424 wow fadeInDown animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInDown;">
                        <p class="f-size-14 text-424">
                            Anupam Travel is a thriving online travel superstore with a wide selection of ultimate products. Joining our affiliate program provides you with countless opportunities to combine your
                            love for travel and your
                            ability to earn additional revenue.

                        </p>
                    </div>
                    <!-- /.text-424 -->
                    <a href="#" class="rt-btn rt-gradient rt-rounded text-uppercase rt-sm rt-mt-25 wow fadeInUp animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">Read more</a>

                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->


        </div>
        <!-- ./ copntainer -->
    </section>


    <div class="spacer-top"></div>
    <!-- /.spacer-top -->


    <section class="portfolio-area rt-section-padding rtbgprefix-full bg-hide-md gradinet-bg-md" style="background-image: url(assets/images/backgrounds/portfoliobg.png)">
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <img src="assets/images/backgrounds/eligi.png" />
                </div>

                <div class="col-xl-6 text-center mx-auto" style="text-align: left !important;">
                    <div class="rt-section-title-wrapper text-white">
                        <h2 class="rt-section-title" style="font-size: 33px;">
                            <span>Take a Look at Our</span>
                            ELIGIBILITY CRITERIA
                        </h2>
                        <div class="text-424 wow fadeInUp animated" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                            <ul class="rt-list f-size-14" style="color: #fff; font-size: 25px;">

                                <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Who can apply ?<p style="color: #ccc;">- Any individual , mobile outlet , E-ticketing counter , Internet cafe , grocery shop , stationery shop and persons who are in any business .</p>
                                </li>
                                <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>What Infrastructure required ?
                                    <p style="color: #ccc;">- One computer with internet connection and very small place to operate .</p>
                                </li>
                                <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Document Required
                                    <p style="color: #ccc;">- Aadhaar Card , Pancard , Mobile No, Email ID, and attached form to be filled up. Being financial product agreement is also required to be done.</p>
                                </li>
                                <li><span class="rt-pr-4"><i class="icofont-check-circled"></i></span>Processing Time
                                    <p style="color: #ccc;">- It will take approx . 1 - 3 Days in successful registration</p>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-title-spacer"></div>

        </div>
    </section>



    <div class="spacer-top"></div>

    <section class="flight-dela-area" data-scrollax-parent="true">
        <div class="rt-shape-emenetns-1" style="background-image: url(assets/images/shape-elements/shape_1.png)"
            data-scrollax="properties: { translateY: '340px' }">
        </div>
        <!-- /.rt-shape-emenetns-1 -->
        <div class="rt-shape-emenetns-2" style="background-image: url(assets/images/shape-elements/shape-2.png)"
            data-scrollax="properties: { translateX: '-140px' }">
        </div>
        <!-- /.rt-shape-elemenets2 -->
        <div class="container">
            <div class="row">
                <div class="col-xl-10 mx-auto text-center">
                    <div class="rt-section-title-wrapper">
                        <h2 class="rt-section-title">
                            <span>Why Choose Us</span>

                        </h2>
                        <!-- /.rt-section-title -->

                    </div>
                    <!-- /.rt-section-title-wrapper -->
                </div>
                <!-- /.col-lg-9 -->
            </div>
            <!-- /.row -->
            <div class="section-title-spacer"></div>
            <!-- /.section-title-spacer -->
            <div class="counter-area">
                <div class="container">
                    <div class="row">



                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="media counter-box-1 align-items-center wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                <img src="assets/images/counter-icons/counter_iocn_1.png" alt="counter_iocn" draggable="false" style="width: 70px;">
                                <div class="media-body">
                                    <h5>Best Performance</h5>
                                    <p>Simple booking process run a lot smoother for our customers.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="media counter-box-1 align-items-center wow fadeInUp animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                                <img src="assets/images/counter-icons/counter_iocn_2.png" alt="counter_iocn" draggable="false" style="width: 70px;">
                                <div class="media-body">
                                    <h5>Fast Processing System</h5>
                                    <p>It is a quick, secure and easy process to make a flight booking.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="media counter-box-1 align-items-center wow fadeInUp animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                <img src="assets/images/counter-icons/counter_iocn_3.png" alt="counter_iocn" draggable="false" style="width: 70px;">
                                <div class="media-body">
                                    <h5>Free Documentation</h5>
                                    <p>We help to build your Minimum Documentation to avail our Services.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-4 -->

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
        </div>
        <!-- /.container -->
    </section>



    <div class="spacer-top"></div>
    <!-- /.spacer-top -->
    <section class="rt-cta-area">

        <div id="contactUS" class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-box-1 d-flex flex-lg-row flex-column align-items-center text-center text-lg-left justify-content-lg-between rtbgprefix-cover text-white justify-content-center" style="background-image: url(assets/images/backgrounds/cat_1.jpg)">
                        <div class="left-column">
                            <h4 class="wow fade-in-top" data-wow-duration="1s" data-wow-delay="0.2s">Request A Call Back - <span>+91-8100999727</span><br />

                                Email - <span>info@anupamtravelonline.com</span><br />
                                Address - <span>232/5, G.T. Road, Belur Math, Howrah, West Bengal-711202</span>
                            </h4>
                            <%-- <p class="wow fade-in-bottom" data-wow-duration="1s" data-wow-delay="0.2s">
                                Send us an email and someone on our team will be in touch with you!
                            </p>--%>
                        </div>
                        <!-- /.left-column -->
                        <div class="right-column">
                            <%--                            <a href="#" class="rt-btn rt-gradient rt-sm text-uppercase rt-rounded rt-Bshadow-2 wow fade-in-left" data-wow-duration="1s" data-wow-delay="0.6s">Contact Now</a><!-- /.rt-btn -->--%>
                        </div>
                        <!-- /.right-column -->
                    </div>
                    <!-- /.inner-content -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->

    </section>
    <br />
    <section class="deal-area   rtbgprefix-full bg-hide-md" style="background-image: url(assets/images/backgrounds/bgshapes_1.png)">
        <div class="container-fluid p-0">
            <div class="deal-carosel-active owl-carousel">
                <div class="row single-deal-carosel align-items-center">
                    <div class="col-lg-5">
                        <!--<div class="deal-bg" style="background-image: url(assets/images/all-img/deal-bg.jpeg)">
                            <div class="inner-content">
                                <h4 data-animation="fadeInDown" data-duration=".2s" data-delay=".4s">14 Day Classic Tour of Thailand & Beaches</h4>
                                <p data-animation="fade-in-bottom" data-duration=".2s" data-delay=".4s">Grab a promo code for extra savings up to 75% on discounted hotels!</p>
                            </div>
                        </div>-->
                        <img src="assets/images/counter-icons/Travel-Vectors.png" />
                    </div>
                    <div class="col-lg-7">
                        <div class="rt-section-title-wrapper text-white" data-animation="fadeIn" data-duration=".4s">
                            <h2 class="rt-section-title">
                                <span></span>
                                About Us
                            </h2>
                            <p>Anupam Travels.com is the leading B2B portal for integrated travel and travel related services in the country. Anupam Travels offers the travel agents a wide spectrum of solutions that include Ticket Bookings, Hotel Bookings, Transfers and Sightseeing. Our principal motto is to provide nonpareil rates and quality Services to the agents all over India.</p>
                            <div class="section-title-spacer"></div>
                            <div class="deal-bottom-content">

                                <div class="section-title-spacer"></div>
                                <h4>Our team works 24X7 to give you unexcelled and heavenly experiences</h4>
                                <p>
                                    Anupam Travels.com is a new-fangled traveling portal managed by a team of immensely experienced people in the B2B Market.
                                </p>
                                <div class="rt-button-group">

                                    <a href="#" class="rt-btn rt-outline-gradientL rt-rounded">Help me to get agent ID</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.deal-carosel-active -->
        </div>
        <!-- /.container -->
    </section>



    <div class="spacer-top"></div>
    <!-- /.spacer-top -->





    <%--    <section class="contact-area">
        <div class="rt-design-elmnts rtbgprefix-contain" style="background-image: url(assets/images/all-img/abt_vec_3.png)">

   </div>
    </section>--%>





    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="/Enquiry/assets/js/jquery-2.2.4.min.js"></script>
    <script src="/Enquiry/assets/js/popper.min.js"></script>
    <script src="/Enquiry/assets/js/bootstrap.min.js"></script>
    <script src="/Enquiry/assets/js/moment.min.js"></script>
    <script src="/Enquiry/assets/js/jquery.easing.1.3.js"></script>
    <script src="/Enquiry/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="/Enquiry/assets/js/isotope.pkgd.min.js"></script>
    <script src="/Enquiry/assets/js/instafeed.min.js"></script>
    <script src="/Enquiry/assets/js/waypoints.min.js"></script>
    <script src="/Enquiry/assets/js/jquery.counterup.min.js"></script>
    <script src="/Enquiry/assets/js/jquery.magnific-popup.min.js"></script>
    <%--  <script src="/Enquiry/assets/js/jquery.scrollUp.min.js"></script>--%>
    <script src="/Enquiry/assets/js/owl.carousel.min.js"></script>
    <script src="/Enquiry/assets/js/TweenMax.min.js"></script>
    <script src="/Enquiry/assets/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
    <script src="/Enquiry/assets/js/wow.js"></script>
    <script src="/Enquiry/assets/js/jquery.overlayScrollbars.min.js"></script>
    <script src="/Enquiry/assets/js/jquery-ui.js"></script>
    <script src="/Enquiry/assets/js/jquery.appear.js"></script>
    <script src="/Enquiry/assets/js/select2.min.js"></script>
    <script src="/Enquiry/assets/js/slick.min.js"></script>
    <script src="/Enquiry/assets/js/slider-range.js"></script>
    <script src="/Enquiry/assets/js/vivus.min.js"></script>
    <script src="/Enquiry/assets/js/tippy.all.min.js"></script>
    <script src="/Enquiry/assets/js/app.js"></script>

</body>
</html>
