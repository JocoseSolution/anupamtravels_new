﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dmt_manager_DMT_MasterPage : System.Web.UI.MasterPage
{
    public string AgencyName = string.Empty;
    public string AgencyId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            AgencyName = Session["AgencyName"].ToString();
            AgencyId = Session["AgencyId"].ToString();
        }
    }
    protected void lnklogout_Click1(object sender, EventArgs e)
    {
        try
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect("~/Login.aspx");
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
}
