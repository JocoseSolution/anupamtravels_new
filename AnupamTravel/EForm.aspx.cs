﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

public partial class EForm : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSendEnquiry_Click(object sender, EventArgs e)
    {
        try
        {
            con.Open();
            String sp = "InsertEnquiryData";
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nme", txtEYourName.Text);
            cmd.Parameters.AddWithValue("@mob", txtEMobileNumber.Text);
            cmd.Parameters.AddWithValue("@email", txtEEmailId.Text);
            cmd.Parameters.AddWithValue("@msg", txtEMessage.Text);
            cmd.ExecuteNonQuery();

            string name = txtEYourName.Text.ToString();
            string mob = txtEMobileNumber.Text.ToString();
            string email = txtEEmailId.Text.ToString();
            string emsg = txtEMessage.Text.ToString();
            EmailTemp(name, mob, emsg, email);
            txtEYourName.Text = "";
            txtEMobileNumber.Text = "";
            txtEEmailId.Text = "";
            txtEMessage.Text = "";
            con.Close();
        }
        catch (Exception ex)
        {


        }
    }

    public void EmailTemp(string name, string mob, string emsg, string email)
    {
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtpout.secureserver.net";
        smtp.Port = 587;
        smtp.Credentials = new System.Net.NetworkCredential("info@anupamtravelonline.com", "NDA@201427");
        smtp.EnableSsl = true;
        MailMessage msg = new MailMessage();
        msg.Subject = "New Enquiry from" + ": " + name;
        msg.Body = @"<html><body><div style='width:800px;
  height: 250px;
  padding: 100px;
  border: 2px solid #2980B9; max-width: 500px;
  margin: auto;
  background: white;
  text-align:center;
  padding: 10px;'><h3>Name :<strong> " + name + @"</strong></h3></br><h4>Mobile No:<strong> " + mob + @"</strong></h4></br><p>Email :<strong>" + email + @"</strong></p><br/><p>Message : <strong>"+emsg+@"</div></body></html>";
        msg.IsBodyHtml = true;
        string toaddress = "info@anupamtravelonline.com";
        msg.To.Add(toaddress);
        string fromaddress = "info@anupamtravelonline.com";
        msg.From = new MailAddress(fromaddress);
        try
        {
            smtp.Send(msg);

        }
        catch
        {
            throw;
        }

    }
}