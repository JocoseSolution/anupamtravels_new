﻿BindA2ZOpratorByType("ddlA2ZMobileOprator", 'A2ZPREPAID', 'Operator');

$(".actiontype").click(function () {
    $(".actiontype").removeAttr("style");

    $(this).css("background", "rgb(255 126 0 / 60%)");
    $(this).removeClass("active");
    actionType = $(this).data("valtext");

    if (actionType == "mobile") { A2ZTabResetMobile(); }
    if (actionType == "dth") { $(".dthsecation").addClass("hidden"); $("#txtA2ZDTHNumber").val(""); $("#txtA2ZDTHAmount").val(""); BindA2ZOpratorByType("ddlA2ZDTHOprator", "A2ZDTH", 'Operator'); }
    if (actionType == "electricity") { $("#ElectricityAmount").val("").addClass("hidden"); $("#txtElectConsumerNumber").val(""); BindA2ZOpratorByType("ddlElectricityBoard", "A2ZELECTRICITY", 'Electricity Board'); $(".electricitydynamicinput").remove(); }
    if (actionType == "landline") { $(".landlinedynamicinput").remove(); $("#txtLandlineNumber").val(""); BindA2ZOpratorByType("ddlLandlineOperator", "A2ZLANDLINE", 'Operator'); }
    if (actionType == "insurance") { $(".insurancedynamicinput").remove(); $("#txtInsuranceNumber").val(""); BindA2ZOpratorByType("ddlInsurance", "A2ZInsurance", 'Insurer'); }
    if (actionType == "gas") { $(".gasdynamicinput").remove(); $("#txtGasNumber").val(""); BindA2ZOpratorByType("ddlGasProvider", "A2ZGAS", 'Gas Provider'); }
    //if (actionType == "internet") { $("#internetnotesmsg").html("").addClass("hidden"); $(".internetdynamicinput").remove(); BindOpratorByType("ddlInternetService", "INTERNET/ISP", 'Operator'); }
    if (actionType == "water") { $(".waterdynamicinput").remove(); $("#txtWaterNumber").val(""); BindA2ZOpratorByType("ddlWaterBoard", "A2ZWater", 'Water Board'); }
    if (actionType == "fastag") { $("#txtFastTagNumber").val(""); BindA2ZOpratorByType("ddlFastTag", "A2ZFastTag", 'FastTag'); }
    if (actionType == "emi") { $(".emidynamicinput").remove(); $("#txtEmiNumber").val(""); BindA2ZOpratorByType("ddlEMI", "A2ZLoanRepayment", 'Loan RePayment'); }
    if (actionType == "taxes") { $(".taxesdynamicinput").remove(); $("#txtMunicipalTaxesNumber").val(""); BindA2ZOpratorByType("ddlMunicipalTaxes", "A2ZMunicipalTaxes", 'Municipal Area'); }
    if (actionType == "billtranshistory") { A2ZBindBillTransHistory('today'); ReBindFilter(); }
});

//$(function () { if (actionType == null) { actionType = "mobile"; } BindA2ZOpratorByType("ddlA2ZMobileOprator", 'A2ZPREPAID', 'Operator'); });

$('input[name=mobiletype]').change(function () {
    if (this.value.toLowerCase() == 'a2zprepaid') {
        $("#btnA2ZMobileRecharge").html("Recharge");
        //$("#divA2ZGetRechargeOffer").css("display", "block");
        $("#A2ZAmountSection").css("display", "block");
        BindA2ZOpratorByType("ddlA2ZMobileOprator", 'A2ZPREPAID', 'Operator');
    }
    else {
        $("#btnA2ZMobileRecharge").html("Get Bill");
        $("#divA2ZGetRechargeOffer").css("display", "none");
        $("#A2ZAmountSection").css("display", "none");
        BindA2ZOpratorByType("ddlA2ZMobileOprator", 'A2ZPOSTPAID', 'Operator');
    }
    A2ZResetMobile();
});

function BindA2ZOpratorByType(dropdownid, servicetype, selectname) {
    $("#" + dropdownid).prop("disabled", true);
    $.ajax({
        type: "Post",
        url: "/BillPayment.aspx/BindA2ZOpratorByType",
        data: '{servicetype: ' + JSON.stringify(servicetype) + ',selectname: ' + JSON.stringify(selectname) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                $("#" + dropdownid).html("").html(data.d);
                $("#" + dropdownid).prop("disabled", false);
            }
        }
    });
}

function A2ZRechargeEvent() {
    $(".close").click();
    var thisbutton = $("#btnA2ZMobileRecharge");
    var txnmode = $("input[name='mobiletype']:checked").val();

    var mobileno = $("#txtA2ZMobileNumber").val();
    var providerKey = $("#ddlA2ZMobileOprator option:selected").val();
    var providerKeyName = $("#ddlA2ZMobileOprator option:selected").text();
    var servicetype = $("#ddlA2ZMobileOprator option:selected").data("servicetype");
    var serviceid = $("#ddlA2ZMobileOprator option:selected").data("serviceid");
    var spkey = $("#ddlA2ZMobileOprator option:selected").data("spkey");

    if (CheckFocusBlankValidation("txtA2ZMobileNumber")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlA2ZMobileOprator")) return !1;
    if (txnmode.toLowerCase() == "a2zprepaid") {
        if (CheckFocusBlankValidation("txtA2ZAmount")) return !1;
    }
    else {
        if (providerKeyName.toLowerCase() != "airtel postpaid") {
            if (CheckFocusBlankValidation("txtA2ZAmount")) return !1;
        }
    }

    var amount = null;
    if (txnmode.toLowerCase() == "a2zprepaid") { amount = $("#txtA2ZAmount").val(); }
    else { amount = 0; }

    if (mobileno.length == 10) {
        if (txnmode.toLowerCase() == "a2zprepaid") {
            if (confirm("Are you sure want to recharge on " + mobileno + " number?")) {
                $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
                $("#a2zmodelmobileheadbody").html("");
                $.ajax({
                    type: "Post",
                    contentType: "application/json; charset=utf-8",
                    url: "/BillPayment.aspx/A2ZMobileRecharge",
                    data: '{mobile: ' + JSON.stringify(mobileno) + ',providerKey: ' + JSON.stringify(providerKey) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + ',servicetype: ' + JSON.stringify(servicetype) + ',amount: ' + JSON.stringify(amount) + ',serviceid: ' + JSON.stringify(serviceid) + ',rchtype: ' + JSON.stringify(txnmode) + ',spkey:' + JSON.stringify(spkey) + '}',
                    datatype: "json",
                    success: function (data) {
                        if (data.d != null) {
                            if (data.d[0] == "success") {
                                if (txnmode.toLowerCase() == "a2zprepaid") { A2ZResetMobile(); }
                                $("#a2zmodelmobileheadbody").html("");
                                $("#a2zmodelmobileheadbody").html(data.d[1]);
                                $(".a2zmobilemodelclickclass").click();
                            }
                            else if (data.d[0] == "failure") {
                                //ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                                $("#a2zmodelmobileheadbody").html("");
                                $("#a2zmodelmobileheadbody").html(data.d[1]);
                                $(".a2zmobilemodelclickclass").click();
                            }
                            else if (data.d[0] == "reload") {
                                window.location.reload();
                            }
                            else {
                                //var resulthtml = "<div class='modal-header'><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'>"
                                //    + "<span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-danger text-center' style='padding: 40px!important;'>"
                                //    + "<h4>" + data.d[1]+"</h4></div>";
                                $("#a2zmodelmobileheadbody").html("");
                                $("#a2zmodelmobileheadbody").html(data.d[1]);
                                $(".a2zmobilemodelclickclass").click();
                            }
                        }
                        if (txnmode.toLowerCase() == "a2zprepaid") {
                            $(thisbutton).html("Recharge").prop('disabled', false);
                        }
                        else {
                            $(thisbutton).html("Get Bill").prop('disabled', false);
                        }
                        BindCreditAmount();
                    },
                    failure: function (response) {
                        alert("failed");
                    }
                });
            }
        }
        else {
            if (providerKeyName.toLowerCase() != "airtel postpaid") {
                if (confirm("Are you sure want to pay bill on " + mobileno + " number?")) {
                    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
                    $("#a2zmodelmobileheadbody").html("");
                    $.ajax({
                        type: "Post",
                        contentType: "application/json; charset=utf-8",
                        url: "/BillPayment.aspx/A2ZPostPaidMobileRecharge",
                        data: '{mobile: ' + JSON.stringify(mobileno) + ',providerKey: ' + JSON.stringify(providerKey) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + ',servicetype: ' + JSON.stringify(servicetype) + ',amount: ' + JSON.stringify(amount) + ',serviceid: ' + JSON.stringify(serviceid) + ',rchtype: ' + JSON.stringify(txnmode) + ',spkey:' + JSON.stringify(spkey) + '}',
                        datatype: "json",
                        success: function (data) {

                        },
                        failure: function (response) {
                            alert("failed");
                        }
                    });
                }
            }
            else {
                if (confirm("Are you sure want to fetch bill of " + mobileno + " number?")) {
                    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
                    $("#a2zmodelmobileheadbody").html("");
                    $.ajax({
                        type: "Post",
                        contentType: "application/json; charset=utf-8",
                        url: "/BillPayment.aspx/A2ZGetAirtelPostPaidBill",
                        data: '{mobile: ' + JSON.stringify(mobileno) + ',providerKey: ' + JSON.stringify(providerKey) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + ',servicetype: ' + JSON.stringify(servicetype) + ',amount: ' + JSON.stringify(amount) + ',serviceid: ' + JSON.stringify(serviceid) + ',rchtype: ' + JSON.stringify(txnmode) + ',spkey:' + JSON.stringify(spkey) + '}',
                        datatype: "json",
                        success: function (data) {
                            $("#a2zmodelmobileheadbody").html("");
                            $("#a2zmodelmobileheadbody").html(data.d[1]);
                            $(".a2zmobilemodelclickclass").click();
                            $("#btnA2ZMobileRecharge").html("Get Bill");
                        },
                        failure: function (response) {
                            alert("failed");
                        }
                    });
                }
            }
        }
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", "Mobile number not valid.", "#d91717");
    }
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html("").html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html("").html(bodyMsg);
}

function A2ZResetMobile() {
    $("#txtA2ZMobileNumber").val("");
    $("#ddlA2ZMobileOprator").val("0").change();
    $("#txtA2ZAmount").val("");
    $("#divA2ZGetRechargeOffer").css("display", "none");
}

function ShowA2ZGetRechargeOffer() {
    var selectedMType = $('input[name=mobiletype]:checked').val();

    let mobileno = $("#txtA2ZMobileNumber").val();
    var providerKey = $("#ddlA2ZMobileOprator option:selected").val();
    var providerKeyName = $("#ddlA2ZMobileOprator option:selected").text();

    if (providerKey != "0") {
        if (mobileno.length == 10) {
            if (selectedMType.toLowerCase() == 'a2zprepaid') {
                if (providerKey != "0" && providerKey != "") {
                    $("#divA2ZGetRechargeOffer").css("display", "block");
                }
                else {
                    $("#divA2ZGetRechargeOffer").css("display", "block");
                }
            }
            else {
                if (providerKeyName.toLowerCase() != "airtel postpaid") {
                    $("#A2ZAmountSection").css("display", "block");
                    $("#btnA2ZMobileRecharge").html("Pay Bill");
                }
                else {
                    $("#A2ZAmountSection").css("display", "none");
                    $("#btnA2ZMobileRecharge").html("Get Bill");
                }
                $("#txtA2ZAmount").val("");
            }
        }
        else {
            $("#ddlA2ZMobileOprator").val("0").change();
            ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", "Mobile number not valid.", "#d91717");
        }
    }
    else {
        $("#divA2ZGetRechargeOffer").css("display", "none");
        $("#txtA2ZAmount").val("");
    }
}

function GetA2ZGetRechargeOffer() {
    var thisbutton = $("#btnA2ZGetRechargeOffer");
    let mobileno = $("#txtA2ZMobileNumber").val();
    var providerKey = $("#ddlA2ZMobileOprator option:selected").val();
    var providerKeyName = $("#ddlA2ZMobileOprator option:selected").text();

    if (mobileno.length == 10) {
        if (providerKey != "0" && providerKey != "") {
            $(thisbutton).html("Fetching... <i class='fa fa-pulse fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/BillPayment.aspx/GetA2ZGetRechargeOffer",
                data: '{mobileno: ' + JSON.stringify(mobileno) + ',providerKey: ' + JSON.stringify(providerKey) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != "") {
                        $("#a2zmodelmobileheadbody").html("");
                        $("#a2zmodelmobileheadbody").html(data.d);
                        $(".a2zmobilemodelclickclass").click();
                    }
                    $(thisbutton).html("Get Offer");
                }
            });
        }
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", "Mobile number not valid.", "#d91717");
    }
}

function A2ZPostPaidBillPaymentSubmit() {
    let mobile = $("#hdnPostPaidMobileDetails").data("mobile");
    let provider = $("#hdnPostPaidMobileDetails").data("provider");

    if (confirm("Are you sure want to pay bill of " + mobile + " number?")) {
        var txnmode = $("input[name='mobiletype']:checked").val();

        var amount = $("#txtPartPostPaidMobileAmount").val();
        var providerKey = $("#ddlA2ZMobileOprator option:selected").val();
        var providerKeyName = $("#ddlA2ZMobileOprator option:selected").text();
        var servicetype = $("#ddlA2ZMobileOprator option:selected").data("servicetype");
        var serviceid = $("#ddlA2ZMobileOprator option:selected").data("serviceid");
        var spkey = $("#ddlA2ZMobileOprator option:selected").data("spkey");

        $("#btnPostPaidBillPayment").html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/BillPayment.aspx/A2ZPostPaidMobileRecharge",
            data: '{mobile: ' + JSON.stringify(mobile) + ',providerKey: ' + JSON.stringify(provider) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + ',servicetype: ' + JSON.stringify(servicetype) + ',amount: ' + JSON.stringify(amount) + ',serviceid: ' + JSON.stringify(serviceid) + ',rchtype: ' + JSON.stringify(txnmode) + ',spkey:' + JSON.stringify(spkey) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        if (txnmode.toLowerCase() == "a2zprepaid") { A2ZResetMobile(); }
                        $("#a2zmodelmobileheadbody").html("");
                        $("#a2zmodelmobileheadbody").html(data.d[1]);
                        //$(".a2zmobilemodelclickclass").click();
                    }
                    else if (data.d[0] == "failure") {
                        $("#a2zmodelmobileheadbody").html("");
                        $("#a2zmodelmobileheadbody").html(data.d[1]);
                        //$(".a2zmobilemodelclickclass").click();
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#a2zmodelmobileheadbody").html("");
                        $("#a2zmodelmobileheadbody").html(data.d[1]);
                        //$(".a2zmobilemodelclickclass").click();
                    }
                }
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function A2ZTabResetMobile() {
    A2ZResetMobile();
    $("#rdoA2ZPrepaid").click();
}

function SelectMobilePlan(amount) {
    if (amount != "") {
        if (parseFloat(amount) > 0) {
            if (confirm("Are you sure want to select this plan ?")) {
                $("#txtA2ZAmount").val("").val(amount);                
            }
        }
        else {
            $("#txtA2ZAmount").val("");
        }
    }
    else {
        $("#txtA2ZAmount").val("");
    }
    $(".close").click();
}

//============================Start DTH Recharge Section=====================================
function A2ZTabResetDTH() {
    $("#txtA2ZDTHNumber").val("");
    $("#ddlA2ZDTHOprator").val("0").change();
    $("#txtA2ZDTHAmount").val("");
    $(".dthsecation").addClass("hidden");
}

$("#ddlA2ZDTHOprator").change(function () {
    $(".dthsecation").removeClass("hidden");

    var spkey = $("#ddlA2ZDTHOprator option:selected").data("spkey");
    var servicetype = $("#ddlA2ZDTHOprator option:selected").data("servicetype");
    var thisval = $("#ddlDTHOprator option:selected").val();
});

function A2ZDTHRechargeEvent() {
    if (CheckFocusDropDownBlankValidation("ddlA2ZDTHOprator")) return !1;
    if (CheckFocusBlankValidation("txtA2ZDTHNumber")) return !1;
    if (CheckFocusBlankValidation("txtA2ZDTHAmount")) return !1;

    var spkey = $("#ddlA2ZDTHOprator option:selected").data("spkey");
    var servicetype = $("#ddlA2ZDTHOprator option:selected").data("servicetype");
    var providerKey = $("#ddlA2ZDTHOprator option:selected").val();
    var providerKeyName = $("#ddlA2ZDTHOprator option:selected").text();
    var dthno = $("#txtA2ZDTHNumber").val();
    var dthamt = $("#txtA2ZDTHAmount").val();

    if (confirm("Are you sure want to recharge on " + dthno + " dth number?")) {
        $("#btnA2ZDTHRecharge").html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/BillPayment.aspx/A2ZDTHRecharge",
            data: '{providerKey: ' + JSON.stringify(providerKey) + ',providerKeyName: ' + JSON.stringify(providerKeyName) + ',servicetype: ' + JSON.stringify(servicetype) + ',amount: ' + JSON.stringify(dthamt) + ',spkey:' + JSON.stringify(spkey) + ',dthno:' + JSON.stringify(dthno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        if (txnmode.toLowerCase() == "a2zprepaid") { A2ZResetMobile(); }
                        $("#modela2zdthheadbody").html("");
                        $("#modela2zdthheadbody").html(data.d[1]);
                        $(".dtha2zmodelclickclass").click();
                    }
                    else if (data.d[0] == "failure") {
                        $("#modela2zdthheadbody").html("");
                        $("#modela2zdthheadbody").html(data.d[1]);
                        $(".dtha2zmodelclickclass").click();
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#modela2zdthheadbody").html("");
                        $("#modela2zdthheadbody").html(data.d[1]);
                        $(".dtha2zmodelclickclass").click();
                    }
                }
                $("#btnA2ZDTHRecharge").html("Recharge").prop('disabled', false);
            },
            failure: function (response) {
                alert("failed");
                $("#btnA2ZDTHRecharge").html("Recharge").prop('disabled', false);
            }
        });
    }
}

function A2ZCallBackCheckStatus(billid) {
    var thisbutton = $("#btnCallBackCheckStatus_" + billid);

    var transid = $(thisbutton).data("transid");
    var clientrefid = $(thisbutton).data("refid");
    var status = $(thisbutton).data("status");
    var trnsdate = $(thisbutton).data("trnsdate");

    var fromdate = trnsdate;//$("#txtBillFromDate").val();
    var todate = $("#txtBillToDate").val();
    var clintrefid = $("#txtClientRefID").val();
    var transtype = $("#ddlBillTransType option:selected").val();
    var inputstatus = $("#ddlBillTransStatus option:selected").val();

    if (transid != "" && clientrefid != "") {
        $(thisbutton).html("Checking...<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true).css("color", "blue");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/BillPayment.aspx/A2ZProcessToCallBackCheckStatus",
            data: '{transid: ' + JSON.stringify(transid) + ',refid: ' + JSON.stringify(clientrefid) + ',status: ' + JSON.stringify(status) + ',billid: ' + JSON.stringify(billid) + ',fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',clintrefid: ' + JSON.stringify(clintrefid) + ',transtype: ' + JSON.stringify(transtype) + ',inputstatus: ' + JSON.stringify(inputstatus) + '}',
            datatype: "json",
            success: function (data) {
                //if (data.d[0] == "000") {
                //    if (data.d[3] != "") {
                //        $("#BillTransRowDetails").html("");
                //        $("#BillTransRowDetails").html(data.d[3]);
                //    }

                //    ShowMessagePopup(data.d[1], "#2dbe60", data.d[2], "#2dbe60");
                //}
                //else if (data.d[0] == "999") {
                //    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;" + data.d[0], "#d91717", data.d[1], "#d91717");
                //}
                //else {
                //    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;" + data.d[0], "#d91717", data.d[1], "#d91717");
                //}

                ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;" + data.d[0], "#d91717", data.d[1], "#d91717");

                $(thisbutton).html("Check Status").prop('disabled', false).css("color", "#ff414d");
                if (data.d[2] != "") {
                    $("#BillTransRowDetails").html(data.d[2]);
                }
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

//============================Start Electricity Recharge Section=====================================
function FeatchElectricityBill() {
    var spkey = $("#ddlElectricityBoard option:selected").data("spkey");
    var servicetype = $("#ddlElectricityBoard option:selected").data("servicetype");
    var provider = $("#ddlElectricityBoard option:selected").val();
    var providername = $("#ddlElectricityBoard option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlElectricityBoard")) return !1;
    if (CheckFocusBlankValidation("txtElectConsumerNumber")) return !1;

    var number = $("#txtElectConsumerNumber").val();

    var thisbutton = $("#btnElectricity");
    if (confirm("Are you sure want to fetch electricity bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Electricity", "modelelectricitycontent", "electricitymodelclickclass", "btnElectricity", "Get Electricity Bill", true);
    }
    else {
        $(thisbutton).html("Get Electricity Bill");
    }
}

function A2Z_ElectricityPaymentSubmit() {
    if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlElectricityBoard option:selected").data("spkey");
    var servicetype = $("#ddlElectricityBoard option:selected").data("servicetype");
    var provider = $("#ddlElectricityBoard option:selected").val();
    var providername = $("#ddlElectricityBoard option:selected").text();
    var number = $("#txtElectConsumerNumber").val();
    var amount = $("#txtPartA2Z_ElectricityAmount").val();
    if (amount == "") {
        amount = $("#hdnTotalA2Z_ElectricityPaidAmt").val();
        $("#txtPartA2Z_ElectricityAmount").val(amount);
    }
    var customername = $("#hdnTotalA2Z_ElectricityPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay electricity bill of " + number + " number?")) {
            $("#btnA2Z_ElectricityPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Electricity", "modelelectricitycontent", customername, "ddlElectricityBoard", "txtElectConsumerNumber");
        }
    }
}

//============================Start Landline Section=====================================
function FeatchLandlineBill() {
    var spkey = $("#ddlLandlineOperator option:selected").data("spkey");
    var servicetype = $("#ddlLandlineOperator option:selected").data("servicetype");
    var provider = $("#ddlLandlineOperator option:selected").val();
    var providername = $("#ddlLandlineOperator option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlLandlineOperator")) return !1;
    if (CheckFocusBlankValidation("txtLandlineNumber")) return !1;

    var number = $("#txtLandlineNumber").val();

    var thisbutton = $("#btnFetchLandlineBill");
    if (confirm("Are you sure want to fetch landline bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Landline", "modellandlineheadbody", "landlinemodelclickclass", "btnFetchLandlineBill", "Get Landline Bill", false);
    }
    else {
        $(thisbutton).html("Get Landline Bill");
    }
}

function A2Z_LandlinePaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlLandlineOperator option:selected").data("spkey");
    var servicetype = $("#ddlLandlineOperator option:selected").data("servicetype");
    var provider = $("#ddlLandlineOperator option:selected").val();
    var providername = $("#ddlLandlineOperator option:selected").text();
    var number = $("#txtLandlineNumber").val();
    var amount = $("#hdnTotalA2Z_LandlinePaidAmt").val();
    var customername = $("#hdnTotalA2Z_LandlinePaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay landline bill of " + number + " number?")) {
            $("#btnA2Z_LandlinePayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Landline", "modellandlineheadbody", customername, "ddlLandlineOperator", "txtLandlineNumber");
        }
    }
}

//============================Start Insurance Section=====================================
function FetchInsurancePremium() {
    var spkey = $("#ddlInsurance option:selected").data("spkey");
    var servicetype = $("#ddlInsurance option:selected").data("servicetype");
    var provider = $("#ddlInsurance option:selected").val();
    var providername = $("#ddlInsurance option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlInsurance")) return !1;
    if (CheckFocusBlankValidation("txtInsuranceNumber")) return !1;

    var number = $("#txtInsuranceNumber").val();

    var thisbutton = $("#btnInsurancePremium");
    if (confirm("Are you sure want to fetch insurance bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Insurance", "modelinsuranceheadcontent", "insurancemodelclickclass", "btnInsurancePremium", "Get Premium Bill", false);
    }
    else {
        $(thisbutton).html("Get Premium Bill");
    }
}

function A2Z_InsurancePaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlInsurance option:selected").data("spkey");
    var servicetype = $("#ddlInsurance option:selected").data("servicetype");
    var provider = $("#ddlInsurance option:selected").val();
    var providername = $("#ddlInsurance option:selected").text();
    var number = $("#txtInsuranceNumber").val();
    var amount = $("#hdnTotalA2Z_InsurancePaidAmt").val();
    var customername = $("#hdnTotalA2Z_InsurancePaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay insurance bill of " + number + " number?")) {
            $("#btnA2Z_InsurancePayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Insurance", "modelinsuranceheadcontent", customername, "ddlInsurance", "txtInsuranceNumber");
        }
    }
}

//============================Start Gas Section=====================================
function FeatchGasBill() {
    var spkey = $("#ddlGasProvider option:selected").data("spkey");
    var servicetype = $("#ddlGasProvider option:selected").data("servicetype");
    var provider = $("#ddlGasProvider option:selected").val();
    var providername = $("#ddlGasProvider option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlGasProvider")) return !1;
    if (CheckFocusBlankValidation("txtGasNumber")) return !1;

    var number = $("#txtGasNumber").val();

    var thisbutton = $("#btnFetchGas");
    if (confirm("Are you sure want to fetch gas bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Gas", "modelgasheadcontent", "gasmodelclickclass", "btnFetchGas", "Get Gas Bill", false);
    }
    else {
        $(thisbutton).html("Get Gas Bill");
    }
}

function A2Z_GasPaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlGasProvider option:selected").data("spkey");
    var servicetype = $("#ddlGasProvider option:selected").data("servicetype");
    var provider = $("#ddlGasProvider option:selected").val();
    var providername = $("#ddlGasProvider option:selected").text();
    var number = $("#txtGasNumber").val();
    var amount = $("#hdnTotalA2Z_GasPaidAmt").val();
    var customername = $("#hdnTotalA2Z_GasPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay gas bill of " + number + " number?")) {
            $("#btnA2Z_GasPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Gas", "modelgasheadcontent", customername, "ddlGasProvider", "txtGasNumber");
        }
    }
}

//============================Start Water Section=====================================
function FetchWaterBill() {
    var spkey = $("#ddlWaterBoard option:selected").data("spkey");
    var servicetype = $("#ddlWaterBoard option:selected").data("servicetype");
    var provider = $("#ddlWaterBoard option:selected").val();
    var providername = $("#ddlWaterBoard option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlWaterBoard")) return !1;
    if (CheckFocusBlankValidation("txtWaterNumber")) return !1;

    var number = $("#txtWaterNumber").val();

    var thisbutton = $("#btnFetchWaterBill");
    if (confirm("Are you sure want to fetch water bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Water", "modelwaterheadbody", "watermodelclickclass", "btnFetchWaterBill", "Get Water Bill", false);
    }
    else {
        $(thisbutton).html("Get Water Bill");
    }
}

function A2Z_WaterPaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlWaterBoard option:selected").data("spkey");
    var servicetype = $("#ddlWaterBoard option:selected").data("servicetype");
    var provider = $("#ddlWaterBoard option:selected").val();
    var providername = $("#ddlWaterBoard option:selected").text();
    var number = $("#txtWaterNumber").val();
    var amount = $("#hdnTotalA2Z_WaterPaidAmt").val();
    var customername = $("#hdnTotalA2Z_WaterPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay water bill of " + number + " number?")) {
            $("#btnA2Z_WaterPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Water", "modelwaterheadbody", customername, "ddlWaterBoard", "txtWaterNumber");
        }
    }
}

//============================Start Water Section=====================================
function FetchWaterBill() {
    var spkey = $("#ddlWaterBoard option:selected").data("spkey");
    var servicetype = $("#ddlWaterBoard option:selected").data("servicetype");
    var provider = $("#ddlWaterBoard option:selected").val();
    var providername = $("#ddlWaterBoard option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlWaterBoard")) return !1;
    if (CheckFocusBlankValidation("txtWaterNumber")) return !1;

    var number = $("#txtWaterNumber").val();

    var thisbutton = $("#btnFetchWaterBill");
    if (confirm("Are you sure want to fetch water bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_Water", "modelwaterheadbody", "watermodelclickclass", "btnFetchWaterBill", "Get Water Bill", false);
    }
    else {
        $(thisbutton).html("Get Water Bill");
    }
}

function A2Z_WaterPaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlWaterBoard option:selected").data("spkey");
    var servicetype = $("#ddlWaterBoard option:selected").data("servicetype");
    var provider = $("#ddlWaterBoard option:selected").val();
    var providername = $("#ddlWaterBoard option:selected").text();
    var number = $("#txtWaterNumber").val();
    var amount = $("#hdnTotalA2Z_WaterPaidAmt").val();
    var customername = $("#hdnTotalA2Z_WaterPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay water bill of " + number + " number?")) {
            $("#btnA2Z_WaterPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Water", "modelwaterheadbody", customername, "ddlWaterBoard", "txtWaterNumber");
        }
    }
}

//============================Start Loan RePayment Section=====================================
function FeatchEMIBill() {
    var spkey = $("#ddlEMI option:selected").data("spkey");
    var servicetype = $("#ddlEMI option:selected").data("servicetype");
    var provider = $("#ddlEMI option:selected").val();
    var providername = $("#ddlEMI option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlEMI")) return !1;
    if (CheckFocusBlankValidation("txtEmiNumber")) return !1;

    var number = $("#txtEmiNumber").val();

    var thisbutton = $("#btnFetchEMIBill");
    if (confirm("Are you sure want to fetch lone repayment bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_EMI", "modelemiheadbody", "emimodelclickclass", "btnFetchEMIBill", "Get Loan RePayment Bill", true);
    }
    else {
        $(thisbutton).html("Get Loan RePayment Bill");
    }
}

function A2Z_EMIPaymentSubmit() {
    if (CheckFocusBlankValidation("txtPartA2Z_EMIAmount")) return !1;

    var spkey = $("#ddlEMI option:selected").data("spkey");
    var servicetype = $("#ddlEMI option:selected").data("servicetype");
    var provider = $("#ddlEMI option:selected").val();
    var providername = $("#ddlEMI option:selected").text();
    var number = $("#txtEmiNumber").val();
    var amount = $("#txtPartA2Z_EMIAmount").val();
    if (amount == "") {
        amount = $("#hdnTotalA2Z_EMIPaidAmt").val();
        $("#txtPartA2Z_EMIAmount").val(amount);
    }
    var customername = $("#hdnTotalA2Z_EMIPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay lone repayment bill of " + number + " number?")) {
            $("#btnA2Z_EMIPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "Lone Repayment", "modelemiheadbody", customername, "ddlEMI", "txtEmiNumber");
        }
    }
}

//============================Start Municipal Taxes Section=====================================
function FetchMunicipalTaxesPremium() {
    var spkey = $("#ddlMunicipalTaxes option:selected").data("spkey");
    var servicetype = $("#ddlMunicipalTaxes option:selected").data("servicetype");
    var provider = $("#ddlMunicipalTaxes option:selected").val();
    var providername = $("#ddlMunicipalTaxes option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlMunicipalTaxes")) return !1;
    if (CheckFocusBlankValidation("txtMunicipalTaxesNumber")) return !1;

    var number = $("#txtMunicipalTaxesNumber").val();

    var thisbutton = $("#btnMunicipalTaxes");
    if (confirm("Are you sure want to fetch municipal taxes of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_MTax", "modeltaxheadcontent", "taxmodelclickclass", "btnMunicipalTaxes", "Get Taxes Bill", false);
    }
    else {
        $(thisbutton).html("Get Taxes Bill");
    }
}

function A2Z_MTaxPaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlMunicipalTaxes option:selected").data("spkey");
    var servicetype = $("#ddlMunicipalTaxes option:selected").data("servicetype");
    var provider = $("#ddlMunicipalTaxes option:selected").val();
    var providername = $("#ddlMunicipalTaxes option:selected").text();
    var number = $("#txtMunicipalTaxesNumber").val();
    var amount = $("#hdnTotalA2Z_MTaxPaidAmt").val();
    var customername = $("#hdnTotalA2Z_MTaxPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay municipal taxes bill of " + number + " number?")) {
            $("#btnA2Z_MTaxPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "MTax", "modeltaxheadcontent", customername, "ddlMunicipalTaxes", "txtMunicipalTaxesNumber");
        }
    }
}

//============================Start Fast Tag Section=====================================
function FeatchFastTagBill() {
    var spkey = $("#ddlFastTag option:selected").data("spkey");
    var servicetype = $("#ddlFastTag option:selected").data("servicetype");
    var provider = $("#ddlFastTag option:selected").val();
    var providername = $("#ddlFastTag option:selected").text();

    if (CheckFocusDropDownBlankValidation("ddlFastTag")) return !1;
    if (CheckFocusBlankValidation("txtFastTagNumber")) return !1;

    var number = $("#txtFastTagNumber").val();

    var thisbutton = $("#btnFastTag");
    if (confirm("Are you sure want to fetch fast tag bill of " + number + " number?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
        FetchAllBillDetails(provider, providername, number, "A2Z_FastTag", "modelfasttagheadbody", "fasttagmodelclickclass", "btnFastTag", "Get Fast Tag Bill", false);
    }
    else {
        $(thisbutton).html("Get Fast Tag Bill");
    }
}

function A2Z_FastTagPaymentSubmit() {
    //if (CheckFocusBlankValidation("txtPartA2Z_ElectricityAmount")) return !1;

    var spkey = $("#ddlFastTag option:selected").data("spkey");
    var servicetype = $("#ddlFastTag option:selected").data("servicetype");
    var provider = $("#ddlFastTag option:selected").val();
    var providername = $("#ddlFastTag option:selected").text();
    var number = $("#txtFastTagNumber").val();
    var amount = $("#hdnTotalA2Z_FastTagPaidAmt").val();
    var customername = $("#hdnTotalA2Z_FastTagPaidAmt").data("customername");
    if (parseFloat(amount) > 0) {
        if (confirm("Are you sure want to pay fast tag bill of " + number + " number?")) {
            $("#btnA2Z_FastTagPayment").html("Processing... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
            PayFetchedBillPayment(number, provider, amount, providername, spkey, "A2Z_FastTag", "modelfasttagheadbody", customername, "ddlFastTag", "txtFastTagNumber");
        }
    }
}


//=============================Common CLass============================================
function FetchAllBillDetails(provider, providername, number, actiontype, contentbodyid, clickclass, spinnerbuttonid, buttonmsg, istxtshow) {
    $("#" + contentbodyid).html("");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/BillPayment.aspx/FetchAllBillDetails",
        data: '{number: ' + JSON.stringify(number) + ',provider: ' + JSON.stringify(provider) + ',providername: ' + JSON.stringify(providername) + ',actiontype: ' + JSON.stringify(actiontype) + ',istxtshow: ' + JSON.stringify(istxtshow) + '}',
        datatype: "json",
        success: function (data) {
            $("#" + contentbodyid).html(data.d[1]);
            $("." + clickclass).click();
            $("#" + spinnerbuttonid).html(buttonmsg).prop('disabled', false);
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function PayFetchedBillPayment(number, provider, amount, providername, spkey, actiontype, contentbodyid, customername, ddldropdown, textid) {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/BillPayment.aspx/PayFetchedBillPayment",
        data: '{number: ' + JSON.stringify(number) + ',provider: ' + JSON.stringify(provider) + ',amount: ' + JSON.stringify(amount) + ',providername: ' + JSON.stringify(providername) + ',spkey: ' + JSON.stringify(spkey) + ',actiontype: ' + JSON.stringify(actiontype) + ',customername: ' + JSON.stringify(customername) + '}',
        datatype: "json",
        success: function (data) {
            $("#" + contentbodyid).html("");
            $("#" + contentbodyid).html(data.d[1]);
            ResetFields(ddldropdown, textid);
            BindCreditAmount();
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function A2ZBindBillTransHistory(defaultdate) {
    var fromdate = $("#txtBillFromDate").val();
    var todate = $("#txtBillToDate").val();
    var clintrefid = $("#txtClientRefID").val();
    var transtype = $("#ddlBillTransType option:selected").val();
    var status = $("#ddlBillTransStatus option:selected").val();

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/BillPayment.aspx/GetBillTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',clintrefid: ' + JSON.stringify(clintrefid) + ',transtype: ' + JSON.stringify(transtype) + ',status: ' + JSON.stringify(status) + ',defaultdate: ' + JSON.stringify(defaultdate) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#BillTransRowDetails").html("");
                    $("#BillTransRowDetails").html(data.d[1]);
                }
                $("#btnBillSearchSubmit").html("Search").prop('disabled', false);
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function ReBindFilter() {
    $("#txtBillFromDate").val('');
    $("#txtBillToDate").val('');
    $("#txtClientRefID").val('');
    $("#ddlBillTransType").prop('selectedIndex', '').change();
    $("#ddlBillTransStatus").prop('selectedIndex', '').change();
}

function BillSearchSubmit() {
    $("#btnBillSearchSubmit").html("Searching... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
    A2ZBindBillTransHistory('filter');
}

function BillSearchClear() {
    $("#btnTransFilter").html("Search");
    $("#txtBillFromDate").val('');
    $("#txtBillToDate").val('');
    $("#txtClientRefID").val('');
    $("#ddlBillTransType").prop('selectedIndex', '').change();
    $("#ddlBillTransStatus").prop('selectedIndex', '').change();
    A2ZBindBillTransHistory('today');
}

function ResetFields(ddldropdown, textid) {
    $("#" + ddldropdown).val("0").change();
    $("#" + textid).val("");
}