﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="BillPayment.aspx.cs" Inherits="BillPayment" %>

<%--<%@ Register Src="~/UserControl/Recharge/ud_Mobile_Recharge.ascx" TagPrefix="uc1" TagName="ud_Mobile_Recharge" %>--%>
<%@ Register Src="~/UserControl/Recharge/ud_DTH_Recharge.ascx" TagPrefix="uc1" TagName="ud_DTH_Recharge" %>
<%@ Register Src="~/UserControl/Recharge/ud_Electricity_Recharge.ascx" TagPrefix="uc1" TagName="ud_Electricity_Recharge" %>
<%@ Register Src="~/UserControl/Recharge/ud_landline.ascx" TagPrefix="uc1" TagName="ud_landline" %>
<%@ Register Src="~/UserControl/Recharge/ud_Insurance.ascx" TagPrefix="uc1" TagName="ud_Insurance" %>
<%@ Register Src="~/UserControl/Recharge/ud_GAS.ascx" TagPrefix="uc1" TagName="ud_GAS" %>
<%--<%@ Register Src="~/UserControl/Recharge/ud_internet_isp.ascx" TagPrefix="uc1" TagName="ud_internet_isp" %>--%>
<%@ Register Src="~/UserControl/Recharge/ud_water.ascx" TagPrefix="uc1" TagName="ud_water" %>
<%@ Register Src="~/UserControl/Recharge/uc_smbp_transhistory.ascx" TagPrefix="uc1" TagName="uc_smbp_transhistory" %>
<%@ Register Src="~/UserControl/Recharge/FastTag.ascx" TagPrefix="uc1" TagName="FastTag" %>
<%@ Register Src="~/UserControl/Recharge/EmiLoan.ascx" TagPrefix="uc1" TagName="EmiLoan" %>
<%@ Register Src="~/UserControl/Recharge/A2ZRecharge.ascx" TagPrefix="uc1" TagName="A2ZRecharge" %>
<%@ Register Src="~/Utilities/User_Control/A2ZTaxes.ascx" TagPrefix="uc1" TagName="A2ZTaxes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="Utilities/css/validationcss.css" rel="stylesheet" />
    <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <style type="text/css">
        a.ui-datepicker-prev.ui-corner-all {
            background: #fff;
        }

        a.ui-datepicker-next.ui-corner-all {
            background: #fff;
        }

            a.ui-datepicker-next.ui-corner-all.ui-state-disabled {
                background: none;
            }

        .r-c {
            display: inline-block;
            color: #c2c2c2;
            text-align: center;
            font-size: 15px;
            cursor: pointer;
        }


        table {
            background: #fff !important;
        }

        label {
            color: #fff;
        }

        .panel {
            margin-bottom: 20px;
            /* background-color: #fff; */
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            background: linear-gradient(to right, rgb(66, 39, 90), rgb(115, 75, 109)) !important;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #f5f5f500 !important;
            border-color: #ddd;
        }

        .r-c1 {
            margin-top: 4px;
            font-size: 12px;
            color: #080808;
            opacity: .5;
            display: block;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }

        .panel.with-nav-tabs .panel-heading {
            padding: 5px 5px 0 5px;
        }

        .panel.with-nav-tabs .nav-tabs {
            border-bottom: none;
        }

        .panel.with-nav-tabs .nav-justified {
            margin-bottom: -1px;
        }
        /********************************************************************/
        /*** PANEL DEFAULT ***/
        .with-nav-tabs.panel-default .nav-tabs > li > a, .with-nav-tabs.panel-default .nav-tabs > li > a:hover, .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
        }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .r-c1 {
                color: #fff !important;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-smart-phone {
                color: #00d0ff;
            }


            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-satellite {
                color: #c63bff;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-light-bulb {
                color: #fbff08;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-telephone {
                color: #ff08b3;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-heart-beat-alt {
                color: red;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-fire-burn {
                color: #ff9108;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-ui-wifi {
                color: #00dc11;
            }

            .with-nav-tabs.panel-default .nav-tabs > li > a > .icofont-water-drop {
                color: #0898ff;
            }



            .with-nav-tabs.panel-default .nav-tabs > .open > a, .with-nav-tabs.panel-default .nav-tabs > .open > a:hover, .with-nav-tabs.panel-default .nav-tabs > .open > a:focus, .with-nav-tabs.panel-default .nav-tabs > li > a:hover, .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
                color: #777;
                background-color: #00000026;
                border-color: transparent;
            }

        .with-nav-tabs.panel-default .nav-tabs > li.active > a, .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
            color: #555;
            background-color: #fff0;
            border-color: #ddd0;
            border-radius: 4px;
            margin-bottom: 6px;
        }


            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-smart-phone, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-smart-phone:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-smart-phone:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-satellite, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-satellite:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-satellite:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }


            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-light-bulb, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-light-bulb:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-light-bulb:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-telephone, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-telephone:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-telephone:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }


            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-heart-beat-alt, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-heart-beat-alt:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-heart-beat-alt:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-fire-burn, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-fire-burn:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-fire-burn:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-ui-wifi, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-ui-wifi:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-ui-wifi:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }

            .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-water-drop, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-water-drop:hover, .with-nav-tabs.panel-default .nav-tabs > li.active > a > .icofont-water-drop:focus {
                color: #fff !important;
                background-color: #fff0;
                border-color: #ddd0;
                border-radius: 4px;
                margin-bottom: 6px;
            }


        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f5f5f5;
            border-color: #ddd;
        }

            .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #777;
            }

                .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #ddd;
                }

            .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff;
                background-color: #555;
            }
        /********************************************************************/
        /*** PANEL PRIMARY ***/
        .with-nav-tabs.panel-primary .nav-tabs > li > a, .with-nav-tabs.panel-primary .nav-tabs > li > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
        }

            .with-nav-tabs.panel-primary .nav-tabs > .open > a, .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover, .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus, .with-nav-tabs.panel-primary .nav-tabs > li > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
                color: #fff;
                background-color: #3071a9;
                border-color: transparent;
            }

        .with-nav-tabs.panel-primary .nav-tabs > li.active > a, .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #428bca;
            border-color: #3071a9;
        }

            .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #fff;
            }

                .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #3071a9;
                }

            .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                background-color: #4a9fe9;
            }
        /********************************************************************/
        /*** PANEL SUCCESS ***/
        .with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
            color: #3c763d;
        }

            .with-nav-tabs.panel-success .nav-tabs > .open > a, .with-nav-tabs.panel-success .nav-tabs > .open > a:hover, .with-nav-tabs.panel-success .nav-tabs > .open > a:focus, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
                color: #3c763d;
                background-color: #d6e9c6;
                border-color: transparent;
            }

        .with-nav-tabs.panel-success .nav-tabs > li.active > a, .with-nav-tabs.panel-success .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
            color: #3c763d;
            background-color: #fff;
            border-color: #d6e9c6;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }

            .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #3c763d;
            }

                .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #d6e9c6;
                }

            .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff;
                background-color: #3c763d;
            }
        /********************************************************************/
        /*** PANEL INFO ***/
        .with-nav-tabs.panel-info .nav-tabs > li > a, .with-nav-tabs.panel-info .nav-tabs > li > a:hover, .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
            color: #31708f;
        }

            .with-nav-tabs.panel-info .nav-tabs > .open > a, .with-nav-tabs.panel-info .nav-tabs > .open > a:hover, .with-nav-tabs.panel-info .nav-tabs > .open > a:focus, .with-nav-tabs.panel-info .nav-tabs > li > a:hover, .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
                color: #31708f;
                background-color: #bce8f1;
                border-color: transparent;
            }

        .with-nav-tabs.panel-info .nav-tabs > li.active > a, .with-nav-tabs.panel-info .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
            color: #31708f;
            background-color: #fff;
            border-color: #bce8f1;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #31708f;
            }

                .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #bce8f1;
                }

            .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff;
                background-color: #31708f;
            }
        /********************************************************************/
        /*** PANEL WARNING ***/
        .with-nav-tabs.panel-warning .nav-tabs > li > a, .with-nav-tabs.panel-warning .nav-tabs > li > a:hover, .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
            color: #8a6d3b;
        }

            .with-nav-tabs.panel-warning .nav-tabs > .open > a, .with-nav-tabs.panel-warning .nav-tabs > .open > a:hover, .with-nav-tabs.panel-warning .nav-tabs > .open > a:focus, .with-nav-tabs.panel-warning .nav-tabs > li > a:hover, .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
                color: #8a6d3b;
                background-color: #faebcc;
                border-color: transparent;
            }

        .with-nav-tabs.panel-warning .nav-tabs > li.active > a, .with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
            color: #8a6d3b;
            background-color: #fff;
            border-color: #faebcc;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #fcf8e3;
            border-color: #faebcc;
        }

            .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #8a6d3b;
            }

                .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #faebcc;
                }

            .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff;
                background-color: #8a6d3b;
            }
        /********************************************************************/
        /*** PANEL DANGER ***/
        .with-nav-tabs.panel-danger .nav-tabs > li > a, .with-nav-tabs.panel-danger .nav-tabs > li > a:hover, .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
            color: #a94442;
        }

            .with-nav-tabs.panel-danger .nav-tabs > .open > a, .with-nav-tabs.panel-danger .nav-tabs > .open > a:hover, .with-nav-tabs.panel-danger .nav-tabs > .open > a:focus, .with-nav-tabs.panel-danger .nav-tabs > li > a:hover, .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
                color: #a94442;
                background-color: #ebccd1;
                border-color: transparent;
            }

        .with-nav-tabs.panel-danger .nav-tabs > li.active > a, .with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover, .with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
            color: #a94442;
            background-color: #fff;
            border-color: #ebccd1;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f2dede; /* bg color */
            border-color: #ebccd1; /* border color */
        }

            .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
                color: #a94442; /* normal text color */
            }

                .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover, .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
                    background-color: #ebccd1; /* hover bg color */
                }

            .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a, .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover, .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
                color: #fff; /* active text color */
                background-color: #a94442; /* active bg color */
            }

        @media (min-width: 992px) {
            .modal-lg {
                width: 700px !important;
            }
        }
    </style>
    <br />
    <div class="col-md-12">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li class="active"><a class="r-c actiontype" style="background: rgb(255 126 0 / 60%);" data-valtext="mobile" href="#tab1default" data-toggle="tab"><i class="icofont-smart-phone icofont-2x"></i><span class="r-c1">Prepaid/Postpaid</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="dth" href="#tab2default" data-toggle="tab"><i class="icofont-satellite icofont-2x"></i><span class="r-c1">DTH</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="electricity" href="#tab3default" data-toggle="tab"><i class="icofont-light-bulb icofont-2x"></i><span class="r-c1">Electricity</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="landline" href="#tab4default" data-toggle="tab"><i class="icofont-telephone icofont-2x"></i><span class="r-c1">Landline</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="insurance" href="#tab5default" data-toggle="tab"><i class="icofont-heart-beat-alt icofont-2x"></i><span class="r-c1">Insurance</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="gas" href="#tab6default" data-toggle="tab"><i class="icofont-fire-burn icofont-2x"></i><span class="r-c1">Gas</span></a></li>
                    <%--<li><a class="r-c actiontype" data-valtext="internet" href="#tab7default" data-toggle="tab">
                        <i class="icofont-ui-wifi icofont-2x"></i><span class="r-c1">Internet</span></a></li>--%>
                    <li><a class="r-c actiontype" data-valtext="water" href="#tab8default" data-toggle="tab"><i class="icofont-water-drop icofont-2x"></i><span class="r-c1">Water</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="fastag" href="#tabfastag" data-toggle="tab"><i class="icofont-car icofont-2x" style="color: #ff9108;"></i><span class="r-c1">Fast Tag</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="emi" href="#tabemi" data-toggle="tab"><i class="icofont-money icofont-2x" style="color: #0898ff;"></i><span class="r-c1">Loan RePayment</span></a></li>
                    <li><a class="r-c actiontype" data-valtext="taxes" href="#tabtaxes" data-toggle="tab"><i class="icofont-list icofont-2x" style="color: red;"></i><span class="r-c1">Municipal Taxes</span></a></li>
                    <li style="float: right!important;"><a class="r-c actiontype" data-valtext="billtranshistory" id="transHistTab-Pills" href="#tab9default" data-toggle="tab"><i class="icofont-eye-open icofont-2x" style="color: red;"></i><span class="r-c1">Transaction History</span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1default">
                        <%--<uc1:ud_Mobile_Recharge runat="server" ID="ud_Mobile_Recharge" />--%>
                        <uc1:A2ZRecharge runat="server" ID="A2ZRecharge" />
                    </div>
                    <div class="tab-pane fade" id="tab2default">
                        <uc1:ud_DTH_Recharge runat="server" ID="ud_DTH_Recharge" />
                    </div>
                    <div class="tab-pane fade" id="tab3default">
                        <uc1:ud_Electricity_Recharge runat="server" ID="ud_Electricity_Recharge" />
                    </div>
                    <div class="tab-pane fade" id="tab4default">
                        <uc1:ud_landline runat="server" ID="ud_landline" />
                    </div>
                    <div class="tab-pane fade" id="tab5default">
                        <uc1:ud_Insurance runat="server" ID="ud_Insurance" />
                    </div>
                    <div class="tab-pane fade" id="tab6default">
                        <uc1:ud_GAS runat="server" ID="ud_GAS" />
                    </div>
                    <%--<div class="tab-pane fade" id="tab7default">
                        <uc1:ud_internet_isp runat="server" ID="ud_internet_isp" />
                    </div>--%>
                    <div class="tab-pane fade" id="tab8default">
                        <uc1:ud_water runat="server" ID="ud_water" />
                    </div>
                    <div class="tab-pane fade" id="tab9default">
                        <uc1:uc_smbp_transhistory runat="server" ID="uc_smbp_transhistory" />
                    </div>
                    <div class="tab-pane fade" id="tabfastag">
                        <uc1:FastTag runat="server" ID="FastTag" />
                    </div>
                    <div class="tab-pane fade" id="tabemi">
                        <uc1:EmiLoan runat="server" ID="EmiLoan" />
                    </div>
                    <div class="tab-pane fade" id="tabtaxes">
                        <uc1:A2ZTaxes runat="server" ID="A2ZTaxes" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-info btn-lg successmessage hidden" data-backdrop="static"
        data-keyboard="false" data-toggle="modal" data-target="#SuccessMsg">
    </button>
    <div class="modal fade" id="SuccessMsg" role="dialog">
        <div class="modal-dialog modal-lg" style="margin: 15% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-header" style="padding: 12px; background: none!important;">
                    <h5 class="modal-title modelheading"></h5>
                </div>
                <div class="modal-body" style="padding-left: 20px!important;">
                    <h5 class="sucessmsg"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;"
                        data-dismiss="modal">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="container" style="display: none;">
        <div class="col-xl-6">
            <section class="mx-2 pb-3">
      
              <ul class="nav with-nav-tabs md-tabs" id="myTabMD" role="tablist">
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link active" id="mobile-tab-md" data-toggle="tab" href="#mobileTabPills" role="tab" aria-controls="mobileTabPills" aria-selected="true"><i class="icofont-smart-phone icofont-2x"></i></a><span class="">Prepaid/Postpaid</span>
                </li>
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="dth-tab-md" data-toggle="tab" href="#dthTabPills" role="tab" aria-controls="dthTabPills" aria-selected="false"><i class="icofont-satellite icofont-2x"></i></a><span class="">DTH</span>
                </li>
                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="electric-tab-md" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false"><i class="icofont-light-bulb icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="landline-tab-md" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false"><i class="icofont-telephone icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="insurance-tab-md" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false"><i class="icofont-heart-beat-alt icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="gas-tab-md" data-toggle="tab" href="#gasTabPills" role="tab" aria-controls="gasTabPills" aria-selected="false"><i class="icofont-fire-burn icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="internet-tab-md" data-toggle="tab" href="#internetTabPills" role="tab" aria-controls="internetTabPills" aria-selected="false"><i class="icofont-ui-wifi icofont-2x"></i></a>
                </li>

                       <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="water-tab-md" data-toggle="tab" href="#waterTabPills" role="tab" aria-controls="waterTabPills" aria-selected="false"><i class="icofont-water-drop icofont-2x"></i></a>
                </li>

                  
              </ul>


              <div class="tab-content card pt-5" id="myTabContentMD">  
               
               
                    <div class="tab-pane  active" id="mobileTabPills" role="tabpanel" aria-labelledby="mobile-tab-md">
                
            </div>

               
                
                   <div class="tab-pane fade" id="dthTabPills" role="tabpanel" aria-labelledby="dth-tab-md"></div>
             
            <div class="tab-pane fade" id="thirdTabPills" role="tabpanel" aria-labelledby="electric-tab-md"></div>
               <div class="tab-pane fade" id="fourthTabPills" role="tabpanel" aria-labelledby="landline-tab-md"></div>                
                       <div class="tab-pane fade" id="fifthTabPills" role="tabpanel" aria-labelledby="insurance-tab-md"></div>
                       <div class="tab-pane fade" id="gasTabPills" role="tabpanel" aria-labelledby="gas-tab-md"></div>
                      <div class="tab-pane fade" id="internetTabPills" role="tabpanel" aria-labelledby="internet-tab-md"></div>                 
                       <div class="tab-pane fade" id="waterTabPills" role="tabpanel" aria-labelledby="water-tab-md"></div>     
                  <div class="tab-pane fade" id="contact-md" role="tabpanel" aria-labelledby="contact-tab-md"></div>
                       <div class="tab-pane fade" id="transHistTabPills" role="tabpanel" aria-labelledby="transHistTab-Pills"></div>
              </div>      
            </section>
        </div>
    </div>--%>
    <%--<script type="text/javascript" src="Utilities/js/billpayment.js?v=2.4"></script>--%>
    <script type="text/javascript" src="Utilities/js/A2ZRecharge.js?v=1.2"></script>
    <script type="text/javascript" src="Utilities/js/common.js"></script>
    <script type="text/javascript">
        $(document.body).on('click', ".commonDate", function (e) {
            $(this).datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true
            });
        });

        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>
