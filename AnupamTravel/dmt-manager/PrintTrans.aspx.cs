﻿using SWIFTMoneyBillPayments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DMT_Manager_PrintTrans : System.Web.UI.Page
{
    public static string HtmlContent { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["agentid"] != null)
        {
            Session["UID"] = Request.QueryString["agentid"] != null ? Request.QueryString["agentid"].ToString() : null;
        }

        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            string UserId = Session["UID"].ToString();//userid, mobile, beneficaryid, trackid
            hdnUserId.Value = UserId;

            string Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            hdnMobile.Value = Mobile;
            string BeneficaryId = Request.QueryString["beneficaryid"] != null ? Request.QueryString["beneficaryid"].ToString() : string.Empty;
            hdnBeneficaryid.Value = BeneficaryId;
            string TrackId = Request.QueryString["trackid"] != null ? Request.QueryString["trackid"].ToString() : string.Empty;
            hdnTrackid.Value = TrackId;

            if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(BeneficaryId) && string.IsNullOrEmpty(TrackId))
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                HtmlContent = BindTransPrint(UserId, Mobile, BeneficaryId, TrackId);
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }

    public string BindTransPrint(string userId, string mobile, string beneficaryId, string trackId)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(beneficaryId) && !string.IsNullOrEmpty(trackId))
        {
            DataTable dtAgency = A2ZService.GetAgencyDetailById(userId);
            string agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
            string agencyAddress = dtAgency.Rows[0]["Address"].ToString() + ", " + dtAgency.Rows[0]["City"].ToString() + ", " + dtAgency.Rows[0]["State"].ToString() + ", " + dtAgency.Rows[0]["Country"].ToString() + ", " + dtAgency.Rows[0]["zipcode"].ToString();
            string agencyMobile = dtAgency.Rows[0]["Mobile"].ToString();
            string agencyEmail = dtAgency.Rows[0]["Email"].ToString();

            sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;' class='main'>");
            sbResult.Append("<div class='col-sm-12'>");
            sbResult.Append("<a href='#'><img src='../Advance_CSS/Icons/logo(ft).png' style='max-width: 200px;' /></a>");
            sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
            sbResult.Append("<span style='font-size: 20px;'>" + agencyName + "</span><br />");
            sbResult.Append("<span>" + agencyAddress + "</span><br />");
            sbResult.Append("<span>Mobile: " + agencyMobile + "</span><br />");
            sbResult.Append("<span>Email: " + agencyEmail + "</span>");
            sbResult.Append("</span>");
            sbResult.Append("</div>");
            sbResult.Append("<br/>");
            sbResult.Append("<div class='col-sm-12'>");
            sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
            sbResult.Append("<h4 class='heading'>Customer Transaction Receipt <div class='sendmail form-validation'><input type='text' placeholder='enter email id' id='ReceiptSendMail' style='width: 200px;' /> <span style='cursor:pointer;' id='btnMailSend'>Send Mail</span></div></h4>");
            sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

            DataTable dtSender = new DataTable();
            DataTable dtBeny = DMT_Service.GetFundTransferVeryficationDetail(userId, beneficaryId, mobile, ref dtSender);

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtSender.Rows[0]["FirstName"].ToString() + " " + dtSender.Rows[0]["LastName"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtSender.Rows[0]["RegMobile"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["bene_Name"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["accountNumber"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["ifscCode"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["bankName"].ToString() + "</td>");
            sbResult.Append("</tr>");

            DataTable dtTrans = DMT_Service.Get_TransactionDetail(userId, mobile, trackId);

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Trans ID</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["txnId"].ToString() + "</td>");
            sbResult.Append("</tr>");

            //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
            //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
            //sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["amount"].ToString() + "</td>");
            sbResult.Append("</tr>");

            //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
            //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
            //sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["updatedDate"].ToString()) + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>Transaction " + dtTrans.Rows[0]["status"].ToString() + "</td>");
            sbResult.Append("</tr>");


            sbResult.Append("</table>");
            sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
            //sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
            //sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
            sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. In case the Agent charges the Customer in excess of the fee/charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. (+91)-9163124365 or email us at info@anupamtravelonline.com .</p><br />");
            sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.anupamtravelonline.com  and shall be binding on the Customer for each transaction.</p><br /><br />");

            sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<tr>");
            sbResult.Append("<td><p>Date..............................</p></td>");
            sbResult.Append("<td><p style='float: right!important;'>Signature Of Customer's......................................................</p></td>");
            sbResult.Append("</tr>");
            sbResult.Append("</table>");
            sbResult.Append("<br /><br />");
            sbResult.Append("</div>");
            sbResult.Append("</div>");
        }

        return sbResult.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static string SendReceiptInMail(string emailid, string userid, string mobile, string beneficaryid, string trackid)
    {
        if (!string.IsNullOrEmpty(userid))
        {
            if (!string.IsNullOrEmpty(emailid.Trim()))
            {
                string mailBody = ReceiptSend(userid, mobile, beneficaryid, trackid);

                DataTable dtAgency = A2ZService.GetAgencyDetailById(userid);

                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    SqlTransactionDom STDOM = new SqlTransactionDom();
                    DataTable MailDt = new DataTable();
                    MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", userid).Tables[0];
                    if (MailDt != null && MailDt.Rows.Count > 0)
                    {
                        bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                        string subject = "Payment Transaction Receipt [Client Ref #" + trackid + "]";
                        if (Status)
                        {
                            int isSuccess = STDOM.SendMail(emailid, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailBody, subject, "");
                            if (isSuccess > 0)
                            {
                                return "sent";
                            }
                            else
                            {
                                return "failed";
                            }
                        }
                    }
                    else
                    {
                        return "failed";
                    }
                }
                else
                {
                    return "failed";
                }
            }
        }

        return string.Empty;
    }

    private static string ReceiptSend(string userId, string mobile, string beneficaryId, string trackId)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(beneficaryId) && !string.IsNullOrEmpty(trackId))
        {
            DataTable dtAgency = A2ZService.GetAgencyDetailById(userId);
            string agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
            string agencyAddress = dtAgency.Rows[0]["Address"].ToString() + ", " + dtAgency.Rows[0]["City"].ToString() + ", " + dtAgency.Rows[0]["State"].ToString() + ", " + dtAgency.Rows[0]["Country"].ToString() + ", " + dtAgency.Rows[0]["zipcode"].ToString();
            string agencyMobile = dtAgency.Rows[0]["Mobile"].ToString();
            string agencyEmail = dtAgency.Rows[0]["Email"].ToString();

            sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;' class='main'>");
            sbResult.Append("<div class='col-sm-12'>");
            sbResult.Append("<a href='#'><img src='../Advance_CSS/Icons/logo(ft).png' style='max-width: 200px;' /></a>");
            sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
            sbResult.Append("<span style='font-size: 20px;'>" + agencyName + "</span><br />");
            sbResult.Append("<span>" + agencyAddress + "</span><br />");
            sbResult.Append("<span>Mobile: " + agencyMobile + "</span><br />");
            sbResult.Append("<span>Email: " + agencyEmail + "</span>");
            sbResult.Append("</span>");
            sbResult.Append("</div>");
            sbResult.Append("<br/>");
            sbResult.Append("<div class='col-sm-12'>");
            sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
            sbResult.Append("<h4 class='heading'>Customer Transaction Receipt <div class='sendmail form-validation'><input type='text' placeholder='enter email id' id='ReceiptSendMail' style='width: 200px;' /> <span style='cursor:pointer;' id='btnMailSend'>Send Mail</span></div></h4>");
            sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

            DataTable dtSender = new DataTable();
            DataTable dtBeny = DMT_Service.GetFundTransferVeryficationDetail(userId, beneficaryId, mobile, ref dtSender);

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtSender.Rows[0]["FirstName"].ToString() + " " + dtSender.Rows[0]["LastName"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtSender.Rows[0]["RegMobile"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["bene_Name"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["accountNumber"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["ifscCode"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBeny.Rows[0]["bankName"].ToString() + "</td>");
            sbResult.Append("</tr>");

            DataTable dtTrans = DMT_Service.Get_TransactionDetail(userId, mobile, trackId);

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Trans ID</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["txnId"].ToString() + "</td>");
            sbResult.Append("</tr>");

            //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
            //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
            //sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["amount"].ToString() + "</td>");
            sbResult.Append("</tr>");

            //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
            //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
            //sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["updatedDate"].ToString()) + "</td>");
            sbResult.Append("</tr>");

            sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
            sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>Transaction " + dtTrans.Rows[0]["status"].ToString() + "</td>");
            sbResult.Append("</tr>");


            sbResult.Append("</table>");
            sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
            //sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
            //sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
            sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. In case the Agent charges the Customer in excess of the fee/charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. (+91)-9163124365 or email us at info@anupamtravelonline.com .</p><br />");
            sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.anupamtravelonline.com  and shall be binding on the Customer for each transaction.</p><br /><br />");

            sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");
            sbResult.Append("<tr>");
            sbResult.Append("<td><p>Date..............................</p></td>");
            sbResult.Append("<td><p style='float: right!important;'>Signature Of Customer's......................................................</p></td>");
            sbResult.Append("</tr>");
            sbResult.Append("</table>");
            sbResult.Append("<br /><br />");
            sbResult.Append("</div>");
            sbResult.Append("</div>");
        }

        return sbResult.ToString();
    }
}