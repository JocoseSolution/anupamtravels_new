﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SWIFTMoneyBillPayments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dmt_manager_sender_verification : System.Web.UI.Page
{
    private static string UserId { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
         {
             UserId = Session["UID"].ToString();
             Session["Validate"] = UserId;            
            // //InsertBankDetail();
         }
         else
         {
             Response.Redirect("~/Login.aspx");
         }
		//Response.Redirect("~/dmtpayment.aspx");
    }

    private void InsertBankDetail()
    {
        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["Validate"].ToString();
                bool is_success = DMT_Service.InsertBankDetail(UserId);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    [WebMethod]
    public static List<string> RemitterMobileSearch(string mobileno)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string msg = string.Empty;
                    string response = DMT_Service.Dmt_Mobile_Verification(UserId, mobileno, ref msg);
                    if (msg != "error")
                    {
                        if (!string.IsNullOrEmpty(response))
                        {
                            JObject jo_response = JObject.Parse(response);
                            var status = jo_response["status"];
                            var message = jo_response["message"];
                            if (status.ToString() == "13")
                            {
                                string mobile = jo_response["message"]["mobile"].ToString();
                                string fname = jo_response["message"]["fname"].ToString();
                                string lname = jo_response["message"]["lname"].ToString();
                                string rem_bal = jo_response["message"]["rem_bal"].ToString();
                                string verify = jo_response["message"]["verify"].ToString();

                                if (DMT_Service.Dmt_UpdateRemitterDetail(UserId, mobile, fname, lname, rem_bal, verify))
                                {
                                    HttpContext.Current.Session["GoToSenderPage"] = "yes";
                                    result.Add("redirect");
                                    result.Add("/dmt-manager/sender-details.aspx?mobile=" + mobile);
                                }
                            }
                            else if (status.ToString() == "11")
                            {
                                result.Add("notreg");
                                result.Add(message.ToString());
                            }
                            else if (status.ToString() == "12")
                            {
                                result.Add("regotpsent");
                                result.Add(message.ToString());
                            }
                            else
                            {
                                result.Add("error");
                                result.Add(message.ToString());
                            }
                        }
                    }
                    else
                    {
                        result.Add("error");
                        result.Add(msg);
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> Remitter_Registration(string fname, string lname, string mobile, string pincode, string location)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string msg = string.Empty;
                    string response = DMT_Service.Dmt_Remitter_Registration(UserId, fname, lname, mobile, 0, pincode, location, true, ref msg);
                    if (msg != "error")
                    {
                        if (!string.IsNullOrEmpty(response))
                        {
                            dynamic dyResult = JObject.Parse(response);
                            string status = dyResult.status;
                            string message = dyResult.message;
                            if (!string.IsNullOrEmpty(status))
                            {
                                if (status == "12")
                                {
                                    result.Add("otpsent");
                                    result.Add(message);
                                }
                                else
                                {
                                    result.Add("error");
                                    result.Add(message);
                                }
                            }
                        }
                    }
                    else
                    {
                        result.Add("error");
                        result.Add(msg);
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> ResendOtpToRemitterMobile(string fname, string lname, string mobile)
    {
        List<string> result = new List<string>();

        try
        {
            try
            {
                if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
                {
                    string UserId = HttpContext.Current.Session["Validate"].ToString();
                    if (!string.IsNullOrEmpty(UserId))
                    {
                        string msg = string.Empty;
                        string response = DMT_Service.Dmt_Remitter_Registration(UserId, fname, lname, mobile, 0, "", "", true, ref msg);
                        if (msg != "error")
                        {
                            if (!string.IsNullOrEmpty(response))
                            {
                                dynamic dyResult = JObject.Parse(response);
                                string status = dyResult.status;
                                string message = dyResult.message;
                                if (!string.IsNullOrEmpty(status))
                                {
                                    if (status == "12")
                                    {
                                        result.Add("otpsent");
                                        result.Add(message);
                                    }
                                    else
                                    {
                                        result.Add("error");
                                        result.Add(message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.Add("error");
                            result.Add(msg);
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> RemitterVarification(string mobile, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["Validate"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["Validate"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string msg = string.Empty;
                    string response = DMT_Service.Dmt_Remitter_OTP_Verification(UserId, mobile, otp, ref msg);
                    if (msg != "error")
                    {
                        if (!string.IsNullOrEmpty(response))
                        {
                            dynamic dyResult = JObject.Parse(response);
                            string status = dyResult.status;
                            if (!string.IsNullOrEmpty(status))
                            {
                                if (status == "17")
                                {
                                    string remsg = string.Empty;
                                    string recheck_response = DMT_Service.Dmt_ReCheck_Mobile_Verification(UserId, mobile, ref remsg);
                                    if (remsg == "success")
                                    {
                                        dynamic dyReResult = JObject.Parse(recheck_response);
                                        string restatus = dyReResult.status;
                                        string remessage = dyReResult.message;

                                        result.Add("success");
                                        result.Add(remessage);
                                    }
                                    else if (remsg == "redirect")
                                    {
                                        result.Add("redirect");
                                        result.Add("/dmt-manager/sender-details.aspx?mobile=" + mobile);
                                    }
                                    else
                                    {
                                        string message = dyResult.message;

                                        result.Add(remsg);
                                        result.Add(message);
                                    }
                                }
                                else if (status == "16")
                                {
                                    result.Add("invalid");
                                    result.Add("You have entered wrong otp, Please enter correct otp!");
                                }
                                else
                                {
                                    string message = dyResult.message;
                                    result.Add("error");
                                    result.Add(message);
                                }
                            }
                        }
                    }
                    else
                    {
                        result.Add("error");
                        result.Add(msg);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }
}