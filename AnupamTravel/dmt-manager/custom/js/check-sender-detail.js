﻿function RemitterMobileSearch() {
    var thisbutton = $("#btnRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtSenderMobileNo")) return !1;
    var mobileno = $("#txtSenderMobileNo").val();
    if (mobileno.length == 10) {
        $("#hdnRegMobNo").val(mobileno);
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/sender-verification.aspx/RemitterMobileSearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    ResetRegistrationForm();
                    if (data.d[0] == "redirect") {
                        window.location.href = data.d[1];
                    }
                    else if (data.d[0] == "notreg") {
                        ResetRegistrationForm();
                        $("#txtRegOtp").val("");
                        $("#RegistMargin").css("margin", "3% auto");
                        $("#RegistSectionForm").removeClass("hidden");
                        $("#OTPSectionForm").addClass("hidden");

                        $("#txtRegMobileNo").val(mobileno);
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        $("#txtSenderMobileNo").val("");
                    }
                    else if (data.d[0] == "regotpsent") {
                        $("#txtRegOtp").val("");
                        $("#RegistMargin").css("margin", "10% auto");
                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $(".remitterreg").click();
                        StartCountDown(60);
                    }
                    else {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Error !", "#ff414d", data.d[1], "#ff414d");
                    }
                }
                $(thisbutton).html("<i class='fa fa-search'></i> Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

function SubmitRegRemitter() {
    $("#perrormessage").html("");
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRemtrRegistration");

    if (CheckFocusBlankValidation("txtRegMobileNo")) return !1;
    var mobile = $("#txtRegMobileNo").val();
    if (mobile.length != 10) { $("#txtRegMobileNo").focus(); return false; }
    if (CheckFocusBlankValidation("txtRegFirstName")) return !1;
    if (CheckFocusBlankValidation("txtRegLastName")) return !1;
    if (CheckFocusBlankValidation("txtRegPinCode")) return !1;
    if (CheckFocusBlankValidation("txtCurrLocalAddress")) return !1;

    var firstname = $("#txtRegFirstName").val();
    var lastname = $("#txtRegLastName").val();
    var pincode = $("#txtRegPinCode").val();
    var localadd = $("#txtCurrLocalAddress").val();

    if (mobile.length == 10) {
        $("#hdnRegMobNo").val(mobile);
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/sender-verification.aspx/Remitter_Registration",
            data: '{fname: ' + JSON.stringify(firstname) + ',lname: ' + JSON.stringify(lastname) + ',mobile: ' + JSON.stringify(mobile) + ',pincode: ' + JSON.stringify(pincode) + ',location: ' + JSON.stringify(localadd) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "otpsent") {
                        $("#RegistMargin").css("margin", "10% auto");
                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        StartCountDown(60);
                    }
					else{
					$("#perrormessage").html("").html(data.d[1]);
					}
                }
                $(thisbutton).html("Register");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        $("#perrormessage").html("Mobile number should be 10 digits!");
    }
}

function ResetResend() {
    $("#btnResendOTP").removeClass("hidden");
    $("#timersection").addClass("hidden");
}

function ResendOTP() {
    $("#otperrormessage").html("");
    $("#btnResendOTP").html("Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var firstname = $("#txtRegFirstName").val();
    var lastname = $("#txtRegLastName").val();
    var mobile = $("#txtRegMobileNo").val();

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/dmt-manager/sender-verification.aspx/ResendOtpToRemitterMobile",
        data: '{fname: ' + JSON.stringify(firstname) + ',lname: ' + JSON.stringify(lastname) + ',mobile: ' + JSON.stringify(mobile) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "otpsent") {
                    $("#otperrormessage").html("OTP sent, Please check your mobile.");
                    StartCountDown(60);
                }
                else {
                    $("#otperrormessage").html(data.d[1]);
                }
            }
            $("#btnResendOTP").html("Resend OTP");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function SubmitRegOtpVerification() {
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRegOtpVarification");
    if (CheckFocusBlankValidation("txtRegOtp")) return !1;

    var mobile = $("#hdnRegMobNo").val();
    var enteredotp = $("#txtRegOtp").val();

    if (enteredotp.length == 4) {
        if (mobile != "" && enteredotp != "") {
            $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

            $.ajax({
                type: "Post",
                contentType: "application/json; charset=utf-8",
                url: "/dmt-manager/sender-verification.aspx/RemitterVarification",
                data: '{mobile: ' + JSON.stringify(mobile) + ',otp: ' + JSON.stringify(enteredotp) + '}',
                datatype: "json",
                success: function (data) {
					console.log(data.d);
                    if (data.d != null) {
                        if (data.d[0] == "redirect") {
                            $("#btnregformclose").click();
                            window.location.href = data.d[1];
                            //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Remitter registration has been successfully completed.", "#28a745");
                        }
                        else if (data.d[0] == "failed") {
                            $("#otperrormessage").html(data.d[1]);
                        }
                        else if (data.d[0] == "invalid") {
                            $("#otperrormessage").html(data.d[1]);
                        }
                        else {
                            $("#btnregformclose").click();
                            ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                        }
                    }
                    $(thisbutton).html("Verify");
                    $("#txtRegOtp").val("");
                },
                failure: function (response) {
                    alert("failed");
                }
            });
        }
    }
}

var _tick = null;
function StartCountDown(startSecond) {
    $("#btnResendOTP").addClass("hidden");
    $("#timersection").removeClass("hidden");

    clearInterval(_tick);
    $("#secRemaing").html("");
    $("#minRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#secRemaing").text(formarteNumber(secondCounters));
        $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _tick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#secRemaing").text(formarteNumber(secondCounters));
                $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_tick);
                ResetResend();
            }
        }, 1000);
    }
    startTick();
}

function ResetRegistrationForm() {
    $("#txtRegMobileNo").val("");
    $("#txtRegFirstName").val("");
    $("#txtRegLastName").val("");
    $("#txtRegPinCode").val("");
    $("#txtRegOtp").val("");
    $("#txtCurrLocalAddress").val("");
    // $("#moberrormsg").html("");
    $("#perrormessage").html("");
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".modaldioclass").css("width", "500px");
    $(".popupheading").css("color", headcolor).html(headerHeading);
    $(".popupcontent").css("color", bodycolor).html(bodyMsg);
    $(".popupopenclass").click();
}