﻿using Newtonsoft.Json.Linq;
using SWIFTMoneyBillPayments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dmt_manager_Sender_Details : System.Web.UI.Page
{
    private static string UserId { get; set; }
    public string SenderDetail { get; set; }
    public string SenderBenDetail { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            if (Session["GoToSenderPage"] != null)
            {
                UserId = Session["UID"].ToString();
                Session["SenderVal"] = UserId;
                string SenderMobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(SenderMobile))
                {
                    Session["SenderMobile"] = SenderMobile;
                    BindSenderDetails(UserId, SenderMobile);
                }
                else
                {
                    Session["SenderVal"] = null;
                    Session["SenderMobile"] = null;
                    Response.Redirect("/dmt-manager/sender-verification.aspx");
                }
            }
            else
            {
                Session["SenderVal"] = null;
                Session["SenderMobile"] = null;
                Response.Redirect("/dmt-manager/sender-verification.aspx");
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    private void BindSenderDetails(string agentId, string regMobile)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(agentId))
        {
            if (!string.IsNullOrEmpty(regMobile.Trim()))
            {
                DataTable dtSender = DMT_Service.GetT_RemitterDetail(agentId, regMobile);
                if (dtSender != null && dtSender.Rows.Count > 0)
                {
                    string fname = dtSender.Rows[0]["FirstName"].ToString();
                    string lname = dtSender.Rows[0]["LastName"].ToString();
                    string mobile = dtSender.Rows[0]["RegMobile"].ToString();
                    string pinCode = dtSender.Rows[0]["PinCode"].ToString();
                    string localAddress = !string.IsNullOrEmpty(dtSender.Rows[0]["CurrentAddress"].ToString()) ? dtSender.Rows[0]["CurrentAddress"].ToString() : "- - -";
                    string remainingLimit = dtSender.Rows[0]["rem_bal"].ToString();
                    SenderDetail = SenderHTMLDetails((fname + " " + lname), mobile, localAddress, pinCode, remainingLimit);
                    SenderBenDetail = SenderBenHTMLDetails(UserId, mobile);
                }
            }
        }
    }

    #region [Html Section Bind]
    public static string SenderHTMLDetails(string sendername, string mobile, string localadd, string pinCode, string remainingLimit)
    {
        StringBuilder sbSender = new StringBuilder();

        sbSender.Append("<div class='row boxSender'>");
        sbSender.Append("<div class='col-sm-6'><p class='p_botm'>Sender Details</p></div>");
        sbSender.Append("<div class='col-sm-6' style='padding: 2px;'><div class='row'><div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " ( " + mobile + " )</p></div></div></div>");
        //sbSender.Append("<div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div>");
        sbSender.Append("</div>");

        sbSender.Append("<div class='row boxSenderdetail'>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>Name</p><p class='pmargin'>Address</p><p class='pmargin'>Ledger Detail</p></div>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>" + sendername.ToUpper() + " ( " + mobile + " )</p><p class='pmargin'>" + localadd.ToUpper() + ", " + pinCode + "</p><p class='pmargin'>Credit Limit: ₹ 50000/- <span style='color:#ff414d!important;font-weight:bold!important;'>RemainingLimit: ₹ " + remainingLimit + "/-</span></p></div>");
        sbSender.Append("</div>");

        return sbSender.ToString();
    }

    public static string SenderBenHTMLDetails(string agentid, string mobile)
    {
        StringBuilder sbSender = new StringBuilder();

        try
        {
            string msg = string.Empty;
            string reponse = DMT_Service.Get_BeneficiaryDetails(agentid, mobile, ref msg);
            if (!string.IsNullOrEmpty(reponse))
            {
                if (msg == "success")
                {
                    DataTable dtBenDetail = DMT_Service.Get_SenderBeneficiaryDetail(agentid, mobile);
                    if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtBenDetail.Rows.Count; i++)
                        {
                            string benid = dtBenDetail.Rows[i]["Beneficiary_Id"].ToString();

                            sbSender.Append("<tr>");
                            sbSender.Append("<td>" + dtBenDetail.Rows[i]["bene_Name"].ToString() + "</td>");
                            //sbSender.Append("<td>Other</td>");
                            sbSender.Append("<td>" + (!string.IsNullOrEmpty(dtBenDetail.Rows[i]["bankName"].ToString()) ? dtBenDetail.Rows[i]["bankName"].ToString() : "- - -") + "</td>");
                            if (dtBenDetail.Rows[i]["status_id"].ToString().ToLower() == "true")
                            {
                                sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                            }
                            else
                            {
                                sbSender.Append("<td><span class='fa fa-question-circle text-warning' data-toogle='tooltip' title='A/C validation is pending'></span></td>");
                            }
                            sbSender.Append("<td>" + dtBenDetail.Rows[i]["accountNumber"].ToString() + "</td>");
                            sbSender.Append("<td>" + dtBenDetail.Rows[i]["ifscCode"].ToString() + "</td>");
                            sbSender.Append("<td><div class='form-validation'> <input type='text' class='form-control textboxamount' id='txtTranfAmount_" + benid + "' name='txtTranfAmount_" + benid + "' maxlength='5' onkeypress='return isNumberOnlyKeyNoDotNoSpace(event)'/></div></td>");
                            sbSender.Append("<td><span class='btn btn-success btn-sm bentransfermoney' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnMoneyTransfer_" + benid + "' data-benid='" + benid + "'  onclick='DirectTransferMoney(" + benid + ");'>Transfer</span></td>");
                            sbSender.Append("<td><span class='btn btn-danger btn-sm bendeleterecord' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnDeleteBeneficiary_" + benid + "' data-benid='" + benid + "' onclick='DeleteBeneficialRecord(" + benid + ");'>Delete</span></td>");
                            sbSender.Append("</tr>");
                        }
                    }
                    else
                    {
                        sbSender.Append("<tr>");
                        sbSender.Append("<td colspan='9' class='text-center text-danger'>Record not found !</td>"); ;
                        sbSender.Append("</tr>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return sbSender.ToString();
    }
    #endregion

    [WebMethod]
    public static string BindAllBank()
    {
        StringBuilder result = new StringBuilder();

        try
        {
            DataTable dtTopBank = DMT_Service.GetBindAllBank("top");
            DataTable dtBank = DMT_Service.GetBindAllBank("topelse");

            if (dtTopBank != null && dtTopBank.Rows.Count > 0)
            {
                result.Append("<option value=''>Select Bank</option>");
                for (int i = 0; i < dtTopBank.Rows.Count; i++)
                {
                    result.Append("<option value='" + dtTopBank.Rows[i]["bankid"].ToString() + "' data-ifsc='" + dtTopBank.Rows[i]["ifsc"].ToString() + "'>" + dtTopBank.Rows[i]["bankname"].ToString() + "</option>");
                }

                result.Append("<option value='' disabled>--------------------------</option>");

                if (dtBank != null && dtBank.Rows.Count > 0)
                {
                    for (int j = 0; j < dtBank.Rows.Count; j++)
                    {
                        result.Append("<option value='" + dtBank.Rows[j]["bankid"].ToString() + "' data-ifsc='" + dtBank.Rows[j]["ifsc"].ToString() + "'>" + dtBank.Rows[j]["bankname"].ToString() + "</option>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> BeneficiaryRegistration(string accountno, string bankname, string ifsccode, string name)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Remt_Mobile))
                {
                    string msg = string.Empty;
                    string response = DMT_Service.Dmt_Add_Beneficiary(UserId, accountno, name, ifsccode, bankname, Remt_Mobile, ref msg);

                    result.Add("success");
                    result.Add(SenderBenHTMLDetails(UserId, Remt_Mobile));
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficialRecord(string benid)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
                if (!string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Remt_Mobile))
                {
                    string msg = string.Empty;
                    string response = DMT_Service.Dmt_DeleteBeneficiary(benid, UserId, ref msg);
                    if (!string.IsNullOrEmpty(response))
                    {
                        JObject jo_response = JObject.Parse(response);
                        var status = jo_response["status"];
                        var message = jo_response["message"];

                        if (msg == "success")
                        {
                            result.Add("success");
                        }
                        else
                        {
                            result.Add("failed");
                        }
                        result.Add(message.ToString());
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficiaryVarification(string benid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Remt_Mobile))
                    {
                        string msg = string.Empty;
                        string response = DMT_Service.Dmt_Confirm_DeleteBeneficiary(benid, otp, Remt_Mobile, UserId, ref msg);

                        if (!string.IsNullOrEmpty(response))
                        {
                            JObject jo_response = JObject.Parse(response);
                            var status = jo_response["status"];
                            var message = jo_response["message"];

                            if (msg == "success")
                            {
                                result.Add("success");
                                result.Add(SenderBenHTMLDetails(UserId, Remt_Mobile));
                            }
                            else
                            {
                                result.Add("failed");
                                result.Add(message.ToString());
                            }
                        }
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetFundTransferVeryficationDetail(string benid, string amount)
    {
        return GetFundTransferVeryficationDetailHtml(benid, amount, "", false);
    }

    private static List<string> GetFundTransferVeryficationDetailHtml(string benid, string amount, string transmode = "", bool isotp = false)
    {
        List<string> result = new List<string>();
        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
                {
                    StringBuilder sbTrans = new StringBuilder();

                    DataTable dtSenderDel = new DataTable();
                    DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, Remt_Mobile, ref dtSenderDel);
                    if (dtSenderDel != null && dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                    {
                        result.Add("success");

                        sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                        sbTrans.Append("<div class='col-sm-4'><p>From</p><span>" + dtSenderDel.Rows[0]["FirstName"].ToString() + " " + dtSenderDel.Rows[0]["LastName"].ToString() + " (" + dtSenderDel.Rows[0]["RegMobile"].ToString() + ")</span><br/><span>" + dtSenderDel.Rows[0]["CurrentAddress"].ToString() + ", " + dtSenderDel.Rows[0]["PinCode"].ToString() + "</span></div>");
                        sbTrans.Append("<div class='col-sm-4' style='text-align: center;'><img src='custom/images/arrowright.png' style='width: 120px;' /></div>");
                        sbTrans.Append("<div class='col-sm-4'><p>To</p><span>" + dtBenDetail.Rows[0]["bene_Name"].ToString() + "</span><br/><span>" + dtBenDetail.Rows[0]["bankName"].ToString() + " - (" + dtBenDetail.Rows[0]["ifscCode"].ToString() + ")</span><br/><span>A/C - " + dtBenDetail.Rows[0]["accountNumber"].ToString() + "</span></div>");
                        sbTrans.Append("</div>");

                        sbTrans.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");

                        sbTrans.Append("<div class='col-sm-12'><div class='row'>");
                        sbTrans.Append("<div class='col-sm-4'><b>Amount to be Remitted</b></div>");
                        sbTrans.Append("<div class='col-sm-4'></div>");
                        sbTrans.Append("<div class='col-sm-4'><b>Amount to be Collected</b></div>");
                        sbTrans.Append("</div></div>");

                        sbTrans.Append("<div class='col-sm-12' style='padding: 15px;'><div class='row'>");
                        sbTrans.Append("<div class='col-sm-4'>₹ " + amount + "</div><div class='col-sm-4'></div><div class='col-sm-4'>₹ " + amount + "</div>");
                        sbTrans.Append("</div></div>");

                        sbTrans.Append("</div>");

                        sbTrans.Append("<div class='row' style='padding: 1rem;'>");
                        sbTrans.Append("<div class='col-sm-1'><input type='checkbox' class='form-control' id='ChkFundTransCheckboxCheck' checked='checked' value='' style='height: 20px;' /></div>");
                        sbTrans.Append("<div class='col-sm-11'>By clicking the checkbox I accept the above declaraction.</div>");
                        sbTrans.Append("</div>");

                        result.Add(sbTrans.ToString());

                        if (isotp)
                        {
                            result.Add("<div class='col-sm-6'><input type='text' class='form-control ' id='txtDmtOtp' name='txtDmtOtp' maxlength='6' placeholder='Enter OTP' onkeypress='return isNumberOnlyKeyNoDotNoSpace(event)'></div><div class='col-sm-6'><button type='button' id='btnCheckOtp' class='btn btn-primary btn-sm' data-benid='" + benid + "' data-transamount='" + amount + "' data-transmode='" + transmode + "' style='width: 100%;' onclick='DmtTransferWithOtp();'>Process To Send Money</button></div>");
                        }
                        else
                        {
                            string remingLimit = dtSenderDel.Rows[0]["rem_bal"].ToString();
                            if (!string.IsNullOrEmpty(remingLimit))
                            {
                                if (Convert.ToDecimal(remingLimit) >= Convert.ToDecimal(amount))
                                {
                                    result.Add("<div class='col-sm-6'><button type='button' class='btn btn-primary btn-sm' id='btnIMPSTrans' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' onclick='IMPSFundTransfer();'>IMPS</button></div><div class='col-sm-6'><button type='button' id='btnNEFTTrans' class='btn btn-primary btn-sm' data-benid='" + benid + "' data-transamount='" + amount + "' style='width: 100%;' onclick='NEFTFundTransfer();'>NEFT</button></div>");
                                    //string imps_enabled = dtBankDetail.Rows[0]["imps_enabled"].ToString();
                                    //if (imps_enabled == "1")
                                    //{
                                    //    result.Add("<div class='col-sm-6'><button type='button' class='btn btn-primary btn-sm' id='btnIMPSTrans' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' onclick='IMPSFundTransfer();'>IMPS</button></div><div class='col-sm-6'><button type='button' id='btnNEFTTrans' class='btn btn-primary btn-sm' data-benid='" + benid + "' data-transamount='" + amount + "' style='width: 100%;' onclick='NEFTFundTransfer();'>NEFT</button></div>");
                                    //}
                                    //else
                                    //{
                                    //    result.Add("<div class='col-sm-6'><button type='button' id='btnNEFTTrans' class='btn btn-primary btn-sm' style='width: 100%;' data-benid='" + benid + "' data-transamount='" + amount + "' onclick='NEFTFundTransfer();'>NEFT</button></div>");
                                    //}
                                }
                                else
                                {
                                    result.Add("<div class='col-sm-12'><span class='btn btn-primary btn-sm' style='width: 100%;'>TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !</span></div>");
                                }
                            }
                            else
                            {
                                result.Add("<div class='col-sm-12'><span class='btn btn-primary btn-sm' style='width: 100%;'>THERE IS NO REMITTER CREDIT LIMIT AVAILABLE !</span></div>");
                            }
                        }
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        return result;
    }

    [WebMethod]
    public static List<string> ProcessToFundTransfer(string benid, string amount, string transmode)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                    DataTable dtSenderDel = new DataTable();
                    DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, Remt_Mobile, ref dtSenderDel);
                    if (dtSenderDel != null && dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                    {
                        string dt_BenId = dtBenDetail.Rows[0]["Beneficiary_Id"].ToString();
                        string accountNumber = dtBenDetail.Rows[0]["accountNumber"].ToString();
                        string new_mobile = dtBenDetail.Rows[0]["customer_number"].ToString();
                        if (dt_BenId == benid)
                        {
                            string remingLimit = dtSenderDel.Rows[0]["rem_bal"].ToString();
                            if (!string.IsNullOrEmpty(remingLimit))
                            {
                                if (Convert.ToDecimal(remingLimit) >= Convert.ToDecimal(amount))
                                {
                                    DataTable dtAgencyRemtBen = A2ZService.GetAgencyDetailById(UserId);

                                    if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                                    {
                                        agencyName = dtAgencyRemtBen.Rows[0]["Agency_Name"].ToString();
                                        agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();

                                        //if (Convert.ToDouble(agencyCreditLimit) > 0)
                                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                                        {
                                            string transferMode = transmode == "1" ? "IMPS" : "NEFT";

                                            // string trackid = "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                                            List<string> ledResult = A2ZLedgerandcreditlimit_Transaction(amount, benid, "Debit", "DR", "DEBIT NOTE");
                                            //string narration = amount + "_Transfer_To_" + benid;
                                            //List<string> isLedger = A2ZLedgerDebitCreditSection(amount, UserId, agencyName, "Debit", "Money_Transfer", "DMT", "DR", narration, "DEBIT NOTE");

                                            //StringBuilder sbResult = new StringBuilder();

                                            if (ledResult != null && ledResult.Count > 0)
                                            {
                                                string status_code = ledResult[0];
                                                string clientRefId = ledResult[1];

                                                if (status_code == "000" && !string.IsNullOrEmpty(clientRefId.Trim()))
                                                {
                                                    string msg = string.Empty;
                                                    string response = DMT_Service.Dmt_Money_Transaction(UserId, dt_BenId, accountNumber, new_mobile, amount, 0, transmode, clientRefId, ref msg);
                                                    if (!string.IsNullOrEmpty(response))
                                                    {
                                                        JObject jo_response = JObject.Parse(response);
                                                        var status = jo_response["status"];
                                                        var message = jo_response["message"];

                                                        if (msg.Contains("success") || msg.Contains("accepted") || msg.Contains("pending"))
                                                        {
                                                            result.Add("success");
                                                            result.Add(BindFundTransactionResponseDetail(response, amount.ToString(), UserId, benid, new_mobile));

                                                            DataTable dtSender = DMT_Service.GetT_RemitterDetail(UserId, new_mobile);
                                                            if (dtSender != null && dtSender.Rows.Count > 0)
                                                            {
                                                                string fname = dtSender.Rows[0]["FirstName"].ToString();
                                                                string lname = dtSender.Rows[0]["LastName"].ToString();
                                                                string mobile = dtSender.Rows[0]["RegMobile"].ToString();
                                                                string pinCode = dtSender.Rows[0]["PinCode"].ToString();
                                                                string localAddress = !string.IsNullOrEmpty(dtSender.Rows[0]["CurrentAddress"].ToString()) ? dtSender.Rows[0]["CurrentAddress"].ToString() : "- - -";
                                                                string remainingLimit = dtSender.Rows[0]["rem_bal"].ToString();
                                                                result.Add(SenderHTMLDetails((fname + " " + lname), mobile, localAddress, pinCode, remainingLimit));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            List<string> ledDetail = A2ZLedgerandcreditlimit_Transaction(amount, benid, "Credit", "CR", "CREDIT NOTE");
                                                            if (ledDetail.Count > 0)
                                                            {
                                                                bool isupdated = DMT_Service.Dmt_UpdateRemainingLimit(UserId, new_mobile, amount.ToString(), "credit");
                                                                result.Add("failed");
                                                                result.Add(message.ToString());
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result.Add("failed");
                                                        result.Add("ERROR OCCURED ! THERE IS SOME ERROR FROM API END !");
                                                    }
                                                }
                                                else
                                                {
                                                    result.Add("failed");
                                                    result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !" + ledResult[0] + "+" + ledResult[1]);
                                                }
                                            }
                                            else
                                            {
                                                result.Add("failed");
                                                result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                            }
                                        }
                                        else
                                        {
                                            result.Add("lowlimit");
                                            result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                                        }
                                    }
                                }
                                else
                                {
                                    result.Add("lowlimit");
                                    result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                                }
                            }
                            else
                            {
                                result.Add("nolimit");
                                result.Add("THERE IS NO REMITTER CREDIT LIMIT AVAILABLE !");
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("BENEFICIARY ID INCORRECT, PLEASE CHECK !");
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BindFundTransactionResponseDetail(string response, string amount, string agentId, string benid, string mobile)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(response))
        {
            string str_status = string.Empty;
            string txnId = string.Empty;
            string bankRefNo = string.Empty;
            string trackId = string.Empty;

            JObject jo_response = JObject.Parse(response);
            var status = jo_response["status"];
            var message = jo_response["message"];

            if (status.ToString() == "34" && message.ToString().ToLower().Contains("success"))
            {
                str_status = "Success";
            }
            else if (status.ToString() == "34" && message.ToString().ToLower().Contains("accepted"))
            {
                str_status = "Accepted";
            }
            else if (status.ToString() == "3" && message.ToString().ToLower().Contains("pending"))
            {
                str_status = "Pending";
            }
            else if (status.ToString() == "3" && message.ToString().ToLower().Contains("initiated"))
            {
                str_status = "Initiated";
            }
            else
            {
                str_status = "Failed";
            }


            if (str_status.ToLower().Contains("success") || str_status.ToLower().Contains("accepted") || str_status.ToLower().Contains("pending"))
            {
                txnId = jo_response["txnId"].ToString();
                bankRefNo = jo_response["bankRefNo"].ToString();
                trackId = jo_response["clientId"].ToString();

                DataTable dtTransaction = DMT_Service.Get_TransactionDetail(agentId, mobile, trackId, txnId);
                string currDatetime = dtTransaction.Rows[0]["updatedDate"].ToString();

                DataTable dtSenderDel = new DataTable();
                DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, mobile, ref dtSenderDel);

                sbResult.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");
                sbResult.Append("<div class='col-sm-4'><p>From</p><span>" + dtSenderDel.Rows[0]["FirstName"].ToString() + " " + dtSenderDel.Rows[0]["LastName"].ToString() + " (" + dtSenderDel.Rows[0]["RegMobile"].ToString() + ")</span><br/><span>" + dtSenderDel.Rows[0]["CurrentAddress"].ToString() + ", " + dtSenderDel.Rows[0]["PinCode"].ToString() + "</span></div>");
                sbResult.Append("<div class='col-sm-4' style='text-align: center;'><i class='fa fa-check-circle text-success' style='font-size: 120px;'></i></div>");
                sbResult.Append("<div class='col-sm-4'><p>To</p><span>" + dtBenDetail.Rows[0]["bene_Name"].ToString() + "</span><br/><span>" + dtBenDetail.Rows[0]["bankName"].ToString() + " - (" + dtBenDetail.Rows[0]["ifscCode"].ToString() + ")</span><br/><span>A/C - " + dtBenDetail.Rows[0]["accountNumber"].ToString() + "</span></div>");
                sbResult.Append("</div>");

                sbResult.Append("<div class='row' style='border-bottom: 1px solid #dee2e6; padding: 0.7rem;'>");

                sbResult.Append("<div class='col-sm-12'><div class='row'>");
                sbResult.Append("<div class='col-sm-4'><b>Amount to be Remitted</b></div>");
                sbResult.Append("<div class='col-sm-4'></div>");
                sbResult.Append("<div class='col-sm-4'><b>Amount to be Collected</b></div>");
                sbResult.Append("</div></div>");

                sbResult.Append("<div class='col-sm-12' style='padding: 15px;'><div class='row'>");
                sbResult.Append("<div class='col-sm-4'>₹ " + amount + "</div><div class='col-sm-4'></div><div class='col-sm-4'>₹ " + amount + "</div>");
                sbResult.Append("</div></div>");

                sbResult.Append("</div>");

                sbResult.Append("<div class='row' style='margin: 20px 0px 0px 0px'>");
                sbResult.Append("<div class='col-sm-12' style='font-weight: bold;'><div class='row'>");
                sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>TrackId</div><div class='col-sm-4 text-center' style='margin-bottom: 10px;'>Date</div><div class='col-sm-4 text-center' style='margin-bottom: 10px;'>Status</div>");
                sbResult.Append("</div></div></div>");

                sbResult.Append("<div class='row' style='margin-bottom: 10px; border-bottom: 1px dotted #ccc;'>");
                sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + trackId + "</div>");
                sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");

                if (status.ToString() == "34" && message.ToString().ToLower().Contains("success"))
                {
                    sbResult.Append("<div class='col-sm-4 text-success text-center' style='margin-bottom: 10px;font-weight: bold;'>SUCCESS</div>");
                }
                else if (status.ToString() == "3" && message.ToString().ToLower().Contains("pending"))
                {
                    sbResult.Append("<div class='col-sm-4 text-warning text-center' style='margin-bottom: 10px;font-weight: bold;'>UNDER PROCESS</div>");
                }
                else if (status.ToString() == "34" && message.ToString().ToLower().Contains("accepted"))
                {
                    sbResult.Append("<div class='col-sm-4 text-warning text-center' style='margin-bottom: 10px;font-weight: bold;'>ACCEPTED</div>");
                }
                else
                {
                    sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;font-weight: bold;'>FAILED</div>");
                }
                sbResult.Append("</div>");

                sbResult.Append("<div class='col-sm-12 text-right'><a href='/dmt-manager/printtrans.aspx?mobile=" + mobile + "&beneficaryid=" + benid + "&trackid=" + trackId + "' target='_blank' style='cursor: pointer;color:#fff;' class='col-sm-4 btn btn-primary pull-right'><i class='fa fa-print'></i> Print Receipt</a></div>");
            }
            else
            {
                DataTable dtTransaction = DMT_Service.Get_TransactionDetail(agentId, mobile, trackId);
                string currDatetime = dtTransaction.Rows[0]["updatedDate"].ToString();

                sbResult.Append("<div class='col-sm-12' style='margin: 10px auto;'><div class='row'>");
                sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'>" + message + "</div>");
                sbResult.Append("<div class='col-sm-4 text-center' style='margin-bottom: 10px;'><span style='color:#b0b0b0;'>" + ConvertStringDateToStringDateFormate(currDatetime) + "</span></div>");
                sbResult.Append("<div class='col-sm-4 text-danger text-center' style='margin-bottom: 10px;'>FAILED</div>");
                sbResult.Append("</div></div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static List<string> GetTransactionHistory()
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                string fromdate = string.Empty;
                DateTime currDate = DateTime.Now;
                fromdate = currDate.ToString("dd/MM/yyyy");

                DataTable dtTrans = DMT_Service.Get_Dmt_TranstionHistory(UserId, Remt_Mobile, fromdate);

                result.Add("success");
                result.Add(BindTransDetails(dtTrans, "today"));
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BindTransDetails(DataTable dtTrans, string datepos)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
            if (dtTrans != null && dtTrans.Rows.Count > 0)
            {
                for (int i = 0; i < dtTrans.Rows.Count; i++)
                {
                    string refund_html = "- - -";
                    string status = dtTrans.Rows[i]["status"].ToString();
                    string checkbtn = string.Empty;
                    if (status.ToLower().Contains("success"))
                    {
                        status = "<td class='text-success'>SUCCESS</td>";
                        checkbtn = "<td class='text-warning'>- - -</td>";
                    }
                    else if (status.ToLower().Contains("accepted") || status.ToLower().Contains("pending") || status.ToLower().Contains("initiated"))
                    {
                        status = "<td class='text-warning'>UNDER PROCESS</td>";
                        checkbtn = "<td class='text-warning'><p class='btn btn-primary' style='cursor: pointer; border-radius: 3px; padding: 7px; line-height: 10px!important; height: 25px;' id='btnCheckStatusTrans_" + dtTrans.Rows[i]["TrackId"].ToString() + "' data-trackid='" + dtTrans.Rows[i]["TrackId"].ToString() + "' onclick='return CheckStatusTrans(\"" + dtTrans.Rows[i]["TrackId"].ToString() + "\");'>Check Trans.</p></td>";
                    }
                    else
                    {
                        status = "<td class='text-danger'>FAILED</td>";
                        checkbtn = "<td class='text-warning'>- - -</td>";
                        refund_html = "<span style='color: blue;font-weight: bold;'>REVERSAL</span>";
                    }


                    sbTrans.Append("<tr>");
                    sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["updatedDate"].ToString()) + "</td>");
                    sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["txnId"].ToString()) ? dtTrans.Rows[i]["txnId"].ToString() : "- - -") + "</td>");
                    //sbTrans.Append("<td>" + dtTrans.Rows[i]["orderid"].ToString() + "</td>");
                    //sbTrans.Append("<td>" + dtTrans.Rows[i]["ref_no"].ToString() + "</td>");
                    sbTrans.Append("<td>" + dtTrans.Rows[i]["TrackId"].ToString() + "</td>");
                    sbTrans.Append("<td>" + dtTrans.Rows[i]["channel"].ToString() + "</td>");
                    sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["amount"].ToString()) ? dtTrans.Rows[i]["amount"].ToString() : "- - -") + "</td>");
                    //sbTrans.Append("<td>" + (!string.IsNullOrEmpty(dtTrans.Rows[i]["charged_amt"].ToString()) ? dtTrans.Rows[i]["charged_amt"].ToString() : "- - -") + "</td>");
                    sbTrans.Append("<td><span class='text-primary' title='View' style='cursor: pointer;' onclick='ShowBenDetails(" + dtTrans.Rows[i]["Beneficiary_Id"].ToString() + ")'>" + dtTrans.Rows[i]["bene_Name"].ToString() + "</span></td>");
                    sbTrans.Append(status);
                    sbTrans.Append(checkbtn);
                    sbTrans.Append("<td>" + refund_html + "</td>");
                    string url = "/dmt-manager/printtrans.aspx?mobile=" + Remt_Mobile + "&beneficaryid=" + dtTrans.Rows[i]["Beneficiary_Id"].ToString() + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";

                    sbTrans.Append("<td><a href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                    sbTrans.Append("</tr>");
                }
            }
            else
            {
                if (datepos == "today")
                {
                    sbTrans.Append("<tr>");
                    sbTrans.Append("<td colspan='12' class='text-danger text-center'>Today's record not found !</td>");
                    sbTrans.Append("</tr>");
                }
                else
                {
                    sbTrans.Append("<tr>");
                    sbTrans.Append("<td colspan='12' class='text-danger text-center'>Record not found !</td>");
                    sbTrans.Append("</tr>");
                }
            }
        }
        return sbTrans.ToString();
    }

    [WebMethod]
    public static string GetBeneficiaryDetailById(string benid)
    {
        StringBuilder result = new StringBuilder();

        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
            if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
            {
                DataTable dtSenderDel = new DataTable();
                DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, Remt_Mobile, ref dtSenderDel);
                if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                {
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Beneficary ID</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Beneficiary_Id"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Name</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["bene_Name"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Account</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["accountNumber"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>IFSC Code</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["ifscCode"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Bank</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["bankName"].ToString() + "</div></div>");
                }
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> GetFilterTransactionHistory(string fromdate, string todate, string trackid, string filstatus)
    {
        List<string> result = new List<string>();

        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
            if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
            {
                DataTable dtTrans = DMT_Service.Get_Dmt_TranstionHistory(UserId, Remt_Mobile, fromdate, todate, trackid, filstatus);

                result.Add("success");
                result.Add(BindTransDetails(dtTrans, "filter"));
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    [WebMethod]
    public static List<string> CheckTransaction_TrackId(string trackid)
    {
        List<string> result = new List<string>();

        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

            if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
            {
                string response = DMT_Service.CheckTransaction_TrackId(UserId, Remt_Mobile, trackid);
                if (!string.IsNullOrEmpty(response))
                {
                    JObject jo_response = JObject.Parse(response);
                    var status = jo_response["status"];
                    var message = jo_response["message"];

                    if (message.ToString().ToLower().Contains("success") || message.ToString().ToLower().Contains("pending") || message.ToString().ToLower().Contains("inprocess") || message.ToString().ToLower().Contains("processing") || message.ToString().ToLower().Contains("initiated"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];

                        result.Add("success");
                        string final_res = "<table class='table table-sm table-bordered'><thead><tr><th>Status</th><th>Track ID</th><th>Bank RefNo</th><th>Transaction ID</th><th>Description</th></tr></thead><tbody><tr><td>" + clientId + "</td><td>" + clientId + "</td><td>" + bankRefNo + "</td><td>" + txnId + "</td><td>" + description + "</td></tr></tbody></table>";
                        result.Add(final_res);
                    }
                    else if (message.ToString().ToLower().Contains("failed"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];

                        result.Add("failed");
                        string final_res = "<table class='table table-sm table-bordered'><thead><tr><th>Status</th><th>Track ID</th><th>Bank RefNo</th><th>Transaction ID</th><th>Description</th></tr></thead><tbody><tr><td>" + clientId + "</td><td>" + clientId + "</td><td>" + bankRefNo + "</td><td>" + txnId + "</td><td>" + description + "</td></tr></tbody></table>";
                        result.Add(final_res);
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("<table class='table table-sm table-bordered'><tbody><tr><td colspan='5' class='text-center text-danger'>" + message + "</td></tr></tbody></table>");
                    }
                }
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    [WebMethod]
    public static List<string> CheckStatusTransaction_TrackId(string trackid)
    {
        List<string> result = new List<string>();

        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

            if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
            {
                string response = DMT_Service.CheckTransaction_TrackId(UserId, Remt_Mobile, trackid);
                if (!string.IsNullOrEmpty(response))
                {
                    JObject jo_response = JObject.Parse(response);
                    var status = jo_response["status"];
                    var message = jo_response["message"];

                    if (message.ToString().ToLower().Contains("success") || message.ToString().ToLower().Contains("pending") || message.ToString().ToLower().Contains("inprocess") || message.ToString().ToLower().Contains("processing") || message.ToString().ToLower().Contains("initiated"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];

                        result.Add("success");
                        result.Add(description.ToString());
                    }
                    else if (message.ToString().ToLower().Contains("failed"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];

                        result.Add("failed");
                        result.Add(description.ToString());
                    }
                    else
                    {
                        result.Add("error");
                        result.Add(message.ToString());
                    }
                }
            }
        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetBeneficiaryName(string accountno, string ifsccode, string bankname)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                    DataTable dtSenderDel = new DataTable();
                    DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, string.Empty, Remt_Mobile, ref dtSenderDel);
                    if (dtSenderDel != null && dtSenderDel.Rows.Count > 0)
                    {
                        string mobile = dtSenderDel.Rows[0]["RegMobile"].ToString();
                        string remingLimit = dtSenderDel.Rows[0]["rem_bal"].ToString();
                        if (!string.IsNullOrEmpty(remingLimit))
                        {
                            if (Convert.ToDecimal(remingLimit) >= 4)
                            {
                                DataTable dtAgencyRemtBen = A2ZService.GetAgencyDetailById(UserId);

                                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                                {
                                    agencyName = dtAgencyRemtBen.Rows[0]["Agency_Name"].ToString();
                                    agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();

                                    if (Convert.ToDouble(agencyCreditLimit) >= 4)
                                    {
                                        List<string> ledResult = A2ZLedgerandcreditlimit_Transaction("4", "Check_BenDetail", "Debit", "DR", "DEBIT NOTE", false);

                                        if (ledResult != null && ledResult.Count > 0)
                                        {
                                            string status_code = ledResult[0];
                                            string clientRefId = ledResult[1];

                                            if (status_code == "000" && !string.IsNullOrEmpty(clientRefId.Trim()))
                                            {
                                                string msg = string.Empty;
                                                string response = DMT_Service.Dmt_Beneficiary_Account_Verification(UserId, accountno, mobile, ifsccode, bankname, clientRefId);
                                                if (!string.IsNullOrEmpty(response))
                                                {
                                                    JObject jo_response = JObject.Parse(response);
                                                    var status = jo_response["status"];
                                                    var message = jo_response["message"];
                                                    if (status.ToString() == "1" && message.ToString().ToLower().Contains("success"))
                                                    {
                                                        var beneName = jo_response["beneName"];
                                                        result.Add("success");
                                                        result.Add(beneName.ToString());
                                                    }
                                                    else if (status.ToString() == "3" && message.ToString().ToLower().Contains("pending"))
                                                    {

                                                    }
                                                    else
                                                    {
                                                        List<string> ledDetail = A2ZLedgerandcreditlimit_Transaction("4", "Check_BenDetail", "Credit", "CR", "CREDIT NOTE", false);
                                                        if (ledDetail.Count > 0)
                                                        {
                                                            //bool isupdated = DMT_Service.Dmt_UpdateRemainingLimit(UserId, mobile, "2", "credit");
                                                            result.Add("failed");
                                                            result.Add(message.ToString());
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    result.Add("failed");
                                                    result.Add("ERROR OCCURED ! THERE IS SOME ERROR FROM API END !");
                                                }
                                            }
                                            else
                                            {
                                                result.Add("failed");
                                                result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !" + ledResult[0] + "+" + ledResult[1]);
                                            }
                                        }
                                        else
                                        {
                                            result.Add("failed");
                                            result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                        }
                                    }
                                    else
                                    {
                                        result.Add("lowlimit");
                                        result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                                    }
                                }
                            }
                            else
                            {
                                result.Add("lowlimit");
                                result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                            }
                        }
                        else
                        {
                            result.Add("nolimit");
                            result.Add("THERE IS NO REMITTER CREDIT LIMIT AVAILABLE !");
                        }
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("BENEFICIARY ID INCORRECT, PLEASE CHECK !");
                    }
                }
            }

            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.ToString());

        }

        return result;
    }

    #region [Payment Section]
    private static List<string> A2ZLedgerandcreditlimit_Transaction(string amount, string beneid, string transType, string transShortType, string transNotes, bool ismarkup = true)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (Convert.ToDouble(amount) > 0)
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
                    DataTable dtAgency = A2ZService.GetAgencyDetailById(UserId);

                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                        agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                        {
                            string narration = amount + "_Transfer_To_" + beneid;
                            List<string> isLedger = A2ZLedgerDebitCreditSection(amount, UserId, agencyName, transType, "Money_Transfer", "DMT", transShortType, narration, transNotes, ismarkup);
                            if (isLedger.Count > 0)
                            {
                                string avlBal = isLedger[1];
                                string trackid = isLedger[0];

                                result.Add("000");
                                result.Add(trackid);
                            }
                            else
                            {
                                result.Add("500");
                                result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                            }
                        }
                        else
                        {
                            result.Add("500");
                            result.Add("INSUFFICIENT CREDIT LIMIT IN YOUR WALLET !");
                        }
                    }
                    else
                    {
                        result.Add("500");
                        result.Add("AGENCY DOES NOT EXIST !");
                    }
                }
                else
                {
                    result.Add("500");
                    result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("500");
            result.Add(ex.Message);
        }

        return result;
    }

    private static List<string> A2ZLedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype, bool ismarkup)
    {
        List<string> result = new List<string>();

        try
        {
            double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
            double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

            if (Uploadtype.Trim().ToLower() == "cr")
            {
                Remark = "DMT Refund";
                shortRemark = "DMT Refund";
            }

            result = LedgerService.Dmt_LedgerDebitCredit(Convert.ToDouble(amount), agentid, agencyName, agentid, GetLocalIPAddress(), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype, ismarkup);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    #endregion

    //#region [Payment Section] 
    //private static List<string> A2ZLedgerandcreditlimit_Transaction(string amount, string beneid, string transType, string transShortType, string transNotes)
    //{
    //    List<string> result = new List<string>();

    //    try
    //    {
    //        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
    //        {
    //            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
    //            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();
    //            if (!string.IsNullOrEmpty(UserId))
    //            {
    //                if (!string.IsNullOrEmpty(amount))
    //                {
    //                    if (Convert.ToDouble(amount) > 0)
    //                    {
    //                        string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
    //                        DataTable dtAgency = A2ZService.GetAgencyDetailById(UserId);

    //                        if (dtAgency != null && dtAgency.Rows.Count > 0)
    //                        {
    //                            agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
    //                            agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

    //                            if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
    //                            {
    //                                string narration = amount + "_Transfer_To_" + beneid;
    //                                List<string> isLedger = A2ZLedgerDebitCreditSection(amount, UserId, agencyName, transType, "Money_Transfer", "DMT", transShortType, narration, transNotes);
    //                                if (isLedger.Count > 0)
    //                                {
    //                                    string avlBal = isLedger[1];
    //                                    string trackid = isLedger[0];

    //                                    result.Add("000");
    //                                    result.Add(trackid);
    //                                }
    //                                else
    //                                {
    //                                    result.Add("500");
    //                                    result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
    //                                }
    //                            }
    //                            else
    //                            {
    //                                result.Add("500");
    //                                result.Add("INSUFFICIENT CREDIT LIMIT IN YOUR WALLET !");
    //                            }
    //                        }
    //                        else
    //                        {
    //                            result.Add("500");
    //                            result.Add("AGENCY DOES NOT EXIST !");
    //                        }
    //                    }
    //                    else
    //                    {
    //                        result.Add("500");
    //                        result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
    //            }
    //        }
    //        else
    //        {
    //            HttpContext.Current.Response.Redirect("~/Login.aspx", false);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ex.ToString();
    //        throw;
    //    }

    //    return result;
    //}

    //private static List<string> A2ZLedgerDebitCreditSection(string amount, string agentid, string agencyName, string actiontype, string Remark, string shortRemark, string Uploadtype, string narration, string ledtype)
    //{
    //    List<string> result = new List<string>();

    //    try
    //    {
    //        double Debit = actiontype.ToLower() == "debit" ? Convert.ToDouble(amount) : 0;
    //        double Credit = actiontype.ToLower() == "credit" ? Convert.ToDouble(amount) : 0;

    //        if (Uploadtype.Trim().ToLower() == "cr")
    //        {
    //            Remark = "DMT Refund";
    //            shortRemark = "DMT Refund";
    //        }

    //        result = LedgerService.Dmt_LedgerDebitCredit(Convert.ToDouble(amount), agentid, agencyName, agentid, GetIp(UserId), Debit, Credit, actiontype, Remark, Uploadtype, shortRemark, narration, ledtype);
    //    }
    //    catch (Exception ex)
    //    {
    //        ex.ToString();
    //    }

    //    return result;
    //}

    [WebMethod]
    public static List<string> ProcessToOTPFundTransfer(string benid, string amount, string transmode)
    {
        List<string> otphtml = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
                {
                    DataTable dtAgencyRemtBen = A2ZService.GetAgencyDetailById(UserId);

                    if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                    {
                        string mobno = dtAgencyRemtBen.Rows[0]["Mobile"].ToString();
                        string smsUser = "103127";
                        string smsKey = "010o3bC30Tzdb0VeR8MV";
                        string entityId = "1201161518475115256";
                        string templateId = "1207162503693690554";
                        string senderId = "ATOATT";

                        DataTable dtSenderDel = new DataTable();
                        DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, Remt_Mobile, ref dtSenderDel);

                        string benname = string.Empty;
                        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                        {
                            benname = dtBenDetail.Rows[0]["bene_Name"].ToString();
                        }

                        if (DMTSMS.SendSms(smsUser, smsKey, senderId, entityId, templateId, amount, UserId, benname, benid, mobno, Remt_Mobile, "dmt_transfer"))
                        {
                            otphtml = GetFundTransferVeryficationDetailHtml(benid, amount, transmode, true);
                        }                        
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return otphtml;
    }

    [WebMethod]
    public static List<string> CheckOTPAndProcessToFundTransfer(string otp, string benid, string amount, string transmode)
    {
        List<string> result = new List<string>();
        if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
        {
            string UserId = HttpContext.Current.Session["SenderVal"].ToString();
            string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

            if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
            {
                bool isotp = DMTSMS.CheckDMTOTPValid(UserId, benid, Remt_Mobile, amount, otp);

                if (isotp)
                {
                    result = ProcessToFundTransferAfterOTPValid(benid, amount, transmode);
                    bool otpdeactive = DMTSMS.DisableDMTOTPValid(UserId, benid, Remt_Mobile, amount, otp);
                }
                else
                {
                    result.Add("wrongotp");
                    result.Add("You have entered wrong otp, please enter valid otp for fund transfer!");
                }
            }
        }

        return result;
    }

    private static List<string> ProcessToFundTransferAfterOTPValid(string benid, string amount, string transmode)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["SenderVal"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["SenderVal"].ToString();
                string Remt_Mobile = HttpContext.Current.Session["SenderMobile"].ToString();

                if (!string.IsNullOrEmpty(Remt_Mobile) && !string.IsNullOrEmpty(UserId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                    DataTable dtSenderDel = new DataTable();
                    DataTable dtBenDetail = DMT_Service.GetFundTransferVeryficationDetail(UserId, benid, Remt_Mobile, ref dtSenderDel);
                    if (dtSenderDel != null && dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                    {
                        string dt_BenId = dtBenDetail.Rows[0]["Beneficiary_Id"].ToString();
                        string accountNumber = dtBenDetail.Rows[0]["accountNumber"].ToString();
                        string new_mobile = dtBenDetail.Rows[0]["customer_number"].ToString();
                        if (dt_BenId == benid)
                        {
                            string remingLimit = dtSenderDel.Rows[0]["rem_bal"].ToString();
                            if (!string.IsNullOrEmpty(remingLimit))
                            {
                                if (Convert.ToDecimal(remingLimit) >= Convert.ToDecimal(amount))
                                {
                                    DataTable dtAgencyRemtBen = A2ZService.GetAgencyDetailById(UserId);

                                    if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                                    {
                                        agencyName = dtAgencyRemtBen.Rows[0]["Agency_Name"].ToString();
                                        agencyCreditLimit = dtAgencyRemtBen.Rows[0]["Crd_Limit"].ToString();

                                        //if (Convert.ToDouble(agencyCreditLimit) > 0)
                                        if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(amount))
                                        {
                                            string transferMode = transmode == "1" ? "IMPS" : "NEFT";

                                            // string trackid = "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                                            List<string> ledResult = A2ZLedgerandcreditlimit_Transaction(amount, benid, "Debit", "DR", "DEBIT NOTE");
                                            //string narration = amount + "_Transfer_To_" + benid;
                                            //List<string> isLedger = A2ZLedgerDebitCreditSection(amount, UserId, agencyName, "Debit", "Money_Transfer", "DMT", "DR", narration, "DEBIT NOTE");

                                            //StringBuilder sbResult = new StringBuilder();

                                            if (ledResult != null && ledResult.Count > 0)
                                            {
                                                string status_code = ledResult[0];
                                                string clientRefId = ledResult[1];

                                                if (status_code == "000" && !string.IsNullOrEmpty(clientRefId.Trim()))
                                                {
                                                    string msg = string.Empty;
                                                    string response = DMT_Service.Dmt_Money_Transaction(UserId, dt_BenId, accountNumber, new_mobile, amount, 0, transmode, clientRefId, ref msg);
                                                    if (!string.IsNullOrEmpty(response))
                                                    {
                                                        JObject jo_response = JObject.Parse(response);
                                                        var status = jo_response["status"];
                                                        var message = jo_response["message"];

                                                        if (msg.Contains("success") || msg.Contains("accepted") || msg.Contains("pending"))
                                                        {
                                                            result.Add("success");
                                                            result.Add(BindFundTransactionResponseDetail(response, amount.ToString(), UserId, benid, new_mobile));

                                                            DataTable dtSender = DMT_Service.GetT_RemitterDetail(UserId, new_mobile);
                                                            if (dtSender != null && dtSender.Rows.Count > 0)
                                                            {
                                                                string fname = dtSender.Rows[0]["FirstName"].ToString();
                                                                string lname = dtSender.Rows[0]["LastName"].ToString();
                                                                string mobile = dtSender.Rows[0]["RegMobile"].ToString();
                                                                string pinCode = dtSender.Rows[0]["PinCode"].ToString();
                                                                string localAddress = !string.IsNullOrEmpty(dtSender.Rows[0]["CurrentAddress"].ToString()) ? dtSender.Rows[0]["CurrentAddress"].ToString() : "- - -";
                                                                string remainingLimit = dtSender.Rows[0]["rem_bal"].ToString();
                                                                result.Add(SenderHTMLDetails((fname + " " + lname), mobile, localAddress, pinCode, remainingLimit));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            List<string> ledDetail = A2ZLedgerandcreditlimit_Transaction(amount, benid, "Credit", "CR", "CREDIT NOTE");
                                                            if (ledDetail.Count > 0)
                                                            {
                                                                bool isupdated = DMT_Service.Dmt_UpdateRemainingLimit(UserId, new_mobile, amount.ToString(), "credit");
                                                                result.Add("failed");
                                                                result.Add(message.ToString());
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result.Add("failed");
                                                        result.Add("ERROR OCCURED ! THERE IS SOME ERROR FROM API END !");
                                                    }
                                                }
                                                else
                                                {
                                                    result.Add("failed");
                                                    result.Add("ERROR OCCURED ! DEDUCT MONEY FROM YOUR WALLET !" + ledResult[0] + "+" + ledResult[1]);
                                                }
                                            }
                                            else
                                            {
                                                result.Add("failed");
                                                result.Add("WE ARE UNABLE TO TRANSFER MONEY AT THE MOMENT. INSTEAD OF TRYING AGAIN, PLEASE CONTACT OUR CALL CENTRE TO AVOID ANY INCONVENIENCE.");
                                            }
                                        }
                                        else
                                        {
                                            result.Add("lowlimit");
                                            result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                                        }
                                    }
                                }
                                else
                                {
                                    result.Add("lowlimit");
                                    result.Add("TRANSACTION NOT POSSIBLE, CREDIT LIMIT IS LESS THAN TRANSFER AMOUNT, PLEASE CHECK !");
                                }
                            }
                            else
                            {
                                result.Add("nolimit");
                                result.Add("THERE IS NO REMITTER CREDIT LIMIT AVAILABLE !");
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("BENEFICIARY ID INCORRECT, PLEASE CHECK !");
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", false);
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    public static string GetIp(string agentId)
    {
        return A2ZService.GetIp(agentId);
    }

    public static string GetLocalIPAddress()
    {
        string result = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                result = ip.ToString();
            }
        }

        return result;
    }
    //#endregion
}