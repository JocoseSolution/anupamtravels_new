﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PopupNotification.ascx.cs" Inherits="dmt_manager_PopupNotification" %>
<style>
    .login-signup-page {
        max-width: 460px;
    }

    .font-weight-400 {
        font-weight: 400 !important;
    }

    h1, h2, h3, h4, h5, h6 {
        color: #1e1d1c;
        font-family: "Rubik", sans-serif;
    }

    a, p, li, td, span {
        font-size: 14.5px;
        line-height: 24px;
        font-family: "Amazon Ember",Arial,sans-serif;
        font-weight: 400;
        font-style: normal;
    }

    .shadow-md {
        -webkit-box-shadow: 0px 0px 50px -35px rgb(0 0 0 / 40%);
        box-shadow: 0px 0px 50px -35px rgb(0 0 0 / 40%);
    }

    .form-control:not(.form-control-sm) {
        padding: 1.3rem 2rem;
        height: inherit;
    }

    .form-control, .custom-select {
        border-color: #dae1e3;
        font-size: 16px;
        color: #656565;
    }

    .package-form label, select, input {
        font-size: 13px !important;
    }

    .btn:not(.btn-link) {
        -webkit-box-shadow: 0px 5px 15px rgb(0 0 0 / 15%);
        box-shadow: 0px 5px 15px rgb(0 0 0 / 15%);
    }

    .btn {
        padding: .8rem 2rem;
        font-weight: 500;
        -webkit-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }

    p {
        line-height: 1.9;
    }
    input {
    font-size:15px!important}
</style>
<button type="button" class="btn btn-info btn-lg popupopenclass" style="display: none;" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#PopupMsg"></button>
<div class="modal fade" id="PopupMsg" role="dialog">
    <div class="modal-dialog modaldioclass" style="margin: 15% auto!important;">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-header" style="padding: 12px;">
                <h4 class="modal-title popupheading" style="text-align: left;"></h4>
            </div>
            <div class="modal-body" style="padding-left: 20px!important;">
                <h5 class="popupcontent"></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type='button' class='btn btn-success btn-sm hidden remitterreg' style="display: none;" data-backdrop='static' data-keyboard='false' data-toggle='modal' data-target='#FormRegistration'></button>
<div class="modal fade" id="FormRegistration" role="dialog">
    <div class="modal-dialog modal-md" id="RegistMargin" style="margin: 3% auto; padding: 0px 30px; width: 600px;">
        <div class="modal-content" id="RegistSectionForm" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container">
                    <%--style="padding: 0px 30px;"--%>
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center">Sign Up for DMT
                         <button type="button" id="btnregclose" class="close" data-dismiss="modal" aria-label="Close" style="top: 7px; opacity: 1;"><span aria-hidden="true" style="color: #fff;">×</span></button>
                        </h3>
                        <p class="lead text-center">Your Sign Up information is safe with us.</p>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <input id="txtRegMobileNo" class="form-control" placeholder="Enter Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                <%--<p id="moberrormsg" class="text-danger text-right"></p>--%>
                            </div>
                            <%--onkeypress="return isNumberValidationPrevent(event);"--%>
                            <div class="form-group form-validation">
                                <input id="txtRegFirstName" class="form-control" placeholder="Enter First Name" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtRegLastName" class="form-control" placeholder="Enter Last Name" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtRegPinCode" class="form-control" placeholder="Enter Area Pin Code" maxlength="6" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <div class="form-group form-validation">
                                <textarea id="txtCurrLocalAddress" class="form-control" placeholder="Current / Local Address"></textarea>
                            </div>
                            <span id="btnRemtrRegistration" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return SubmitRegRemitter();">Register</span>
                            <p id="perrormessage" class="text-danger"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-content hidden" style="text-align: center; border-radius: 5px !important;" id="OTPSectionForm">
            <div class="container">
                <div class="login-signup-page mx-auto">
                    <h3 class="font-weight-400 text-center text-success">
                        <button type="button" id="btnregformclose" class="close" data-dismiss="modal" aria-label="Close" style="top: 10px;">
                            <span aria-hidden="true" style="color: #ff414d;">×</span>
                        </button>
                    </h3>
                    <p class="lead text-center text-success" id="RegOtpHeading">Otp sent successfully to your mobile number.</p>
                    <div class="bg-light shadow-md rounded p-4 mx-2">
                        <div class="form-group form-validation">                            
                            <input type="text" id="txtRegOtp" class="form-control" placeholder="Enter OTP Number." maxlength="4" onkeypress="return isNumberValidationPrevent(event);" />
                        </div>
                        <div class="row form-group form-validation">
                            <div class="col-sm-6"><span id="btnRegOtpVarification" style="cursor: pointer; font-size: 15px;" class="btn btn-primary btn-block my-4" onclick="javascript: return SubmitRegOtpVerification();">Verify</span></div>
                            <div class="col-sm-6">
                                <span id="btnResendOTP" style="cursor: pointer; font-size: 15px;" class="btn btn-primary btn-block my-4 hidden" onclick="ResendOTP();">Resend OTP</span>
                                <span id="timersection" class="btn btn-primary btn-block my-4 hidden"><span id="minRemaing"></span>:<span id="secRemaing"></span></span>
                            </div>
                        </div>
                        <p id="otperrormessage" class="text-danger"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input id="hdnRegMobNo" type="hidden" />