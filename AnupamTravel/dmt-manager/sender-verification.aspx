﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT_MasterPage.master" AutoEventWireup="true" CodeFile="sender-verification.aspx.cs" Inherits="dmt_manager_sender_verification" %>

<%@ Register Src="~/dmt-manager/PopupNotification.ascx" TagPrefix="uc1" TagName="PopupNotification" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/search.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="theme-hero-area" style="margin-top: -9px;">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg" style="background-image: url(../Advance_CSS/img/brands/blockchain-background.jpg); -webkit-filter: blur(5px); -moz-filter: blur(15px); -o-filter: blur(15px); -ms-filter: blur(15px); filter: blur(5px); -webkit-transform: scale(1.2, 1.2); -moz-transform: scale(1.2, 1.2); -o-transform: scale(1.2, 1.2); -ms-transform: scale(1.2, 1.2); transform: scale(1.2, 1.2);" id="hero-banner"></div>
            <div class="blur-area" data-bg-area="#hero-banner" data-blur-area="#hero-search-form" data-blur="20" style="filter: blur(20px); background-image: url(&quot;http://remtsoy.com/tf_templates/tf-bookify-demo/img/daak8aqd_-i_1500x500.jpeg&quot;); clip-path: inset(200px 159.177px 100px 159.156px);"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="theme-search-area _pv-100 theme-search-area-stacked" style="padding-bottom: 50px !important;">
                            <div class="theme-search-area-header _ta-c">
                                <h1 class="theme-search-area-title" style="color: #fff;">Enter Sender’s Mobile Number</h1>
                            </div>
                            <div class="theme-search-area-form" id="hero-search-form">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                                            <div class="theme-search-area-section-inner form-validation">
                                                <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                                                <input class="theme-search-area-section-input typeahead" type="text" id="txtSenderMobileNo" style="font-size: 20px!important; color: #ff7e00;" maxlength="10" onkeypress="return isNumberOnlyKeyNoDotNoSpace(event)" placeholder="Sender Mobile Number" />
                                            </div>
                                        </div>
                                        <p style="color: #ccc;">(10 digits) Please don not use prefix zero (0)</p>
                                    </div>
                                    <div class="col-md-4 ">
                                        <span id="btnRemitterMobileSearch" class="theme-search-area-submit _mt-0 theme-search-area-submit-glow theme-search-area-submit-curved" style="cursor: pointer!important;" onclick="RemitterMobileSearch();">
                                            <i class="fa fa-search"></i>
                                            Search
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row-col-mob-gap" data-gutter="20">
                    <div class="col-sm-4">
                        <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                            <br />
                            <h3 class="text-5 text-center">Step 1</h3>
                            <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                            <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                            <%-- <p>मूलभूत जानकारी भरकर प्रेषक को पैसा भेजने के लिए पंजीकृत करें | एक फोटो आई डी और पता प्रमाण अपलोड कर प्रेषक की जानकारीअपग्रेड करें।</p>--%>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                            <br />
                            <h3 class="text-5 text-center">Step 2</h3>
                            <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                            <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                            <%--<p>प्रत्येक प्रेषक के अन्तर्गत विभिन्न लाभार्थीं / प्राप्तकर्ता जोड़ सकते हैं।</p>--%>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                            <br />
                            <h3 class="text-5 text-center">Step 3</h3>
                            <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                            <p>Instantly begin transferring money.</p>
                            <%--<p>तुरंत पैसा भेजने की प्रक्रिया शुरू करें।</p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
    </div>
    <uc1:PopupNotification runat="server" ID="PopupNotification" />

    <script src="custom/js/common.js" type="text/javascript"></script>
    <script src="custom/js/check-sender-detail.js?v=1.3" type="text/javascript"></script>
</asp:Content>

