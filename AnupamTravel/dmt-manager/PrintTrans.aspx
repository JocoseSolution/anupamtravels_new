﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintTrans.aspx.cs" Inherits="DMT_Manager_PrintTrans" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
    <script src="vendor/jquery/jquery.min.js"></script>
    <style>
        .main {
            margin-left: 20% !important;
        }

        .heading {
            text-align: left;
        }

        .sendmail {
            float: right;
            color: #fff;
            background-color: #ff434f;
            border: 1px solid #ff434f;
            border-radius: 0.2rem;
            padding: 2px;
        }

        @media print {
            .main {
                margin-left: 0px !important;
            }

            .heading {
                text-align: center !important;
            }

            .sendmail {
                display: none !important;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnUserId" runat="server" />
        <asp:HiddenField ID="hdnMobile" runat="server" />
        <asp:HiddenField ID="hdnBeneficaryid" runat="server" />
        <asp:HiddenField ID="hdnTrackid" runat="server" />
        <%=HtmlContent %>


        <script src="custom/js/common.js"></script>
        <script>
            $(window).on('load', function () { window.print(); });

            $(document.body).on('click', "#btnMailSend", function (e) {
                if (CheckFocusBlankValidation("ReceiptSendMail")) return !1;
                if (PrintCheckEmailValidatoin("ReceiptSendMail")) return !1;

                var emailid = $("#ReceiptSendMail").val();
                var userid = $("#hdnUserId").val();
                var mobile = $("#hdnMobile").val();
                var beneficaryid = $("#hdnBeneficaryid").val();
                var trackid = $("#hdnTrackid").val();
                $("#btnMailSend").html("Sending... <i class='fa fa-pulse fa-spinner'></i>");
                if (emailid != "") {
                    $.ajax({
                        type: "Post",
                        contentType: "application/json; charset=utf-8",
                        url: "/dmt-manager/PrintTrans.aspx/SendReceiptInMail",
                        data: '{emailid: ' + JSON.stringify(emailid) + ',userid: ' + JSON.stringify(userid) + ',mobile: ' + JSON.stringify(mobile) + ',beneficaryid: ' + JSON.stringify(beneficaryid) + ',trackid: ' + JSON.stringify(trackid) + '}',
                        datatype: "json",
                        success: function (data) {
                            if (data.d != null) {
                                if (data.d == "sent") {
                                    alert("Transaction receipt has been sent to ' " + emailid + " ' successfully.");
                                    $("#ReceiptSendMail").val("");
                                }
                                else {
                                    alert("Transaction receipt send failed!");
                                }
                            }
                            $("#btnMailSend").html("Send Mail");
                        },
                        failure: function (response) {
                            alert("failed");
                        }
                    });
                }
            });
        </script>
    </form>
</body>
</html>
