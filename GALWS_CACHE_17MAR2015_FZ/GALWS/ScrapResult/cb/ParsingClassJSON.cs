﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net.Mail;
using Microsoft.VisualBasic;
using System.Threading;

namespace scrap.cb
{
    public class ParsingClassJSON
    {
        // Methods
        //public ParsingClassJSON();
        //public static string flightJsondata(string flightJson, string trip, string flagdom, string Supplier, string currencyCode);

        // Nested Types
        [Serializable]
        public class flightOutbound
        {
            // Methods
            //  public flightOutbound();

            // Properties
            public string CabinClass { get; set; }
            public decimal cFareAmt { get; set; }
            public string Class { get; set; }
            public decimal cTaxAmt { get; set; }
            public string CurrencyCode { get; set; }
            public decimal cYQ { get; set; }
            public string DateOfArrival { get; set; }
            public string DateOfDeparture { get; set; }
            public string Destination { get; set; }
            public string DestinationCity { get; set; }
            public string equiptype { get; set; }
            public decimal FareAmt { get; set; }
            public string FareBasis { get; set; }
            public string FlightDuration { get; set; }
            public int FlightId { get; set; }
            public string FlightName { get; set; }
            public List<ParsingClassJSON.flights> Flights { get; set; }
            public decimal iFareAmt { get; set; }
            public string IsRefundable { get; set; }
            public decimal iTaxAmt { get; set; }
            public string productdetailqualifier { get; set; }
            public int Sequence { get; set; }
            public string Source { get; set; }
            public string SourceCity { get; set; }
            public int Stop { get; set; }
            public decimal TaxAmt { get; set; }
            public string TimeOfArrival { get; set; }
            public string TimeOfDepartuere { get; set; }
            public decimal YQ { get; set; }
        }

        [Serializable]
        public class flightRound
        {
            // Methods
            //public flightRound();

            // Properties
            public string CabinClass { get; set; }
            public string CabinClass_in { get; set; }
            public decimal cFareAmt { get; set; }
            public decimal cFareAmt_in { get; set; }
            public string Class { get; set; }
            public string Class_in { get; set; }
            public decimal Commission_in { get; set; }
            public decimal cTaxAmt { get; set; }
            public decimal cTaxAmt_in { get; set; }
            public string CurrencyCode { get; set; }
            public decimal cYQ { get; set; }
            public decimal cYQ_in { get; set; }
            public string DateOfArrival { get; set; }
            public string DateOfArrival_in { get; set; }
            public string DateOfDeparture { get; set; }
            public string DateOfDeparture_in { get; set; }
            public string Destination { get; set; }
            public string Destination_in { get; set; }
            public string DestinationCity { get; set; }
            public string DestinationCity_in { get; set; }
            public decimal DistCommission_in { get; set; }
            public string equiptype { get; set; }
            public string equiptype_in { get; set; }
            public decimal FareAmt { get; set; }
            public decimal FareAmt_in { get; set; }
            public string FareBasis { get; set; }
            public string FareBasis_in { get; set; }
            public string FlightDuration { get; set; }
            public string FlightDuration_in { get; set; }
            public int FlightId { get; set; }
            public int FlightId_in { get; set; }
            public string FlightName { get; set; }
            public string FlightName_in { get; set; }
            public List<ParsingClassJSON.flights> Flights { get; set; }
            public List<ParsingClassJSON.flights> Flights_in { get; set; }
            public decimal iFareAmt { get; set; }
            public decimal iFareAmt_in { get; set; }
            public string IsRefundable { get; set; }
            public string IsRefundable_in { get; set; }
            public decimal iTaxAmt { get; set; }
            public decimal iTaxAmt_in { get; set; }
            public string productdetailqualifier { get; set; }
            public string productdetailqualifier_in { get; set; }
            public int Sequence { get; set; }
            public int Sequence_in { get; set; }
            public decimal ServiceTax_in { get; set; }
            public string Source { get; set; }
            public string Source_in { get; set; }
            public string SourceCity { get; set; }
            public string SourceCity_in { get; set; }
            public int Stop { get; set; }
            public int Stop_in { get; set; }
            public string Supplier { get; set; }
            public string Supplier_in { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TaxAmt_in { get; set; }
            public string TimeOfArrival { get; set; }
            public string TimeOfArrival_in { get; set; }
            public string TimeOfDepartuere { get; set; }
            public string TimeOfDepartuere_in { get; set; }
            public decimal TransactionFee_in { get; set; }
            public decimal YQ { get; set; }
            public decimal YQ_in { get; set; }
        }

        [Serializable]
        public class flights
        {
            // Methods
            // public flights();

            // Properties
            public string CabinClass { get; set; }
            public string Class { get; set; }
            public string DateOfArrival { get; set; }
            public string DateOfDeparture { get; set; }
            public string Destination { get; set; }
            public string DestinationCity { get; set; }
            public string DestinationTerminal { get; set; }
            public string Duration { get; set; }
            public string EquipType { get; set; }
            public string FlightDuration { get; set; }
            public string FlightName { get; set; }
            public string FlightNo { get; set; }
            public string Image { get; set; }
            public string MarketingCarrier { get; set; }
            public string msgQual { get; set; }
            public string OperationCarrier { get; set; }
            public string RefNo { get; set; }
            public string Source { get; set; }
            public string SourceCity { get; set; }
            public string SourceTerminal { get; set; }
            public string TimeOfArrival { get; set; }
            public string TimeOfDepartuere { get; set; }
        }
        public static string flightJsondata(string flightJson, string trip, string flagdom, string Supplier, string currencyCode)
        {
            DataRow[] rowArray;
            string str = "";
            DataSet set = new DataSet();
            set.ReadXml("http://resources.ibe4all.com/CommonIBE/App_Data/xml/airport.xml");
            DataSet set2 = new DataSet();
            set2.ReadXml("http://resources.ibe4all.com/CommonIBE/App_Data/xml/airlines.xml");
            if (flightJson == null)
            {
                flightJson = string.Empty;
            }
            if (flightJson.Contains("ErrorResponse"))
            {
                flightJson = string.Empty;
            }
            if ((!string.IsNullOrEmpty(flightJson) && (flagdom.ToLower() == "i")) && (trip.ToLower() == "two"))
            {
                try
                {
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(flightJson);
                    XmlNodeList elementsByTagName = document.GetElementsByTagName("SegmentFlightRef");
                    List<flightRound> list2 = new List<flightRound>();
                    int num = 0;
                    foreach (XmlNode node in elementsByTagName)
                    {
                        int num2 = 0;
                        flightRound item = new flightRound();
                        XmlNode node2 = node.SelectSingleNode("Outbound");
                        if (node2 != null)
                        {
                            string innerText = "";
                            XmlNodeList list3 = node2.SelectNodes("Flightinfo");
                            if (list3 != null)
                            {
                                List<ParsingClassJSON.flights> list4 = new List<ParsingClassJSON.flights>();
                                item.FlightId = num;
                                item.Sequence = num2;
                                item.TimeOfDepartuere = Convert.ToInt32(list3[0].SelectSingleNode("TOD").InnerText).ToString("#0000");
                                item.equiptype = list3[0].SelectSingleNode("equiptype").InnerText;
                                item.productdetailqualifier = list3[0].SelectSingleNode("productdetailqualifier").InnerText.Replace(" checked=", "").Replace("-", "_");
                                if (item.TimeOfDepartuere.Length > 2)
                                {
                                    item.TimeOfDepartuere = item.TimeOfDepartuere.Insert(2, ":");
                                }
                                item.TimeOfArrival = Convert.ToInt32(list3[list3.Count - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                if (item.TimeOfArrival.Length > 2)
                                {
                                    item.TimeOfArrival = item.TimeOfArrival.Insert(2, ":");
                                }
                                item.DateOfDeparture = list3[0].SelectSingleNode("DOD").InnerText;
                                if (item.DateOfDeparture.Length > 2)
                                {
                                    item.DateOfDeparture = item.DateOfDeparture.Insert(2, "-");
                                    item.DateOfDeparture = item.DateOfDeparture.Insert(5, "-");
                                }
                                item.DateOfArrival = list3[list3.Count - 1].SelectSingleNode("DOA").InnerText;
                                if (item.DateOfArrival.Length > 2)
                                {
                                    item.DateOfArrival = item.DateOfArrival.Insert(2, "-");
                                    item.DateOfArrival = item.DateOfArrival.Insert(5, "-");
                                }
                                item.FlightDuration = list3[0].SelectSingleNode("flighttime").InnerText;
                                if (item.FlightDuration.Length > 2)
                                {
                                    item.FlightDuration = item.FlightDuration.Insert(2, "h ");
                                    item.FlightDuration = item.FlightDuration.Insert(item.FlightDuration.Length, "m");
                                }
                                item.Stop = list3.Count;
                                item.Source = list3[0].SelectSingleNode("SrcLocation").InnerText;
                                item.Destination = list3[list3.Count - 1].SelectSingleNode("DstLocation").InnerText;
                                rowArray = set.Tables[0].Select("AirPortCode ='" + item.Source + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row = rowArray[0];
                                    item.SourceCity = row["City"].ToString();
                                }
                                rowArray = set.Tables[0].Select("AirPortCode ='" + item.Destination + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row2 = rowArray[0];
                                    item.DestinationCity = row2["City"].ToString();
                                }
                                DataRow[] source = set2.Tables[0].Select("AirLineCode ='" + list3[0].SelectSingleNode("marketingCarrier").InnerText + "'");
                                if (source.Count<DataRow>() > 0)
                                {
                                    DataRow row3 = source[0];
                                    item.FlightName = row3["AirLine"].ToString();
                                }
                                XmlNode node3 = node.SelectSingleNode("FareDetails");
                                int num3 = 0;
                                foreach (XmlNode node4 in list3)
                                {
                                    if (innerText == "")
                                    {
                                        innerText = node4.SelectSingleNode("FlightClass").InnerText;
                                    }
                                    ParsingClassJSON.flights flights = new ParsingClassJSON.flights
                                    {
                                        RefNo = node4.SelectSingleNode("refno").InnerText,
                                        TimeOfArrival = Convert.ToInt32(node4.SelectSingleNode("TOA").InnerText).ToString("#0000"),
                                        TimeOfDepartuere = Convert.ToInt32(node4.SelectSingleNode("TOD").InnerText).ToString("#0000")
                                    };
                                    if (flights.TimeOfArrival.Length > 2)
                                    {
                                        flights.TimeOfArrival = flights.TimeOfArrival.Insert(2, ":");
                                    }
                                    if (flights.TimeOfDepartuere.Length > 2)
                                    {
                                        flights.TimeOfDepartuere = flights.TimeOfDepartuere.Insert(2, ":");
                                    }
                                    flights.DateOfDeparture = node4.SelectSingleNode("DOD").InnerText;
                                    flights.DateOfArrival = node4.SelectSingleNode("DOA").InnerText;
                                    if (flights.DateOfDeparture.Length > 2)
                                    {
                                        flights.DateOfDeparture = flights.DateOfDeparture.Insert(2, "-");
                                        flights.DateOfDeparture = flights.DateOfDeparture.Insert(5, "-");
                                    }
                                    if (flights.DateOfArrival.Length > 2)
                                    {
                                        flights.DateOfArrival = flights.DateOfArrival.Insert(2, "-");
                                        flights.DateOfArrival = flights.DateOfArrival.Insert(5, "-");
                                    }
                                    flights.FlightDuration = node4.SelectSingleNode("flighttime").InnerText;
                                    if (flights.FlightDuration.Length > 2)
                                    {
                                        flights.FlightDuration = flights.FlightDuration.Insert(2, "h ");
                                        flights.FlightDuration = flights.FlightDuration.Insert(flights.FlightDuration.Length, "m");
                                    }
                                    flights.MarketingCarrier = node4.SelectSingleNode("marketingCarrier").InnerText;
                                    source = set2.Tables[0].Select("AirLineCode ='" + flights.MarketingCarrier + "'");
                                    if (source.Count<DataRow>() > 0)
                                    {
                                        DataRow row4 = source[0];
                                        flights.FlightName = row4["AirLine"].ToString();
                                        flights.Image = row4["Image"].ToString();
                                    }
                                    else
                                    {
                                        flights.FlightName = "";
                                        flights.Image = "";
                                    }
                                    flights.OperationCarrier = node4.SelectSingleNode("operatingCarrier").InnerText;
                                    flights.FlightNo = node4.SelectSingleNode("flightno").InnerText;
                                    flights.EquipType = node4.SelectSingleNode("equiptype").InnerText;
                                    XmlNode node5 = node4.SelectSingleNode("SrcLocation");
                                    if (node5 != null)
                                    {
                                        flights.Source = node5.SelectSingleNode("Source").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights.Source + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row5 = rowArray[0];
                                            flights.SourceCity = row5["City"].ToString();
                                        }
                                        if (node5.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights.SourceTerminal = " T-" + node5.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights.SourceTerminal = "";
                                        }
                                    }
                                    XmlNode node6 = node4.SelectSingleNode("DstLocation");
                                    if (node6 != null)
                                    {
                                        flights.Destination = node6.SelectSingleNode("Destination").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights.Destination + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row6 = rowArray[0];
                                            flights.DestinationCity = row6["City"].ToString();
                                        }
                                        if (node6.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights.DestinationTerminal = " T-" + node6.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights.DestinationTerminal = "";
                                        }
                                    }
                                    if (num3 > 0)
                                    {
                                        DateTime time = DateTime.Parse(flights.DateOfDeparture + " " + flights.TimeOfDepartuere, new CultureInfo("en-GB"));
                                        string str3 = Convert.ToInt32(list3[num3 - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                        if (str3.Length > 2)
                                        {
                                            str3 = str3.Insert(2, ":");
                                        }
                                        DateTime time2 = DateTime.Parse(list3[num3 - 1].SelectSingleNode("DOA").InnerText.Insert(2, "-").Insert(5, "-") + " " + str3, new CultureInfo("en-GB"));
                                        string introduced140 = time.Subtract(time2).Hours.ToString("#00");
                                        string str5 = introduced140 + time.Subtract(time2).Minutes.ToString("#00");
                                        if (str5.Length > 2)
                                        {
                                            str5 = str5.Insert(2, "h ");
                                            str5 = str5.Insert(str5.Length, "m");
                                        }
                                        flights.Duration = str5;
                                    }
                                    if (node3 != null)
                                    {
                                        XmlNode node7 = node3.SelectSingleNode("Adult");
                                        if (node7 != null)
                                        {
                                            XmlNodeList list5 = node7.SelectNodes("FareDetail");
                                            XmlNode node8 = node7.SelectSingleNode("Fare");
                                            flights.msgQual = node8.SelectSingleNode("msgQual").InnerText;
                                            flights.CabinClass = innerText;
                                            try
                                            {
                                                flights.Class = list5[num3].SelectSingleNode("rbd").InnerText;
                                            }
                                            catch (Exception)
                                            {
                                                flights.Class = list5[0].SelectSingleNode("rbd").InnerText;
                                            }
                                        }
                                    }
                                    list4.Add(flights);
                                    num3++;
                                }
                                item.Flights = list4;
                                if (node3 != null)
                                {
                                    XmlNode node9 = node3.SelectSingleNode("Adult");
                                    if (node9 != null)
                                    {
                                        item.Supplier = Supplier;
                                        item.FareAmt = Math.Round((decimal)(Convert.ToDecimal(node9.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("Adult").Count)));
                                        item.TaxAmt = Math.Round((decimal)(Convert.ToDecimal(node9.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("Adult").Count)));
                                        item.YQ = Math.Round((decimal)(Convert.ToDecimal(node9.SelectSingleNode("YQ").InnerText) * Convert.ToDecimal(node3.SelectNodes("Adult").Count)));
                                        item.IsRefundable = node9.SelectSingleNode("travellerref").InnerText;
                                        XmlNode node10 = node9.SelectSingleNode("FareDetail");
                                        item.CabinClass = innerText;
                                        item.FareBasis = node10.SelectSingleNode("FareBasis").InnerText;
                                        item.Class = node10.SelectSingleNode("rbd").InnerText;
                                    }
                                    XmlNode node11 = node3.SelectSingleNode("Child");
                                    if (node11 != null)
                                    {
                                        item.cFareAmt = Math.Round((decimal)(Convert.ToDecimal(node11.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("Child").Count)));
                                        item.cTaxAmt = Math.Round((decimal)(Convert.ToDecimal(node11.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("Child").Count)));
                                        item.cYQ = Math.Round((decimal)(Convert.ToDecimal(node11.SelectSingleNode("YQ").InnerText) * Convert.ToDecimal(node3.SelectNodes("Child").Count)));
                                    }
                                    else
                                    {
                                        item.cFareAmt = 0M;
                                        item.cTaxAmt = 0M;
                                        item.cYQ = 0M;
                                    }
                                    XmlNode node12 = node3.SelectSingleNode("INF");
                                    if (node12 != null)
                                    {
                                        item.iFareAmt = Math.Round((decimal)(Convert.ToDecimal(node12.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("INF").Count)));
                                        item.iTaxAmt = Math.Round((decimal)(Convert.ToDecimal(node12.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node3.SelectNodes("INF").Count)));
                                    }
                                    else
                                    {
                                        item.iFareAmt = 0M;
                                        item.iTaxAmt = 0M;
                                    }
                                    item.CurrencyCode = currencyCode;
                                }
                            }
                        }
                        num2++;
                        XmlNode node13 = node.SelectSingleNode("Inbound");
                        if (node13 != null)
                        {
                            string str6 = "";
                            XmlNodeList list6 = node13.SelectNodes("Flightinfo");
                            if (list6 != null)
                            {
                                List<ParsingClassJSON.flights> list7 = new List<ParsingClassJSON.flights>();
                                item.FlightId_in = num;
                                item.Sequence_in = num2;
                                item.TimeOfDepartuere_in = Convert.ToInt32(list6[0].SelectSingleNode("TOD").InnerText).ToString("#0000");
                                item.equiptype_in = list6[0].SelectSingleNode("equiptype").InnerText;
                                item.productdetailqualifier_in = list6[0].SelectSingleNode("productdetailqualifier").InnerText;
                                if (item.TimeOfDepartuere_in.Length > 2)
                                {
                                    item.TimeOfDepartuere_in = item.TimeOfDepartuere_in.Insert(2, ":");
                                }
                                item.TimeOfArrival_in = Convert.ToInt32(list6[list6.Count - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                if (item.TimeOfArrival_in.Length > 2)
                                {
                                    item.TimeOfArrival_in = item.TimeOfArrival_in.Insert(2, ":");
                                }
                                item.DateOfDeparture_in = list6[0].SelectSingleNode("DOD").InnerText;
                                if (item.DateOfDeparture_in.Length > 2)
                                {
                                    item.DateOfDeparture_in = item.DateOfDeparture_in.Insert(2, "-");
                                    item.DateOfDeparture_in = item.DateOfDeparture_in.Insert(5, "-");
                                }
                                item.DateOfArrival_in = list6[list6.Count - 1].SelectSingleNode("DOA").InnerText;
                                if (item.DateOfArrival_in.Length > 2)
                                {
                                    item.DateOfArrival_in = item.DateOfArrival_in.Insert(2, "-");
                                    item.DateOfArrival_in = item.DateOfArrival_in.Insert(5, "-");
                                }
                                item.FlightDuration_in = list6[0].SelectSingleNode("flighttime").InnerText;
                                if (item.FlightDuration_in.Length > 2)
                                {
                                    item.FlightDuration_in = item.FlightDuration_in.Insert(2, "h ");
                                    item.FlightDuration_in = item.FlightDuration_in.Insert(item.FlightDuration_in.Length, "m");
                                }
                                item.Stop_in = list6.Count;
                                item.Source_in = list6[0].SelectSingleNode("SrcLocation").InnerText;
                                item.Destination_in = list6[list6.Count - 1].SelectSingleNode("DstLocation").InnerText;
                                rowArray = set.Tables[0].Select("AirPortCode ='" + item.Source_in + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row7 = rowArray[0];
                                    item.SourceCity_in = row7["City"].ToString();
                                }
                                rowArray = set.Tables[0].Select("AirPortCode ='" + item.Destination_in + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row8 = rowArray[0];
                                    item.DestinationCity_in = row8["City"].ToString();
                                }
                                DataRow[] rowArray3 = set2.Tables[0].Select("AirLineCode ='" + list6[0].SelectSingleNode("marketingCarrier").InnerText + "'");
                                if (rowArray3.Count<DataRow>() > 0)
                                {
                                    DataRow row9 = rowArray3[0];
                                    item.FlightName_in = row9["AirLine"].ToString();
                                }
                                int num4 = 0;
                                XmlNode node14 = node.SelectSingleNode("FareDetails");
                                foreach (XmlNode node15 in list6)
                                {
                                    if (str6 == "")
                                    {
                                        str6 = node15.SelectSingleNode("FlightClass").InnerText;
                                    }
                                    ParsingClassJSON.flights flights2 = new ParsingClassJSON.flights
                                    {
                                        RefNo = node15.SelectSingleNode("refno").InnerText,
                                        TimeOfArrival = Convert.ToInt32(node15.SelectSingleNode("TOA").InnerText).ToString("#0000"),
                                        TimeOfDepartuere = Convert.ToInt32(node15.SelectSingleNode("TOD").InnerText).ToString("0000")
                                    };
                                    if (flights2.TimeOfArrival.Length > 2)
                                    {
                                        flights2.TimeOfArrival = flights2.TimeOfArrival.Insert(2, ":");
                                    }
                                    if (flights2.TimeOfDepartuere.Length > 2)
                                    {
                                        flights2.TimeOfDepartuere = flights2.TimeOfDepartuere.Insert(2, ":");
                                    }
                                    flights2.DateOfDeparture = node15.SelectSingleNode("DOD").InnerText;
                                    flights2.DateOfArrival = node15.SelectSingleNode("DOA").InnerText;
                                    if (flights2.DateOfDeparture.Length > 2)
                                    {
                                        flights2.DateOfDeparture = flights2.DateOfDeparture.Insert(2, "-");
                                        flights2.DateOfDeparture = flights2.DateOfDeparture.Insert(5, "-");
                                    }
                                    if (flights2.DateOfArrival.Length > 2)
                                    {
                                        flights2.DateOfArrival = flights2.DateOfArrival.Insert(2, "-");
                                        flights2.DateOfArrival = flights2.DateOfArrival.Insert(5, "-");
                                    }
                                    flights2.FlightDuration = node15.SelectSingleNode("flighttime").InnerText;
                                    if (flights2.FlightDuration.Length > 2)
                                    {
                                        flights2.FlightDuration = flights2.FlightDuration.Insert(2, "h ");
                                        flights2.FlightDuration = flights2.FlightDuration.Insert(flights2.FlightDuration.Length, "m");
                                    }
                                    flights2.MarketingCarrier = node15.SelectSingleNode("marketingCarrier").InnerText;
                                    rowArray3 = set2.Tables[0].Select("AirLineCode ='" + flights2.MarketingCarrier + "'");
                                    if (rowArray3.Count<DataRow>() > 0)
                                    {
                                        DataRow row10 = rowArray3[0];
                                        flights2.FlightName = row10["AirLine"].ToString();
                                        flights2.Image = row10["Image"].ToString();
                                    }
                                    else
                                    {
                                        flights2.FlightName = "";
                                        flights2.Image = "";
                                    }
                                    flights2.OperationCarrier = node15.SelectSingleNode("operatingCarrier").InnerText;
                                    flights2.FlightNo = node15.SelectSingleNode("flightno").InnerText;
                                    flights2.EquipType = node15.SelectSingleNode("equiptype").InnerText;
                                    XmlNode node16 = node15.SelectSingleNode("SrcLocation");
                                    if (node16 != null)
                                    {
                                        flights2.Source = node16.SelectSingleNode("Source").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights2.Source + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row11 = rowArray[0];
                                            flights2.SourceCity = row11["City"].ToString();
                                        }
                                        if (node16.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights2.SourceTerminal = " T-" + node16.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights2.SourceTerminal = "";
                                        }
                                    }
                                    XmlNode node17 = node15.SelectSingleNode("DstLocation");
                                    if (node17 != null)
                                    {
                                        flights2.Destination = node17.SelectSingleNode("Destination").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights2.Destination + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row12 = rowArray[0];
                                            flights2.DestinationCity = row12["City"].ToString();
                                        }
                                        if (node17.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights2.DestinationTerminal = " T-" + node17.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights2.DestinationTerminal = "";
                                        }
                                    }
                                    if (num4 > 0)
                                    {
                                        DateTime time3 = DateTime.Parse(flights2.DateOfDeparture + " " + flights2.TimeOfDepartuere, new CultureInfo("en-GB"));
                                        string str7 = Convert.ToInt32(list6[num4 - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                        if (str7.Length > 2)
                                        {
                                            str7 = str7.Insert(2, ":");
                                        }
                                        DateTime time4 = DateTime.Parse(list6[num4 - 1].SelectSingleNode("DOA").InnerText.Insert(2, "-").Insert(5, "-") + " " + str7, new CultureInfo("en-GB"));
                                        string str9 = time3.Subtract(time4).Hours.ToString("#00") + ":" + time3.Subtract(time4).Minutes.ToString("#00");
                                        flights2.Duration = str9;
                                    }
                                    if (node14 != null)
                                    {
                                        XmlNode node18 = node14.SelectSingleNode("Adult");
                                        XmlNodeList list8 = node18.SelectNodes("FareDetail");
                                        int count = list8.Count;
                                        if (node18 != null)
                                        {
                                            XmlNode node19 = node18.SelectSingleNode("Fare");
                                            flights2.msgQual = node19.SelectSingleNode("msgQual").InnerText;
                                            flights2.CabinClass = str6;
                                            item.CabinClass_in = str6;
                                            try
                                            {
                                                flights2.Class = list8.Item((count - list6.Count) + num4).SelectSingleNode("rbd").InnerText;
                                                string text1 = list8.Item((count - list6.Count) + num4).SelectSingleNode("AvlStatus").InnerText;
                                                item.Class_in = flights2.Class;
                                                item.FareBasis_in = list8.Item((count - list6.Count) + num4).SelectSingleNode("FareBasis").InnerText;
                                            }
                                            catch (Exception)
                                            {
                                                flights2.Class = list8.Item(0).SelectSingleNode("rbd").InnerText;
                                                string text2 = list8.Item(0).SelectSingleNode("AvlStatus").InnerText;
                                                item.Class_in = flights2.Class;
                                                item.FareBasis_in = list8.Item(0).SelectSingleNode("FareBasis").InnerText;
                                            }
                                        }
                                    }
                                    list7.Add(flights2);
                                    num4++;
                                }
                                item.Flights_in = list7;
                                list2.Add(item);
                            }
                        }
                        num++;
                    }
                    list2 = (from x in list2
                             where x.FlightName != null
                             select x).ToList<flightRound>();
                    new StringBuilder();
                    try
                    {
                        str = new JavaScriptSerializer().Serialize(list2);
                    }
                    catch (Exception)
                    {
                        str = new JavaScriptSerializer { MaxJsonLength = 0x7fffffff }.Serialize(list2);
                    }
                }
                catch (Exception)
                {
                    flightJson = "''";
                }
                return str;
            }
            if (!string.IsNullOrEmpty(flightJson))
            {
                try
                {
                    XmlDocument document2 = new XmlDocument();
                    document2.LoadXml(flightJson);
                    XmlNodeList list9 = document2.GetElementsByTagName("SegmentFlightRef");
                    List<flightOutbound> list10 = new List<flightOutbound>();
                    List<flightOutbound> collection = new List<flightOutbound>();
                    int num6 = 0;
                    foreach (XmlNode node20 in list9)
                    {
                        int num7 = 0;
                        flightOutbound outbound = new flightOutbound();
                        XmlNode node21 = node20.SelectSingleNode("Outbound");
                        if (node21 != null)
                        {
                            string str10 = "";
                            XmlNodeList list12 = node21.SelectNodes("Flightinfo");
                            if (list12 != null)
                            {
                                List<ParsingClassJSON.flights> list13 = new List<ParsingClassJSON.flights>();
                                outbound.FlightId = num6;
                                outbound.Sequence = num7;
                                outbound.TimeOfDepartuere = Convert.ToInt32(list12[0].SelectSingleNode("TOD").InnerText).ToString("#0000");
                                outbound.equiptype = list12[0].SelectSingleNode("equiptype").InnerText;
                                outbound.productdetailqualifier = list12[0].SelectSingleNode("productdetailqualifier").InnerText.Replace(" checked=", "").Replace("-", "_");
                                if (outbound.TimeOfDepartuere.Length > 2)
                                {
                                    outbound.TimeOfDepartuere = outbound.TimeOfDepartuere.Insert(2, ":");
                                }
                                outbound.TimeOfArrival = Convert.ToInt32(list12[list12.Count - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                if (outbound.TimeOfArrival.Length > 2)
                                {
                                    outbound.TimeOfArrival = outbound.TimeOfArrival.Insert(2, ":");
                                }
                                outbound.DateOfDeparture = list12[0].SelectSingleNode("DOD").InnerText;
                                if (outbound.DateOfDeparture.Length > 2)
                                {
                                    outbound.DateOfDeparture = outbound.DateOfDeparture.Insert(2, "-");
                                    outbound.DateOfDeparture = outbound.DateOfDeparture.Insert(5, "-");
                                }
                                outbound.DateOfArrival = list12[list12.Count - 1].SelectSingleNode("DOA").InnerText;
                                if (outbound.DateOfArrival.Length > 2)
                                {
                                    outbound.DateOfArrival = outbound.DateOfArrival.Insert(2, "-");
                                    outbound.DateOfArrival = outbound.DateOfArrival.Insert(5, "-");
                                }
                                outbound.FlightDuration = list12[0].SelectSingleNode("flighttime").InnerText;
                                if (outbound.FlightDuration.Length > 2)
                                {
                                    outbound.FlightDuration = outbound.FlightDuration.Insert(2, "h ");
                                    outbound.FlightDuration = outbound.FlightDuration.Insert(outbound.FlightDuration.Length, "m");
                                }
                                outbound.Stop = list12.Count;
                                outbound.Source = list12[0].SelectSingleNode("SrcLocation").InnerText;
                                outbound.Destination = list12[list12.Count - 1].SelectSingleNode("DstLocation").InnerText;
                                rowArray = set.Tables[0].Select("AirPortCode ='" + outbound.Source + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row13 = rowArray[0];
                                    outbound.SourceCity = row13["City"].ToString();
                                }
                                rowArray = set.Tables[0].Select("AirPortCode ='" + outbound.Destination + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row14 = rowArray[0];
                                    outbound.DestinationCity = row14["City"].ToString();
                                }
                                DataRow[] rowArray4 = set2.Tables[0].Select("AirLineCode ='" + list12[0].SelectSingleNode("marketingCarrier").InnerText + "'");
                                if (rowArray4.Count<DataRow>() > 0)
                                {
                                    DataRow row15 = rowArray4[0];
                                    outbound.FlightName = row15["AirLine"].ToString();
                                }
                                XmlNode node22 = node20.SelectSingleNode("FareDetails");
                                int num8 = 0;
                                foreach (XmlNode node23 in list12)
                                {
                                    if (str10 == "")
                                    {
                                        str10 = node23.SelectSingleNode("FlightClass").InnerText;
                                    }
                                    ParsingClassJSON.flights flights3 = new ParsingClassJSON.flights
                                    {
                                        RefNo = node23.SelectSingleNode("refno").InnerText,
                                        TimeOfArrival = Convert.ToInt32(node23.SelectSingleNode("TOA").InnerText).ToString("#0000"),
                                        TimeOfDepartuere = Convert.ToInt32(node23.SelectSingleNode("TOD").InnerText).ToString("#0000")
                                    };
                                    if (flights3.TimeOfArrival.Length > 2)
                                    {
                                        flights3.TimeOfArrival = flights3.TimeOfArrival.Insert(2, ":");
                                    }
                                    if (flights3.TimeOfDepartuere.Length > 2)
                                    {
                                        flights3.TimeOfDepartuere = flights3.TimeOfDepartuere.Insert(2, ":");
                                    }
                                    flights3.DateOfDeparture = node23.SelectSingleNode("DOD").InnerText;
                                    flights3.DateOfArrival = node23.SelectSingleNode("DOA").InnerText;
                                    if (flights3.DateOfDeparture.Length > 2)
                                    {
                                        flights3.DateOfDeparture = flights3.DateOfDeparture.Insert(2, "-");
                                        flights3.DateOfDeparture = flights3.DateOfDeparture.Insert(5, "-");
                                    }
                                    if (flights3.DateOfArrival.Length > 2)
                                    {
                                        flights3.DateOfArrival = flights3.DateOfArrival.Insert(2, "-");
                                        flights3.DateOfArrival = flights3.DateOfArrival.Insert(5, "-");
                                    }
                                    flights3.FlightDuration = node23.SelectSingleNode("flighttime").InnerText;
                                    if (flights3.FlightDuration.Length > 2)
                                    {
                                        flights3.FlightDuration = flights3.FlightDuration.Insert(2, "h ");
                                        flights3.FlightDuration = flights3.FlightDuration.Insert(flights3.FlightDuration.Length, "m");
                                    }
                                    flights3.MarketingCarrier = node23.SelectSingleNode("marketingCarrier").InnerText;
                                    rowArray4 = set2.Tables[0].Select("AirLineCode ='" + flights3.MarketingCarrier + "'");
                                    if (rowArray4.Count<DataRow>() > 0)
                                    {
                                        DataRow row16 = rowArray4[0];
                                        flights3.FlightName = row16["AirLine"].ToString();
                                        flights3.Image = row16["Image"].ToString();
                                    }
                                    else
                                    {
                                        flights3.FlightName = "";
                                        flights3.Image = "";
                                    }
                                    flights3.OperationCarrier = node23.SelectSingleNode("operatingCarrier").InnerText;
                                    flights3.FlightNo = node23.SelectSingleNode("flightno").InnerText;
                                    flights3.EquipType = node23.SelectSingleNode("equiptype").InnerText;
                                    XmlNode node24 = node23.SelectSingleNode("SrcLocation");
                                    if (node24 != null)
                                    {
                                        flights3.Source = node24.SelectSingleNode("Source").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights3.Source + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row17 = rowArray[0];
                                            flights3.SourceCity = row17["City"].ToString();
                                        }
                                        if (node24.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights3.SourceTerminal = " T-" + node24.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights3.SourceTerminal = "";
                                        }
                                    }
                                    XmlNode node25 = node23.SelectSingleNode("DstLocation");
                                    if (node25 != null)
                                    {
                                        flights3.Destination = node25.SelectSingleNode("Destination").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights3.Destination + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row18 = rowArray[0];
                                            flights3.DestinationCity = row18["City"].ToString();
                                        }
                                        if (node25.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights3.DestinationTerminal = " T-" + node25.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights3.DestinationTerminal = "";
                                        }
                                    }
                                    if (num8 > 0)
                                    {
                                        DateTime time5 = DateTime.Parse(flights3.DateOfDeparture + " " + flights3.TimeOfDepartuere, new CultureInfo("en-GB"));
                                        string str11 = Convert.ToInt32(list12[num8 - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                        if (str11.Length > 2)
                                        {
                                            str11 = str11.Insert(2, ":");
                                        }
                                        DateTime time6 = DateTime.Parse(list12[num8 - 1].SelectSingleNode("DOA").InnerText.Insert(2, "-").Insert(5, "-") + " " + str11, new CultureInfo("en-GB"));
                                        string introduced147 = time5.Subtract(time6).Hours.ToString("#00");
                                        string str13 = introduced147 + time5.Subtract(time6).Minutes.ToString("#00");
                                        if (str13.Length > 2)
                                        {
                                            str13 = str13.Insert(2, "h ");
                                            str13 = str13.Insert(str13.Length, "m");
                                        }
                                        flights3.Duration = str13;
                                    }
                                    if (node22 != null)
                                    {
                                        XmlNode node26 = node22.SelectSingleNode("Adult");
                                        if (node26 != null)
                                        {
                                            XmlNodeList list14 = node26.SelectNodes("FareDetail");
                                            XmlNode node27 = node26.SelectSingleNode("Fare");
                                            flights3.msgQual = node27.SelectSingleNode("msgQual").InnerText;
                                            flights3.CabinClass = str10;
                                            try
                                            {
                                                flights3.Class = list14[num8].SelectSingleNode("rbd").InnerText;
                                            }
                                            catch (Exception)
                                            {
                                                flights3.Class = list14[0].SelectSingleNode("rbd").InnerText;
                                            }
                                        }
                                    }
                                    list13.Add(flights3);
                                    num8++;
                                }
                                outbound.Flights = list13;
                                if (node22 != null)
                                {
                                    XmlNode node28 = node22.SelectSingleNode("Adult");
                                    if (node28 != null)
                                    {
                                        outbound.FareAmt = Math.Round((decimal)(Convert.ToDecimal(node28.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("Adult").Count)));
                                        outbound.TaxAmt = Math.Round((decimal)(Convert.ToDecimal(node28.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("Adult").Count)));
                                        outbound.YQ = Math.Round((decimal)(Convert.ToDecimal(node28.SelectSingleNode("YQ").InnerText) * Convert.ToDecimal(node22.SelectNodes("Adult").Count)));
                                        outbound.IsRefundable = node28.SelectSingleNode("travellerref").InnerText;
                                        XmlNode node29 = node28.SelectSingleNode("FareDetail");
                                        outbound.CabinClass = node29.SelectSingleNode("FareBasis").InnerText;
                                        outbound.FareBasis = node29.SelectSingleNode("FareBasis").InnerText;
                                        outbound.Class = node29.SelectSingleNode("rbd").InnerText;
                                    }
                                    XmlNode node30 = node22.SelectSingleNode("Child");
                                    if (node30 != null)
                                    {
                                        outbound.cFareAmt = Math.Round((decimal)(Convert.ToDecimal(node30.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("Child").Count)));
                                        outbound.cTaxAmt = Math.Round((decimal)(Convert.ToDecimal(node30.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("Child").Count)));
                                        outbound.cYQ = Math.Round((decimal)(Convert.ToDecimal(node30.SelectSingleNode("YQ").InnerText) * Convert.ToDecimal(node22.SelectNodes("Child").Count)));
                                    }
                                    else
                                    {
                                        outbound.cFareAmt = 0M;
                                        outbound.cTaxAmt = 0M;
                                        outbound.cYQ = 0M;
                                    }
                                    XmlNode node31 = node22.SelectSingleNode("INF");
                                    if (node31 != null)
                                    {
                                        outbound.iFareAmt = Math.Round((decimal)(Convert.ToDecimal(node31.SelectSingleNode("fareamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("INF").Count)));
                                        outbound.iTaxAmt = Math.Round((decimal)(Convert.ToDecimal(node31.SelectSingleNode("taxamt").InnerText) * Convert.ToDecimal(node22.SelectNodes("INF").Count)));
                                    }
                                    else
                                    {
                                        outbound.iFareAmt = 0M;
                                        outbound.iTaxAmt = 0M;
                                    }
                                    outbound.CurrencyCode = currencyCode;
                                }
                            }
                            list10.Add(outbound);
                        }
                        num7++;
                        XmlNode node32 = node20.SelectSingleNode("Inbound");
                        if (node32 != null)
                        {
                            string str14 = "";
                            XmlNodeList list15 = node32.SelectNodes("Flightinfo");
                            if (list15 != null)
                            {
                                flightOutbound outbound2 = new flightOutbound();
                                List<ParsingClassJSON.flights> list16 = new List<ParsingClassJSON.flights>();
                                outbound2.FlightId = num6;
                                outbound2.Sequence = num7;
                                outbound2.TimeOfDepartuere = Convert.ToInt32(list15[0].SelectSingleNode("TOD").InnerText).ToString("#0000");
                                outbound2.equiptype = list15[0].SelectSingleNode("equiptype").InnerText;
                                outbound2.productdetailqualifier = list15[0].SelectSingleNode("productdetailqualifier").InnerText;
                                if (outbound2.TimeOfDepartuere.Length > 2)
                                {
                                    outbound2.TimeOfDepartuere = outbound2.TimeOfDepartuere.Insert(2, ":");
                                }
                                outbound2.TimeOfArrival = Convert.ToInt32(list15[list15.Count - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                if (outbound2.TimeOfArrival.Length > 2)
                                {
                                    outbound2.TimeOfArrival = outbound2.TimeOfArrival.Insert(2, ":");
                                }
                                outbound2.DateOfDeparture = list15[0].SelectSingleNode("DOD").InnerText;
                                if (outbound2.DateOfDeparture.Length > 2)
                                {
                                    outbound2.DateOfDeparture = outbound2.DateOfDeparture.Insert(2, "-");
                                    outbound2.DateOfDeparture = outbound2.DateOfDeparture.Insert(5, "-");
                                }
                                outbound2.DateOfArrival = list15[list15.Count - 1].SelectSingleNode("DOA").InnerText;
                                if (outbound2.DateOfArrival.Length > 2)
                                {
                                    outbound2.DateOfArrival = outbound2.DateOfArrival.Insert(2, "-");
                                    outbound2.DateOfArrival = outbound2.DateOfArrival.Insert(5, "-");
                                }
                                outbound2.FlightDuration = list15[0].SelectSingleNode("flighttime").InnerText;
                                if (outbound2.FlightDuration.Length > 2)
                                {
                                    outbound2.FlightDuration = outbound2.FlightDuration.Insert(2, "h ");
                                    outbound2.FlightDuration = outbound2.FlightDuration.Insert(outbound2.FlightDuration.Length, "m");
                                }
                                outbound2.Stop = list15.Count;
                                outbound2.Source = list15[0].SelectSingleNode("SrcLocation").InnerText;
                                outbound2.Destination = list15[list15.Count - 1].SelectSingleNode("DstLocation").InnerText;
                                rowArray = set.Tables[0].Select("AirPortCode ='" + outbound2.Source + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row19 = rowArray[0];
                                    outbound2.SourceCity = row19["City"].ToString();
                                }
                                rowArray = set.Tables[0].Select("AirPortCode ='" + outbound2.Destination + "'");
                                if (rowArray.Count<DataRow>() > 0)
                                {
                                    DataRow row20 = rowArray[0];
                                    outbound2.DestinationCity = row20["City"].ToString();
                                }
                                DataRow[] rowArray5 = set2.Tables[0].Select("AirLineCode ='" + list15[0].SelectSingleNode("marketingCarrier").InnerText + "'");
                                if (rowArray5.Count<DataRow>() > 0)
                                {
                                    DataRow row21 = rowArray5[0];
                                    outbound2.FlightName = row21["AirLine"].ToString();
                                }
                                int num9 = 0;
                                XmlNode node33 = node20.SelectSingleNode("FareDetails");
                                foreach (XmlNode node34 in list15)
                                {
                                    if (str14 == "")
                                    {
                                        str14 = node34.SelectSingleNode("FlightClass").InnerText;
                                    }
                                    ParsingClassJSON.flights flights4 = new ParsingClassJSON.flights
                                    {
                                        RefNo = node34.SelectSingleNode("refno").InnerText,
                                        TimeOfArrival = Convert.ToInt32(node34.SelectSingleNode("TOA").InnerText).ToString("#0000"),
                                        TimeOfDepartuere = Convert.ToInt32(node34.SelectSingleNode("TOD").InnerText).ToString("0000")
                                    };
                                    if (flights4.TimeOfArrival.Length > 2)
                                    {
                                        flights4.TimeOfArrival = flights4.TimeOfArrival.Insert(2, ":");
                                    }
                                    if (flights4.TimeOfDepartuere.Length > 2)
                                    {
                                        flights4.TimeOfDepartuere = flights4.TimeOfDepartuere.Insert(2, ":");
                                    }
                                    flights4.DateOfDeparture = node34.SelectSingleNode("DOD").InnerText;
                                    flights4.DateOfArrival = node34.SelectSingleNode("DOA").InnerText;
                                    if (flights4.DateOfDeparture.Length > 2)
                                    {
                                        flights4.DateOfDeparture = flights4.DateOfDeparture.Insert(2, "-");
                                        flights4.DateOfDeparture = flights4.DateOfDeparture.Insert(5, "-");
                                    }
                                    if (flights4.DateOfArrival.Length > 2)
                                    {
                                        flights4.DateOfArrival = flights4.DateOfArrival.Insert(2, "-");
                                        flights4.DateOfArrival = flights4.DateOfArrival.Insert(5, "-");
                                    }
                                    flights4.FlightDuration = node34.SelectSingleNode("flighttime").InnerText;
                                    if (flights4.FlightDuration.Length > 2)
                                    {
                                        flights4.FlightDuration = flights4.FlightDuration.Insert(2, "h ");
                                        flights4.FlightDuration = flights4.FlightDuration.Insert(flights4.FlightDuration.Length, "m");
                                    }
                                    flights4.MarketingCarrier = node34.SelectSingleNode("marketingCarrier").InnerText;
                                    rowArray5 = set2.Tables[0].Select("AirLineCode ='" + flights4.MarketingCarrier + "'");
                                    if (rowArray5.Count<DataRow>() > 0)
                                    {
                                        DataRow row22 = rowArray5[0];
                                        flights4.FlightName = row22["AirLine"].ToString();
                                        flights4.Image = row22["Image"].ToString();
                                    }
                                    else
                                    {
                                        flights4.FlightName = "";
                                        flights4.Image = "";
                                    }
                                    flights4.OperationCarrier = node34.SelectSingleNode("operatingCarrier").InnerText;
                                    flights4.FlightNo = node34.SelectSingleNode("flightno").InnerText;
                                    flights4.EquipType = node34.SelectSingleNode("equiptype").InnerText;
                                    XmlNode node35 = node34.SelectSingleNode("SrcLocation");
                                    if (node35 != null)
                                    {
                                        flights4.Source = node35.SelectSingleNode("Source").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights4.Source + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row23 = rowArray[0];
                                            flights4.SourceCity = row23["City"].ToString();
                                        }
                                        if (node35.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights4.SourceTerminal = " T-" + node35.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights4.SourceTerminal = "";
                                        }
                                    }
                                    XmlNode node36 = node34.SelectSingleNode("DstLocation");
                                    if (node36 != null)
                                    {
                                        flights4.Destination = node36.SelectSingleNode("Destination").InnerText;
                                        rowArray = set.Tables[0].Select("AirPortCode ='" + flights4.Destination + "'");
                                        if (rowArray.Count<DataRow>() > 0)
                                        {
                                            DataRow row24 = rowArray[0];
                                            flights4.DestinationCity = row24["City"].ToString();
                                        }
                                        if (node36.SelectSingleNode("Terminal").InnerText != "")
                                        {
                                            flights4.DestinationTerminal = " T-" + node36.SelectSingleNode("Terminal").InnerText;
                                        }
                                        else
                                        {
                                            flights4.DestinationTerminal = "";
                                        }
                                    }
                                    if (num9 > 0)
                                    {
                                        DateTime time7 = DateTime.Parse(flights4.DateOfDeparture + " " + flights4.TimeOfDepartuere, new CultureInfo("en-GB"));
                                        string str15 = Convert.ToInt32(list15[num9 - 1].SelectSingleNode("TOA").InnerText).ToString("#0000");
                                        if (str15.Length > 2)
                                        {
                                            str15 = str15.Insert(2, ":");
                                        }
                                        DateTime time8 = DateTime.Parse(list15[num9 - 1].SelectSingleNode("DOA").InnerText.Insert(2, "-").Insert(5, "-") + " " + str15, new CultureInfo("en-GB"));
                                        string str17 = time7.Subtract(time8).Hours.ToString("#00") + ":" + time7.Subtract(time8).Minutes.ToString("#00");
                                        flights4.Duration = str17;
                                    }
                                    if (node33 != null)
                                    {
                                        XmlNode node37 = node33.SelectSingleNode("Adult");
                                        XmlNodeList list17 = node37.SelectNodes("FareDetail");
                                        int num10 = list17.Count;
                                        if (node37 != null)
                                        {
                                            XmlNode node38 = node37.SelectSingleNode("Fare");
                                            flights4.msgQual = node38.SelectSingleNode("msgQual").InnerText;
                                            flights4.CabinClass = str14;
                                            flights4.Class = list17.Item((num10 - list15.Count) + num9).SelectSingleNode("rbd").InnerText;
                                        }
                                    }
                                    list16.Add(flights4);
                                    num9++;
                                }
                                outbound2.Flights = list16;
                                outbound2.equiptype = outbound.equiptype;
                                outbound2.FareAmt = outbound.FareAmt;
                                outbound2.TaxAmt = outbound.TaxAmt;
                                outbound2.CurrencyCode = currencyCode;
                                outbound2.cFareAmt = outbound.cFareAmt;
                                outbound2.cTaxAmt = outbound.cTaxAmt;
                                outbound2.cYQ = outbound.cYQ;
                                outbound2.iFareAmt = outbound.iFareAmt;
                                outbound2.iTaxAmt = outbound.iTaxAmt;
                                outbound2.CabinClass = outbound.CabinClass;
                                outbound2.FareBasis = outbound.FareBasis;
                                outbound2.Class = outbound.Class;
                                outbound2.productdetailqualifier = outbound.productdetailqualifier;
                                collection.Add(outbound2);
                            }
                        }
                        num6++;
                    }
                    list10.AddRange(collection);
                    try
                    {
                        str = new JavaScriptSerializer().Serialize(list10);
                    }
                    catch (Exception)
                    {
                        str = new JavaScriptSerializer { MaxJsonLength = 0x7fffffff }.Serialize(list10);
                    }
                }
                catch (Exception)
                {
                    flightJson = "''";
                }
                return str;
            }
            JavaScriptSerializer serializer5 = new JavaScriptSerializer();
            return serializer5.Serialize(null);
        }


    }
}