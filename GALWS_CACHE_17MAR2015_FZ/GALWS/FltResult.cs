﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using STD.DAL;
using System.Threading;
using System.Data;
using Microsoft.VisualBasic;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using LCCINTL;
using AirArabia;
using LCCRTFDISTR;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using STD.BAL.TBO;
using System.IO;
using STD.BAL.G8NAV;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using GALWS;
using STD.BAL._6ENAV420;
using STD.BAL.SGNAV420;

namespace STD.BAL
{
    public class FltResult : ITrans
    {
        HttpContext ctx;
        #region Constructor
        public FltResult(FlightSearch searchParam)
        {
            searchInputs = searchParam;
        }
        #endregion
        #region List and Variable Declaration
        // public string GdsReqXml { get; set; }
        //public string GdsResXml { get; set; }
        public string connectionString { get; set; }
        private List<CredentialList> CrdList;
        // private List<CredentialList> CPNCrdList;
        private FlightSearch searchInputs;
        private string BlockServiceList { get; set; }
        //FltRequest objFltReq;
        ArrayList objGoairFltResultList = new ArrayList();
        ArrayList objFltResultList = new ArrayList();
        ArrayList objFltResultList_O = new ArrayList();
        ArrayList objFltResultList_R = new ArrayList();
        List<FltSrvChargeList> SrvChargeList;
        List<FlightCityList> CityList;
        List<AirlineList> Airlist;
        List<MISCCharges> MiscList = new List<MISCCharges>();
        DataSet MarkupDs = new DataSet();
        float srvCharge = 0;
        ArrayList objIndigoLCCRTList = new ArrayList();
        ArrayList objIndigoDomLCCRTList = new ArrayList();
        ArrayList objAirVenturaFltResultList = new ArrayList();
        ArrayList objAirCostaFltResultList = new ArrayList();
        ICacheManager objCacheManager = CacheFactory.GetCacheManager("MyCacheManager");
        FlightCommonBAL objFlightCommonBAL = new FlightCommonBAL();
        List<FareTypeSettings> FareTypeSettingsList;


        DataTable PFCodeDt;
        DataTable PFCodeDt_LCC;
        private List<SearchResultBlock> SearchBlockList;
        private List<FlightFareType> FareTypeMasterList;
        #endregion
        #region Calling Normal and Coupon Result
        public T ProcessRequest_OLD<T>() where T : class
        {
            ArrayList Finallist = new ArrayList();
            try
            {

                ctx = HttpContext.Current;
                #region Citylist and Airlinelist
                Credentials objCrd = new Credentials(connectionString);
                FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");
                ArrayList objFltResultList = new ArrayList();
                CrdList = objCrd.GetServiceCredentials("");
                BlockServiceList = Data.GetBlockServices(connectionString, searchInputs.UID, searchInputs.Trip.ToString());
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), connectionString);// Get Data From DB or Cache
                FltDeal objDeal = new FltDeal(connectionString);
                PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                //FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");

                if (objCacheManager.Contains("CityList"))
                {
                    CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");

                }
                else
                {
                    CityList = Data.GetCityList("I", connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
                }
                if (objCacheManager.Contains("Airlist"))
                {
                    Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
                }
                else
                {
                    Airlist = Data.GetAirlineList(connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
                }
                #endregion
                #region Markup
                string Provider = "";
                Provider = searchInputs.Provider;
                //MarkupDs = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.Trip.ToString()); //"MML1","DI1","DI","DI1"
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();
                //Calculation For AgentMarkUp
                dtAgentMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                //Calculation For AdminMarkUp'
                dtAdminMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "AD");
                dtAdminMarkup.TableName = "AdminMarkUp";
                MarkupDs.Tables.Add(dtAdminMarkup);
                #endregion
                #region Misc Charges
                FlightCommonBAL objFltComm = new FlightCommonBAL(connectionString);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }
                #endregion
                #region Check Airline
                string AirCode = "";
                if (searchInputs.HidTxtAirLine.Length > 1) { AirCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
                // to add other not listed flight.
                if (Provider.Trim().ToUpper() == "OF" || searchInputs.HidTxtAirLine == "2T" || searchInputs.HidTxtAirLine == "LB" || searchInputs.HidTxtAirLine == "VA" || searchInputs.HidTxtAirLine == "OD" || searchInputs.HidTxtAirLine == "KB" || searchInputs.HidTxtAirLine == "OP" || searchInputs.HidTxtAirLine == "FZ")
                {
                    AirCode = "OF";
                }
                else if (Provider.Trim().ToUpper() == "OF")
                {


                }

                #endregion
                #region List Creation
                DataRow[] PFRow = null;
                try
                {
                    PFRow = PFCodeDt.Select("AppliedOn <>'BOOK'");
                }
                catch { }


                ArrayList vlist1 = new ArrayList();
                ArrayList vlist = new ArrayList();
                bool RTFare = false;
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType != "CRP" && x.CrdType != "CPN") && x.Provider == AirCode && x.Status == true).ToList();
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType != "CRP" && x.CrdType != "CPN" && x.CrdType != "SME") && x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == "NRM") && x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();

                #region Get GDS Corporate credential
                List<CredentialList> Crd1GCRP = null;
                try
                {
                    if (Convert.ToString(searchInputs.Trip) == "D")
                    {

                        if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == searchInputs.HidTxtAirLine.ToString() && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                        else
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                        if (Crd1GCRP.Count < 1)
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                    }
                }
                catch (Exception ex)
                { }
                #endregion

                #region Get Search Airline Block List
                List<SearchResultBlock> ResultBLockList = null;
                List<SearchResultBlock> ResultExcludeList = null;

                List<SearchResultBlock> ResultBLockListHandBag = null;
                List<SearchResultBlock> ResultBLockListSMEFare = null;
                try
                {
                    //Block1
                    SearchBlockList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "");
                    if (searchInputs.HidTxtAirLine.Length > 1 && (AirCode == "SG" || AirCode == "6E" || AirCode == "G8"))
                    {
                        ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        if (ResultExcludeList == null || ResultExcludeList.Count < 1)
                        {
                            ResultBLockList = SearchBlockList.Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        }
                        #region Hand Bag Fare and SME Fare List
                        ResultBLockListHandBag = SearchBlockList.Where(x => x.FareType == "HANDBAG" && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        ResultBLockListSMEFare = SearchBlockList.Where(x => x.FareType == "SMEFARE" && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        #endregion
                    }
                }
                catch (Exception ex)
                { }
                #endregion

                ConcurrentBag<Task> taskList = new ConcurrentBag<Task>();


                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    RTFare = true;
                    // SearchBlockList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "");                    
                    switch (AirCode)
                    {
                        case "SG":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                if (CrdListCPN[0].ServerIP == "V4")
                                {
                                    taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(CrdListCPN[0])));
                                    Thread.Sleep(200);
                                    taskList.Add(Task.Run(() => SpicejetAvailabilityV4_R(CrdListCPN[0])));
                                    //#region Baggage
                                    //if (searchInputs.Trip.ToString() == "D")
                                    //{
                                    //    Thread.Sleep(200);
                                    //    taskList.Add(Task.Run(() => SpicejetAvailabilityBAGV4_O(CrdListCPN[0])));
                                    //    Thread.Sleep(200);
                                    //    taskList.Add(Task.Run(() => SpicejetAvailabilityBAGV4_R(CrdListCPN[0])));
                                    //}
                                    //#endregion
                                }
                                else
                                {
                                    taskList.Add(Task.Run(() => SpicejetAvailability_O(CrdListCPN[0])));
                                    Thread.Sleep(200);
                                    taskList.Add(Task.Run(() => SpicejetAvailability_R(CrdListCPN[0])));
                                    #region Baggage
                                    if (searchInputs.Trip.ToString() == "D")
                                    {
                                        //Thread thSG_BO = new Thread(() => SpicejetAvailabilityBAG_O(CrdListCPN[0]));//new Thread(new ThreadStart(SpicejetAvailability_O));
                                        //thSG_BO.Start();
                                        //vlist.Add(thSG_BO);
                                        //vlist1.Add(DateTime.Now);

                                        //Thread thSG_BR = new Thread(() => SpicejetAvailabilityBAG_R(CrdListCPN[0]));//new Thread(new ThreadStart(SpicejetAvailability_O));
                                        //thSG_BR.Start();
                                        //vlist.Add(thSG_BR);
                                        //vlist1.Add(DateTime.Now);
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => SpicejetAvailabilityBAG_O(CrdListCPN[0])));
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => SpicejetAvailabilityBAG_R(CrdListCPN[0])));
                                    }
                                    #endregion
                                }
                            }




                            break;
                        case "6E":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                if (CrdListCPN[0].ServerIP == "V4")
                                {
                                    #region 6ENAV4
                                    taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(CrdListCPN[0])));
                                    Thread.Sleep(200);
                                    taskList.Add(Task.Run(() => IndigoAvailabilityV4_R(CrdListCPN[0])));

                                    if (searchInputs.Trip.ToString() == "D")
                                    {

                                        //#region Baggage
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAGV4_O(CrdListCPN[0])));
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAGV4_R(CrdListCPN[0])));
                                        //#endregion

                                        //#region SME FARE
                                        //if (ResultBLockListSMEFare == null || ResultBLockListSMEFare.Count < 1)
                                        //{
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFareV4_O(CrdListCPN[0])));
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFareV4_R(CrdListCPN[0])));
                                        //}
                                        //#endregion

                                    }
                                    #endregion
                                }
                                else
                                {
                                    taskList.Add(Task.Run(() => IndigoAvailability_O(CrdListCPN[0])));
                                    Thread.Sleep(200);
                                    taskList.Add(Task.Run(() => IndigoAvailability_R(CrdListCPN[0])));

                                    if (searchInputs.Trip.ToString() == "D")
                                    {

                                        //#region Baggage
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAG_O(CrdListCPN[0])));
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAG_R(CrdListCPN[0])));
                                        //#endregion

                                        //#region SME FARE
                                        //if (ResultBLockListSMEFare == null || ResultBLockListSMEFare.Count < 1)
                                        //{
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFare_O(CrdListCPN[0])));
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFare_R(CrdListCPN[0])));
                                        //}
                                        //#endregion

                                    }
                                }


                            }

                            break;
                        case "G8":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                taskList.Add(Task.Run(() => G8Availability_O(CrdListCPN[0])));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_R(CrdListCPN[0])));


                            }
                            break;
                        case "IX":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => G8Availability_O_old(CrdListCPN[0])));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_R_old(CrdListCPN[0])));


                            }
                            break;
                        case "AK":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => AirAsiaAvailability_O(CrdListCPN[0])));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => AirAsiaAvailability_R(CrdListCPN[0])));
                            }
                            break;

                        default:
                            #region check Search  Result Block or Not Block in GDS

                            bool NrmResult = true;
                            List<SearchResultBlock> GDSResultBLockList_NRM;
                            bool CrpResult = true;
                            List<SearchResultBlock> GDSResultBLockList_CRP;
                            if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                            {
                                try
                                {
                                    ResultExcludeList = null;
                                    List<SearchResultBlock> ResultExcludeList_NRM;
                                    List<SearchResultBlock> ResultExcludeList_CRP;
                                    ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    ResultExcludeList_NRM = ResultExcludeList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    ResultExcludeList_CRP = ResultExcludeList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();

                                    //ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    GDSResultBLockList_NRM = SearchBlockList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    if (GDSResultBLockList_NRM != null && GDSResultBLockList_NRM.Count > 0 && (ResultExcludeList_NRM == null || ResultExcludeList_NRM.Count < 1))
                                    {
                                        NrmResult = false;
                                    }

                                    GDSResultBLockList_CRP = SearchBlockList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    if (GDSResultBLockList_CRP != null && GDSResultBLockList_CRP.Count > 0 && (ResultExcludeList_CRP == null || ResultExcludeList_CRP.Count < 1))
                                    {
                                        CrpResult = false;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }


                            }
                            #endregion
                            if (NrmResult == true)//&& AirCode=="DEVESH"
                            {


                                taskList.Add(Task.Run(() => GDSAvailability_O(false, "", "", false, searchInputs)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => GDSAvailability_R(false, "", "")));
                            }

                            if (CrpResult == true && (PFRow != null && PFRow.Count() > 0) || (Crd1GCRP != null && Crd1GCRP.Count > 0))
                            {

                                //Thread.Sleep(200);
                                //taskList.Add(Task.Run(() => GDSAvailability_O(true,"","")));
                                //Thread.Sleep(200);
                                //taskList.Add(Task.Run(() => GDSAvailability_R(true,"","")));

                                #region Call Service according to Deal Code in GDS 17-01-2019
                                try
                                {
                                    DataRow[] PCRow = { };
                                    PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");
                                    if (PCRow.Count() <= 0)
                                    {
                                        PCRow = PFCodeDt.Select("AirCode='ALL'   and   AppliedOn = 'BOTH'");
                                        //PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "'  or AirCode='ALL'");
                                    }
                                    if (PCRow.Count() > 0)
                                    {
                                        for (int i = 0; i < PCRow.Count(); i++)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                {
                                                    string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                    string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => GDSAvailability_R(true, D_T_Code, AppliedOn)));
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O-GDSAvailability_R");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                }
                                #endregion
                            }



                            break;
                    }
                }
                else
                {
                    switch (AirCode)
                    {
                        //case "G9":
                        //    // AirArabiaAvailability_O();
                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }

                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        case "6E":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                if (CrdListCPN[0].ServerIP == "V4")
                                {
                                    #region 6ENAV4
                                    taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(CrdListCPN[0])));
                                    if (searchInputs.Trip.ToString() == "D")
                                    {

                                        //#region Baggage
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAGV4_O(CrdListCPN[0])));
                                        //#endregion

                                        //#region SME Fare
                                        //if (ResultBLockListSMEFare == null || ResultBLockListSMEFare.Count < 1)
                                        //{
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFareV4_O(CrdListCPN[0])));
                                        //}
                                        //#endregion
                                    }
                                    #endregion
                                }
                                else
                                {
                                    taskList.Add(Task.Run(() => IndigoAvailability_O(CrdListCPN[0])));
                                    if (searchInputs.Trip.ToString() == "D")
                                    {

                                        //#region Baggage
                                        //Thread.Sleep(200);
                                        //taskList.Add(Task.Run(() => IndigoAvailabilityBAG_O(CrdListCPN[0])));
                                        //#endregion

                                        //#region SME Fare
                                        //if (ResultBLockListSMEFare == null || ResultBLockListSMEFare.Count < 1)
                                        //{
                                        //    Thread.Sleep(200);
                                        //    taskList.Add(Task.Run(() => IndigoAvailabilitySMEFare_O(CrdListCPN[0])));
                                        //}
                                        //#endregion
                                    }
                                }
                            }

                            break;
                        case "SG":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                if (CrdListCPN[0].ServerIP == "V4")
                                {
                                    taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(CrdListCPN[0])));
                                    //#region Baggage
                                    //if (searchInputs.Trip.ToString() == "D")
                                    //{
                                    //    Thread.Sleep(200);
                                    //    taskList.Add(Task.Run(() => SpicejetAvailabilityBAGV4_O(CrdListCPN[0])));
                                    //}
                                    //#endregion
                                }
                                else
                                {
                                    //SpicejetAvailability_O(CrdListCPN[0]);
                                    taskList.Add(Task.Run(() => SpicejetAvailability_O(CrdListCPN[0])));
                                    //#region Baggage
                                    //if (searchInputs.Trip.ToString() == "D")
                                    //{
                                    //    Thread.Sleep(200);
                                    //    //SpicejetAvailabilityBAG_O(CrdListCPN[0]);
                                    //    taskList.Add(Task.Run(() => SpicejetAvailabilityBAG_O(CrdListCPN[0])));
                                    //}
                                    //#endregion
                                }
                            }
                            //if (Provider.Trim().ToUpper() == "TB")
                            //    TBOAvailability_O(true);
                            //else if (Provider.Trim().ToUpper() == "YA")
                            //    YAAvailability_O(true);
                            break;
                        case "G8":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1) && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                //G8Availability_O(CrdListCPN[0]);
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_O(CrdListCPN[0])));
                            }

                            break;
                        case "IX":
                            //if (ResultBLockList == null || ResultBLockList.Count < 1)
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                //G8Availability_O(CrdListCPN[0]);
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_O_old(CrdListCPN[0])));
                            }

                            break;
                        case "AK":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => AirAsiaAvailability_O(CrdListCPN[0])));
                            }
                            break;
                        case "G9":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => AirArabiaAvailabilityReq(CrdListCPN[0])));
                            }
                            break;
                        //case "LB":

                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }

                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "VA":
                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        // YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "FZ":

                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "2T":
                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }

                        //    break;

                        //case "OF":
                        //    //if (Provider.Trim().ToUpper() == "TB")
                        //    //TBOAvailability_O(true);
                        //    taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    //else if (Provider.Trim().ToUpper() == "YA")
                        //    //    YAAvailability_O(true);
                        //    break;

                        default:
                            //GDSAvailability_O(false);
                            #region check Search  Result Block or Not Block in GDS

                            bool NrmResult = true;
                            List<SearchResultBlock> GDSResultBLockList_NRM;
                            bool CrpResult = true;
                            List<SearchResultBlock> GDSResultBLockList_CRP;
                            if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                            {
                                ResultExcludeList = null;
                                List<SearchResultBlock> ResultExcludeList_NRM;
                                List<SearchResultBlock> ResultExcludeList_CRP;
                                ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                ResultExcludeList_NRM = ResultExcludeList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                ResultExcludeList_CRP = ResultExcludeList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();


                                GDSResultBLockList_NRM = SearchBlockList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                if (GDSResultBLockList_NRM != null && GDSResultBLockList_NRM.Count > 0 && (ResultExcludeList_NRM == null || ResultExcludeList_NRM.Count < 1))
                                {
                                    NrmResult = false;
                                }

                                GDSResultBLockList_CRP = SearchBlockList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                if (GDSResultBLockList_CRP != null && GDSResultBLockList_CRP.Count > 0 && (ResultExcludeList_CRP == null || ResultExcludeList_CRP.Count < 1))
                                {
                                    CrpResult = false;
                                }
                            }
                            #endregion

                            if (NrmResult == true)// && AirCode == "DEVESH")
                            {
                                //Thread thGDS_O = new Thread(() => GDSAvailability_O(false));//new Thread(new ThreadStart());
                                //thGDS_O.Start();
                                //vlist.Add(thGDS_O);
                                //vlist1.Add(DateTime.Now);
                                taskList.Add(Task.Run(() => GDSAvailability_O(false, "", "", false, searchInputs)));
                                Thread.Sleep(200);
                            }

                            if (CrpResult == true && ((PFRow != null && PFRow.Count() > 0) || ((Crd1GCRP != null && Crd1GCRP.Count > 0) && Convert.ToString(searchInputs.Trip) == "D")))
                            {
                                if (Convert.ToString(searchInputs.Trip) == "I" && PFRow != null && PFRow.Count() > 0)
                                {
                                    #region Call corporate 1 -- Comment by devesh 17-01-2019
                                    //if (string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                                    //{
                                    //    searchInputs.HidTxtAirLine = "9W";
                                    //    searchInputs.AirLine = "Jet Airways,9W";
                                    //}
                                    //Thread.Sleep(200);
                                    //taskList.Add(Task.Run(() => GDSAvailability_O(true, "", "")));
                                    //Thread.Sleep(300);
                                    //if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine)) //&& PFRow != null && PFRow.Count() > 0)
                                    //{
                                    //    searchInputs.HidTxtAirLine = "";
                                    //    searchInputs.AirLine = "";
                                    //}
                                    #endregion

                                    #region Call Service according to Deal Code in GDS 17-01-2019
                                    bool IsHidAirLineClear = true; ;
                                    try
                                    {
                                        DataRow[] PCRow = { };
                                        if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                                        {
                                            PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");

                                            IsHidAirLineClear = false;
                                        }
                                        else
                                        {
                                            PCRow = PFCodeDt.Select("AirCode<>'ALL' and AppliedOn = 'BOTH'");
                                        }

                                        // FlightSearch ObjfltSearch = (FlightSearch) searchInputs.Clone();
                                        if (PCRow.Count() > 0)
                                        {
                                            for (int i = 0; i < PCRow.Count(); i++)
                                            {
                                                try
                                                {
                                                    FlightSearch searchInputsClone = (FlightSearch)searchInputs.Clone();
                                                    if (string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                                                    {
                                                        //searchInputs.HidTxtAirLine = "9W";
                                                        //searchInputs.AirLine = "Jet Airways,9W";
                                                        searchInputsClone.HidTxtAirLine = Convert.ToString(PCRow[i]["AirCode"]);//"9W";
                                                        searchInputsClone.AirLine = Convert.ToString(PCRow[i]["AirlineName"]) + "," + Convert.ToString(PCRow[i]["AirCode"]); //"Jet Airways,9W";
                                                    }

                                                    if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                    {
                                                        string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                        string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, true, searchInputsClone)));
                                                        Thread.Sleep(200);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_CRP_INT");
                                                }
                                                //if (IsHidAirLineClear == true)
                                                //{
                                                //    searchInputs.HidTxtAirLine = "";
                                                //    searchInputs.AirLine = "";
                                                //}
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                    }
                                    #endregion
                                }
                                else
                                {
                                    //Thread thGDS_O1 = new Thread(() => GDSAvailability_O(true));//new Thread(new ThreadStart());
                                    //thGDS_O1.Start();
                                    //vlist.Add(thGDS_O1);
                                    //vlist1.Add(DateTime.Now);
                                    #region Call corporate  -- Comment by devesh 17-01-2019
                                    //taskList.Add(Task.Run(() => GDSAvailability_O(true, "", "")));
                                    //Thread.Sleep(200);
                                    #endregion 

                                    #region Call Service according to Deal Code in GDS 17-01-2019
                                    try
                                    {
                                        DataRow[] PCRow = { };
                                        PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");
                                        if (PCRow.Count() <= 0)
                                        {
                                            PCRow = PFCodeDt.Select("AirCode='ALL'   and   AppliedOn = 'BOTH'");
                                            //PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "'  or AirCode='ALL'");
                                        }
                                        if (PCRow.Count() > 0)
                                        {
                                            // Call 1G Service deal code wise
                                            for (int i = 0; i < PCRow.Count(); i++)
                                            {
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                    {
                                                        string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                        string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                        taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                                        Thread.Sleep(200);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_CRP");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //deal code not avilable only  corporate credential only
                                            string D_T_Code = "";
                                            string AppliedOn = "";
                                            taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                            Thread.Sleep(200);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                    }
                                    #endregion

                                }
                            }
                            //TBOAvailability_O(false);
                            break;
                    }

                }
                #endregion

                Task.WhenAll(taskList).Wait();
                //#region Wait for Thread
                //int counter = 0;
                //while ((counter < vlist.Count))
                //{
                //    Thread TH = (Thread)vlist[counter];
                //    if ((TH.ThreadState == ThreadState.WaitSleepJoin))
                //    {
                //        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                //        if ((DIFF.Seconds > 45))
                //        {
                //            TH.Abort();
                //            counter += 1;
                //        }
                //    }
                //    else if ((TH.ThreadState == ThreadState.Stopped))
                //    {
                //        counter += 1;
                //    }
                //}
                //#endregion
                #region Add Different Result Lists Here
                //Merge into 1 List with Consecutive Line No.s
                if (objFltResultList_O.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_O);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_O);

                }
                if (objFltResultList_R.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_R);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_R);

                }

                #endregion
                Finallist = objFlightCommonBAL.MergeResultList(objFltResultList);
                //Insert Cache
                //Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //thread.Start();

                //if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF" && (AirCode == "6E" || AirCode == "SG" || AirCode == "G8"))
                if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF" && (string.IsNullOrEmpty(searchInputs.Cabin.Trim()) || searchInputs.Cabin.Trim() == "Y"))
                {
                    Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                    thread.Start();
                }





                //Insert Cache
                //Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //thread.Start();

                //if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF")
                //{
                //    Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //    thread.Start();
                //}

                //string s = GetEntityXml(objFltResultList);
                //try
                //{
                //    File.AppendAllText("C:\\\\CPN_SP\\\\Result1G_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + s + Environment.NewLine);
                //}
                //catch (Exception ex)
                //{

                //}
            }
            catch (Exception ex)
            {

                var abc = ex.Message.ToString();
                // WriteToDrive(@"C:\resulterror\" + DateTime.Now.ToString("dd-MM-yyyy") + "\\" + "", "" + "_" + DateTime.Now.ToString("hh_mm_ss_tt") + "_IATACommission_Request.txt", abc);
            }
            return Finallist as T;

        }
        public T ProcessRequestCoupon<T>() where T : class
        {
            ArrayList Finallist = new ArrayList();
            try
            {
                ctx = HttpContext.Current;
                ConcurrentBag<Task> taskList = new ConcurrentBag<Task>();
                string Provider = ""; Provider = searchInputs.Provider;
                Credentials objCrd = new Credentials(connectionString);
                ArrayList objFltResultList = new ArrayList();
                CrdList = objCrd.GetServiceCredentials("");
                //CPNCrdList = objCrd.GetCouponCRD();
                FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");

                //BlockServiceList = Data.GetBlockServices(connectionString, searchInputs.UID, searchInputs.Trip.ToString());
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), connectionString);// Get Data From DB or Cache

                FltDeal objDeal = new FltDeal(connectionString);
                PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                if (objCacheManager.Contains("CityList"))
                {
                    CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");
                }
                else
                {
                    CityList = Data.GetCityList("I", connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
                }
                if (objCacheManager.Contains("Airlist"))
                {
                    Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
                }
                else
                {
                    Airlist = Data.GetAirlineList(connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
                }


                //MarkupDs = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.Trip.ToString()); //"MML1","DI1","DI","DI1"
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();
                //Calculation For AgentMarkUp
                dtAgentMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                //Calculation For AdminMarkUp'
                dtAdminMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "AD");
                dtAdminMarkup.TableName = "AdminMarkUp";
                MarkupDs.Tables.Add(dtAdminMarkup);
                #region Misc Charges
                FlightCommonBAL objFltComm = new FlightCommonBAL(connectionString);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }
                #endregion
                string AirCode = "";
                if (searchInputs.HidTxtAirLine.Length > 1) { AirCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
                ArrayList vlist1 = new ArrayList();
                ArrayList vlist = new ArrayList();
                #region List Creation
                bool RTFare = false;
                HttpContext context = HttpContext.Current;
                // List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == "CRP" || x.CrdType == "CPN") && x.Provider == Provider && x.Status == true).ToList();
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == "CRP" || x.CrdType == "CPN" || x.CrdType == "SME") && x.Provider == Provider && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                List<CredentialList> CrdListCPN = CrdList.Where(x => x.CrdType != "NRM" && x.Provider == Provider && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();

                #region Get Search Airline Block List
                List<SearchResultBlock> ResultBLockList = null;
                List<SearchResultBlock> ResultExcludeList = null;
                try
                {
                    SearchBlockList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "");
                    if (searchInputs.HidTxtAirLine.Length > 1 && (AirCode == "SG" || AirCode == "6E" || AirCode == "G8"))
                    {
                       // ResultBLockList = SearchBlockList.Where(x => (x.FareType == "CRP" || x.FareType == "CPN" || x.FareType == "SME") && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        ResultBLockList = SearchBlockList.Where(x => x.FareType != "NRM" && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        //ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        //if (ResultExcludeList == null || ResultExcludeList.Count < 1)
                        //{
                        //    ResultBLockList = SearchBlockList.Where(x => (x.FareType == "CRP" || x.FareType == "CPN") && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        //    //ResultBLockList = SearchBlockList.Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        //}

                    }
                }
                catch (Exception ex)
                { }
                #endregion

                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false)
                {
                    RTFare = true;
                    switch (Provider)
                    {
                        case "SG":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                //if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {
                                    CredentialList objCRD = CrdListCPN[p];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread thSGCPN_O = null;

                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    // thSGCPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(context, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_R(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        //thSGCPN_O = new Thread(() => SpicejetAvailability_O(CrdListCPN[p]));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_R(objCRD)));
                                                    }


                                                }
                                                //thSGCPN_O.Start();
                                                //vlist.Add(thSGCPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread thSGCPN_R = null;

                                                //if (CrdListCPN[p].ResultFrom == "SP")
                                                //{
                                                //    //thSGCPN_R = new Thread(() => CPNAvailability_R(context, CrdListCPN[p]));

                                                //}
                                                //else {
                                                //    //thSGCPN_R = new Thread(() => SpicejetAvailability_R(CrdListCPN[p]));

                                                //}

                                                //thSGCPN_R.Start();
                                                //vlist.Add(thSGCPN_R);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(40);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                            break;
                        case "6E":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                //if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {

                                    CredentialList objCRD = CrdListCPN[p];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                //if (ResultBLockList != null && ResultBLockList.Count > 0)
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                // Thread th6ECPN_O = null;
                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    // th6ECPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(context, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_R(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_R(objCRD)));
                                                    }

                                                }

                                                //th6ECPN_O.Start();
                                                //vlist.Add(th6ECPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread th6ECPN_R = null;
                                                //if (CrdListCPN[p].ResultFrom == "SP")
                                                //{
                                                //    //th6ECPN_R = new Thread(() => CPNAvailability_R(context, CrdListCPN[p]));

                                                //}
                                                //else 
                                                //{
                                                //    //th6ECPN_R = new Thread(() => IndigoAvailability_R(CrdListCPN[p]));

                                                //}

                                                //th6ECPN_R.Start();
                                                //vlist.Add(th6ECPN_R);
                                                //vlist1.Add(DateTime.Now);
                                                //Thread.Sleep(40);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }

                            //Thread th6ECORP_O = new Thread(new ThreadStart(IndigoCorpAvailability_O));
                            //th6ECORP_O.Start();
                            //vlist.Add(th6ECORP_O);
                            //vlist1.Add(DateTime.Now);

                            //Thread th6ECORP_R = new Thread(new ThreadStart(IndigoCorpAvailability_R));
                            //th6ECORP_R.Start();
                            //vlist.Add(th6ECORP_R);
                            //vlist1.Add(DateTime.Now);
                            break;

                        case "G8":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                //if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {

                                    CredentialList objCRD = CrdListCPN[p];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                //if (ResultBLockList != null && ResultBLockList.Count > 0)
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread thG8CPN_O = null;

                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    //thG8CPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(context, objCRD)));
                                                }
                                                else
                                                {
                                                    //thG8CPN_O = new Thread(() => G8Availability_O(CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_O(objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_R(objCRD)));
                                                }
                                                //thG8CPN_O.Start();
                                                //vlist.Add(thG8CPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread thG8CPN_R = null;

                                                //if (CrdListCPN[p].ResultFrom == "SP")
                                                //{
                                                //   // thG8CPN_R = new Thread(() => CPNAvailability_R(context, CrdListCPN[p]));

                                                //}
                                                //else 
                                                //{ 
                                                //    //thG8CPN_R = new Thread(() => G8Availability_R(CrdListCPN[p]));

                                                //}
                                                //thG8CPN_R.Start();
                                                //vlist.Add(thG8CPN_R);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(40);
                                            }
                                        }
                                        catch { }
                                    }


                                }
                            }

                            //Thread thG8CORP_O = new Thread(new ThreadStart(GoairCorpAvailability_O));
                            //thG8CORP_O.Start();
                            //vlist.Add(thG8CORP_O);
                            //vlist1.Add(DateTime.Now);

                            //Thread thG8CORP_R = new Thread(new ThreadStart(GoairCorpAvailability_R));
                            //thG8CORP_R.Start();
                            //vlist.Add(thG8CORP_R);
                            //vlist1.Add(DateTime.Now);
                            break;
                            //case "IX":
                            //    if (searchInputs.Infant == 0)
                            //    {
                            //        //Thread thIXCPN_O = new Thread(new ThreadStart(IXCpnAvailability_O));
                            //        //thIXCPN_O.Start();
                            //        //vlist.Add(thIXCPN_O);
                            //        //vlist1.Add(DateTime.Now);

                            //        //Thread thIXCPN_R = new Thread(new ThreadStart(IXCpnAvailability_R));
                            //        //thIXCPN_R.Start();
                            //        //vlist.Add(thIXCPN_R);
                            //        //vlist1.Add(DateTime.Now);

                            //        //taskList.Add(Task.Run(() => IXCpnAvailability_O()));
                            //        //taskList.Add(Task.Run(() => IXCpnAvailability_R()));
                            //    }
                            //    break;

                            //case "AK":
                            //    //Thread thAKCPN_O = new Thread(new ThreadStart(AirAsiaCpnAvailability_O));
                            //    //thAKCPN_O.Start();
                            //    //vlist.Add(thAKCPN_O);
                            //    //vlist1.Add(DateTime.Now);

                            //    //Thread thAKCPN_R = new Thread(new ThreadStart(AirAsiaCpnAvailability_R));
                            //    //thAKCPN_R.Start();
                            //    //vlist.Add(thAKCPN_R);
                            //    //vlist1.Add(DateTime.Now);
                            //    //taskList.Add(Task.Run(() => AirAsiaCpnAvailability_O()));
                            //    //taskList.Add(Task.Run(() => AirAsiaCpnAvailability_R()));
                            //    break;
                    }
                }
                else
                {
                    switch (Provider)
                    {
                        case "SG":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                // if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {

                                    CredentialList objCRD = CrdListCPN[p];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(ProcessRequestCoupon)", "Error_001", ex, "Check CPN and CRP result block");
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //STD.Shared.CredentialList cc = CrdListCPN[p];
                                                // Thread thSGCPN_O = null;

                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    //thSGCPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        //thSGCPN_O = new Thread(() => SpicejetAvailability_O(CrdListCPN[p]));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_O(objCRD)));
                                                    }
                                                }
                                                //thSGCPN_O.Start();
                                                //vlist.Add(thSGCPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(40);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                            break;
                        case "6E":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                //if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int e = 0; e < CrdListCPN.Count(); e++)
                                {


                                    CredentialList objCRD = CrdListCPN[e];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (e < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread th6ECPN_O = null;

                                                if (CrdListCPN[e].ResultFrom == "SP")
                                                {
                                                    //th6ECPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[e]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                    // taskList.Add(Task.Run(async () => await CPNAvailability_O(context, CrdListCPN[e])));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        //th6ECPN_O = new Thread(() => IndigoAvailability_O(CrdListCPN[e])); 
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_O(objCRD)));
                                                        //taskList.Add(Task.Run(async () => await IndigoAvailability_O(CrdListCPN[e])));
                                                    }



                                                }
                                                //th6ECPN_O.Start();
                                                //vlist.Add(th6ECPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(40);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                            //Thread th6ECORP_O = new Thread(new ThreadStart(IndigoCorpAvailability_O));
                            //th6ECORP_O.Start();
                            //vlist.Add(th6ECORP_O);
                            //vlist1.Add(DateTime.Now);

                            break;

                        case "G8":
                            if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                //if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                                for (int g = 0; g < CrdListCPN.Count(); g++)
                                {

                                    CredentialList objCRD = CrdListCPN[g];

                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (g < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread thG8CPN_O = null;
                                                if (CrdListCPN[g].ResultFrom == "SP")
                                                {
                                                    // thG8CPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[g]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(context, objCRD)));
                                                }
                                                else
                                                {
                                                    //thG8CPN_O = new Thread(() => G8Availability_O(CrdListCPN[g]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_O(objCRD)));
                                                }
                                                //thG8CPN_O.Start();
                                                //vlist.Add(thG8CPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(60);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }

                            //Thread thG8CORP_O = new Thread(new ThreadStart(GoairCorpAvailability_O));
                            //thG8CORP_O.Start();
                            //vlist.Add(thG8CORP_O);
                            //vlist1.Add(DateTime.Now);
                            break;
                            //case "IX":
                            //    if (searchInputs.Infant == 0)
                            //    {
                            //        //IXCpnAvailability_O();
                            //       // taskList.Add(Task.Run(() => IXCpnAvailability_O()));
                            //    }
                            //    break;
                            //case "AK":
                            //    //AirAsiaCpnAvailability_O();
                            //    taskList.Add(Task.Run(() => AirAsiaCpnAvailability_O()));
                            //    break;

                    }

                }
                #endregion

                Task.WhenAll(taskList).Wait();
                //#region Wait for Thread
                //int counter = 0;
                //while ((counter < vlist.Count))
                //{
                //    Thread TH = (Thread)vlist[counter];
                //    if ((TH.ThreadState == ThreadState.WaitSleepJoin))
                //    {
                //        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                //        if ((DIFF.Seconds > 60))
                //        {
                //            TH.Abort();
                //            counter += 1;
                //        }
                //    }
                //    else if ((TH.ThreadState == ThreadState.Stopped))
                //    {
                //        counter += 1;
                //    }
                //}
                //#endregion
                #region Add Different Result Lists Here
                if (objFltResultList_O.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_O);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_O);
                }
                if (objFltResultList_R.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_R);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_R);
                }
                #endregion
                Finallist = objFlightCommonBAL.MergeResultList(objFltResultList);

                //Thread th_Cache = new Thread(InsertCache);
                //th_Cache.Start(Finallist, AirCode);

                if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF" && (string.IsNullOrEmpty(searchInputs.Cabin.Trim()) || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                {
                    Thread thread = new Thread(() => InsertCache(Finallist, AirCode, "CPN"));
                    thread.Start();
                    //}
                }


                //Thread thread = new Thread(() => InsertCache(Finallist, AirCode, "CPN"));
                //thread.Start();
                //vlist.Add(th_Cache);
                //vlist1.Add(DateTime.Now);


                //string s = GetEntityXml(objFltResultList);
            }
            catch (Exception ex)
            {
            }
            return Finallist as T;
        }
        //private void InsertCache(ArrayList Finallist, string AirCode, string Type)
        //{
        //    try
        //    {
        //        string Response1 = "";
        //        if (((List<FlightSearchResults>)Finallist[0]).Count >= 1)
        //        {
        //            Response1 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[0]);
        //            CacheFareUpdate clsCache = new CacheFareUpdate(connectionString);
        //            if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
        //            {
        //                string Response2 = "";
        //                clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate);
        //                //if (((List<List<FlightSearchResults>>)Finallist).Count > 1)
        //                if (Finallist.Count > 1 && ((List<FlightSearchResults>)Finallist[1]).Count > 0)
        //                {
        //                    Response2 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[1]);
        //                    clsCache.InsertCache(searchInputs.HidTxtArrCity, searchInputs.HidTxtDepCity, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response2, searchInputs.RetDate);
        //                }
        //            }
        //            else
        //            {
        //                clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        private void InsertCache(ArrayList Finallist, string AirCode, string Type)
        {
            try
            {
                string Response1 = "";
                if (Type == "CPN")
                {

                    Response1 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[0]);
                    CacheFareUpdate clsCache = new CacheFareUpdate(connectionString);
                    if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                    {
                        string Response2 = "";
                        clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate, searchInputs.Cabin);
                        Response2 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[1]);
                        clsCache.InsertCache(searchInputs.HidTxtArrCity, searchInputs.HidTxtDepCity, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response2, searchInputs.RetDate, searchInputs.Cabin);

                    }
                    else
                    {
                        clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate, searchInputs.Cabin);
                    }


                }
                else
                {

                    if (((List<FlightSearchResults>)Finallist[0]).Count >= 1)
                    {
                        Response1 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[0]);
                        CacheFareUpdate clsCache = new CacheFareUpdate(connectionString);
                        if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                        {
                            string Response2 = "";
                            clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.DepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate, searchInputs.Cabin);
                            //if (((List<List<FlightSearchResults>>)Finallist).Count > 1)
                            if (Finallist.Count > 1 && ((List<FlightSearchResults>)Finallist[1]).Count > 0)
                            {
                                Response2 = Newtonsoft.Json.JsonConvert.SerializeObject(Finallist[1]);
                                clsCache.InsertCache(searchInputs.HidTxtArrCity, searchInputs.HidTxtDepCity, searchInputs.RetDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response2, searchInputs.RetDate, searchInputs.Cabin);
                            }
                        }
                        else
                        {
                            clsCache.InsertCache(searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.DepDate, searchInputs.RetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, searchInputs.GDSRTF, searchInputs.RTF, AirCode.Trim() + Type, Response1, searchInputs.DepDate, searchInputs.Cabin);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(FltResult-InsertCache)", "Error_001", ex, "InsertCache");
            }
        }

        #endregion
        #region Serialize to XML
        public string GetEntityXml(ArrayList Finallist)
        {
            XmlAttributeOverrides overrides = new XmlAttributeOverrides();
            XmlAttributes attr = new XmlAttributes();
            attr.XmlRoot = new XmlRootAttribute("XML");
            overrides.Add(typeof(List<FlightSearchResults>), attr);

            XmlDocument xmlDoc = new XmlDocument();
            XPathNavigator nav = xmlDoc.CreateNavigator();
            using (XmlWriter writer = nav.AppendChild())
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<FlightSearchResults>), overrides);
                List<FlightSearchResults> parameters = (List<FlightSearchResults>)((System.Collections.ArrayList)(Finallist[0]))[0];
                ser.Serialize(writer, parameters);
            }
            return xmlDoc.OuterXml.ToString();
        }
        #endregion
        #region GDS
        //mk done
        private async Task GDSAvailability_O(bool isPrivateFare, string DealCode, string AppliedOn, bool FlightSearchClone, FlightSearch searchInputsClone)
        {
            List<CredentialList> CrdListGDS = null;
            List<SearchResultBlock> ResultBlock1G = null;
            try
            {
                #region Call 1G API searchInputsClone
                Result_1G objRes1G = new Result_1G(connectionString);
                string FareType = "";
                if (isPrivateFare)
                {
                    FareType = "CRP";
                }
                else
                {
                    FareType = "NRM";
                }

                #region Get Search Airline Block List
                try
                {
                    if (Convert.ToString(searchInputsClone.Trip) == "I" && string.IsNullOrEmpty(searchInputsClone.HidTxtAirLine))
                    {
                        List<SearchResultBlock> ResultExcludeList = null;
                        List<SearchResultBlock> ResultBlock1GALL = null;
                        FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                        ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputsClone.Trip.ToString(), searchInputsClone.Provider, "", searchInputsClone.AgentType, searchInputsClone.UID, "", "Exclude").Where(x => x.FareType == FareType && x.Trip == searchInputsClone.Trip.ToString()).ToList();
                        //ResultBlock1G = SearchBlockList.Where(x => x.FareType == FareType && x.Trip == searchInputs.Trip.ToString()).ToList();
                        ResultBlock1GALL = SearchBlockList.Where(x => x.FareType == FareType && x.Trip == searchInputsClone.Trip.ToString()).ToList();
                        if (ResultBlock1GALL.Count > 0 && ResultExcludeList.Count > 0)
                        {
                            ResultBlock1G = ResultBlock1GALL.Where(p => !ResultExcludeList.Any(x => x.Airline == p.Airline)).ToList();
                        }
                        else
                        {
                            ResultBlock1G = SearchBlockList.Where(x => x.FareType == FareType && x.Trip == searchInputsClone.Trip.ToString()).ToList();
                        }
                    }

                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_O)", "Error_001", ex, "GetFlightResultBlockList");
                }
                #endregion

                try
                {
                    if (!string.IsNullOrEmpty(searchInputsClone.HidTxtAirLine))
                    {
                        CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == searchInputsClone.HidTxtAirLine.ToString() && x.Trip == searchInputsClone.Trip.ToString() && x.WebResult == true).ToList();
                    }
                    else
                    {
                        CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputsClone.Trip.ToString() && x.WebResult == true).ToList();
                    }
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_O)", "Error_001", ex, "CrdListGDS");
                }
                if (CrdListGDS.Count < 1)
                {

                    CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputsClone.Trip.ToString() && x.WebResult == true).ToList();
                }


                FltRequest objFltReq = new FltRequest(searchInputsClone, CrdListGDS);
                string url = "", userid = "", Pwd = "", ResultCount = "", CrdType = "";

                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (CrdListGDS.Count > 0)
                {
                    if (searchInputsClone.TripType1 == "rdbMultiCity" && searchInputsClone.TripType.ToString() == "MultiCity")
                    {
                        #region Multicity
                        url = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                        userid = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].UserID;
                        Pwd = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].Password;
                        ResultCount = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].SearchType;
                        CrdType = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].CrdType;


                        string perfareresultCount = "20";
                        try
                        {
                            perfareresultCount = ResultCount.Split(',')[1];
                        }
                        catch { perfareresultCount = "20"; }



                        t1 = Task.Run(() => objRes1G.GetFltResultMultiCity(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq.GDS1G(false, true, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn, ResultCount.Split(',')[0]), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, searchInputsClone.HidTxtAirLine, CrdType), searchInputsClone, "O", "OutBound", MiscList, perfareresultCount, CrdType, userid, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0]);


                        #endregion
                    }
                    else
                    {

                        url = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                        userid = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].UserID;
                        Pwd = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].Password;
                        CrdType = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].CrdType;
                        #region Dom
                        if (searchInputsClone.Trip.ToString() == "D")
                        {

                            if (searchInputsClone.GDSRTF == false)
                            {
                                t1 = Task.Run(() => objRes1G.GetFltResult(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq.GDS1G(false, false, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, searchInputsClone.HidTxtAirLine, CrdType), searchInputsClone, "O", "OutBound", MiscList, CrdType, userid, ctx));

                                var tt = Task.WhenAll(t1).Result;
                                objFltResultList_O.Add(tt[0]);

                            }
                            else
                            {

                                t1 = Task.Run(() => objRes1G.GetFltResultRoundTrip(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq.GDS1G(true, true, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, searchInputsClone.HidTxtAirLine, CrdType), searchInputsClone, "R", "InBound", MiscList, CrdType, userid, ctx));
                                var tt = Task.WhenAll(t1).Result;
                                objFltResultList_O.Add(tt[0]);
                            }
                        }
                        #endregion

                        #region Intl

                        else
                        {

                            if (searchInputsClone.TripType.ToString() == "OneWay")
                            {

                                t1 = Task.Run(() => objRes1G.GetFltResult(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq.GDS1G(false, false, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, "GDS", CrdType), searchInputsClone, "O", "OutBound", MiscList, CrdType, userid, ctx));
                                var tt = Task.WhenAll(t1).Result;
                                objFltResultList_O.Add(tt[0]);

                            }
                            else
                            {
                                t1 = Task.Run(() => objRes1G.GetFltResultRoundTrip(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq.GDS1G(true, true, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, "GDS", CrdType), searchInputsClone, "R", "InBound", MiscList, CrdType, userid, ctx));
                                var tt = Task.WhenAll(t1).Result;
                                objFltResultList_O.Add(tt[0]);


                            }
                        }
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_O)", "Error_001", ex, "GDSAvailability_O");
            }

        }
        private async Task GDSAvailability_R(bool isPrivateFare, string DealCode, string AppliedOn)
        {
            List<CredentialList> CrdListGDS = null;
            List<SearchResultBlock> ResultBlock1G = null;
            try
            {
                Result_1G objRes1G = new Result_1G(connectionString);

                string FareType = "";
                if (isPrivateFare)
                { FareType = "CRP"; }
                else
                { FareType = "NRM"; }

                #region Get Search Airline Block List
                try
                {
                    if (Convert.ToString(searchInputs.Trip) == "I" && string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                    {
                        List<SearchResultBlock> ResultExcludeList = null;
                        List<SearchResultBlock> ResultBlock1GALL = null;
                        FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                        ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == FareType && x.Airline == searchInputs.HidTxtAirLine && x.Trip == searchInputs.Trip.ToString()).ToList();
                        ResultBlock1GALL = SearchBlockList.Where(x => x.FareType == FareType && x.Trip == searchInputs.Trip.ToString()).ToList();
                        if (ResultBlock1GALL.Count > 0 && ResultExcludeList.Count > 0)
                        {
                            ResultBlock1G = ResultBlock1GALL.Where(p => !ResultExcludeList.Any(x => x.Airline == p.Airline)).ToList();
                        }
                        else
                        {
                            ResultBlock1G = SearchBlockList.Where(x => x.FareType == FareType && x.Trip == searchInputs.Trip.ToString()).ToList();
                        }
                    }
                }
                catch (Exception ex)
                { ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_R)", "Error_001", ex, "GetFlightResultBlockList"); }
                #endregion

                try
                {
                    if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                    {
                        CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == searchInputs.HidTxtAirLine.ToString() && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                    }
                    else
                    {
                        CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                    }
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_R)", "Error_001", ex, "CrdListGDS");
                }
                if (CrdListGDS.Count < 1)
                {

                    CrdListGDS = CrdList.Where(x => x.CrdType == FareType && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                }



                FltRequest objFltReq12 = new FltRequest(searchInputs, CrdListGDS);

                string url = "", userid = "", Pwd = "", CrdType = "";


                #region Dom
                if (searchInputs.Trip.ToString() == "D" && CrdListGDS.Count > 0)
                {
                    url = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                    userid = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].UserID;
                    Pwd = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].Password;
                    CrdType = ((from crd in CrdListGDS where crd.Provider == "1G" select crd).ToList())[0].CrdType;
                    if (searchInputs.GDSRTF == false)
                    {

                        if (searchInputs.TripType.ToString() == "RoundTrip")
                        {

                            await Task.Delay(0);
                            var t1 = (dynamic)null;
                            //t1 = Task.Run(() => objRes1G.GetFltResult(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq12.GDS1G(true, false, PFCodeDt, isPrivateFare, ResultBlock1G), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, searchInputs.HidTxtAirLine, "NRM"), searchInputs, "O", "InBound", MiscList, "NRM", userid,ctx));
                            t1 = Task.Run(() => objRes1G.GetFltResult(SrvChargeList, CityList, Airlist, MarkupDs, objFlightCommonBAL.PostXml(url, objFltReq12.GDS1G(true, false, PFCodeDt, isPrivateFare, ResultBlock1G, DealCode, AppliedOn), userid, Pwd, "http://webservices.galileo.com/SubmitXml", 0, searchInputs.HidTxtAirLine, CrdType), searchInputs, "O", "InBound", MiscList, CrdType, userid, ctx));
                            var tt = Task.WhenAll(t1).Result;
                            objFltResultList_R.Add(tt[0]);
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(GDSAvailability_R)", "Error_001", ex, "GDSAvailability_R");
            }

        }
        #endregion
        #region Indigo
        //mk done
        //private void IndigoAvailability_O(CredentialList crdNew) 
        private async Task IndigoAvailability_O(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;

                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //CredentialList objSpiceCrd = new CredentialList();
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList));
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);

                    //objFltResultList_O.Add(tt[0].Result[0]);
                    //objIndigoFltResultList.Add(((List<FlightSearchResults>)tt[0].Result[0])
                    // O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList);
                    // objFltResultList_O.Add(O[0]);
                }
                else
                {
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                    //objFltResultList_O.Add(tt[0].Result[0]);
                    //O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                }

                //var tt = Task.WhenAll(t1).Result;
                //if (searchInputs.Trip == Trip.I)
                //{
                //    objIndigoFltResultList.Add(((List<FlightSearchResults>)tt[0].Result[0]).ApplyPreferencing(searchInputs.TCCode, FareType.Retail.ToString(), connectionString, searchInputs, "6E").ApplyRevenueManagement(searchInputs.TCCode, "R", RMList).AddFlightKey(searchInputs.RTF));//.ApplyCommSrvtaxMkrpForInterNational(searchInputs, connectionString)); ;
                //}
                //else
                //{

                //    objIndigoFltResultList.Add(((List<FlightSearchResults>)tt[0].Result[0]).ApplyPreferencing(searchInputs.TCCode, FareType.Retail.ToString(), connectionString, searchInputs, "6E").ApplyRevenueManagement(searchInputs.TCCode, "R", RMList).AddFlightKey(searchInputs.RTF));//.ApplyCommSrvtaxMkrp(connectionString));
                //}

            }
            catch (Exception ex)
            { }
        }
        //private void IndigoAvailability_R(CredentialList crdNew)
        private async Task IndigoAvailability_R(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                // CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ////srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList);
                        //objFltResultList_R.Add(R[0]);
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList));
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        //private void IndigoCpnAvailability_O()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECPN");
        //        objFltResultList_O.Add(objCpn.GetLccCpnResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void IndigoCpnAvailability_R()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {

        //        if (searchInputs.TripType.ToString() == "RoundTrip")
        //        {
        //            srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECPN");
        //            objFltResultList_R.Add(objCpn.GetLccCpnResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void IndigoCorpAvailability_O()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECORP");
        //        objFltResultList_O.Add(objCpn.GetLccCpnResultCorp(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void IndigoCorpAvailability_R()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        if (searchInputs.TripType.ToString() == "RoundTrip")
        //        {
        //            srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECORP");
        //            objFltResultList_R.Add(objCpn.GetLccCpnResultCorp(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void IndigoTBFAvailability_O()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECPN");
        //    try
        //    {
        //        objFltResultList_O.Add(objCpn.GetLccCpnResultTBF(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void IndigoTBFAvailability_R()
        //{
        //    ClsLccCouponAvailability objCpn = new ClsLccCouponAvailability(connectionString);
        //    objCpn.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        if (searchInputs.TripType.ToString() == "RoundTrip")
        //        {
        //            srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6ECPN");
        //            objFltResultList_R.Add(objCpn.GetLccCpnResultTBF(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        #endregion
        #region Spicejet
        //mk done
        //private void SpicejetAvailability_O(CredentialList crdNew)
        private async Task SpicejetAvailability_O(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                // string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;



                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC); //,promocode
                //ArrayList O = new ArrayList();
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
            }
        }
        private async Task SpicejetAvailability_R(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                if (searchInputs.Trip == Trip.D)
                {
                    // ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                        //objFltResultList_R.Add(R[0]);
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        //private void SpicejetTBFAvailability_O()
        //{
        //    try
        //    {
        //        CredentialList objSpiceCrd = new CredentialList();
        //        string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
        //        url = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].AvailabilityURL;
        //        userid = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].UserID;
        //        Pwd = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].Password;
        //        OrgCode = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].CorporateID;
        //        smUrl = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].LoginID;
        //        bmUrl = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].LoginPWD;
        //        promocode = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].APISource;
        //        SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
        //        ArrayList O = new ArrayList();
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGTBF");
        //        if (searchInputs.Trip == Trip.D)
        //        {
        //            O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, "SGTBF", srvCharge, "SG", "", FareTypeSettingsList);
        //            objFltResultList_O.Add(O[0]);
        //        }
        //        else
        //        {
        //            O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, "SGTBF", srvCharge, "SG", "", FareTypeSettingsList);
        //            objFltResultList_O.Add(O[0]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void SpicejetTBFAvailability_R()
        //{
        //    try
        //    {
        //        CredentialList objSpiceCrd = new CredentialList();
        //        string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
        //        url = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].AvailabilityURL;
        //        userid = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].UserID;
        //        Pwd = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].Password;
        //        OrgCode = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].CorporateID;
        //        smUrl = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].LoginID;
        //        bmUrl = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].LoginPWD;
        //        promocode = ((from crd in CrdList where crd.Provider == "SGTBF" select crd).ToList())[0].APISource;
        //        SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
        //        if (searchInputs.Trip == Trip.D)
        //        {
        //            ArrayList R = new ArrayList();
        //            if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
        //            {
        //                srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGTBF");
        //                R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, "SGTBF", srvCharge, "SG", "", FareTypeSettingsList);
        //                objFltResultList_R.Add(R[0]);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void SpicejetSTRAvailability_O()
        //{
        //    try
        //    {
        //        CredentialList objSpiceCrd = new CredentialList();
        //        string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
        //        url = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].AvailabilityURL;
        //        userid = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].UserID;
        //        Pwd = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].Password;
        //        OrgCode = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].CorporateID;
        //        smUrl = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].LoginID;
        //        bmUrl = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].LoginPWD;
        //        promocode = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].APISource;
        //        SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
        //        ArrayList O = new ArrayList();
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
        //        if (searchInputs.Trip == Trip.D)
        //        {
        //            O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, "SGSTR", srvCharge, "SG", "", FareTypeSettingsList);
        //            objFltResultList_O.Add(O[0]);
        //        }
        //        else
        //        {
        //            O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, "SGSTR", srvCharge, "SG", "", FareTypeSettingsList);
        //            objFltResultList_O.Add(O[0]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void SpicejetSTRAvailability_R()
        //{
        //    try
        //    {
        //        CredentialList objSpiceCrd = new CredentialList();
        //        string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
        //        url = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].AvailabilityURL;
        //        userid = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].UserID;
        //        Pwd = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].Password;
        //        OrgCode = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].CorporateID;
        //        smUrl = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].LoginID;
        //        bmUrl = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].LoginPWD;
        //        promocode = ((from crd in CrdList where crd.Provider == "SGSTR" select crd).ToList())[0].APISource;
        //        SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
        //        if (searchInputs.Trip == Trip.D)
        //        {
        //            ArrayList R = new ArrayList();
        //            if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
        //            {
        //                srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
        //                R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, "SGSTR", srvCharge, "SG", "", FareTypeSettingsList);
        //                objFltResultList_R.Add(R[0]);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void SpiceCpnAvailability_O()
        //{
        //    ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
        //    objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
        //        objFltResultList_O.Add(objCpn1.GetLccCpnResultSG(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge, "SGCPN"));

        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void SpiceCpnAvailability_R()
        //{
        //    ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
        //    objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        if (searchInputs.TripType.ToString() == "RoundTrip")
        //        {
        //            srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
        //            objFltResultList_R.Add(objCpn1.GetLccCpnResultSG(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge, "SGCPN"));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void SpiceCpnWithHtlAvailability_O()
        //{
        //    ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
        //    objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
        //        objFltResultList_O.Add(objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge_g8cpn));
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        //private void SpiceCpnWithHtlAvailability_R()
        //{
        //    ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
        //    objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //    objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //    try
        //    {
        //        if (searchInputs.TripType.ToString() == "RoundTrip")
        //        {
        //            objFltResultList_R.Add(objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge_g8cpn));
        //            //objSpiceCpnWithHtlFltResultList.Add(objCpn1.GetLccCpnResultSG(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge_SGcpnhtl, "SGCPNHTL"));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //}
        #endregion
        #region G8CPAPI
        private void G8Availability_O_old(CredentialList crdNew)
        {
            try
            {
                LCCResult objLccRes = new LCCResult(connectionString);
                // CredentialList objGoairCrd = new CredentialList();
                // for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider == "G8") { objGoairCrd = CrdList[i]; break; } }
                objLccRes.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
                objLccRes.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
                ArrayList objG8ListO = new ArrayList();
                objG8ListO = objLccRes.G8Avilability(searchInputs, crdNew, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, false);
                //objFltResultList_O.Add(objGoairFltResultList);

                for (int i = 0; i <= objG8ListO.Count - 1; i++)
                {
                    if (i == 0)
                        objFltResultList_O.Add(objG8ListO[i]);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void G8Availability_R_old(CredentialList crdNew)
        {
            try
            {
                if (searchInputs.RTF == false)
                {
                    LCCResult objLccRes = new LCCResult(connectionString);
                    CredentialList objGoairCrd = new CredentialList();
                    // for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider == "G8") { objGoairCrd = CrdList[i]; break; } }
                    objLccRes.LccDepDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
                    objLccRes.LccRetDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
                    ArrayList objG8ListR = new ArrayList();
                    objG8ListR = objLccRes.G8Avilability(searchInputs, crdNew, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, true);

                    //objFltResultList_R.Add(objGoairFltResultList);
                    for (int i = 0; i <= objG8ListR.Count - 1; i++)
                    {
                        if (i == 0)
                            objFltResultList_R.Add(objG8ListR[i]);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion
        #region G8_NAVITIRE
        // mk done
        private async Task G8Availability_O(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //CredentialList objSpiceCrd = new CredentialList();
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                G8NAV4 objLcc = new G8NAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 411, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
                bool SMEFare = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);

                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx, SMEFare));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                    // t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }
        }
        private async Task G8Availability_R(CredentialList crdNew)
        {
            try
            {

                await Task.Delay(0);
                var t1 = (dynamic)null;
                // CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                G8NAV4 objLcc = new G8NAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 411, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                                                                                                                                                                                                                                                                                               // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
                bool SMEFare = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                if (searchInputs.Trip == Trip.D)
                {
                    //ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList);
                        //objFltResultList_R.Add(R[0]);
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "G8", crdNew.CrdType, FareTypeSettingsList));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, MiscList, "G8", crdNew.CrdType, FareTypeSettingsList, ctx, SMEFare));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion
        #region Goair
        //private void GoairAvailability_O()
        //{
        //    try
        //    {
        //        LCCResult objLccRes = new LCCResult(connectionString);
        //        CredentialList objGoairCrd = new CredentialList();
        //        for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider == "G8") { objGoairCrd = CrdList[i]; break; } }
        //        objLccRes.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //        objLccRes.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
        //        objGoairFltResultList = objLccRes.GoairAvilability(searchInputs, objGoairCrd, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge);
        //        for (int i = 0; i <= objGoairFltResultList.Count - 1; i++)
        //        {
        //            if (i == 0)
        //                objFltResultList_O.Add(objGoairFltResultList[i]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void GoairAvailability_R()
        //{
        //    try
        //    {
        //        LCCResult objLccRes = new LCCResult(connectionString);
        //        CredentialList objGoairCrd = new CredentialList();
        //        for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider == "G8") { objGoairCrd = CrdList[i]; break; } }
        //        objLccRes.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
        //        objLccRes.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
        //        srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8");
        //        objGoairFltResultList = objLccRes.GoairAvilability(searchInputs, objGoairCrd, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge);
        //        for (int i = 0; i <= objGoairFltResultList.Count - 1; i++)
        //        {
        //            if (i > 0)
        //                objFltResultList_R.Add(objGoairFltResultList[i]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        private async Task GoairCpnAvailability_O()
        {
            await Task.Delay(0);
            var t1 = (dynamic)null;
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8CPN");
                //objFltResultList_O.Add(objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                //t1 = Task.Run(() => objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                t1 = Task.Run(() => objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_O.Add(tt[0][0]);

            }
            catch (Exception ex)
            { }

        }
        private async Task GoairCpnAvailability_R()
        {
            await Task.Delay(0);
            var t1 = (dynamic)null;
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                if (searchInputs.TripType.ToString() == "RoundTrip")
                {
                    // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8CPN");
                    //objFltResultList_R.Add(objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    //t1 = Task.Run(() => objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    t1 = Task.Run(() => objCpn2.GetLccCpnResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }

        }
        private async Task GoairFlxAvailability_O()
        {
            await Task.Delay(0);
            var t1 = (dynamic)null;
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8FLX");
                //objFltResultList_O.Add(objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                //t1 = Task.Run(() => objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                t1 = Task.Run(() => objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_O.Add(tt[0][0]);
            }
            catch (Exception ex)
            { }

        }
        private async Task GoairFlxAvailability_R()
        {
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.TripType.ToString() == "RoundTrip")
                {
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8FLX");
                    //objFltResultList_R.Add(objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    //t1 = Task.Run(() => objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    t1 = Task.Run(() => objCpn2.GetLccFlxResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }

        }
        private async Task GoairCorpAvailability_O()
        {
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;

                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8CORP");
                //objFltResultList_O.Add(objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                //t1 = Task.Run(() => objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                t1 = Task.Run(() => objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_R.Add(tt[0][0]);
            }
            catch (Exception ex)
            { }

        }
        private async Task GoairCorpAvailability_R()
        {
            ClsLccCouponAvailability objCpn2 = new ClsLccCouponAvailability(connectionString);
            objCpn2.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn2.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.TripType.ToString() == "RoundTrip")
                {
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G8CORP");
                    //objFltResultList_R.Add(objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));

                    //t1 = Task.Run(() => objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    t1 = Task.Run(() => objCpn2.GetLccCorpResultGO(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }

        }
        #endregion
        #region AirIndiaExpress
        private async Task IXCpnAvailability_O()
        {
            ClsLccCouponAvailability objCpn3 = new ClsLccCouponAvailability(connectionString);
            objCpn3.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn3.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "IXCPN");
                if (searchInputs.Trip.ToString() == "I")
                {
                    //objFltResultList_O.Add(objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge)); 
                    t1 = Task.Run(() => objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //objFltResultList_O.Add(objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                    t1 = Task.Run(() => objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }

        }
        private async Task IXCpnAvailability_R()
        {
            ClsLccCouponAvailability objCpn3 = new ClsLccCouponAvailability(connectionString);
            objCpn3.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn3.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D")
                {
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "IXCPN");
                    //objFltResultList_R.Add(objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    t1 = Task.Run(() => objCpn3.GetLccCpnResultIX(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }

        }
        #endregion
        #region LCC INTL
        private async Task AirArabiaAvailability_O()
        {
            //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G9");
            GetFinalResult objArabia = new GetFinalResult(searchInputs.IsCorp, searchInputs.AgentType, srvCharge);
            string url = "", userid = "", Pwd = "";
            string depDate = "", retDate = "";
            url = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].AvailabilityURL;
            userid = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].UserID;
            Pwd = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].Password;
            depDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            retDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            await Task.Delay(0);
            var t1 = (dynamic)null;
            if (searchInputs.TripType.ToString() == "RoundTrip")
            {

                //objFltResultList_R.Add(objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "Return", searchInputs.UID, "SPRING"));
                t1 = Task.Run(() => objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "Return", searchInputs.UID, "SPRING"));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_R.Add(tt[0][0]);
            }
            else
            {
                //objFltResultList_O.Add(objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "OneWay", searchInputs.UID, "SPRING"));
                t1 = Task.Run(() => objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "OneWay", searchInputs.UID, "SPRING"));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_O.Add(tt[0][0]);
            }
        }
        private async Task AirArabiaAvailability_R()
        {
            //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "G9");
            GetFinalResult objArabia = new GetFinalResult(searchInputs.IsCorp, searchInputs.AgentType, srvCharge);
            string url = "", userid = "", Pwd = "";
            string depDate = "", retDate = "";
            url = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].AvailabilityURL;
            userid = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].UserID;
            Pwd = ((from crd in CrdList where crd.Provider == "G9" select crd).ToList())[0].Password;
            depDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            retDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            if (searchInputs.TripType.ToString() == "RoundTrip")
            {
                //objFltResultList_R.Add(objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "Return", searchInputs.UID, "SPRING"));
                await Task.Delay(0);
                var t1 = (dynamic)null;
                t1 = Task.Run(() => objArabia.GetResponse(url, userid, Pwd, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), depDate, retDate, searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "Return", searchInputs.UID, "SPRING"));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_R.Add(tt[0][0]);

            }
        }
        private async Task AirAsiaCpnAvailability_O()
        {

            ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
            objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "AKCPN");
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.Trip.ToString() == "D")
                {
                    //BAL_AV objBAL_AV = new BAL_AV(connectionString);
                    //bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "AK");
                    //if (Status == true)
                    //{
                    if (searchInputs.GDSRTF == false)
                    {

                        //objFltResultList_O.Add(objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));

                        t1 = Task.Run(() => objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);
                    }
                    else
                    {
                        //objFltResultList_O.Add(objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "R", "InBound", srvCharge));

                        t1 = Task.Run(() => objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "R", "InBound", srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);
                    }
                    //}
                }
                else
                {
                    if (searchInputs.TripType.ToString() == "OneWay")
                    {
                        //objFltResultList_O.Add(objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                        t1 = Task.Run(() => objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);
                    }
                    else
                    {
                        //objFltResultList_O.Add(objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "R", "InBound", srvCharge));
                        t1 = Task.Run(() => objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "R", "InBound", srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            { }


        }
        private async Task AirAsiaCpnAvailability_R()
        {

            ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
            objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {

                if (searchInputs.Trip.ToString() == "D")
                {
                    BAL_AV objBAL_AV = new BAL_AV(connectionString);
                    bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "AK");
                    if (Status == true)
                    {
                        if (searchInputs.GDSRTF == false)
                        {
                            if (searchInputs.TripType.ToString() == "RoundTrip")
                            {
                                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "AKCPN");

                                //objFltResultList_R.Add(objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));

                                await Task.Delay(0);
                                var t1 = (dynamic)null;
                                t1 = Task.Run(() => objCpn1.GetLccCpnResultAirAsia(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", srvCharge));
                                var tt = Task.WhenAll(t1).Result;
                                objFltResultList_R.Add(tt[0][0]);

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            { }


        }
        #endregion
        #region AirVentura
        public async Task AirVenturaAvailability_O()
        {
            try
            {
                //BAL_AV objBAL_AV = new BAL_AV(connectionString);
                //bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "VA");
                //if (Status == true)
                //{

                string CorporateID = "", userid = "", Pwd = "";
                CorporateID = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].CorporateID;
                userid = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].Password;

                GetFlightAvailability_AV objAVRes = new GetFlightAvailability_AV(connectionString);
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "VA");
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.RTF == false)
                {
                    //objFltResultList_O.Add(objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, false, false, "AirVentura"));                   
                    t1 = Task.Run(() => objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, false, false, "AirVentura"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //objFltResultList_O.Add(objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, true, "AirVentura")); 
                    t1 = Task.Run(() => objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, false, false, "AirVentura"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }

                //}

            }
            catch (Exception ex)
            {
            }
        }
        public async Task AirVenturaAvailability_R()
        {
            try
            {
                //BAL_AV objBAL_AV = new BAL_AV(connectionString);
                //bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "VA");
                //if (Status == true)
                //{

                string CorporateID = "", userid = "", Pwd = "";
                CorporateID = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].CorporateID;
                userid = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "VA" select crd).ToList())[0].Password;

                GetFlightAvailability_AV objAVRes = new GetFlightAvailability_AV(connectionString);
                if (searchInputs.RTF == false && searchInputs.TripType.ToString() == "RoundTrip")
                {
                    // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "AKCPN");

                    //objFltResultList_R.Add(objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, false, "AirVentura"));


                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objAVRes.AirVenturaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, false, "AirVentura"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }

                //}

            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region AirCosta
        public async Task AirCostaAvailability_O()
        {
            try
            {
                //BAL_AV objBAL_AV = new BAL_AV(connectionString);
                //bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "LB");
                //if (Status == true)
                //{

                string CorporateID = "", userid = "", Pwd = "";
                CorporateID = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].CorporateID;
                userid = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].Password;

                GetFlightAvailability_AC objAVRes = new GetFlightAvailability_AC(connectionString);
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "LB");
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.RTF == false)
                {
                    //objFltResultList_O.Add(objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, false, false, "AirCosta"));

                    t1 = Task.Run(() => objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, false, false, "AirCosta"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //objFltResultList_O.Add(objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, true, "AirCosta")); 

                    t1 = Task.Run(() => objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, true, "AirCosta"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }

                //}

            }
            catch (Exception ex)
            {
            }
        }
        public async Task AirCostaAvailability_R()
        {
            try
            {
                //BAL_AV objBAL_AV = new BAL_AV(connectionString);
                //bool Status = objBAL_AV.GetVASearchStatus(searchInputs.HidTxtDepCity.Split(',')[0].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), "LB");
                //if (Status == true)
                //{

                string CorporateID = "", userid = "", Pwd = "";
                CorporateID = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].CorporateID;
                userid = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "LB" select crd).ToList())[0].Password;

                GetFlightAvailability_AC objAVRes = new GetFlightAvailability_AC(connectionString);
                if (searchInputs.RTF == false && searchInputs.TripType.ToString() == "RoundTrip")
                {
                    // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "AKCPN");

                    //objFltResultList_R.Add(objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, false, "AirCosta"));


                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objAVRes.AirCostaAvilability(searchInputs, CityList, SrvChargeList, MarkupDs, userid, Pwd, CorporateID, srvCharge, true, false, "AirCosta"));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }

                //}

            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region Flydubai
        private async Task FlydubaiAvailability_O()
        {
            try
            {
                if (searchInputs.Trip == Trip.I)
                {
                    LCCResult objLccRes = new LCCResult(connectionString);
                    CredentialList objFZCrd = new CredentialList();
                    string provider = searchInputs.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                    for (int i = 0; i <= CrdList.Count - 1; i++) { if (CrdList[i].Provider.Trim() == provider) { objFZCrd = CrdList[i]; break; } }
                    objLccRes.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
                    objLccRes.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "FZ");
                    //objFltResultList_O.AddRange(objLccRes.FlydubaiAvilability(searchInputs, objFZCrd, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge));


                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objLccRes.FlydubaiAvilability(searchInputs, objFZCrd, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }

            }
            catch (Exception ex)
            {
            }
        }

        #endregion
        #region Offline
        private async Task OfflineAvailability_O()
        {
            ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
            objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
                // objFltResultList_O.Add(objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", MiscList));
                await Task.Delay(0);
                var t1 = (dynamic)null;
                t1 = Task.Run(() => objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "OutBound", MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_O.Add(tt[0][0]);
            }
            catch (Exception ex)
            { }

        }
        private async Task OfflineAvailability_R()
        {
            ClsLccCouponAvailability objCpn1 = new ClsLccCouponAvailability(connectionString);
            //objCpn1.LccDepDate = Utility.Right(searchInputs.DepDate, 4) + "-" + Utility.Mid(searchInputs.DepDate, 3, 2) + "-" + Utility.Left(searchInputs.DepDate, 2);
            //objCpn1.LccRetDate = Utility.Right(searchInputs.RetDate, 4) + "-" + Utility.Mid(searchInputs.RetDate, 3, 2) + "-" + Utility.Left(searchInputs.RetDate, 2);
            try
            {
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SGCPN");
                //objFltResultList_R.Add(objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", MiscList));
                await Task.Delay(0);
                var t1 = (dynamic)null;
                t1 = Task.Run(() => objCpn1.GetOfflineFlightResult(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, "O", "InBound", MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_R.Add(tt[0][0]);
            }
            catch (Exception ex)
            { }

        }

        #endregion
        #region TBO

        private async Task TBOAvailability_O(bool isLcc)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].Password;
                OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].ServerIP;
                // loginUrl = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].LoginID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;
                //SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode

                if (searchInputs.Cabin.ToUpper().Trim() != "W")
                {
                    TBOSearchflight objTboSearchFlt = new TBOSearchflight(OrgCode, userid, Pwd, ip);
                    ArrayList O = new ArrayList();
                    //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    if ((searchInputs.Trip == Trip.D && searchInputs.RTF != true) || (searchInputs.Trip == Trip.I && searchInputs.TripType != TripType.RoundTrip))
                    {
                        //objFltResultList_O.Add(objTboSearchFlt.GetTBOAvailibility(searchInputs, false, "INB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));

                        //await Task.Delay(0);
                        //var t1 = (dynamic)null;
                        t1 = Task.Run(() => objTboSearchFlt.GetTBOAvailibility(searchInputs, false, "INB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);

                    }
                    else
                    {
                        //objFltResultList_O.Add(objTboSearchFlt.GetTBOAvailibility(searchInputs, true, "INB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));

                        t1 = Task.Run(() => objTboSearchFlt.GetTBOAvailibility(searchInputs, true, "INB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_O.Add(tt[0][0]);

                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        private async Task TBOAvailability_R(bool isLcc)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].Password;
                OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].ServerIP;
                // loginUrl = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].LoginID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;
                //SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode

                TBOSearchflight objTboSearchFlt = new TBOSearchflight(OrgCode, userid, Pwd, ip);
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);
                if (searchInputs.Trip == Trip.D && searchInputs.RTF != true)
                {
                    //objFltResultList_R.Add(objTboSearchFlt.GetTBOAvailibility(searchInputs, false, "OutB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objTboSearchFlt.GetTBOAvailibility(searchInputs, false, "OutB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);


                }
                //else
                //{
                //    objFltResultList_O.Add(objTboSearchFlt.GetTBOAvailibility(searchInputs, true, "INB", true, SrvChargeList, MarkupDs));
                //}
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region Travarena
        private async Task TravarenaAvailability_O(bool isLcc)
        {
            await Task.Delay(0);

            //File.AppendAllText("C:\\ErrorRiyaLog\\fltresult" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", "TravarenaAvailability_O Start" + Environment.NewLine);

            try
            {
                List<MISCCharges> MiscListRI = new List<MISCCharges>();
               // MiscListRI = MiscList.Where(x => x.Provider == "RI").ToList();
                string url = "", userid = "", Pwd = "", OrgCode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].Password;
                OrgCode = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].ServerIP;
                double INFBasic = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].INFBasic;
                double INFTax = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].INFTax;

                //  CrdType = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CrdType;
                //  string url = crdNew.AvailabilityURL,
                //userid = crdNew.UserID,
                //Pwd = crdNew.Password,
                //OrgCode = crdNew.CorporateID;
                GALWS.Travarena.TravarenaSearch objUApi2 = new GALWS.Travarena.TravarenaSearch(userid, Pwd, searchInputs.OwnerId, "", "", url, OrgCode);

                var t1 = (dynamic)null;
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objUApi2.GetFlightAvailability(searchInputs, false, "INB", isLcc, CityList, Airlist, SrvChargeList, MarkupDs, "O", "OutBound", searchInputs.RTF, INFBasic, INFTax, connectionString, MiscListRI, ctx));
                }
                else
                {
                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                    {
                        t1 = Task.Run(() => objUApi2.GetFlightAvailability(searchInputs, false, "INB", isLcc, CityList, Airlist, SrvChargeList, MarkupDs, "O", "OutBound", searchInputs.RTF, INFBasic, INFTax, connectionString, MiscListRI, ctx));
                    }
                    else
                    {
                        t1 = Task.Run(() => objUApi2.GetFlightAvailability(searchInputs, false, "INB", isLcc, CityList, Airlist, SrvChargeList, MarkupDs, "O", "OutBound", searchInputs.RTF, INFBasic, INFTax, connectionString, MiscListRI, ctx));
                    }
                }
                var tt = Task.WhenAll(t1).Result;
                //objFltResultList_O.Add(tt[0][0]);
                objFltResultList_O.Add(tt[0]);
             //   File.AppendAllText("C:\\ErrorRiyaLog\\fltresult" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", "TravarenaAvailability_O_end" + Environment.NewLine);

            }
            catch (Exception ex)
            { //File.AppendAllText("C:\\ErrorRiyaLog\\fltresult" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", "TravarenaAvailability_O " + ex.Message + "__ " + ex.StackTrace + Environment.NewLine); 
            }
        }
        private async Task TravarenaAvailability_R(bool isLcc)
        {
            await Task.Delay(0);

            try
            {
                List<MISCCharges> MiscListRI = new List<MISCCharges>();
               // MiscListRI = MiscList.Where(x => x.Provider == "RI").ToList();
                var t1 = (dynamic)null;
                string url = "", userid = "", Pwd = "", OrgCode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].Password;
                OrgCode = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].ServerIP;
                double INFBasic = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].INFBasic;
                double INFTax = ((from crd in CrdList where crd.Provider == "RI" select crd).ToList())[0].INFTax;
                GALWS.Travarena.TravarenaSearch objUApi2 = new GALWS.Travarena.TravarenaSearch(userid, Pwd, searchInputs.OwnerId, "", "", url, OrgCode);

                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        t1 = Task.Run(() => objUApi2.GetFlightAvailability(searchInputs, false, "OutB", isLcc, CityList, Airlist, SrvChargeList, MarkupDs, "O", "InBound", searchInputs.RTF, INFBasic, INFTax, connectionString, MiscListRI, ctx));
                    }
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0]);
                    //objFltResultList_R.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion
        #region Yatra API

        private async Task YAAvailability_O(bool isLcc)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "YA" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "YA" select crd).ToList())[0].ServerIP;

                YASearchBAL objYASearchFlt = new YASearchBAL(ip);
                ArrayList O = new ArrayList();
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if ((searchInputs.Trip == Trip.D && searchInputs.RTF != true) || (searchInputs.Trip == Trip.I && searchInputs.TripType != TripType.RoundTrip))
                {
                    //objFltResultList_O.Add(objTboSearchFlt.GetTBOAvailibility(searchInputs, false, "INB", isLcc, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    //objFltResultList_O.Add(objYASearchFlt.GetYAAvailibility(searchInputs, false, "INB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));


                    t1 = Task.Run(() => objYASearchFlt.GetYAAvailibility(searchInputs, false, "INB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //objFltResultList_O.Add(objYASearchFlt.GetYAAvailibility(searchInputs, true, "INB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    t1 = Task.Run(() => objYASearchFlt.GetYAAvailibility(searchInputs, true, "INB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task YAAvailability_R(bool isLcc)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "YA" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "YA" select crd).ToList())[0].ServerIP;
                YASearchBAL objYASearchFlt = new YASearchBAL(ip);

                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);
                if (searchInputs.Trip == Trip.D && searchInputs.RTF != true)
                {
                    //objFltResultList_R.Add(objYASearchFlt.GetYAAvailibility(searchInputs, false, "OutB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objYASearchFlt.GetYAAvailibility(searchInputs, false, "OutB", false, url, SrvChargeList, MarkupDs, connectionString, srvCharge));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0][0]);
                }

            }
            catch (Exception ex)
            {
            }
        }

        #endregion
        #region Air Asia
        //mk done
        private async Task AirAsiaAvailability_O(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                GALWS.AirAsia.AirAsiaSearchFlight objairasia = new GALWS.AirAsia.AirAsiaSearchFlight();
                await Task.Delay(0);
                var t1 = (dynamic)null;

                ArrayList O = new ArrayList();
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objairasia.GetAirAsiaAvailibility(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
                else
                {
                    t1 = Task.Run(() => objairasia.GetAirAsiaAvailibility(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
            }
            catch (Exception ex)
            { }
        }
        private async Task AirAsiaAvailability_R(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                GALWS.AirAsia.AirAsiaSearchFlight objairasia = new GALWS.AirAsia.AirAsiaSearchFlight();
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objairasia.GetAirAsiaAvailibility(searchInputs, searchInputs.RTF, "IB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0]);
                }

            }
            catch (Exception ex)
            { }
        }

        #endregion
        #region ITQ API
        //mk done
        private async Task ITQAirApiAvailability_O(bool isLcc)
        {
            try
            {

                List<CredentialList> CrdListCPN = CrdList.Where(x => x.Provider == "TQ").ToList();
                CredentialList crdNew = CrdListCPN[0];

                CredentialList objSpiceCrd = new CredentialList();
                GALWS.ITQAirApi.AvailabilityAndPricing objairasia = new GALWS.ITQAirApi.AvailabilityAndPricing();
                await Task.Delay(0);
                var t1 = (dynamic)null;

                ArrayList O = new ArrayList();
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objairasia.ITQAirApiAvailibility(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
                else
                {
                    t1 = Task.Run(() => objairasia.ITQAirApiAvailibility(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
            }
            catch (Exception ex)
            { }
        }
        private async Task ITQAirApiAvailability_R(bool isLcc)
        {
            try
            {
                List<CredentialList> CrdListCPN = CrdList.Where(x => x.Provider == "TQ").ToList();
                CredentialList crdNew = CrdListCPN[0];
                CredentialList objSpiceCrd = new CredentialList();
                GALWS.ITQAirApi.AvailabilityAndPricing objairasia = new GALWS.ITQAirApi.AvailabilityAndPricing();
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objairasia.ITQAirApiAvailibility(searchInputs, searchInputs.RTF, "IB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0]);
                }

            }
            catch (Exception ex)
            { }
        }

        #endregion
        #region Iween Air Asia
        private async Task IweenAirAsiaAvailability_O(CredentialList crdNew)
        {
            await Task.Delay(0);
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                GALWS.IWEENAirAsia.IweenSearch objiweenairasia = new GALWS.IWEENAirAsia.IweenSearch();

                var t1 = (dynamic)null;

                
                if (searchInputs.Trip == Trip.D)
                {
                    t1 = Task.Run(() => objiweenairasia.GetFlightAvailability(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
                else
                {
                    t1 = Task.Run(() => objiweenairasia.GetFlightAvailability(searchInputs, searchInputs.RTF, "OB", true, SrvChargeList, MarkupDs, connectionString, CityList, crdNew, ctx, MiscList));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion
        #region Air Arabia
        private async Task AirArabiaAvailabilityReq(CredentialList crdNew)
        {
            try
            {
                GALWS.AirArabia.AirArabiaFlightSearch objairasia = new GALWS.AirArabia.AirArabiaFlightSearch();
                await Task.Delay(0);
                var t1 = (dynamic)null;

                t1 = Task.Run(() => objairasia.GetAirArabiaAvailibility(searchInputs, searchInputs.RTF, "OB", SrvChargeList, MarkupDs, connectionString, CityList, crdNew, PFCodeDt_LCC, MiscList));
                var tt = Task.WhenAll(t1).Result;
                objFltResultList_O.Add(tt[0]);

            }
            catch (Exception ex)
            { }
        }
        #endregion
        private void WriteToDrive(string DirPath, string PathReq, string XML)
        {
            try
            {
                string dir = "";
                string newPathResp = "";
                dir = DirPath;
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                newPathResp = Path.Combine(dir, PathReq);
                FileStream fs1 = new FileStream(newPathResp, FileMode.CreateNew, FileAccess.Write);
                StreamWriter sw1 = new StreamWriter(fs1);
                sw1.Write(XML.Trim());
                sw1.Flush();
                sw1.Close();
                fs1.Close();
            }
            catch (Exception ex)
            {
                //ExecptionLogger.FileHandling("UpdateImportPNR(WriteToDrive)", "Error_001", ex, "ImportPnr");
            }
        }

        private async Task CPNAvailability_O(HttpContext ctx, CredentialList crdNew)
        {

            try
            {
                List<CredentialList> Crdlist = new List<CredentialList>();
                Crdlist.Add(crdNew);
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                //// url = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].Password;
                ////OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                //ip = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].ServerIP;
                userid = crdNew.UserID;
                Pwd = crdNew.Password;
                ip = crdNew.ServerIP;
                //userid = ((from crd in CrdList where crd.Provider == searchInputs.HidTxtAirLine && crd.ResultFrom == "SP" && crd.CrdType == "CPN" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == searchInputs.HidTxtAirLine && crd.ResultFrom == "SP" && crd.CrdType == "CPN" select crd).ToList())[0].Password;                
                //ip = ((from crd in CrdList where crd.Provider == searchInputs.HidTxtAirLine && crd.ResultFrom == "SP" && crd.CrdType == "CPN" select crd).ToList())[0].ServerIP;

                ScrapSeacrchBAL objCPSearchFlt = new ScrapSeacrchBAL(userid, Pwd, ip);
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);
                await Task.Delay(0);
                var t1 = (dynamic)null;
                if ((searchInputs.Trip == Trip.D && searchInputs.RTF != true) || (searchInputs.Trip == Trip.I && searchInputs.TripType != TripType.RoundTrip))
                {

                    //objFltResultList_O.Add(objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));

                    //t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));
                    t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, MiscList, ctx, CityList, Airlist));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
                else
                {
                    //objFltResultList_O.Add(objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));

                    //t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));
                    t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "INB", Crdlist, SrvChargeList, MarkupDs, connectionString, MiscList, ctx, CityList, Airlist));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0]);
                }
            }
            catch (Exception ex)
            {
            }

        }

        private async Task CPNAvailability_R(HttpContext ctx, CredentialList crdNew)
        {
            try
            {

                List<CredentialList> Crdlist = new List<CredentialList>();
                Crdlist.Add(crdNew);
                string url = "", userid = "", Pwd = "", OrgCode = "", loginUrl = "", bmUrl = "", promocode = "", ip = "";
                url = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "TB" select crd).ToList())[0].CorporateID;
                ip = ((from crd in CrdList where crd.Provider == "CP" select crd).ToList())[0].ServerIP;

                ScrapSeacrchBAL objCPSearchFlt = new ScrapSeacrchBAL(userid, Pwd, ip);
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, searchInputs.HidTxtAirLine);
                if (searchInputs.Trip == Trip.D && searchInputs.RTF != true)
                {

                    //objFltResultList_R.Add(objCPSearchFlt.GetFlightSearch(searchInputs, "OutB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    //t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "OutB", Crdlist, SrvChargeList, MarkupDs, connectionString, srvCharge, ctx, CityList, Airlist));
                    t1 = Task.Run(() => objCPSearchFlt.GetFlightSearch(searchInputs, "OutB", Crdlist, SrvChargeList, MarkupDs, connectionString, MiscList, ctx, CityList, Airlist));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_R.Add(tt[0]);
                }

            }
            catch (Exception ex)
            {
            }
        }
        #region Baggage

        private async Task SpicejetAvailabilityBAG_O(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();

                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                // string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;



                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true);
                    //objFltResultList_O.Add(O[0]);

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                //else
                //{
                //    O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                //    objFltResultList_O.Add(O[0]);
                //}
            }
            catch (Exception ex)
            {
            }
        }
        private async Task SpicejetAvailabilityBAG_R(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true);
                        //objFltResultList_R.Add(R[0]);

                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private async Task IndigoAvailabilityBAG_O(CredentialList crdNew)
        {
            try
            {
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //CredentialList objSpiceCrd = new CredentialList();
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true);
                    //objFltResultList_O.Add(O[0]);

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }
        }
        private async Task IndigoAvailabilityBAG_R(CredentialList crdNew)
        {
            try
            {
                // CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true);
                        //objFltResultList_R.Add(R[0]);

                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region Indigo SME Fare
        private async Task IndigoAvailabilitySMEFare_O(CredentialList crdNew)
        {
            try
            {
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //CredentialList objSpiceCrd = new CredentialList();
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true);
                    //objFltResultList_O.Add(O[0]);

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 0, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, true, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            { }
        }
        private async Task IndigoAvailabilitySMEFare_R(CredentialList crdNew)
        {
            try
            {
                // CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //string url = "", userid = "", Pwd = "", OrgCode = "", smUrl = "", bmUrl = "", promocode = "";
                //url = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].AvailabilityURL;
                //userid = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].UserID;
                //Pwd = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].Password;
                //OrgCode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].CorporateID;
                //smUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginID;
                //bmUrl = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].LoginPWD;
                //promocode = ((from crd in CrdList where crd.Provider == "6E" select crd).ToList())[0].APISource;
                SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "6E");
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true);
                        //objFltResultList_R.Add(R[0]);

                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 973, 27, 1, crdNew.CrdType, srvCharge, "6E", crdNew.CrdType, FareTypeSettingsList, true));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, true, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion



        #region Indigo NAV 6E_V.4
        private async Task IndigoAvailabilityV4_O(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                #region Check Fare Type SMEFare or HandBaggage
                bool SMEFare = false;
                bool HandBag = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                else if (crdNew.CrdType.ToUpper() == "HBAG")
                {
                    HandBag = true;
                }
                #endregion
                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                if (searchInputs.Trip == Trip.D)
                {
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, HandBag, SMEFare, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1906, 94, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilityV4_O)", "Error_001", ex, "IndigoAvailabilityV4_O");
            }
        }

        private async Task IndigoAvailabilityV4_R(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;

                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                #region Check Fare Type SMEFare or HandBaggage
                bool SMEFare = false;
                bool HandBag = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                else if (crdNew.CrdType.ToUpper() == "HBAG")
                {
                    HandBag = true;
                }
                #endregion
                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode                
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, HandBag, SMEFare, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilityV4_R)", "Error_001", ex, "IndigoAvailabilityV4_R");
            }
        }

        private async Task IndigoAvailabilityBAGV4_O(CredentialList crdNew)
        {
            try
            {
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();

                if (searchInputs.Trip == Trip.D)
                {
                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilityBAGV4_O)", "Error_001", ex, "IndigoAvailabilityV4_O");
            }
        }
        private async Task IndigoAvailabilityBAGV4_R(CredentialList crdNew)
        {
            try
            {
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode                
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilityBAGV4_R)", "Error_001", ex, "IndigoAvailabilityBAGV4_R");
            }
        }


        private async Task IndigoAvailabilitySMEFareV4_O(CredentialList crdNew)
        {
            try
            {
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;

                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                ArrayList O = new ArrayList();
                if (searchInputs.Trip == Trip.D)
                {
                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 0, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, true, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilitySMEFareV4_O)", "Error_001", ex, "IndigoAvailabilitySMEFareV4_O");
            }
        }
        private async Task IndigoAvailabilitySMEFareV4_R(CredentialList crdNew)
        {
            try
            {

                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, crdNew.CrdType, PFCodeDt_LCC);//,promocode                
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1190, 60, 1, crdNew.CrdType, MiscList, "6E", crdNew.CrdType, FareTypeSettingsList, false, true, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(IndigoAvailabilitySMEFareV4_R)", "Error_001", ex, "IndigoAvailabilitySMEFareV4_R");
            }
        }

        #endregion
        #region SpiceJet NAV SG_V.4
        private async Task SpicejetAvailabilityV4_O(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                #region Check Fare Type SMEFare or HandBaggage
                bool SMEFare = false;
                bool HandBag = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                else if (crdNew.CrdType.ToUpper() == "HBAG")
                {
                    HandBag = true;
                }
                #endregion
                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC); //,promocode               
                //ArrayList O = new ArrayList();
                // srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, HandBag, SMEFare, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                else
                {
                    //O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                    //objFltResultList_O.Add(O[0]);
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
            }
            catch (Exception ex)
            {
            }
        }
        private async Task SpicejetAvailabilityV4_R(CredentialList crdNew)
        {
            try
            {
                await Task.Delay(0);
                var t1 = (dynamic)null;
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                #region Check Fare Type SMEFare or HandBaggage
                bool SMEFare = false;
                bool HandBag = false;
                if (crdNew.CrdType.ToUpper() == "SME")
                {
                    SMEFare = true;
                }
                else if (crdNew.CrdType.ToUpper() == "HBAG")
                {
                    HandBag = true;
                }
                #endregion
                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, crdNew.CrdType, PFCodeDt_LCC);//,promocode
                if (searchInputs.Trip == Trip.D)
                {
                    // ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                        //objFltResultList_R.Add(R[0]);
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList));
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, false, false, ctx));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, HandBag, SMEFare, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private async Task SpicejetAvailabilityBAGV4_O(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();

                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                //promocode = ((from crd in CrdList where crd.Provider == "SG" select crd).ToList())[0].APISource;
                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
                ArrayList O = new ArrayList();
                //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                if (searchInputs.Trip == Trip.D)
                {
                    //O = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true);
                    //objFltResultList_O.Add(O[0]);

                    await Task.Delay(0);
                    var t1 = (dynamic)null;
                    //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true));
                    t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 0, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                    var tt = Task.WhenAll(t1).Result;
                    objFltResultList_O.Add(tt[0][0]);
                }
                //else
                //{
                //    O = objLcc.Spice_GetAvailability_Intl(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 2367, 133, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList);
                //    objFltResultList_O.Add(O[0]);
                //}
            }
            catch (Exception ex)
            {
            }
        }
        private async Task SpicejetAvailabilityBAGV4_R(CredentialList crdNew)
        {
            try
            {
                CredentialList objSpiceCrd = new CredentialList();
                string url = crdNew.AvailabilityURL, userid = crdNew.UserID, Pwd = crdNew.Password, OrgCode = crdNew.CorporateID, smUrl = crdNew.LoginID, bmUrl = crdNew.LoginPWD, promocode = crdNew.APISource;
                SGNAV4 objLcc = new SGNAV4(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0);//,promocode
                if (searchInputs.Trip == Trip.D)
                {
                    ArrayList R = new ArrayList();
                    if (searchInputs.TripType == TripType.RoundTrip && searchInputs.RTF != true)
                    {
                        //srvCharge = objFlightCommonBAL.MISCServiceFee(MiscList, "SG");
                        //R = objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true);
                        //objFltResultList_R.Add(R[0]);

                        await Task.Delay(0);
                        var t1 = (dynamic)null;
                        //t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, srvCharge, "SG", crdNew.CrdType, FareTypeSettingsList, true));
                        t1 = Task.Run(() => objLcc.Spice_GetAvailability_Dom(searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, 1184, 66, 1, crdNew.CrdType, MiscList, "SG", crdNew.CrdType, FareTypeSettingsList, true, false, ctx));
                        var tt = Task.WhenAll(t1).Result;
                        objFltResultList_R.Add(tt[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region Calling  for All Fare
        public T ProcessRequest<T>() where T : class
        {
            ArrayList Finallist = new ArrayList();
            try
            {
                ctx = HttpContext.Current;
                //HttpContext context = HttpContext.Current;
                #region Citylist and Airlinelist
                Credentials objCrd = new Credentials(connectionString);
                FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");
                ArrayList objFltResultList = new ArrayList();
                CrdList = objCrd.GetServiceCredentials("");
                try
                {
                   // DataTable table = new DataTable();
                    try
                    {
                        DataTable dtn = (DataTable)System.Web.HttpContext.Current.Session["CommissionNewDynm"];
                        DataTable table = dtn.Select("TripType='" + searchInputs.Trip + "'").CopyToDataTable();
                        //DataTable table = new DataTable();
                        // foreach(DataRow rows in rowsfiltr)
                        // {
                        //     table.ImportRow(rows);
                        // }
                        HttpContext.Current.Session["CommissionNew"] = table;
                    }
                    catch(Exception ex)
                    { }                  
                }
                catch { }
                BlockServiceList = Data.GetBlockServices(connectionString, searchInputs.UID, searchInputs.Trip.ToString());
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), connectionString);// Get Data From DB or Cache
                FltDeal objDeal = new FltDeal(connectionString);
                PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                //FareTypeSettingsList = objfltBal.GetFareTypeSettings("", searchInputs.Trip.ToString(), "");

                if (objCacheManager.Contains("CityList"))
                {
                    CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");

                }
                else
                {
                    CityList = Data.GetCityList("I", connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
                }
                if (objCacheManager.Contains("Airlist"))
                {
                    Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
                }
                else
                {
                    Airlist = Data.GetAirlineList(connectionString);
                    FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                    objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
                }
                #endregion
                #region Markup
                string Provider = "";
                Provider = searchInputs.Provider;
                //MarkupDs = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.Trip.ToString()); //"MML1","DI1","DI","DI1"
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();
                //Calculation For AgentMarkUp
                dtAgentMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                //Calculation For AdminMarkUp'
                dtAdminMarkup = Data.GetMarkup(connectionString, searchInputs.UID, searchInputs.DISTRID, searchInputs.Trip.ToString(), "AD");
                dtAdminMarkup.TableName = "AdminMarkUp";
                MarkupDs.Tables.Add(dtAdminMarkup);
                #endregion
                #region Misc Charges
                FlightCommonBAL objFltComm = new FlightCommonBAL(connectionString);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }
                #endregion
                #region Check Airline
                string AirCode = "";
                if (searchInputs.HidTxtAirLine.Length > 1) { AirCode = Utility.Right(searchInputs.HidTxtAirLine, 2); }
                // to add other not listed flight.
                if (Provider.Trim().ToUpper() == "OF" || searchInputs.HidTxtAirLine == "2T" || searchInputs.HidTxtAirLine == "LB" || searchInputs.HidTxtAirLine == "VA" || searchInputs.HidTxtAirLine == "OD" || searchInputs.HidTxtAirLine == "KB" || searchInputs.HidTxtAirLine == "OP" || searchInputs.HidTxtAirLine == "FZ" || searchInputs.HidTxtAirLine == "OF")
                {
                    AirCode = "OF";
                }
                else if (Provider.Trim().ToUpper() == "OF")
                {
                }
                #endregion
                #region List Creation
                DataRow[] PFRow = null;
                try
                {
                    PFRow = PFCodeDt.Select("AppliedOn <>'BOOK'");
                }
                catch(Exception ex) { }
                ArrayList vlist1 = new ArrayList();
                ArrayList vlist = new ArrayList();
                bool RTFare = false;
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType != "CRP" && x.CrdType != "CPN") && x.Provider == AirCode && x.Status == true).ToList();
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType != "CRP" && x.CrdType != "CPN" && x.CrdType != "SME") && x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                //List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == "NRM") && x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                //List<CredentialList> CrdListCPN = CrdList.Where(x => x.Provider == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                List<CredentialList> CrdListCPN = CrdList.Where(x => x.AirlineCode == AirCode && x.Status == true && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();

                #region Get GDS Corporate credential
                List<CredentialList> Crd1GCRP = null;
                try
                {
                    if (Convert.ToString(searchInputs.Trip) == "D")
                    {

                        if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == searchInputs.HidTxtAirLine.ToString() && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                        else
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                        if (Crd1GCRP.Count < 1)
                        {
                            Crd1GCRP = CrdList.Where(x => x.CrdType == "CRP" && x.Provider == "1G" && x.Status == true && x.AirlineCode == "ALL" && x.Trip == searchInputs.Trip.ToString() && x.WebResult == true).ToList();
                        }
                    }
                }
                catch (Exception ex)
                { }
                #endregion

                #region Get Search Airline Block List
                List<SearchResultBlock> ResultBLockList = null;
                List<SearchResultBlock> ResultExcludeList = null;

                List<SearchResultBlock> ResultBLockListHandBag = null;
                List<SearchResultBlock> ResultBLockListSMEFare = null;
                try
                {
                    //Block1
                    SearchBlockList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "");
                    if (searchInputs.HidTxtAirLine.Length > 1 && (AirCode == "SG" || AirCode == "6E" || AirCode == "G8"))
                    {
                        ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        if (ResultExcludeList == null || ResultExcludeList.Count < 1)
                        {
                            ResultBLockList = SearchBlockList.Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        }
                        #region Hand Bag Fare and SME Fare List
                        ResultBLockListHandBag = SearchBlockList.Where(x => x.FareType == "HANDBAG" && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        ResultBLockListSMEFare = SearchBlockList.Where(x => x.FareType == "SMEFARE" && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                        #endregion
                    }

                    FareTypeMasterList = objfltBal.GetFlightFareTypeMaster("","ALL");
                }
                catch (Exception ex)
                { }
                #endregion

                ConcurrentBag<Task> taskList = new ConcurrentBag<Task>();
                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    RTFare = true;
                    // SearchBlockList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "");                    
                    switch (AirCode)
                    {
                        case "SG":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_R(true)));
                                Thread.Sleep(200);
                            }
                           else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                #region SG CALLING
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {
                                    CredentialList objCRD = CrdListCPN[p];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }
                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //FlightSearch searchInputsClone = (FlightSearch)searchInputs.Clone();
                                               // CredentialList CrdList = (CredentialList)CrdListCPN[p].Clone();
                                                //Thread thSGCPN_O = null;
                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    // thSGCPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(ctx, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_R(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        //thSGCPN_O = new Thread(() => SpicejetAvailability_O(CrdListCPN[p]));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_R(objCRD)));
                                                    }
                                                }                                                
                                            }
                                        }
                                        catch { }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case "6E":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_R(true)));
                                Thread.Sleep(200);
                            }
                           else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                #region 6E CALLING
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {
                                    CredentialList objCRD = CrdListCPN[p];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                //if (ResultBLockList != null && ResultBLockList.Count > 0)
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                // Thread th6ECPN_O = null;
                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {                                                    
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(ctx, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_R(objCRD)));
                                                    }
                                                    else
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_O(objCRD)));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_R(objCRD)));
                                                    }
                                                }                                               
                                            }
                                        }
                                        catch { }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case "G8":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_R(true)));
                                Thread.Sleep(200);
                            }
                           else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                #region G8 CALLING
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {
                                    CredentialList objCRD = CrdListCPN[p];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                //if (ResultBLockList != null && ResultBLockList.Count > 0)
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                //if (ResultExcludeListById == null || ResultExcludeListById.Count < 1){ }                                                
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }
                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread thG8CPN_O = null;
                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    //thG8CPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_R(ctx, objCRD)));
                                                }
                                                else
                                                {
                                                    //thG8CPN_O = new Thread(() => G8Availability_O(CrdListCPN[p]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_O(objCRD)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_R(objCRD)));
                                                }                                                
                                            }
                                        }
                                        catch { }
                                    }                                   
                                }
                                #endregion
                            }
                            break;
                        case "IX":                            
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                #region IX CALLING
                                taskList.Add(Task.Run(() => G8Availability_O_old(CrdListCPN[0])));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_R_old(CrdListCPN[0])));
                                #endregion
                            }                           
                            break;
                        case "AK":                            
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                #region AK CALLING
                                taskList.Add(Task.Run(() => AirAsiaAvailability_O(CrdListCPN[0])));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => AirAsiaAvailability_R(CrdListCPN[0])));
                                #endregion
                            }                           
                            break;
                        case "AI":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_R(true)));
                                Thread.Sleep(200);
                            }
                            break;
                        case "UK":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_R(true)));
                                Thread.Sleep(200);
                            }
                            break;
                        default:
                            #region check Search  Result Block or Not Block in GDS

                            bool NrmResult = true;
                            List<SearchResultBlock> GDSResultBLockList_NRM;
                            bool CrpResult = true;
                            List<SearchResultBlock> GDSResultBLockList_CRP;
                            if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                            {
                                try
                                {
                                    ResultExcludeList = null;
                                    List<SearchResultBlock> ResultExcludeList_NRM;
                                    List<SearchResultBlock> ResultExcludeList_CRP;
                                    ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    ResultExcludeList_NRM = ResultExcludeList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    ResultExcludeList_CRP = ResultExcludeList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();

                                    //ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == CrdListCPN[0].CrdType && x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    GDSResultBLockList_NRM = SearchBlockList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    if (GDSResultBLockList_NRM != null && GDSResultBLockList_NRM.Count > 0 && (ResultExcludeList_NRM == null || ResultExcludeList_NRM.Count < 1))
                                    {
                                        NrmResult = false;
                                    }

                                    GDSResultBLockList_CRP = SearchBlockList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                    if (GDSResultBLockList_CRP != null && GDSResultBLockList_CRP.Count > 0 && (ResultExcludeList_CRP == null || ResultExcludeList_CRP.Count < 1))
                                    {
                                        CrpResult = false;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            #endregion
                            if (NrmResult == true)
                            {
                                taskList.Add(Task.Run(() => GDSAvailability_O(false, "", "", false, searchInputs)));
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => GDSAvailability_R(false, "", "")));
                            }

                            if (CrpResult == true && (PFRow != null && PFRow.Count() > 0) || (Crd1GCRP != null && Crd1GCRP.Count > 0))
                            {
                                #region Call Service according to Deal Code in GDS 17-01-2019
                                try
                                {
                                    DataRow[] PCRow = { };
                                    PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");
                                    if (PCRow.Count() <= 0)
                                    {
                                        PCRow = PFCodeDt.Select("AirCode='ALL'   and   AppliedOn = 'BOTH'");
                                        //PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "'  or AirCode='ALL'");
                                    }
                                    if (PCRow.Count() > 0)
                                    {
                                        for (int i = 0; i < PCRow.Count(); i++)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                {
                                                    string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                    string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => GDSAvailability_R(true, D_T_Code, AppliedOn)));
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O-GDSAvailability_R");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                }
                                #endregion
                            }
                            break;
                    }
                }
                else
                {
                    switch (AirCode)
                    {                        
                        case "6E":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                            }
                            else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y"))
                            {
                                #region 6E CALLING                             
                                for (int e = 0; e < CrdListCPN.Count(); e++)
                                {
                                    CredentialList objCRD = CrdListCPN[e];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (e < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread th6ECPN_O = null;

                                                if (CrdListCPN[e].ResultFrom == "SP")
                                                {
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                    // taskList.Add(Task.Run(async () => await CPNAvailability_O(context, CrdListCPN[e])));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailabilityV4_O(objCRD)));
                                                    }
                                                    else if (objCRD.Provider == "RI")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => TravarenaAvailability_O(true)));

                                                    }
                                                    else
                                                    {
                                                        //th6ECPN_O = new Thread(() => IndigoAvailability_O(CrdListCPN[e])); 
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => IndigoAvailability_O(objCRD)));
                                                        //taskList.Add(Task.Run(async () => await IndigoAvailability_O(CrdListCPN[e])));
                                                    }
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case "SG":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                            }
                            else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                #region SG CALLING                             
                                for (int p = 0; p < CrdListCPN.Count(); p++)
                                {
                                    CredentialList objCRD = CrdListCPN[p];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                                ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(ProcessRequestCoupon)", "Error_001", ex, "Check CPN and CRP result block");
                                            }

                                            #endregion

                                            if (p < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                if (CrdListCPN[p].ResultFrom == "SP")
                                                {
                                                    
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                }
                                                else
                                                {
                                                    if (objCRD.ServerIP == "V4")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailabilityV4_O(objCRD)));
                                                    }
                                                    else if (objCRD.Provider == "RI")
                                                    {
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => TravarenaAvailability_O(true)));

                                                    }
                                                    else
                                                    {
                                                        //thSGCPN_O = new Thread(() => SpicejetAvailability_O(CrdListCPN[p]));
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => SpicejetAvailability_O(objCRD)));
                                                    }
                                                }                                                
                                            }
                                        }
                                        catch { }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case "G8":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                            }
                            else if (CrdListCPN.Count > 0 && (searchInputs.Cabin == "" || searchInputs.Cabin.Trim().ToUpper() == "Y" || searchInputs.Cabin.Trim().ToUpper() == "C"))
                            {
                                #region G8 CALLING                                
                                for (int g = 0; g < CrdListCPN.Count(); g++)
                                {
                                    CredentialList objCRD = CrdListCPN[g];
                                    if (objCRD != null)
                                    {
                                        try
                                        {
                                            #region Check CPN and CRP result block
                                            bool ResultShow = true;
                                            try
                                            {
                                                List<SearchResultBlock> AirlineResultBLockList = null;
                                                List<SearchResultBlock> ResultExcludeListById = null;
                                                ResultExcludeListById = objfltBal.GetFlightResultBlockList(objCRD.Trip, searchInputs.Provider, objCRD.AirlineCode, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();
                                                if (ResultBLockList != null && ResultBLockList.Count > 0 && (ResultExcludeListById == null || ResultExcludeListById.Count < 1))
                                                {
                                                    AirlineResultBLockList = ResultBLockList.Where(x => x.FareType == objCRD.CrdType && x.Airline == objCRD.AirlineCode && x.Trip == objCRD.Trip).ToList();

                                                    if (AirlineResultBLockList == null || AirlineResultBLockList.Count < 1)
                                                    {
                                                        ResultShow = true;
                                                    }
                                                    else
                                                    {
                                                        ResultShow = false;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ResultShow = true;
                                            }

                                            #endregion

                                            if (g < CrdListCPN.Count() && ResultShow == true)
                                            {
                                                //Thread thG8CPN_O = null;
                                                if (CrdListCPN[g].ResultFrom == "SP")
                                                {
                                                    // thG8CPN_O = new Thread(() => CPNAvailability_O(context, CrdListCPN[g]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => CPNAvailability_O(ctx, objCRD)));
                                                }
                                                else if (objCRD.Provider == "RI")
                                                {
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => TravarenaAvailability_O(true)));

                                                }
                                                else
                                                {
                                                    //thG8CPN_O = new Thread(() => G8Availability_O(CrdListCPN[g]));
                                                    Thread.Sleep(200);
                                                    taskList.Add(Task.Run(() => G8Availability_O(objCRD)));
                                                }
                                                //thG8CPN_O.Start();
                                                //vlist.Add(thG8CPN_O);
                                                //vlist1.Add(DateTime.Now);

                                                //Thread.Sleep(60);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                                #endregion
                            }
                            break;
                        case "IX":                            
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                #region IX CALLING   
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => G8Availability_O_old(CrdListCPN[0])));
                                #endregion
                            }


                            break;

                        case "AK":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(true)));
                            }
                            else if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {                                  
                                taskList.Add(Task.Run(() => AirAsiaAvailability_O(CrdListCPN[0])));                                
                            }
                            else if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => IweenAirAsiaAvailability_O(CrdListCPN[0])));
                            }
                            break;
                        case "G9":
                            if (CrdListCPN.Count > 0 && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                taskList.Add(Task.Run(() => AirArabiaAvailabilityReq(CrdListCPN[0])));
                            }
                            break;
                        case "UK":
                            // AirArabiaAvailability_O();
                            if (Provider.Trim().ToUpper() == "TB" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                //TBOAvailability_O(true);
                                taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                                Thread.Sleep(200);
                            }
                            else if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(false)));
                            }
                            break;
                        case "AI":
                            // AirArabiaAvailability_O();
                            if (Provider.Trim().ToUpper() == "TB" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                //TBOAvailability_O(true);
                                taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                                Thread.Sleep(200);
                            }
                            else if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(false)));
                            }
                            break;
                        case "OF":
                            if (Provider.Trim().ToUpper() == "TQ" && (ResultBLockList == null || ResultBLockList.Count < 1))
                            {
                                Thread.Sleep(200);
                                taskList.Add(Task.Run(() => ITQAirApiAvailability_O(false)));
                            }
                            break;
                        //case "LB":

                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }

                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "VA":
                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        // YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "FZ":

                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }
                        //    break;
                        //case "2T":
                        //    if (Provider.Trim().ToUpper() == "TB")
                        //    {
                        //        //TBOAvailability_O(true);
                        //        taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    }
                        //    else if (Provider.Trim().ToUpper() == "YA")
                        //    {
                        //        //YAAvailability_O(true);
                        //        taskList.Add(Task.Run(() => YAAvailability_O(true)));
                        //    }

                        //    break;

                        //case "OF":
                        //    //if (Provider.Trim().ToUpper() == "TB")
                        //    //TBOAvailability_O(true);
                        //    taskList.Add(Task.Run(() => TBOAvailability_O(true)));
                        //    //else if (Provider.Trim().ToUpper() == "YA")
                        //    //    YAAvailability_O(true);
                        //    break;

                        default:
                            //GDSAvailability_O(false);
                            #region check Search  Result Block or Not Block in GDS

                            bool NrmResult = true;
                            List<SearchResultBlock> GDSResultBLockList_NRM;
                            bool CrpResult = true;
                            List<SearchResultBlock> GDSResultBLockList_CRP;
                            if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                            {
                                ResultExcludeList = null;
                                List<SearchResultBlock> ResultExcludeList_NRM;
                                List<SearchResultBlock> ResultExcludeList_CRP;
                                ResultExcludeList = objfltBal.GetFlightResultBlockList(searchInputs.Trip.ToString(), searchInputs.Provider, searchInputs.HidTxtAirLine, searchInputs.AgentType, searchInputs.UID, "", "Exclude").Where(x => x.Airline == AirCode && x.Trip == searchInputs.Trip.ToString()).ToList();
                                ResultExcludeList_NRM = ResultExcludeList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                ResultExcludeList_CRP = ResultExcludeList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();


                                GDSResultBLockList_NRM = SearchBlockList.Where(x => x.FareType == "NRM" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                if (GDSResultBLockList_NRM != null && GDSResultBLockList_NRM.Count > 0 && (ResultExcludeList_NRM == null || ResultExcludeList_NRM.Count < 1))
                                {
                                    NrmResult = false;
                                }

                                GDSResultBLockList_CRP = SearchBlockList.Where(x => x.FareType == "CRP" && x.Airline == Convert.ToString(searchInputs.HidTxtAirLine) && x.Trip == searchInputs.Trip.ToString()).ToList();
                                if (GDSResultBLockList_CRP != null && GDSResultBLockList_CRP.Count > 0 && (ResultExcludeList_CRP == null || ResultExcludeList_CRP.Count < 1))
                                {
                                    CrpResult = false;
                                }
                            }
                            #endregion

                            if (NrmResult == true)// && AirCode == "DEVESH")
                            {                                
                                taskList.Add(Task.Run(() => GDSAvailability_O(false, "", "", false, searchInputs)));
                                Thread.Sleep(200);
                            }

                            if (CrpResult == true && ((PFRow != null && PFRow.Count() > 0) || ((Crd1GCRP != null && Crd1GCRP.Count > 0) && Convert.ToString(searchInputs.Trip) == "D")))
                            {
                                if (Convert.ToString(searchInputs.Trip) == "I" && PFRow != null && PFRow.Count() > 0)
                                {
                                    #region Call Service according to Deal Code in GDS 17-01-2019
                                    bool IsHidAirLineClear = true; ;
                                    try
                                    {
                                        DataRow[] PCRow = { };
                                        if (!string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                                        {
                                            PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");
                                            IsHidAirLineClear = false;
                                        }
                                        else
                                        {
                                            PCRow = PFCodeDt.Select("AirCode<>'ALL' and AppliedOn = 'BOTH'");
                                        }
                                        // FlightSearch ObjfltSearch = (FlightSearch) searchInputs.Clone();
                                        if (PCRow.Count() > 0)
                                        {
                                            for (int i = 0; i < PCRow.Count(); i++)
                                            {
                                                try
                                                {
                                                    FlightSearch searchInputsClone = (FlightSearch)searchInputs.Clone();
                                                    if (string.IsNullOrEmpty(searchInputs.HidTxtAirLine))
                                                    {
                                                        //searchInputs.HidTxtAirLine = "9W";
                                                        //searchInputs.AirLine = "Jet Airways,9W";
                                                        searchInputsClone.HidTxtAirLine = Convert.ToString(PCRow[i]["AirCode"]);//"9W";
                                                        searchInputsClone.AirLine = Convert.ToString(PCRow[i]["AirlineName"]) + "," + Convert.ToString(PCRow[i]["AirCode"]); //"Jet Airways,9W";
                                                    }

                                                    if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                    {
                                                        string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                        string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                        Thread.Sleep(200);
                                                        taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, true, searchInputsClone)));
                                                        Thread.Sleep(200);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_CRP_INT");
                                                }
                                                //if (IsHidAirLineClear == true)
                                                //{
                                                //    searchInputs.HidTxtAirLine = "";
                                                //    searchInputs.AirLine = "";
                                                //}
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Call Service according to Deal Code in GDS 17-01-2019
                                    try
                                    {
                                        DataRow[] PCRow = { };
                                        PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "' and   AppliedOn = 'BOTH' ");
                                        if (PCRow.Count() <= 0)
                                        {
                                            PCRow = PFCodeDt.Select("AirCode='ALL'   and   AppliedOn = 'BOTH'");
                                            //PCRow = PFCodeDt.Select("AirCode='" + Utility.Right(searchInputs.HidTxtAirLine, 2) + "'  or AirCode='ALL'");
                                        }
                                        if (PCRow.Count() > 0)
                                        {
                                            // Call 1G Service deal code wise
                                            for (int i = 0; i < PCRow.Count(); i++)
                                            {
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(Convert.ToString(PCRow[i]["D_T_Code"])))
                                                    {
                                                        string D_T_Code = Convert.ToString(PCRow[i]["D_T_Code"]);
                                                        string AppliedOn = Convert.ToString(PCRow[i]["AppliedOn"]);
                                                        taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                                        Thread.Sleep(200);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_CRP");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //deal code not avilable only  corporate credential only
                                            string D_T_Code = "";
                                            string AppliedOn = "";
                                            taskList.Add(Task.Run(() => GDSAvailability_O(true, D_T_Code, AppliedOn, false, searchInputs)));
                                            Thread.Sleep(200);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("FltResult(T ProcessRequest)", "Error_001", ex, "GDSAvailability_O");
                                    }
                                    #endregion

                                }
                            }
                            //TBOAvailability_O(false);
                            break;
                    }

                }
                #endregion

                Task.WhenAll(taskList).Wait();
                //#region Wait for Thread
                //int counter = 0;
                //while ((counter < vlist.Count))
                //{
                //    Thread TH = (Thread)vlist[counter];
                //    if ((TH.ThreadState == ThreadState.WaitSleepJoin))
                //    {
                //        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                //        if ((DIFF.Seconds > 45))
                //        {
                //            TH.Abort();
                //            counter += 1;
                //        }
                //    }
                //    else if ((TH.ThreadState == ThreadState.Stopped))
                //    {
                //        counter += 1;
                //    }
                //}
                //#endregion
                //DataTable dtFareType = (DataTable)System.Web.HttpContext.Current.Session["Faretypemaster"];

                #region Add Different Result Lists Here
                //Merge into 1 List with Consecutive Line No.s
                if (objFltResultList_O.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_O);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_O);

                }
                if (objFltResultList_R.Count >= 1)
                {
                    objFltResultList.Add(objFltResultList_R);
                }
                else
                {
                    if (RTFare == true)
                        objFltResultList.Add(objFltResultList_R);

                }

                #endregion
                bool isSpl = searchInputs.GDSRTF == true || searchInputs.RTF == true ? true : false;
                //objFltResultList.AddSuperLineNum(searchInputs.TripType, isSpl);
                objFltResultList.AddSuperLineNumNew(searchInputs.TripType, isSpl,FareTypeMasterList);

                Finallist = objFlightCommonBAL.MergeResultList(objFltResultList);
                //Insert Cache
                //Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //thread.Start();

                //if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF" && (AirCode == "6E" || AirCode == "SG" || AirCode == "G8"))
                if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF" && (string.IsNullOrEmpty(searchInputs.Cabin.Trim()) || searchInputs.Cabin.Trim() == "Y"))
                {
                    Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                    thread.Start();
                }





                //Insert Cache
                //Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //thread.Start();

                //if (searchInputs.Trip.ToString() != "I" && searchInputs.Provider != "OF")
                //{
                //    Thread thread = new Thread(() => InsertCache(Finallist, AirCode, ""));
                //    thread.Start();
                //}

                string s = GetEntityXml(Finallist);
                try
                {
                    File.AppendAllText("C:\\\\CPN_SP\\\\ListXml_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + s + Environment.NewLine);
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

                var abc = ex.Message.ToString();
                // WriteToDrive(@"C:\resulterror\" + DateTime.Now.ToString("dd-MM-yyyy") + "\\" + "", "" + "_" + DateTime.Now.ToString("hh_mm_ss_tt") + "_IATACommission_Request.txt", abc);
            }
            return Finallist as T;

        }
        
        #endregion
    }
}