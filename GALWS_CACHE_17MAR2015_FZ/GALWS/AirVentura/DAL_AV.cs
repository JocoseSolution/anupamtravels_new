﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using STD.Shared;
using System.Data.Common;
using System.Collections;

namespace STD.DAL
{
    public class DAL_AV
    {
        private SqlDatabase DBHelper;
        public DAL_AV(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }
        public int InsertAVBookingLogs(string ORDERID, string PNR, string HOLDREQ, string HOLDENCRYPTEDRES, string HOLDRES, string BOOKINGREQ, string BOOKINGENCRYPTEDRES, string BOOKINGRES, string FARERULEONEWAY, string FARERULEROUNDTRIP, string OTHER, string PROVIDER)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERTAVBOOKINGLOGS";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@PNR", DbType.String, PNR);
            DBHelper.AddInParameter(cmd, "@HOLDREQ", DbType.String, HOLDREQ);
            DBHelper.AddInParameter(cmd, "@HOLDENCRYPTEDRES", DbType.String, HOLDENCRYPTEDRES);
            DBHelper.AddInParameter(cmd, "@HOLDRES", DbType.String, HOLDRES);
            DBHelper.AddInParameter(cmd, "@BOOKINGREQ", DbType.String, BOOKINGREQ);
            DBHelper.AddInParameter(cmd, "@BOOKINGENCRYPTEDRES", DbType.String, BOOKINGENCRYPTEDRES);
            DBHelper.AddInParameter(cmd, "@BOOKINGRES", DbType.String, BOOKINGRES);
            DBHelper.AddInParameter(cmd, "@FARERULEONEWAY", DbType.String, FARERULEONEWAY);
            DBHelper.AddInParameter(cmd, "@FARERULEROUNDTRIP", DbType.String, FARERULEROUNDTRIP);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);
            DBHelper.AddInParameter(cmd, "@PROVIDER", DbType.String, PROVIDER);
            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }
        public bool GetVASearchStatus(string ORIGIN, string DEST, string PROVIDER)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_GETROUTELIST";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORIGIN", DbType.String, ORIGIN);
            DBHelper.AddInParameter(cmd, "@DEST", DbType.String, DEST);
            DBHelper.AddInParameter(cmd, "@PROVIDER", DbType.String, PROVIDER);
            bool i = Convert.ToBoolean(DBHelper.ExecuteScalar(cmd));
            return i;
        }
    }
}
