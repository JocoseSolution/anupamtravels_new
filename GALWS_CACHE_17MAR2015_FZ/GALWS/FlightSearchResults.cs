﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace STD.Shared
//{

//    public class FlightSearchResults : ICloneable
//    {
//        public string OrgDestFrom { get; set; }
//        public string OrgDestTo { get; set; }
//        public string DepartureLocation { get; set; }
//        public string DepartureCityName { get; set; }
//        public string DepAirportCode { get; set; }
//        public string DepartureAirportName { get; set; }
//        public string DepartureTerminal { get; set; }
//        public string ArrivalLocation { get; set; }
//        public string ArrivalCityName { get; set; }
//        public string ArrAirportCode { get; set; }
//        public string ArrivalAirportName { get; set; }
//        public string ArrivalTerminal { get; set; }
//        public string DepartureDate { get; set; }
//        public string Departure_Date { get; set; }
//        public string DepartureTime { get; set; }
//        public string ArrivalDate { get; set; }
//        public string Arrival_Date { get; set; }
//        public string ArrivalTime { get; set; }
//        public string MarketingCarrier { get; set; }
//        public string OperatingCarrier { get; set; }
//        public string ValiDatingCarrier { get; set; }
//        public string AirlineName { get; set; }
//        public string FlightIdentification { get; set; }
//        public string EQ { get; set; }
//        public string ElectronicTicketing { get; set; }
//        public string ProductDetailQualifier { get; set; }
//        public string RBD { get; set; }
//        public int Adult { get; set; }
//        public int Child { get; set; }
//        public int Infant { get; set; }
//        public int TotPax { get; set; }
//        public float InfBfare { get; set; }
//        public float InfTax { get; set; }
//        public float InfFSur { get; set; }
//        public float InfYR { get; set; }
//        public float InfWO { get; set; }
//        public float InfIN { get; set; }
//        public float InfQ { get; set; }
//        public float InfJN { get; set; }
//        public float InfEduCess { get; set; }
//        public float InfHighEduCess { get; set; }
//        public float InfOT { get; set; }
//        public float InfFare { get; set; }
//        public string InfBreakPoint { get; set; }
//        public string InfFareType { get; set; }
//        public string InfFarebasis { get; set; }
//        public string InfAvlStatus { get; set; }
//        public string InfCabin { get; set; }
//        public string InfRbd { get; set; }
//        public float InfDiscount { get; set; }
//        public float InfCB { get; set; }
//        public float InfAdminMrk { get; set; }
//        public float InfDistMrk { get; set; }
//        public float InfAgentMrk { get; set; }
//        public float InfSrvTax { get; set; }
//        public float ChdBFare { get; set; }
//        public float ChdTax { get; set; }
//        public float ChdFSur { get; set; }
//        public float ChdYR { get; set; }
//        public float ChdWO { get; set; }
//        public float ChdIN { get; set; }
//        public float ChdQ { get; set; }
//        public float ChdJN { get; set; }
//        public float ChdEduCess { get; set; }
//        public float ChdHighEduCess { get; set; }
//        public float ChdOT { get; set; }
//        public float ChdFare { get; set; }
//        public string ChdBreakPoint { get; set; }
//        public string ChdfareType { get; set; }
//        public string ChdFarebasis { get; set; }
//        public string ChdAvlStatus { get; set; }
//        public string ChdCabin { get; set; }
//        public string ChdRbd { get; set; }
//        public float ChdDiscount { get; set; }
//        public float ChdCB { get; set; }
//        public float CHDAdminMrk { get; set; }
//        public float CHDDistMrk { get; set; }
//        public float CHDAgentMrk { get; set; }
//        public float ChdSrvTax { get; set; }
//        public float AdtBfare { get; set; }
//        public float AdtTax { get; set; }
//        public float AdtFSur { get; set; }
//        public float AdtYR { get; set; }
//        public float AdtWO { get; set; }
//        public float AdtIN { get; set; }
//        public float AdtQ { get; set; }
//        public float AdtJN { get; set; }
//        public float AdtEduCess { get; set; }
//        public float AdtHighEduCess { get; set; }
//        public float AdtOT { get; set; }
//        public float AdtFare { get; set; }
//        public string AdtBreakPoint { get; set; }
//        public string AdtFareType { get; set; }
//        public string AdtFarebasis { get; set; }
//        public string AdtAvlStatus { get; set; }
//        public string AdtCabin { get; set; }
//        public string AdtRbd { get; set; }
//        public float AdtDiscount { get; set; }
//        public float AdtCB { get; set; }
//        public float ADTAdminMrk { get; set; }
//        public float ADTDistMrk { get; set; }
//        public float ADTAgentMrk { get; set; }
//        public float AdtSrvTax { get; set; }
//        public float STax { get; set; }
//        public float TFee { get; set; }
//        public float TotDis { get; set; }
//        public float TotMrkUp { get; set; }
//        public float IATAComm { get; set; }
//        public float TotBfare { get; set; }
//        public float TotalTax { get; set; }
//        public float TotalFare { get; set; }
//        public float TotalFuelSur { get; set; }
//        public int LineNumber { get; set; }
//        public int Leg { get; set; }
//        public string Searchvalue { get; set; }
//        public string Flight { get; set; }
//        public string TotDur { get; set; }
//        public string TripType { get; set; }       
//        public string Stops { get; set; }
//        public string AirLineName { get; set; }
//        public string Trip { get; set; }
//        public string Sector { get; set; }
//        public string TripCnt { get; set; }

//        public string fareBasis { get; set; }
//        public string FBPaxType { get; set; }
//        public string sno { get; set; }
//        public string depdatelcc { get; set; }
//        public string arrdatelcc { get; set; }
//        public float OriginalTF { get; set; }
//        public float OriginalTT { get; set; }
//        public string Track_id { get; set; }
//        public string FType { get; set; }
//        public string FareRule { get; set; }
//        public string InfFar { get; set; }
//        public string ChdFar { get; set; }
//        public string AdtFar { get; set; }
//        public string FareDet { get; set; }
//        public bool IsLTC { get; set; }

//        #region ICloneable Members

//        public object Clone()
//        {
//            return this.MemberwiseClone();
//        }


//        #endregion
//    }


//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace STD.Shared
{

    public class FlightSearchResults : ICloneable
    {
        #region FlightDetails
        public string OrgDestFrom { get; set; }
        public string OrgDestTo { get; set; }
        public string DepartureLocation { get; set; }
        public string DepartureCityName { get; set; }
        public string DepAirportCode { get; set; }//added TO TABLE
        public string DepartureAirportName { get; set; }
        public string DepartureTerminal { get; set; }//added TO TABLE
        public string ArrivalLocation { get; set; }
        public string ArrivalCityName { get; set; }
        public string ArrAirportCode { get; set; }//added TO TABLE
        public string ArrivalAirportName { get; set; }
        public string ArrivalTerminal { get; set; }// added  TO TABLE
        public string DepartureDate { get; set; }
        public string Departure_Date { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalDate { get; set; }
        public string Arrival_Date { get; set; }
        public string ArrivalTime { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public int Infant { get; set; }
        public int TotPax { get; set; }
        public string MarketingCarrier { get; set; }
        public string OperatingCarrier { get; set; }//added TO TABLE
        public string FlightIdentification { get; set; }
        public string ValiDatingCarrier { get; set; }
        public string AirLineName { get; set; }
        public string AvailableSeats { get; set; }
        public string AvailableSeats1 { get; set; }
        public string ElectronicTicketing { get; set; }// added TO TABLE
        public string ProductDetailQualifier { get; set; }////
        public string getVia { get; set; }
        public string BagInfo { get; set; }
        public string ProviderUserID { get; set; }

       // public string AdtCabin { get; set; }//added TO TABLE
        string _AdtCabin;
        public string AdtCabin
        {
            get { return _AdtCabin ?? ""; }
            set { _AdtCabin = value; }
        }

        public string ChdCabin { get; set; }//added TO TABLE
        public string InfCabin { get; set; }//added TO TABLE
        public string AdtRbd { get; set; }//added TO TABLE
        public string ChdRbd { get; set; }//added TO TABLE
        public string InfRbd { get; set; }//added TO TABLE
        public string AdtFareType { get; set; }//added TO TABLE
        public string AdtFarebasis { get; set; }//added TO TABLE
        public string ChdfareType { get; set; }//added TO TABLE
        public string ChdFarebasis { get; set; }//added TO TABLE
        public string InfFareType { get; set; }//added TO TABLE
        public string InfFarebasis { get; set; }//added TO TABLE -farebasis changed-to- Farebasis
        public string InfFar { get; set; }////
        public string ChdFar { get; set; }////
        public string AdtFar { get; set; }/////
        public string AdtBreakPoint { get; set; }
        public string AdtAvlStatus { get; set; }
        public string ChdBreakPoint { get; set; }
        public string ChdAvlStatus { get; set; }
        public string InfBreakPoint { get; set; }
        public string InfAvlStatus { get; set; }
        public string fareBasis { get; set; }
        public string FBPaxType { get; set; }
        public string RBD { get; set; }
        public string FareRule { get; set; }
        public string FareDet { get; set; }
        #endregion

        #region AdtFareDetails       
        public float AdtFare { get; set; }
        public float AdtBfare { get; set; }
        public float AdtTax { get; set; }
        public float AdtFSur { get; set; }//add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
        public float AdtYR { get; set; }
        public float AdtWO { get; set; }
        public float AdtIN { get; set; }
        [DefaultValue(0)]
        public float AdtQ { get; set; }
        public float AdtJN { get; set; }
        public float AdtOT { get; set; }//// added TO TABLE
        [DefaultValue(0)]
        public float AdtSrvTax { get; set; }   // added TO TABLE
        [DefaultValue(0)]
        public float AdtSrvTax1 { get; set; }   // added TO TABLE
        public float AdtEduCess { get; set; }
        public float AdtHighEduCess { get; set; }
        public float AdtTF { get; set; }
        public float ADTAdminMrk { get; set; }
        public float ADTAgentMrk { get; set; }
        public float ADTDistMrk { get; set; }
        [DefaultValue(0)]
        public float AdtDiscount { get; set; }  //-AdtComm
        [DefaultValue(0)]
        public float AdtDiscount1 { get; set; }
        public float AdtCB { get; set; }
        public float AdtTds { get; set; }
        [DefaultValue(0)]
        public float AdtMgtFee { get; set; }
        #endregion

        #region CHDFareDetails
        [DefaultValue(0)]
        public float ChdFare { get; set; }
        [DefaultValue(0)]
        public float ChdBFare { get; set; }
        [DefaultValue(0)]
        public float ChdTax { get; set; }
        [DefaultValue(0)]
        public float ChdFSur { get; set; }
        [DefaultValue(0)]
        public float ChdYR { get; set; }
        [DefaultValue(0)]
        public float ChdWO { get; set; }
        [DefaultValue(0)]
        public float ChdIN { get; set; }
        [DefaultValue(0)]
        public float ChdQ { get; set; }
        [DefaultValue(0)]
        public float ChdJN { get; set; }
        [DefaultValue(0)]
        public float ChdOT { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float ChdSrvTax { get; set; }// added TO TABLE 
        [DefaultValue(0)]
        public float ChdSrvTax1 { get; set; }// added TO TABLE 
        [DefaultValue(0)]
        public float ChdEduCess { get; set; }
        [DefaultValue(0)]
        public float ChdHighEduCess { get; set; }
        [DefaultValue(0)]
        public float ChdTF { get; set; }
        [DefaultValue(0)]
        public float CHDAdminMrk { get; set; }
        [DefaultValue(0)]
        public float CHDAgentMrk { get; set; }
        [DefaultValue(0)]
        public float CHDDistMrk { get; set; }
        [DefaultValue(0)]
        public float ChdTds { get; set; }
        [DefaultValue(0)]
        public float ChdDiscount { get; set; } //-ChdComm
        [DefaultValue(0)]
        public float ChdDiscount1 { get; set; } //-ChdComm
        [DefaultValue(0)]
        public float ChdCB { get; set; }
        [DefaultValue(0)]
        public float ChdMgtFee { get; set; }
        #endregion

        #region INFFareDetails
        [DefaultValue(0)]
        public float InfFare { get; set; }
        [DefaultValue(0)]
        public float InfBfare { get; set; }
        [DefaultValue(0)]
        public float InfTax { get; set; }
        [DefaultValue(0)]
        public float InfFSur { get; set; }//YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              
        [DefaultValue(0)]
        public float InfYR { get; set; }
        [DefaultValue(0)]
        public float InfWO { get; set; }
        [DefaultValue(0)]
        public float InfIN { get; set; }
        [DefaultValue(0)]
        public float InfQ { get; set; }
        [DefaultValue(0)]
        public float InfJN { get; set; }
        [DefaultValue(0)]
        public float InfOT { get; set; }// added TO TABLE         
        [DefaultValue(0)]
        public float InfSrvTax { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float InfSrvTax1 { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float InfEduCess { get; set; }////
        [DefaultValue(0)]
        public float InfHighEduCess { get; set; }////
        [DefaultValue(0)]
        public float InfTF { get; set; }
        [DefaultValue(0)]
        public float InfAdminMrk { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float InfAgentMrk { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float InfDistMrk { get; set; }
        [DefaultValue(0)]
        public float InfTds { get; set; }
        [DefaultValue(0)]
        public float InfDiscount { get; set; }//InfComm       
        [DefaultValue(0)]
        public float InfDiscount1 { get; set; }//InfComm 
        [DefaultValue(0)]
        public float InfCB { get; set; }// added TO TABLE
        [DefaultValue(0)]
        public float InfMgtFee { get; set; }
        #endregion

        #region TotFareDetails
        [DefaultValue(0)]
        public float NetFareSRTF { get; set; }
        [DefaultValue(0)]
        public float TotBfare { get; set; }
        [DefaultValue(0)]
        public float TotalFare { get; set; }
        [DefaultValue(0)]
        public float TotalTax { get; set; }
        [DefaultValue(0)]
        public float TotalFuelSur { get; set; }
        [DefaultValue(0)]
        public float NetFare { get; set; }
        [DefaultValue(0)]
        public float TotMrkUp { get; set; }
        [DefaultValue(0)]
        public float STax { get; set; }
        [DefaultValue(0)]
        public float TFee { get; set; }
        [DefaultValue(0)]
        public float TotDis { get; set; }
        [DefaultValue(0)]
        public float TotCB { get; set; }
        [DefaultValue(0)]
        public float TotTds { get; set; }
        [DefaultValue(0)]
        public float TotMgtFee { get; set; }
        #endregion

        #region OtherDetails
        [DefaultValue(0)]
        public float IATAComm { get; set; }
        public string Searchvalue { get; set; }
        public int LineNumber { get; set; }
        public int Leg { get; set; }
        public string Flight { get; set; }
        public string TotDur { get; set; }
        public string TripType { get; set; }
        public string EQ { get; set; }
        public string Stops { get; set; }
        public string Trip { get; set; }
        public string Sector { get; set; }
        public string TripCnt { get; set; }
        //Not in List But in Table
        //public string Currency { get; set; }       
        public string sno { get; set; }
        public string depdatelcc { get; set; }
        public string arrdatelcc { get; set; }
        public float OriginalTF { get; set; }
        public float OriginalTT { get; set; }
        public string Track_id { get; set; }
        public string FType { get; set; }//FlightStatus-Ftype
        //Not in List But in Table
        //Adt_Tax- ,Chd_Tax,Inf_Tax-Need to be Calculated        
        public bool IsCorp { get; set; }
        public bool IsLTC { get; set; }
        public string Provider { get; set; }
        public string SubKey { get; set; }
        public string MainKey { get; set; }
        public string SLineNumber { get; set; }
        public string SubSLineNumber { get; set; }
        public string TotalTripDur { get; set; }
        public string SearchId { get; set; }

        public string DisplayFareType { get; set; }
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        #endregion


        #region Baggage        
        public bool IsBagFare { get; set; }
        public string SSRCode { get; set; }
        #endregion

        #region SME FARE IsSMEFare
        public bool IsSMEFare { get; set; }
        #endregion
    }
}

