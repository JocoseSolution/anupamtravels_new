﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL.TBO.BOOKSHARED
{
    public class Fare
    {
        public decimal BaseFare { get; set; }
        public decimal Tax { get; set; }
        public decimal TransactionFee { get; set; }
        public decimal YQTax { get; set; }
        public decimal AdditionalTxnFeeOfrd { get; set; }
        public decimal AdditionalTxnFeePub { get; set; }
        public decimal AirTransFee { get; set; }
        public decimal? AdditionalTxnFee { get; set; }
    }

    public class Meal
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class Seat
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class Passenger
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PaxType { get; set; }
        public string DateOfBirth { get; set; }
        public int Gender { get; set; }
        public string PassportNo { get; set; }
        public string PassportExpiry { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public bool IsLeadPax { get; set; }
        public string FFAirline { get; set; }
        public string FFNumber { get; set; }
        public Fare Fare { get; set; }
        public Meal Meal { get; set; }
        public Seat Seat { get; set; }
    }

    public class PassengerLCC
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PaxType { get; set; }
        public string DateOfBirth { get; set; }
        public int Gender { get; set; }
        public string PassportNo { get; set; }
        public string PassportExpiry { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public bool IsLeadPax { get; set; }
        public string FFAirline { get; set; }
        public string FFNumber { get; set; }
        public Fare Fare { get; set; }
        public List<MealDynamic> MealDynamic { get; set; }
        public List<Baggage> Baggage { get; set; }
    }

    public class TboBookReqLCC
    {
        public string PreferredCurrency { get; set; }
        public string IsBaseCurrencyRequired { get; set; }
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string ResultIndex { get; set; }
        public List<PassengerLCC> Passengers { get; set; }
    }



    public class TboNonLCCTktReq
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string PNR { get; set; }
        public string BookingId { get; set; }
    }





    public class TboBookReq
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string ResultIndex { get; set; }
        public List<Passenger> Passengers { get; set; }
    }


    public class Reprice
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string TraceId { get; set; }
        public string ResultIndex { get; set; }
    }



    public class MealDynamic
    {
        public int WayType { get; set; }
        public string Code { get; set; }
        public int Description { get; set; }
        public string AirlineDescription { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public string Currency { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }

    public class Baggage
    {
        public int WayType { get; set; }
        public string Code { get; set; }
        public int Description { get; set; }
        public int Weight { get; set; }
        public string Currency { get; set; }
        public int Price { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }
}
