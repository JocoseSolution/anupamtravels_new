﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;


namespace GALWS
{
    public static class SaveResponse
    {
        public static Boolean AirlineCode(string air)
        {
            Boolean value = false;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            DataSet ds = new DataSet();
            SqlDataAdapter adap = new SqlDataAdapter("SP_Tbl_LogAirlinePermission", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@air", air);
            adap.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                value = Convert.ToBoolean(ds.Tables[0].Rows[0]["Status"]);
            }

            return value;
        }

        public static void SAVElOGFILE(string FILE, string REQRES, string type, string GDSORLCC, string Airline, string FareType)
        {
            try
            {
                  Random _r = new Random();
                  int rnd =  _r.Next(9999);
                //string newFileName = Res + DateTime.Now.ToString("hh:mm:ss");
                Boolean str = false;
                str = AirlineCode(Airline);
                if (str == true)
                {
                    string filename = Airline + "_" + REQRES + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Convert.ToString(rnd).ToString();
                    // string activeDir = ConfigurationManager.AppSettings["LOGSAVEURL"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + Airline + @"\";
                    string activeDir = ConfigurationManager.AppSettings["LOGSAVEURL"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + Airline + @"\" + FareType + @"\";
                    DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
                    if (!Directory.Exists(objDirectoryInfo.FullName))
                    {
                        Directory.CreateDirectory(activeDir);
                    }
                    if (type.ToUpper() == "XML")
                    {
                        XDocument xmlDoc = XDocument.Parse(FILE);
                        xmlDoc.Save(activeDir + @"\" + filename + ".xml");
                    }
                    if (type.ToUpper() == "TXT")
                    {

                        string path = activeDir + @"\" + filename + ".txt";//@"c:\temp\MyTest.txt";
                        if (!File.Exists(path))
                        {
                            // Create a file to write to.
                            using (StreamWriter sw = File.CreateText(path))
                            {
                                sw.Write(FILE);
                            }
                        }
                    }


                }
                
            }
            catch (Exception ex)
            {
            }



        }
    }
   
}
