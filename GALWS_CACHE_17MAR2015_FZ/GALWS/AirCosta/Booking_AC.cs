﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using STD.Shared;

namespace STD.BAL
{
    public class Booking_AC
    {
        BAL_AV BALAV = new BAL_AV(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        string VAPNR = ""; string AirlinePNR = ""; ArrayList TktNoArray = new ArrayList();
        public Hashtable AirCosta_Booking(string UserId, string Password, string OfficeId, DataSet FltDsGAL, DataSet FltHdr, DataSet PaxDS)
        {
            string HoldInventoryReqXML = ""; string HoldInventoryEncyptedREQ = ""; string HoldInventoryEncyptedRES = ""; string HoldInventoryEncyptedRESXML = "";
            string BookingREQXML = ""; string BookingEncyptedREQ = ""; string BookingEncyptedRES = ""; string BookingEncyptedRESXML = ""; string FareRuleOneWay = ""; string FareRuleRoundTrip = "";
            string FareRuleReqXMLOW = ""; string FareRuleEncyptedREQOW = ""; string FareRuleEncyptedRESOW = ""; string FareRuleEncyptedRESXMLOW = ""; string FareRuleReqXMLRT = ""; string FareRuleEncyptedREQRT = ""; string FareRuleEncyptedRESRT = ""; string FareRuleEncyptedRESXMLRT = "";
            string ErrorStatus = ""; Hashtable HSVCONNECT = new Hashtable(); string ORDERID = ""; string PROVIDER = "";
            try
            {

                ORDERID = FltHdr.Tables[0].Rows[0]["OrderId"].ToString();
                PROVIDER = FltHdr.Tables[0].Rows[0]["VC"].ToString();
                try
                {
                    DataRow[] droneway;
                    DataRow[] drround = new DataRow[0];

                    if (FltHdr.Tables[0].Rows[0]["TripType"].ToString().ToUpper() == "O")
                    {
                        droneway = FltDsGAL.Tables[0].Select("flight=1", "counter asc");

                    }
                    else
                    {
                        droneway = FltDsGAL.Tables[0].Select("flight=1", "counter asc");
                        drround = FltDsGAL.Tables[0].Select("flight=2", "counter asc");
                    }
                    if (droneway.Length > 0 || drround.Length > 0)
                    {
                        FareRuleReqXMLOW = GetFareRuleRQ(UserId, Password, OfficeId, droneway[0]["FareBasis"].ToString().ToUpper());
                        FareRuleEncyptedREQOW = Encrypt(FareRuleReqXMLOW);
                        FareRuleEncyptedRESOW = FareRulesAndRestrictions(FareRuleEncyptedREQOW, "https://www.aircosta.in/AirCostaWS/wsreservation.asmx");
                        FareRuleEncyptedRESXMLOW = Decrypt(FareRuleEncyptedRESOW);
                        if (drround.Length > 0)
                        {
                            FareRuleReqXMLRT = GetFareRuleRQ(UserId, Password, OfficeId, FltDsGAL.Tables[0].Rows[0]["FareBasis"].ToString().ToUpper());
                            FareRuleEncyptedREQRT = Encrypt(FareRuleReqXMLRT);
                            FareRuleEncyptedRESRT = FareRulesAndRestrictions(FareRuleEncyptedREQRT, "https://www.aircosta.in/AirCostaWS/wsreservation.asmx");
                            FareRuleEncyptedRESXMLRT = Decrypt(FareRuleEncyptedRESRT);
                        }
                    }
                }
                catch (Exception ex)
                { }

                try
                {
                    HoldInventoryReqXML = GetHoldInventoryRQ(UserId, Password, OfficeId, FltHdr.Tables[0].Rows[0]["TripType"].ToString().ToUpper(), FltDsGAL);
                    HoldInventoryEncyptedREQ = Encrypt(HoldInventoryReqXML);
                    HoldInventoryEncyptedRES = HoldInventory(HoldInventoryEncyptedREQ, "https://www.aircosta.in/AirCostaWS/wsFlight.asmx");
                    HoldInventoryEncyptedRESXML = Decrypt(HoldInventoryEncyptedRES);
                }
                catch (Exception ex)
                {
                    VAPNR = "LB" + Utility.GetRndm() + "-FQ";
                    HSVCONNECT.Add("VAPNR", VAPNR);
                    HSVCONNECT.Add("AIRLINEPNR", AirlinePNR);
                    HSVCONNECT.Add("TKTNOARRAY", TktNoArray);
                    BALAV.InsertAVBookingLogs(ORDERID, "", HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, "", "", "", FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, ex.ToString(), PROVIDER);
                }
                XDocument xdoc = XDocument.Parse(HoldInventoryEncyptedRESXML);
                ErrorStatus = xdoc.Descendants("HoldInventoryRS").Descendants("Errors").Attributes("Status").First().Value;
                if (ErrorStatus.Trim().ToUpper() == "FALSE")
                {
                    try
                    {
                        var holdId = from IMP in xdoc.Descendants("HoldInventoryRS").Descendants("Hold")
                                     select new
                                     {
                                         HoldId = IMP.Attribute("HoldID").Value,
                                         Status = IMP.Attribute("Status").Value

                                     };
                        ArrayList HoldId = new ArrayList();
                        foreach (var hID in holdId)
                        {
                            HoldId.Add(hID.HoldId.ToString());
                        }
                        BookingREQXML = GetCreateBookingRQ(UserId, Password, OfficeId, FltHdr.Tables[0].Rows[0]["TripType"].ToString().ToUpper(), Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Adult"].ToString()), Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Child"].ToString()), Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Infant"].ToString()), HoldId, FltDsGAL, FltHdr, PaxDS);
                        BookingEncyptedREQ = Encrypt(BookingREQXML);
                        BookingEncyptedRES = CreateBooking(BookingEncyptedREQ, "https://www.aircosta.in/AirCostaWS/wsreservation.asmx");
                        BookingEncyptedRESXML = Decrypt(BookingEncyptedRES);
                        ErrorStatus = "";
                        XDocument xdocpnr = XDocument.Parse(BookingEncyptedRESXML);
                        ErrorStatus = xdocpnr.Descendants("CreateBookingRS").Descendants("Errors").Attributes("Status").First().Value;
                        if (ErrorStatus.Trim().ToUpper() == "FALSE")
                        {
                            try
                            {
                                VAPNR = xdocpnr.Descendants("CreateBookingRS").Descendants("Reservation").Attributes("PNRNo").First().Value;
                                var PaxInfo = from IMP in xdocpnr.Descendants("CreateBookingRS").Descendants("PaxInfo").Descendants("Pax")
                                              select new
                                              {
                                                  Title = IMP.Attribute("Title").Value,
                                                  FirstName = IMP.Attribute("FirstName").Value,
                                                  LastName = IMP.Attribute("LastName").Value,
                                                  PaxTypeName = IMP.Attribute("PaxTypeName").Value,
                                                  TicketNo = IMP.Attribute("TicketNo").Value
                                              };

                                ArrayList TktNoArray = new ArrayList();

                                foreach (var TicketNo in PaxInfo)
                                {
                                    if (TicketNo.Title.ToString().Trim().ToUpper() != "INF")
                                    {
                                        TktNoArray.Add(TicketNo.FirstName.ToString().Trim().Replace(" ", "") + TicketNo.Title.ToString().Trim() + TicketNo.LastName.ToString().Trim() + "/" + "666-" + TicketNo.TicketNo.ToString().Trim());
                                    }
                                    else
                                    {
                                        TktNoArray.Add(TicketNo.FirstName.ToString().Trim().Replace(" ", "") + TicketNo.LastName.ToString().Trim() + "/" + "666-" + TicketNo.TicketNo.ToString().Trim());
                                    }

                                }
                                string AirlinePNR = VAPNR.Trim().ToUpper();
                                HSVCONNECT.Add("VAPNR", VAPNR.Trim().ToUpper());
                                HSVCONNECT.Add("AIRLINEPNR", AirlinePNR);
                                HSVCONNECT.Add("TKTNOARRAY", TktNoArray);
                                BALAV.InsertAVBookingLogs(ORDERID, VAPNR, HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, BookingREQXML, BookingEncyptedRES, BookingEncyptedRESXML, FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, "", PROVIDER);

                            }

                            catch (Exception ex)
                            {
                                BALAV.InsertAVBookingLogs(ORDERID, VAPNR, HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, BookingREQXML, BookingEncyptedRES, BookingEncyptedRESXML, FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, ex.ToString(), PROVIDER);
                            }

                        }
                        else
                        {
                            VAPNR = "LB" + Utility.GetRndm() + "-FQ";
                            HSVCONNECT.Add("VAPNR", VAPNR);
                            HSVCONNECT.Add("AIRLINEPNR", AirlinePNR);
                            HSVCONNECT.Add("TKTNOARRAY", TktNoArray);
                            BALAV.InsertAVBookingLogs(ORDERID, "", HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, BookingREQXML, BookingEncyptedRES, BookingEncyptedRESXML, FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, "", PROVIDER);
                        }

                    }
                    catch (Exception ex)
                    {
                        VAPNR = "LB" + Utility.GetRndm() + "-FQ";
                        HSVCONNECT.Add("VAPNR", VAPNR);
                        HSVCONNECT.Add("AIRLINEPNR", AirlinePNR);
                        HSVCONNECT.Add("TKTNOARRAY", TktNoArray);
                        BALAV.InsertAVBookingLogs(ORDERID, "", HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, BookingREQXML, BookingEncyptedRES, BookingEncyptedRESXML, FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, ex.ToString(), PROVIDER);
                    }
                }
                else
                {
                    //insert logs
                    VAPNR = "LB" + Utility.GetRndm() + "-FQ";
                    HSVCONNECT.Add("VAPNR", VAPNR);
                    HSVCONNECT.Add("AIRLINEPNR", AirlinePNR);
                    HSVCONNECT.Add("TKTNOARRAY", TktNoArray);
                    BALAV.InsertAVBookingLogs(ORDERID, "", HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, "", "", "", FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, "", PROVIDER);
                }
            }
            catch (Exception ex)
            {
                BALAV.InsertAVBookingLogs(ORDERID, "", HoldInventoryReqXML, HoldInventoryEncyptedRES, HoldInventoryEncyptedRESXML, BookingREQXML, BookingEncyptedRES, BookingEncyptedRESXML, FareRuleEncyptedRESXMLOW, FareRuleEncyptedRESXMLRT, ex.ToString(), PROVIDER);
            }

            return HSVCONNECT;
        }
        private string GetFareRuleRQ(string UserId, string Password, string OfficeId, string FareBasisCode)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<FareRulesRQ>");
            StringBuilder.Append("<Authenticate UserLogin='" + UserId + "' Password='" + Password + "' OfficeId='" + OfficeId + "' />");
            StringBuilder.Append("<FareBasisCode>" + FareBasisCode + "</FareBasisCode>");
            StringBuilder.Append("</FareRulesRQ>");
            return StringBuilder.ToString();
        }
        private string GetHoldInventoryRQ(string UserId, string Password, string OfficeId, string TripType, DataSet FltDsGAL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<HoldInventoryRQ>");
            StringBuilder.Append("<Authenticate UserLogin='" + UserId + "' Password='" + Password + "' OfficeId='" + OfficeId + "' />");
            string SeatCount = (Convert.ToInt32(FltDsGAL.Tables[0].Rows[0]["Adult"].ToString()) + Convert.ToInt32(FltDsGAL.Tables[0].Rows[0]["Child"].ToString())).ToString();
            for (int i = 0; i < FltDsGAL.Tables[0].Rows.Count; i++)
            {
                string FlightNo = "";
                FlightNo = FltDsGAL.Tables[0].Rows[i]["ValiDatingCarrier"].ToString().Trim() + FltDsGAL.Tables[0].Rows[i]["FlightIdentification"].ToString().Trim();
                StringBuilder.Append("<Hold OrderNo='" + FltDsGAL.Tables[0].Rows[i]["sno"].ToString().Trim() + "' FlightNo='" + FlightNo.Trim() + "' Origin='" + FltDsGAL.Tables[0].Rows[i]["DepartureLocation"].ToString().Trim() + "' Destination='" + FltDsGAL.Tables[0].Rows[i]["ArrivalLocation"].ToString().Trim() + "' FlightDate='" + FltDsGAL.Tables[0].Rows[i]["depdatelcc"].ToString().Trim() + "' RBD='" + FltDsGAL.Tables[0].Rows[i]["RBD"].ToString().Trim() + "' SeatCount='" + SeatCount + "' />");
            }
            StringBuilder.Append("</HoldInventoryRQ>");
            return StringBuilder.ToString();

        }
        private string GetCreateBookingRQ(string UserId, string Password, string OfficeId, string TripType, int Adult, int Child, int Infant, ArrayList HoldId, DataSet FltDsGAL, DataSet FltHdr, DataSet PaxDS)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<CreateBookingRQ>");
            StringBuilder.Append("<Authenticate UserLogin='" + UserId + "' Password='" + Password + "' OfficeId='" + OfficeId + "' />");
            StringBuilder.Append("<Nationality>IN</Nationality>");
            StringBuilder.Append("<PaxInfo>");
            for (int i = 0; i <= PaxDS.Tables[0].Rows.Count - 1; i++)
            {
                string PaxTitle = ""; string PaxTypeName = ""; string DOB = ""; string FFNumber = "";
                string PaxType = PaxDS.Tables[0].Rows[i]["PaxType"].ToString();
                if (!string.IsNullOrEmpty(PaxDS.Tables[0].Rows[i]["FFNumber"].ToString().Trim()))
                {
                    FFNumber = PaxDS.Tables[0].Rows[i]["FFNumber"].ToString().Trim();
                }
                if (PaxType.ToUpper() == "ADT")
                {
                    PaxTypeName = "Adult";
                    PaxTitle = PaxDS.Tables[0].Rows[i]["Title"].ToString();

                }
                if (PaxType.ToUpper() == "CHD")
                {
                    PaxTypeName = "Child";
                    PaxTitle = PaxDS.Tables[0].Rows[i]["Title"].ToString();
                    string[] DOBSTR = PaxDS.Tables[0].Rows[i]["DOB"].ToString().Trim().Split('/');
                    DOB = DOBSTR[2] + DOBSTR[1] + DOBSTR[0];
                }
                if (PaxType.ToUpper() == "INF")
                {
                    PaxTypeName = "Infant";
                    PaxTitle = "INF";
                    string[] DOBSTR = PaxDS.Tables[0].Rows[i]["DOB"].ToString().Trim().Split('/');
                    DOB = DOBSTR[2] + DOBSTR[1] + DOBSTR[0];
                }
                string FirstName = PaxDS.Tables[0].Rows[i]["FName"].ToString().Trim() + " " + PaxDS.Tables[0].Rows[i]["MName"].ToString().Trim();
                StringBuilder.Append("<Pax Title='" + PaxTitle + "' LastName='" + PaxDS.Tables[0].Rows[i]["LName"].ToString() + "' FirstName='" + FirstName + "' PaxTypeName='" + PaxTypeName + "' DateOfBirth='" + DOB + "' FFPNo='" + FFNumber + "' Identity_Type='' Identity_Detail='' />");
            }
            StringBuilder.Append("</PaxInfo>");
            StringBuilder.Append("<SegmentInfo>");
            string Seats = (Adult + Child).ToString();
            for (int i = 0; i < FltDsGAL.Tables[0].Rows.Count; i++)
            {
                if (FltDsGAL.Tables[0].Rows[i]["flight"].ToString().Trim() == "1")
                {
                    StringBuilder.Append("<Segment Seats='" + Seats + "' HoldID='" + HoldId[i].ToString() + "' SeatHoldID='' FlightConnectionGroup='0' ReturnFlightGroup='' />");
                }
                else
                {
                    StringBuilder.Append("<Segment Seats='" + Seats + "' HoldID='" + HoldId[i].ToString() + "' SeatHoldID='' FlightConnectionGroup='1' ReturnFlightGroup='' />");
                }
            }
            StringBuilder.Append("</SegmentInfo>");
            StringBuilder.Append("<PaymentInfo>");
            StringBuilder.Append("<Payment PaymentType='CreditLimit' PaymentInfo1='' PaymentInfo2='' PaymentInfo3='' PaymentInfo4='' PaymentCharges='' AmountPaid='' />");
            StringBuilder.Append("<Card CardType='' CardNo='' CVVNo='' ExpiryYear='' ExpiryMonth='' NameOnCard='' PayMode='' />");
            StringBuilder.Append("</PaymentInfo>");

            StringBuilder.Append("  <Transaction Fax='' Address='' City='' Country='IN' Pincode='' Mobile='" + FltHdr.Tables[0].Rows[0]["PgMobile"].ToString() + "' ContactNo='' Email='" + FltHdr.Tables[0].Rows[0]["PgEmail"].ToString() + "' CorporateCode='' />");
            StringBuilder.Append("<Discount DiscountCode='' />");
            StringBuilder.Append("<SeatInfo>");
            StringBuilder.Append("<Seat SeatNumber='' SeatHoldID='' Title='' LastName='' FirstName='' BoardingPointCode='' BoardingPointName='' />");
            StringBuilder.Append("</SeatInfo>");
            StringBuilder.Append("</CreateBookingRQ>");
            return StringBuilder.ToString();

        }
        //private string Encrypt(string XML)
        //{
        //    StringBuilder EncryptREQ = new StringBuilder();
        //    EncryptREQ.Append("<?xml version='1.0' encoding='utf-8'?>");
        //    EncryptREQ.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
        //    EncryptREQ.Append("<soap:Body>");
        //    EncryptREQ.Append("<Encrypt xmlns='http://tempuri.org/'>");
        //    EncryptREQ.Append("<EncryptionXML><![CDATA[" + XML + "]]></EncryptionXML>");
        //    EncryptREQ.Append("</Encrypt>");
        //    EncryptREQ.Append("</soap:Body>");
        //    EncryptREQ.Append("</soap:Envelope>");
        //    string EncryptedXML = PostXml("http://www.springtravelsap.com/AIRCOSTACRYPTOGRAPHY/AIRCOSTACRYPTOGRAPHY.asmx", EncryptREQ.ToString());
        //    return EncryptedXML;
        //}
        //private string Decrypt(string XML)
        //{
        //    StringBuilder DecryptREQ = new StringBuilder();
        //    DecryptREQ.Append("<?xml version='1.0' encoding='utf-8'?>");
        //    DecryptREQ.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
        //    DecryptREQ.Append("<soap:Body>");
        //    DecryptREQ.Append("<Decrypt xmlns='http://tempuri.org/'>");
        //    DecryptREQ.Append("<DecryptionXML>" + XML + "</DecryptionXML>");
        //    DecryptREQ.Append("</Decrypt>");
        //    DecryptREQ.Append("</soap:Body>");
        //    DecryptREQ.Append("</soap:Envelope>");
        //    string DecryptedXML = PostXml("http://www.springtravelsap.com/AIRCOSTACRYPTOGRAPHY/AIRCOSTACRYPTOGRAPHY.asmx", DecryptREQ.ToString());
        //    return DecryptedXML;
        //}
        private string Encrypt(string  XML)
        {
           // AIRS.Cryptography.scCryptography
            //Object objString = (Object)XML;
            AIRS.Cryptography.scCryptography scCrypt = new AIRS.Cryptography.scCryptography();
            //string EncryptREQ = scCrypt.Encrypt(XML);
            object EncryptREQ = scCrypt.Encrypt(XML);
            return EncryptREQ.ToString();
        }

        private string Decrypt(string XML)
        {
            AIRS.Cryptography.scCryptography scDecrypt = new AIRS.Cryptography.scCryptography();
            string DecryptREQ = scDecrypt.Decrypt(XML);
            return DecryptREQ;
        }
        private string PostXml(string url, string xml)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(xml);
            string strResult = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 3600 * 1000;
                request.ReadWriteTimeout = 3600 * 1000;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        StreamReader reader = null;
                        Stream responseStream = response.GetResponseStream();
                        reader = new StreamReader(responseStream);
                        strResult = reader.ReadToEnd();
                        response.Close();
                        responseStream.Close();
                        reader.Close();
                        xmldoc.LoadXml(strResult);
                        strResult = xmldoc.InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.ToString();
            }
            return strResult;
        }
        private string FareRulesAndRestrictions(string XML, string SoapURL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            StringBuilder.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
            StringBuilder.Append("<soap:Body>");
            StringBuilder.Append("<FareRulesAndRestrictions xmlns='http://localhost/AirsWebServicesTP4/wsReservation.asmx'>");
            StringBuilder.Append("<strInput>" + XML + "</strInput>");
            StringBuilder.Append("</FareRulesAndRestrictions>");
            StringBuilder.Append("</soap:Body>");
            StringBuilder.Append("</soap:Envelope>");
            string FareRuleXML = PostXml(SoapURL, StringBuilder.ToString());
            return FareRuleXML;

        }
        private string HoldInventory(string XML, string SoapURL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            StringBuilder.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
            StringBuilder.Append("<soap:Body>");
            StringBuilder.Append("<HoldInventory  xmlns='http://localhost/AirsWebServicesTP4/wsFlight'>");
            StringBuilder.Append("<strInput>" + XML + "</strInput>");
            StringBuilder.Append("</HoldInventory>");
            StringBuilder.Append("</soap:Body>");
            StringBuilder.Append("</soap:Envelope>");
            string FareRuleXML = PostXml(SoapURL, StringBuilder.ToString());
            return FareRuleXML;

        }
        private string CreateBooking(string XML, string SoapURL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            StringBuilder.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
            StringBuilder.Append("<soap:Body>");
            StringBuilder.Append("<CreateBooking   xmlns='http://localhost/AirsWebServicesTP4/wsReservation.asmx'>");
            StringBuilder.Append("<strInput>" + XML + "</strInput>");
            StringBuilder.Append("</CreateBooking >");
            StringBuilder.Append("</soap:Body>");
            StringBuilder.Append("</soap:Envelope>");
            string FareRuleXML = PostXml(SoapURL, StringBuilder.ToString());
            return FareRuleXML;

        }
    }
}
