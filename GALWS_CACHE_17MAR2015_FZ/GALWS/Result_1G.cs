﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using GALWS;
using System.Web;
using System.IO;

namespace STD.BAL
{
    public class Result_1G
    {
        string ConnStr = "";
        public Result_1G()
        { }
        public Result_1G(string ConnectionString)
        {
            ConnStr = ConnectionString;
        }
        /// <summary>
        /// Parse 1G FQSBB response
        /// 
        /// </summary>
        /// <returns>List of FltResultList</returns>
        ///         

        public List<FlightSearchResults> GetFltResult(List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, string strResponse, FlightSearch searchInputs, string TType, string FType, List<MISCCharges> MiscList, string CrdType, string SearchId, HttpContext contx)
        {          
            HttpContext.Current = contx;
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity, depCity = "";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListOutBound = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListInBound = new List<FlightSearchResults>();
            XmlDocument xd = new XmlDocument();
            try
            {
                if (!string.IsNullOrEmpty(strResponse))
                {
                    xd.LoadXml(strResponse);
                    XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                    XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
                    mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

                    //Split Air Part
                    XmlNodeList temp = xd.SelectNodes("//AirAvail", mgr);
                    //FarePart


                    #region Varible decleration
                    int i = 1;
                    int j = 0;
                    int l = 0;
                    int lineNumber = 1;
                    string strFlightNo = "";
                    string strValidatingCarrier = "";
                    string FlttmDur = "";//added by abhilash
                    #endregion
                    //OutBoundFlight with index           
                    #region OutBoundFlight

                    XmlNodeList xnOutboundFlights = temp[0].SelectNodes("AvailFlt");
                    XmlNodeList xnOutboundFlightsBIC = temp[0].SelectNodes("BICAvail");
                    string strOutboundFlight = "";
                    for (int q = 0; q < xnOutboundFlights.Count; q++)
                    {
                        XmlNode xn = xd.CreateNode(XmlNodeType.Element, "Index", "");
                        xn.InnerText = (q + 1).ToString();
                        xnOutboundFlights[q].AppendChild(xn);
                        strOutboundFlight = strOutboundFlight + "<AvailFlt>" + xnOutboundFlights[q].InnerXml + xnOutboundFlightsBIC[q].InnerXml + "</AvailFlt>";
                    }

                    strOutboundFlight = "<SubmitXmlResult><AirAvail>" + strOutboundFlight + "</AirAvail></SubmitXmlResult>";
                    XmlDocument xdOutboundFlight = new XmlDocument();
                    xdOutboundFlight.LoadXml(strOutboundFlight);

                    XDocument xdoc = XDocument.Load(new XmlNodeReader(xdOutboundFlight));

                    #endregion

                    //InBoundFlight with index     
                    #region InBoundFlight

                    //XmlNodeList xnInboundFlights = temp[1].SelectNodes("AvailFlt");
                    //string strInboundFlight = "";
                    //for (int q = 0; q < xnInboundFlights.Count; q++)
                    //{
                    //    XmlNode xn = xd.CreateNode(XmlNodeType.Element, "Index", "");
                    //    xn.InnerText = (q + 1).ToString();
                    //    xnInboundFlights[q].AppendChild(xn);
                    //    strInboundFlight = strInboundFlight + "<AvailFlt>" + xnInboundFlights[q].InnerXml + "</AvailFlt>";
                    //}

                    //strInboundFlight = "<SubmitXmlResult><AirAvail>" + strInboundFlight + "</AirAvail></SubmitXmlResult>";
                    //XmlDocument xdInboundFlight = new XmlDocument();
                    //xdInboundFlight.LoadXml(strInboundFlight);

                    //XDocument xdocIn = XDocument.Load(new XmlNodeReader(xdInboundFlight));

                    #endregion

                    #region Find Index from Fareinfo

                    XmlNodeList xnfareNodes = xd.SelectNodes("//FareInfo");

                    #endregion

                    int gtIndex = 0;
                    try
                    {
                        for (int ff = 0; ff < xnfareNodes.Count; ff++)
                        {
                            l = 0;
                            #region select flight cross reference

                            XmlNodeList xnFlightNode = xnfareNodes[ff].SelectNodes("FlightItemRef");
                            XmlDocument xdFareDoc = new XmlDocument();
                            xdFareDoc.LoadXml("<FareInfo>" + xnfareNodes[ff].InnerXml + "</FareInfo>");
                            XDocument xdFare = XDocument.Load(new XmlNodeReader(xdFareDoc));


                            #endregion

                            // lineNumber = lineNumber + 1;
                            string strConectingFlight = xnFlightNode[0].SelectSingleNode("ODNumLegs").InnerText;

                            #region select flight cross reference

                            XmlNodeList xnFlightCrossRef = xnFlightNode[0].ChildNodes[3].ChildNodes;

                            XDocument xDocU = new XDocument();

                            #endregion

                            for (int se = 0; se < xnFlightCrossRef.Count; se++)
                            {
                                l = l + 1;
                                try
                                {
                                    string flightIndex = xnFlightCrossRef[se].FirstChild.InnerText;
                                    FlightList.Add(parseFlight(xdoc, flightIndex, l, searchInputs, xdFare, lineNumber, TType, FType, SrvchargeList, CityList, AirList, MrkupDS, "1", "O", depCity, arrCity, MiscList, CrdType, SearchId));
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                if (int.Parse(strConectingFlight) == l)
                                {
                                    l = 0;
                                    lineNumber = lineNumber + 1;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResult-parseFlight)", "Error_008", ex, "GetFltResult");
                        throw ex;
                    }

                    if (searchInputs.Trip.ToString() == "D")
                    {
                        try
                        {
                            DataTable dt = new DataTable();
                            dt = objFltComm.GetFQCSDetails();
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    FlightList = FQCSTransaction(FlightList, searchInputs, ConnStr, MrkupDS, SrvchargeList, dr["RBD"].ToString(), dr["Airline"].ToString(), MiscList, CrdType);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResult-FQCSTransaction)", "Error_008", ex, "GetFltResult");
                        }
                    }
                    //XmlAttributeOverrides overrides = new XmlAttributeOverrides();
                    //XmlAttributes attr = new XmlAttributes();
                    //attr.XmlRoot = new XmlRootAttribute("XML");
                    //overrides.Add(typeof(List<FlightSearchResults>), attr);

                    //XmlDocument xmlDoc = new XmlDocument();
                    //XPathNavigator nav = xmlDoc.CreateNavigator();
                    //using (XmlWriter writer = nav.AppendChild())
                    //{
                    //    XmlSerializer ser = new XmlSerializer(typeof(List<FlightSearchResults>), overrides);
                    //    List<FlightSearchResults> parameters = (List<FlightSearchResults>)(((FlightList)));
                    //    ser.Serialize(writer, parameters);
                    //}
                    //string bbbbbb = xmlDoc.OuterXml.ToString();
                }
                else
                {
                    //File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_BLANK_RES_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "CRDTYPE: " + CrdType + Environment.NewLine + "SEARCH_ID: " + SearchId + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_BLANK_RES_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                        "CRDTYPE: " + CrdType + Environment.NewLine +
                        "SEARCH_ID: " + SearchId + Environment.NewLine +
                        "AgentId: " + searchInputs.UID + Environment.NewLine +
                        "AgentType: " + searchInputs.AgentType + Environment.NewLine +
                        "Adult: " + searchInputs.Adult + Environment.NewLine +
                        "Child: " + searchInputs.Child + Environment.NewLine +
                        "Infant: " + searchInputs.Infant + Environment.NewLine +
                        "TripType: " + searchInputs.TripType + Environment.NewLine +
                        "Trip: " + searchInputs.Trip + Environment.NewLine +
                        "AirLine: " + searchInputs.AirLine + Environment.NewLine +
                        "ArrivalCity: " + searchInputs.ArrivalCity + Environment.NewLine +
                        "DepartureCity: " + searchInputs.DepartureCity + Environment.NewLine +
                        "Provider: " + searchInputs.Provider + Environment.NewLine +
                        "DepDate: " + searchInputs.DepDate + Environment.NewLine +
                        "RetDate: " + searchInputs.RetDate + Environment.NewLine + Environment.NewLine +
                        "1G_XML_RESPONSE:" + Environment.NewLine + Environment.NewLine + strResponse +
                        Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResult)", "Error_001", ex, "GetFltResult");
                    File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_RESPONSE_Exception_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                        "CRDTYPE: " + CrdType + Environment.NewLine +
                        "SEARCH_ID: " + SearchId + Environment.NewLine +
                        "AgentId: " + searchInputs.UID + Environment.NewLine +
                        "AgentType: " + searchInputs.AgentType + Environment.NewLine +
                        "Adult: " + searchInputs.Adult + Environment.NewLine +
                        "Child: " + searchInputs.Child + Environment.NewLine +
                        "Infant: " + searchInputs.Infant + Environment.NewLine +
                        "TripType: " + searchInputs.TripType + Environment.NewLine +
                        "Trip: " + searchInputs.Trip + Environment.NewLine +
                        "AirLine: " + searchInputs.AirLine + Environment.NewLine +
                        "ArrivalCity: " + searchInputs.ArrivalCity + Environment.NewLine +
                        "DepartureCity: " + searchInputs.DepartureCity + Environment.NewLine +
                        "Provider: " + searchInputs.Provider + Environment.NewLine +
                        "DepDate: " + searchInputs.DepDate + Environment.NewLine +
                        "RetDate: " + searchInputs.RetDate + Environment.NewLine + Environment.NewLine +
                        "Exception_Message: " + ex.Message + Environment.NewLine +
                        "Exception_StackTrace: " + ex.StackTrace.ToString() + Environment.NewLine + Environment.NewLine +
                        "1G_XML_RESPONSE:" + Environment.NewLine + Environment.NewLine + strResponse + Environment.NewLine +
                        Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    //File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_RESPONSE__" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "CRDTYPE: " + CrdType + Environment.NewLine + "SEARCH_ID: " + SearchId + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    //File.AppendAllText("C:\\\\CPN_SP\\\\1G_Exception_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "Error:" + ex.StackTrace.ToString() + ex.Message + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exx)
                {

                }
            }
            // return FlightList;
            return objFltComm.AddFlightKey(FlightList, searchInputs.GDSRTF);
        }
        public List<FlightSearchResults> GetFltResultRoundTrip(List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, string strResponse, FlightSearch searchInputs, string TType, string FType, List<MISCCharges> MiscList, string CrdType, string SearchId, HttpContext contx)
        {
            HttpContext.Current = contx;
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity, depCity = "";
            //if (FType == "InBound")
            //{
            //    arrCity = searchInputs.HidTxtDepCity;
            //    depCity = searchInputs.HidTxtArrCity;
            //}
            //else
            //{
            arrCity = searchInputs.HidTxtArrCity;
            depCity = searchInputs.HidTxtDepCity;
            //}
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListOutBound = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListInBound = new List<FlightSearchResults>();
            XmlDocument xd = new XmlDocument();
            try
            {
                if (!string.IsNullOrEmpty(strResponse))
                {

                    xd.LoadXml(strResponse);
                    XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                    XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
                    mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

                    //Split Air Part
                    XmlNodeList temp = xd.SelectNodes("//AirAvail", mgr);
                    //FarePart

                    #region Varible decleration
                    int i = 1;
                    int j = 0;
                    int l = 0;
                    int lineNumber = 0;
                    string strFlightNo = "";
                    string strValidatingCarrier = "";
                    string FlttmDur = "";//added by abhilash
                    #endregion
                    //OutBoundFlight with index           
                    #region OutBoundFlight

                    XmlNodeList xnOutboundFlights = temp[0].SelectNodes("AvailFlt");
                    XmlNodeList xnOutboundFlightsBIC = temp[0].SelectNodes("BICAvail");
                    string strOutboundFlight = "";
                    for (int q = 0; q < xnOutboundFlights.Count; q++)
                    {
                        XmlNode xn = xd.CreateNode(XmlNodeType.Element, "Index", "");
                        xn.InnerText = (q + 1).ToString();
                        xnOutboundFlights[q].AppendChild(xn);
                        strOutboundFlight = strOutboundFlight + "<AvailFlt>" + xnOutboundFlights[q].InnerXml + xnOutboundFlightsBIC[q].InnerXml + "</AvailFlt>";
                    }

                    strOutboundFlight = "<SubmitXmlResult><AirAvail>" + strOutboundFlight + "</AirAvail></SubmitXmlResult>";
                    XmlDocument xdOutboundFlight = new XmlDocument();
                    xdOutboundFlight.LoadXml(strOutboundFlight);

                    XDocument xdoc = XDocument.Load(new XmlNodeReader(xdOutboundFlight));

                    #endregion

                    //InBoundFlight with index     
                    #region InBoundFlight

                    XmlNodeList xnInboundFlights = temp[1].SelectNodes("AvailFlt");
                    XmlNodeList xnInboundFlightsBIC = temp[1].SelectNodes("BICAvail");
                    string strInboundFlight = "";
                    for (int q = 0; q < xnInboundFlights.Count; q++)
                    {
                        XmlNode xn = xd.CreateNode(XmlNodeType.Element, "Index", "");
                        xn.InnerText = (q + 1).ToString();
                        xnInboundFlights[q].AppendChild(xn);
                        strInboundFlight = strInboundFlight + "<AvailFlt>" + xnInboundFlights[q].InnerXml + xnInboundFlightsBIC[q].InnerXml + "</AvailFlt>";
                    }

                    strInboundFlight = "<SubmitXmlResult><AirAvail>" + strInboundFlight + "</AirAvail></SubmitXmlResult>";
                    XmlDocument xdInboundFlight = new XmlDocument();
                    xdInboundFlight.LoadXml(strInboundFlight);

                    XDocument xdocIn = XDocument.Load(new XmlNodeReader(xdInboundFlight));

                    #endregion

                    #region Find Index from Fareinfo

                    XmlNodeList xnfareNodes = xd.SelectNodes("//FareInfo");

                    #endregion

                    int gtIndex = 0;
                    try
                    {
                        for (int ff = 0; ff < xnfareNodes.Count; ff++)
                        {
                            gtIndex = ff;
                            #region select flight cross reference
                            XmlNodeList xnFlightNode = xnfareNodes[ff].SelectNodes("FlightItemRef");
                            XmlDocument xdFareDoc = new XmlDocument();
                            xdFareDoc.LoadXml("<FareInfo>" + xnfareNodes[ff].InnerXml + "</FareInfo>");
                            XDocument xdFare = XDocument.Load(new XmlNodeReader(xdFareDoc));
                            #endregion

                            string strConectingFlight = xnFlightNode[0].SelectSingleNode("ODNumLegs").InnerText;

                            #region select flight cross reference
                            XmlNodeList xnFlightCrossRef = xnFlightNode[0].ChildNodes[3].ChildNodes;
                            XDocument xDocU = new XDocument();
                            #endregion
                            int mm = 0;
                            for (int se = 0; se < xnFlightCrossRef.Count; se++)
                            {
                                l = 0;

                                while (l < int.Parse(strConectingFlight))
                                {
                                    try
                                    {


                                        // 
                                        if (mm == xnFlightCrossRef.Count)
                                            break;
                                        string flightIndex = xnFlightCrossRef[mm].FirstChild.InnerText;
                                        //int odNumleg2 = int.Parse(xnFlightNode[1].SelectSingleNode("ODNumLegs").InnerText);
                                        //gtIndex = xnFlightCrossRef[se].ChildNodes[3].ChildNodes.Count / odNumleg2;
                                        FlightListOutBound.Add(parseFlight(xdoc, flightIndex, l + 1, searchInputs, xdFare, lineNumber, TType, FType, SrvchargeList, CityList, AirList, MrkupDS, "1", "O", depCity, arrCity, MiscList, CrdType, SearchId));
                                        l = l + 1;
                                        mm = mm + 1;

                                    }
                                    catch (Exception ex)
                                    {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResultRoundTrip)", "Error_001", ex, "GetFltResultRoundTrip-parseFlight");
                                        throw ex;
                                    }

                                }



                                if (l == int.Parse(strConectingFlight))
                                {

                                    l = 0;
                                }
                                if (FlightListInBound.Count == 0 && gtIndex == ff && l == 0)
                                {
                                    l = 0;
                                    int tt = 0;
                                    for (int ser = 0; ser < xnFlightNode[1].ChildNodes[3].ChildNodes.Count; ser++)
                                    {
                                        int odNumleg2R = int.Parse(xnFlightNode[1].SelectSingleNode("ODNumLegs").InnerText);

                                        while (l < odNumleg2R)
                                        {

                                            if (tt == xnFlightNode[1].ChildNodes[3].ChildNodes.Count)
                                                break;
                                            string flightIndexR = xnFlightNode[1].ChildNodes[3].ChildNodes[tt].FirstChild.InnerText;
                                            FlightListInBound.Add(parseFlight(xdocIn, flightIndexR, l + 1, searchInputs, xdFare, lineNumber, TType, FType, SrvchargeList, CityList, AirList, MrkupDS, "2", "R", depCity, arrCity, MiscList, CrdType, SearchId));
                                            l = l + 1;
                                            tt = tt + 1;

                                        }

                                        if (l == odNumleg2R)
                                            l = 0;
                                    }
                                }

                                if (FlightListInBound.Count > 0 && FlightListOutBound.Count > 0)
                                {


                                    for (int h = 0; h < FlightListInBound.Count; h++)
                                    {

                                        var objFSA = (FlightSearchResults)FlightListInBound[h].Clone();
                                        if (objFSA.Leg == 1)
                                            lineNumber = lineNumber + 1;


                                        if (objFSA.Leg == 1)
                                        {

                                            int q = 0;


                                            for (int hh = 0; hh < FlightListOutBound.Count; hh++)
                                            {
                                                var objFSAI = (FlightSearchResults)FlightListOutBound[hh].Clone();
                                                objFSAI.LineNumber = lineNumber;
                                                objFSAI.Leg = q + 1;
                                                objFSAI.Flight = "1";
                                                FlightList.Add(objFSAI);
                                            }

                                        }

                                        objFSA.LineNumber = lineNumber;
                                        //objFSA.Leg = h + 1;
                                        objFSA.Flight = "2";
                                        FlightList.Add(objFSA);
                                        // }

                                    }
                                    FlightListOutBound.Clear();

                                }

                            }

                            FlightListInBound.Clear();
                            l = 0;
                        }
                    }
                    //}


                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResultRoundTrip)", "Error_001", ex, "GetFltResultRoundTrip-parseFlight");
                        throw ex;
                    }


                    //XmlAttributeOverrides overrides = new XmlAttributeOverrides();
                    //XmlAttributes attr = new XmlAttributes();
                    //attr.XmlRoot = new XmlRootAttribute("XML");
                    //overrides.Add(typeof(List<FlightSearchResults>), attr);

                    //XmlDocument xmlDoc = new XmlDocument();
                    //XPathNavigator nav = xmlDoc.CreateNavigator();
                    //using (XmlWriter writer = nav.AppendChild())
                    //{
                    //    XmlSerializer ser = new XmlSerializer(typeof(List<FlightSearchResults>), overrides);
                    //    List<FlightSearchResults> parameters = (List<FlightSearchResults>)(((FlightList)));
                    //    ser.Serialize(writer, parameters);
                    //}
                    //string bbbbbb = xmlDoc.OuterXml.ToString();
                    //return FlightList;
                }
                else
                {
                    File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_NO_RES_RoundTrip_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                        "CRDTYPE: " + CrdType + Environment.NewLine +
                        "SEARCH_ID: " + SearchId + Environment.NewLine +
                        "AgentId: " + searchInputs.UID + Environment.NewLine +
                        "AgentType: " + searchInputs.AgentType + Environment.NewLine +
                        "Adult: " + searchInputs.Adult + Environment.NewLine +
                        "Child: " + searchInputs.Child + Environment.NewLine +
                        "Infant: " + searchInputs.Infant + Environment.NewLine +
                        "TripType: " + searchInputs.TripType + Environment.NewLine +
                        "Trip: " + searchInputs.Trip + Environment.NewLine +
                        "AirLine: " + searchInputs.AirLine + Environment.NewLine +
                        "ArrivalCity: " + searchInputs.ArrivalCity + Environment.NewLine +
                        "DepartureCity: " + searchInputs.DepartureCity + Environment.NewLine +
                        "Provider: " + searchInputs.Provider + Environment.NewLine +
                        "DepDate: " + searchInputs.DepDate + Environment.NewLine +
                        "RetDate: " + searchInputs.RetDate + Environment.NewLine + Environment.NewLine +
                        "1G_XML_RESPONSE:" + Environment.NewLine + strResponse + Environment.NewLine +
                        Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }

            }
            catch (Exception ex)
            {
                try
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("Result_1G(GetFltResultRoundTrip)", "Error_001", ex, "GetFltResultRoundTrip");
                    File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_RESPONSE_RoundTrip_Exception_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                        "CRDTYPE: " + CrdType + Environment.NewLine +
                        "SEARCH_ID: " + SearchId + Environment.NewLine +
                        "AgentId: " + searchInputs.UID + Environment.NewLine +
                        "AgentType: " + searchInputs.AgentType + Environment.NewLine +
                        "Adult: " + searchInputs.Adult + Environment.NewLine +
                        "Child: " + searchInputs.Child + Environment.NewLine +
                        "Infant: " + searchInputs.Infant + Environment.NewLine +
                        "TripType: " + searchInputs.TripType + Environment.NewLine +
                        "Trip: " + searchInputs.Trip + Environment.NewLine +
                        "AirLine: " + searchInputs.AirLine + Environment.NewLine +
                        "ArrivalCity: " + searchInputs.ArrivalCity + Environment.NewLine +
                        "DepartureCity: " + searchInputs.DepartureCity + Environment.NewLine +
                        "Provider: " + searchInputs.Provider + Environment.NewLine +
                        "DepDate: " + searchInputs.DepDate + Environment.NewLine +
                        "RetDate: " + searchInputs.RetDate + Environment.NewLine +
                        "Exception_Message: " + ex.Message + Environment.NewLine +
                        "Exception_StackTrace: " + ex.StackTrace.ToString() + Environment.NewLine + Environment.NewLine +
                        "1G_XML_RESPONSE:" + Environment.NewLine + strResponse + Environment.NewLine +
                        Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    //File.AppendAllText("C:\\\\CPN_SP\\\\1G_SEARCH_RESPONSE_RoundTrip_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "CRDTYPE: " + CrdType + Environment.NewLine + "SEARCH_ID: " + SearchId + Environment.NewLine + "1G_XML_RESPONSE_RoundTrip:" + Environment.NewLine + strResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    //File.AppendAllText("C:\\\\CPN_SP\\\\1G_Exception_RoundTrip_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "Error:" + ex.StackTrace.ToString() + ex.Message + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exx)
                {

                }
            }
            return objFltComm.AddFlightKey(FlightList, searchInputs.GDSRTF);
        }

        public List<FlightSearchResults> GetFltResultMultiCity(List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, string strResponse, FlightSearch searchInputs, string TType, string FType, List<MISCCharges> MiscList, string resultperFare, string CrdType, string SearchId, HttpContext contx)
        {
            HttpContext.Current = contx;
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity, depCity = "";
            int ResultperFareM = 4;

            try
            {

                ResultperFareM = Convert.ToInt32(resultperFare);
            }
            catch { ResultperFareM = 4; }

            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListOutBound = new List<FlightSearchResults>();
            List<FlightSearchResults> FlightListInBound = new List<FlightSearchResults>();
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strResponse);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
            mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

            //Split Air Part
            XmlNodeList temp = xd.SelectNodes("//AirAvail", mgr);
            //FarePart

            #region Varible decleration
            int i = 1;
            int j = 0;
            int l = 0;
            int lineNum = 1;
            List<XDocument> AirAvlList = new List<XDocument>();
            #endregion
            //OutBoundFlight with index           
            #region OutBoundFlight

            for (int avl = 0; avl < temp.Count; avl++)
            {
                XmlNodeList xnOutboundFlights = temp[avl].SelectNodes("AvailFlt");
                XmlNodeList xnOutboundFlightsBIC = temp[avl].SelectNodes("BICAvail");
                string strOutboundFlight = "";
                for (int q = 0; q < xnOutboundFlights.Count; q++)
                {
                    XmlNode xn = xd.CreateNode(XmlNodeType.Element, "Index", "");
                    xn.InnerText = (q + 1).ToString();
                    xnOutboundFlights[q].AppendChild(xn);
                    strOutboundFlight = strOutboundFlight + "<AvailFlt>" + xnOutboundFlights[q].InnerXml + xnOutboundFlightsBIC[q].InnerXml + "</AvailFlt>";
                }

                strOutboundFlight = "<SubmitXmlResult><AirAvail>" + strOutboundFlight + "</AirAvail></SubmitXmlResult>";
                XmlDocument xdOutboundFlight = new XmlDocument();
                xdOutboundFlight.LoadXml(strOutboundFlight);
                XDocument xdoc = XDocument.Load(new XmlNodeReader(xdOutboundFlight));
                AirAvlList.Add(xdoc);
            }




            #endregion


            #region Find Index from Fareinfo

            XmlNodeList xnfareNodes = xd.SelectNodes("//FareInfo");

            #endregion

            int gtIndex = 0;
            try
            {

                for (int ff = 0; ff < xnfareNodes.Count; ff++)
                {
                    gtIndex = ff;
                    #region select flight cross reference
                    XmlNodeList xnFlightNode = xnfareNodes[ff].SelectNodes("FlightItemRef");
                    XmlDocument xdFareDoc = new XmlDocument();
                    xdFareDoc.LoadXml("<FareInfo>" + xnfareNodes[ff].InnerXml + "</FareInfo>");
                    XDocument xdFare = XDocument.Load(new XmlNodeReader(xdFareDoc));
                    #endregion




                    #region FlightItemArray Loop

                    List<List<string>> RsltList = new List<List<string>>();


                    for (int fia = 0; fia < xnFlightNode.Count; fia++)
                    {

                        List<string> sublist = new List<string>();

                        int AirAvlIndex = Convert.ToInt32(xnFlightNode[fia].SelectSingleNode("ODNum").InnerText);
                        int flightseq = fia + 1;
                        int leg = 1;
                        int strConectingFlight = Convert.ToInt32(xnFlightNode[fia].SelectSingleNode("ODNumLegs").InnerText);

                        XmlNodeList xnFlightCrossRef = xnFlightNode[fia].ChildNodes[3].ChildNodes;

                        string rs = "";
                        int legCount = 1;
                        for (int fa = 0; fa < xnFlightCrossRef.Count; fa++)
                        {
                            string flightIndex = xnFlightCrossRef[fa].FirstChild.InnerText;
                            rs = rs + AirAvlIndex.ToString() + "_" + legCount + "_" + flightIndex + ",";

                            if (legCount == strConectingFlight)
                            {
                                sublist.Add(rs);
                                rs = "";
                                legCount = 1;
                            }
                            else
                            {
                                legCount++;

                            }
                        }


                        RsltList.Add(sublist);


                    }

                    List<string> rsltMain = new List<string>();

                    #region Nested Loop
                    // level 1


                    if (RsltList.Count > 0)
                    {
                        string elm = "";
                        for (int l1 = 0; l1 < RsltList[0].Count; l1++)
                        {
                            // level 2
                            string elm2 = "";
                            if (RsltList.Count > 1)
                            {
                                elm = RsltList[0][l1];
                                for (int l2 = 0; l2 < RsltList[1].Count; l2++)
                                {

                                    elm2 = elm + "," + RsltList[1][l2];

                                    // level 3
                                    string elm3 = "";
                                    if (RsltList.Count > 2)
                                    {

                                        for (int l3 = 0; l3 < RsltList[2].Count; l3++)
                                        {
                                            elm3 = elm2 + "," + RsltList[2][l3];
                                            // level 4
                                            string elm4 = "";
                                            if (RsltList.Count > 3)
                                            {

                                                for (int l4 = 0; l4 < RsltList[3].Count; l4++)
                                                {

                                                    elm4 = elm3 + "," + RsltList[3][l4];
                                                    // level 5
                                                    string elm5 = "";
                                                    if (RsltList.Count > 4)
                                                    {

                                                        for (int l5 = 0; l5 < RsltList[4].Count; l5++)
                                                        {

                                                            elm5 = elm4 + "," + RsltList[4][l5];

                                                            // level 6
                                                            string elm6 = "";
                                                            if (RsltList.Count > 5)
                                                            {

                                                                for (int l6 = 0; l6 < RsltList[5].Count; l6++)
                                                                {

                                                                    elm6 = elm5 + "," + RsltList[5][l6];

                                                                    rsltMain.Add(elm6);
                                                                    if (rsltMain.Count == ResultperFareM)
                                                                        goto ResultContinue;

                                                                }

                                                            }
                                                            else
                                                            {

                                                                rsltMain.Add(elm5);
                                                                if (rsltMain.Count == ResultperFareM)
                                                                    goto ResultContinue;

                                                            }


                                                        }
                                                    }

                                                    else
                                                    {

                                                        rsltMain.Add(elm4);
                                                        if (rsltMain.Count == ResultperFareM)
                                                            goto ResultContinue;

                                                    }



                                                }
                                            }
                                            else
                                            {

                                                rsltMain.Add(elm3);
                                                if (rsltMain.Count == ResultperFareM)
                                                    goto ResultContinue;

                                            }


                                        }
                                    }
                                    else
                                    {

                                        rsltMain.Add(elm2);
                                        if (rsltMain.Count == ResultperFareM)
                                            goto ResultContinue;

                                    }




                                }
                            }
                            else
                            {

                                rsltMain.Add(elm);

                                if (rsltMain.Count == ResultperFareM)
                                    goto ResultContinue;


                            }



                        }
                    }


                    #endregion



                ResultContinue:

                    for (int ln = 0; ln < rsltMain.Count; ln++)
                    {
                        string rssl = rsltMain[ln];
                        char[] splstr = { ',' };

                        string[] rsslAr = rssl.Split(splstr, StringSplitOptions.RemoveEmptyEntries);

                        for (int lg = 0; lg < rsslAr.Count(); lg++)
                        {
                            char[] splstr1 = { '_' };
                            string[] rsslAr1 = rsslAr[lg].Split(splstr1, StringSplitOptions.RemoveEmptyEntries);

                            if (rsslAr1.Count() >= 3)
                            {

                                int avlItem = Convert.ToInt32(rsslAr1[0]);
                                depCity = searchInputs.HidTxtDepCity;
                                arrCity = searchInputs.HidTxtArrCity;
                                if (avlItem == 2)
                                {
                                    depCity = searchInputs.HidTxtDepCity2;
                                    arrCity = searchInputs.HidTxtArrCity2;
                                }
                                else if (avlItem == 3)
                                {
                                    depCity = searchInputs.HidTxtDepCity3;
                                    arrCity = searchInputs.HidTxtArrCity3;
                                }
                                else if (avlItem == 4)
                                {
                                    depCity = searchInputs.HidTxtDepCity4;
                                    arrCity = searchInputs.HidTxtArrCity4;
                                }
                                else if (avlItem == 5)
                                {
                                    depCity = searchInputs.HidTxtDepCity5;
                                    arrCity = searchInputs.HidTxtArrCity5;
                                }
                                else if (avlItem == 6)
                                {
                                    depCity = searchInputs.HidTxtDepCity6;
                                    arrCity = searchInputs.HidTxtArrCity6;
                                }


                                FlightListOutBound.Add(parseFlightMultiCity(AirAvlList[Convert.ToInt32(rsslAr1[0]) - 1], rsslAr1[2], Convert.ToInt32(rsslAr1[1]), searchInputs, xdFare, lineNum, TType, FType, SrvchargeList, CityList, AirList, MrkupDS, avlItem.ToString(), "O", depCity, arrCity, MiscList, rsslAr1[0], CrdType, SearchId));
                            }
                        }

                        lineNum++;

                    }





                    #endregion



                }
            }
            //}


            catch (Exception ex)
            {
                throw ex;
            }


            //System.Xml.Serialization.XmlAttributeOverrides overrides = new System.Xml.Serialization.XmlAttributeOverrides();
            //System.Xml.Serialization.XmlAttributes attr = new System.Xml.Serialization.XmlAttributes();
            //attr.XmlRoot = new System.Xml.Serialization.XmlRootAttribute("XML");
            //overrides.Add(typeof(List<FlightSearchResults>), attr);

            //XmlDocument xmlDoc = new XmlDocument();
            //System.Xml.XPath.XPathNavigator nav = xmlDoc.CreateNavigator();
            //using (XmlWriter writer = nav.AppendChild())
            //{
            //    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(List<FlightSearchResults>), overrides);
            //    List<FlightSearchResults> parameters = (List<FlightSearchResults>)(((FlightListOutBound)));
            //    ser.Serialize(writer, parameters);
            //}
            //string bbbbbb = xmlDoc.OuterXml.ToString();
            //return FlightList;
            return objFltComm.AddFlightKey(FlightListOutBound, searchInputs.GDSRTF);
        }


        private FlightSearchResults parseFlight(XDocument xDocU, string flightIndex, int l, FlightSearch searchInputs, XDocument xdFare, int lineNumber, string TType, string FType, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, string flightTip, string FlightWay, string depCity, string arrCity, List<MISCCharges> MiscList, string CrdType, string SearchId)
        {
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            //  string arrCity, depCity = "";
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            HttpContext contx = HttpContext.Current;
            //arrCity = searchInputs.HidTxtDepCity;
            //depCity = searchInputs.HidTxtArrCity;
            #region Select Flihts


            var dump1 = from item in xDocU.Descendants("AirAvail").Descendants("AvailFlt")
                        where item.Element("Index").Value == flightIndex

                        select new
                        {
                            index = "",
                            #region flt list per item
                            fltNeeded = "",// item.Element("OpAirV").Name + ":" + item.Element("OpAirV").Value + "," + item.Element("LinkSellAgrmnt").Name + ":" + item.Element("LinkSellAgrmnt").Value + "," + item.Element("DaysOperates").Name + ":" + item.Element("DaysOperates").Value + "," + item.Element("DaysOperates").Name + ":" + item.Element("DaysOperates").Value + "," + item.Element("ETktEligibility").Name + ":" + item.Element("ETktEligibility").Value + "," + item.Element("AvailSource").Name + ":" + item.Element("AvailSource").Value + "," + item.Element("SegRelatedInfo").Element("BagInfo").Name + ":" + item1.Element("SegRelatedInfo").Element("BagInfo").Value,
                            opreatingcarrier = item.Element("OpAirV").Value,
                            jrnyTime = item.Element("JrnyTm").Value,


                            via1 = int.Parse(item.Element("NumStops").Value) > 0 ? item.Element("FrstDwnlnStp").Value : " ",
                            via2 = int.Parse(item.Element("NumStops").Value) > 1 ? item.Element("LastDwnlnStp").Value : " ",
                            dest = searchInputs.DepartureCity,
                            lineNumber = item.Element("NumStops").Value,
                            src = searchInputs.ArrivalCity,
                            deptime = item.Element("StartTm").Value,
                            arrtime = item.Element("EndTm").Value,
                            conx = item.Element("Conx").Value,
                            AirCode = item.Element("AirV").Value,
                            FltNo = item.Element("FltNum").Value,
                            StartDate = item.Element("StartDt").Value,
                            leg = item.Element("NumStops").Value,
                            //**************
                            OrgDestFrom = searchInputs.DepartureCity,
                            OrgDestTo = searchInputs.ArrivalCity,
                            Adult = searchInputs.Adult,
                            Child = searchInputs.Child,
                            Infant = searchInputs.Infant,
                            TotPax = searchInputs.Adult + searchInputs.Child,
                            DepartureLocation = item.Element("StartAirp").Value,
                            DepartureCityName = item.Element("StartAirp").Value,
                            DepartureAirportName = item.Element("StartAirp").Value,
                            DepartureTerminal = item.Element("StartTerminal").Value,
                            ArrivalLocation = item.Element("EndAirp").Value,
                            ArrivalCityName = item.Element("EndAirp").Value,
                            ArrivalAirportName = item.Element("EndAirp").Value,
                            ArrivalTerminal = item.Element("EndTerminal").Value,
                            DepartureDate = item.Element("StartDt").Value,
                            Departure_Date = item.Element("StartDt").Value,
                            DepartureTime = item.Element("StartTm").Value,
                            ArrivalDate = item.Element("EndDt").Value,
                            Arrival_Date = item.Element("EndDt").Value,
                            MarketingCarrier = item.Element("AirV").Value,
                            eq = item.Element("Equip").Value,
                            FlightIdentification = item.Element("FltNum").Value,
                            // RBD = item.Element("EndDt").Value,
                            AvailableSeats = item.Elements("BICStatusAry"),
                            ValiDatingCarrier = item.Element("AirV").Value,
                            adtRsvnRules = "",//item1.Elements("RsvnRules").Where(x => x.Element("UniqueKey").Value == "0"),
                            FltTm = item.Element("FltTm").Value//added by abhilash
                            #endregion

                        };
            var dump2 = from item1 in xdFare.Descendants("FareInfo")
                        //where item1.Elements("FlightItemRef").Skip(0).First().Elements("FltItemAry").Elements("FltItem").Any(x => x.Element("IndexNum").Value == (i - 1).ToString())
                        select new
                        {
                            #region fltfare list per item
                            FltItem = item1.Elements("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Where(x => x.Element("IndexNum").Value == flightIndex),
                            itemindex = item1.Elements("FlightItemRef"),
                            adtRsvnRules = item1.Elements("RsvnRules").Where(x => x.Element("UniqueKey").Value == "0"),
                            stop = int.Parse(item1.Elements("FlightItemRef").Where(x => x.Element("ODNum").Value == flightTip).Elements("ODNumLegs").First().Value) - 1,
                            adtSegRelatedInfo = item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "1"),
                            chdSegRelatedInfo = item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            infSegRelatedInfo = searchInputs.Child > 0 ? item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "2"),

                            ItinSegInfo = item1.Elements("ItinSeg"),//.Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 1),
                            //chdItinSegInfo = item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 2),
                            //infItinSegInfo = searchInputs.Child > 0 ? item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 3) : item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 2),
                            adtClass = item1.Elements("FlightItemRef").Where(x => x.Element("ODNum").Value == flightTip).Elements("FltItemAry").Elements("FltItem"),
                            chdClass = item1.Descendants("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Elements("BICAry"),
                            infClass = item1.Elements("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Where(y => y.Elements("BICAry").Any(x => x.Elements("BICInfo").Any(m => m.Elements("PsgrDescNumAry").Any(t => t.Element("Num").Value == "3")))),
                            //chdUniqueKey = item1.Elements("PsgrTypes").Any(x => x.Element("PICReq").Value == "CNN") ? item1.Element("GenQuoteDetails").Element("UniqueKey").Value : "0",
                            AdtFare = item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "1"),
                            AdtRuleInfo = item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "1"),
                            chdRuleInfo = item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            infRuleInfo = searchInputs.Child > 0 ? item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            chdfare = item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "2"),
                            inffare = searchInputs.Child > 0 ? item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "2"),

                            adtFarebasis = item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "1" && x.Element("ODNum").Value == flightTip),
                            chdFarebasis = item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "2" && x.Element("ODNum").Value == flightTip),
                            infFarebasis = searchInputs.Child > 0 ? item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "3" && x.Element("ODNum").Value == flightTip) : item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "2" && x.Element("ODNum").Value == flightTip),
                            infoMsg = item1.Elements("InfoMsg").Where(x => x.Element("MsgNum").Value == "1171"),

                            adtFConstructor = item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0001"),
                            chdFConstructor = item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0002"),
                            infFConstructor = searchInputs.Child > 0 ? item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0003") : item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0002"),
                            PricingCode = item1.Elements("EnhancedPrivateFare").Any() ? item1.Elements("EnhancedPrivateFare").Elements("Acct").Any() ? item1.Elements("EnhancedPrivateFare").Elements("Acct").FirstOrDefault().Value : "" : ""



                            #endregion
                        };

            List<FlightSearchResults> FlightListInbound = new List<FlightSearchResults>();
            foreach (var p in dump1)
            {
                int TotalViaSector = 1;
                try
                {
                    int mn = 2;
                    bool flag = true;
                    if (flag)
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        try
                        {
                            List<string> fltCntList = new List<string>();
                            foreach (var gg1 in dump2)
                            {
                                gg1.ItinSegInfo.Descendants("FltNum").ToList().ForEach(y => fltCntList.Add(y.Value));
                            }

                            TotalViaSector = fltCntList.Distinct().ToList().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                            System.IO.File.AppendAllText("C:\\\\CPN_SP\\\\Result_1G_TotalViaSector_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + ex.StackTrace.ToString() + ex.Message + Environment.NewLine + Environment.NewLine);
                        }
                        #endregion

                        foreach (var gg in dump2)
                        {

                            //mn = mn + 1;
                            if (mn == 2)
                            {
                                FlightSearchResults objFS = new FlightSearchResults();
                                #region Flight Details
                                // objFS.TotDur = "";
                                objFS.getVia = getCityName(p.via1.ToString().Trim(), CityList);
                                if (p.via2.ToString().Trim() != "")
                                    objFS.getVia += getCityName(p.via2.ToString().Trim(), CityList);
                                objFS.DepartureDate = p.DepartureDate;
                                objFS.Departure_Date = Utility.Right(objFS.DepartureDate, 2) + " " + Utility.datecon(Utility.Mid(objFS.DepartureDate, 4, 2));
                                if (p.deptime.Length < 4 && p.deptime.Length == 1)
                                    objFS.DepartureTime = "000" + p.deptime;
                                else if (p.deptime.Length < 4 && p.deptime.Length == 2)
                                    objFS.DepartureTime = "00" + p.deptime;
                                else if (p.deptime.Length < 4 && p.deptime.Length == 3)
                                    objFS.DepartureTime = "0" + p.deptime;
                                else
                                    objFS.DepartureTime = p.deptime;

                                objFS.ArrivalDate = p.ArrivalDate;
                                objFS.Arrival_Date = Utility.Right(objFS.ArrivalDate, 2) + " " + Utility.datecon(Utility.Mid(objFS.ArrivalDate, 4, 2));

                                if (p.arrtime.Length < 4 && p.arrtime.Length == 1)
                                    objFS.ArrivalTime = "000" + p.arrtime;
                                else if (p.arrtime.Length < 4 && p.arrtime.Length == 2)
                                    objFS.ArrivalTime = "00" + p.arrtime;
                                else if (p.arrtime.Length < 4 && p.arrtime.Length == 3)
                                    objFS.ArrivalTime = "0" + p.arrtime;
                                else
                                    objFS.ArrivalTime = p.arrtime;

                                objFS.OrgDestFrom = Utility.Left(depCity, 3);// p.OrgDestFrom;
                                objFS.OrgDestTo = Utility.Left(arrCity, 3);//.OrgDestTo;
                                if (TType == "R")
                                    objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                else
                                    objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                objFS.DepAirportCode = p.DepartureAirportName;
                                objFS.DepartureLocation = p.DepartureAirportName;
                                objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);// ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].City;// ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["city"].ToString();
                                objFS.DepartureAirportName = getCityName(objFS.DepartureLocation, CityList);// ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["name"].ToString();
                                objFS.DepartureTerminal = p.DepartureTerminal;
                                objFS.ArrAirportCode = p.ArrivalAirportName;
                                objFS.ArrivalLocation = p.ArrivalAirportName;
                                objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList); //((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].City; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["city"].ToString();
                                objFS.ArrivalAirportName = getCityName(objFS.ArrivalLocation, CityList); //((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName; // ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["name"].ToString();
                                objFS.ArrivalTerminal = p.ArrivalTerminal;
                                objFS.BagInfo = gg.adtSegRelatedInfo.Skip(l - 1).First().Element("BagInfo").Value;
                                //objFS.AvailableSeats1 = p.AvailableSeats;
                                objFS.MarketingCarrier = p.MarketingCarrier;
                                objFS.OperatingCarrier = p.ValiDatingCarrier;
                                try
                                {
                                    if (gg.infoMsg.Count() > 0)
                                        objFS.ValiDatingCarrier = Utility.Right(gg.infoMsg.Skip(0).First().Element("Text").Value, 2);
                                    else
                                        objFS.ValiDatingCarrier = p.ValiDatingCarrier;
                                }
                                catch (Exception ex2)
                                {
                                    objFS.ValiDatingCarrier = p.ValiDatingCarrier;
                                }
                                objFS.FlightIdentification = p.FlightIdentification;
                                objFS.EQ = p.eq;
                                objFS.AirLineName = getAirlineName(objFS.MarketingCarrier, AirList); //((from ar in AirList where ar.AirlineCode == objFS.MarketingCarrier select ar).ToList())[0].AlilineName;// p.MarketingCarrier;
                                objFS.LineNumber = lineNumber;
                                objFS.Leg = l;

                                objFS.Adult = searchInputs.Adult;
                                objFS.Child = searchInputs.Child;
                                objFS.Infant = searchInputs.Infant;
                                objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                                #endregion

                                ////SMS charge calc
                                float srvCharge = 0;
                                float srvChargeAdt = 0;
                                float srvChargeChd = 0;
                                //// if (searchInputs.Trip.ToString() == "I")
                                //try
                                //{
                                //    srvCharge = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType,);//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                //}
                                //catch { srvCharge = 0; } 
                                ////SMS charge calc end
                                #region Adult
                                try
                                {   //objFS.AdtCabin = gg.adtSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;int.Parse(x.Element("UniqueKey").Value.ToString())
                                    objFS.AdtCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;
                                }
                                catch (Exception ex)
                                { }


                                if (gg.AdtFare.Elements("BaseFareAmt").Count() > 0)
                                {
                                    #region ADTFR
                                    int m = 0;
                                    while (m < (gg.AdtRuleInfo.Count()))
                                    {
                                        var adtreader = gg.AdtRuleInfo.Skip(m).First().CreateReader();
                                        adtreader.MoveToContent();
                                        objFS.AdtFar = "";// objFS.AdtFar + "<RulesInfo>" + adtreader.ReadInnerXml() + "</RulesInfo>ADTFAR";
                                        m = m + 1;
                                    }
                                    objFS.AdtFareType = chkFareRule(gg.adtRsvnRules);


                                    //if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                    //{
                                    //    string PenfeeC, PenfeeF, PenfeeI;
                                    //    PenfeeC = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("Cancellation").Value.ToString().Trim();
                                    //    PenfeeF = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("FailConfirmSpace").Value.ToString().Trim();
                                    //    PenfeeI = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("ItinChg").Value.ToString().Trim();
                                    //    if (PenfeeC == "Y" && PenfeeF == "Y")
                                    //        objFS.AdtFareType = "Refundable";
                                    //    else if (PenfeeC == "N" && PenfeeF == "N" && PenfeeI == "Y")
                                    //        objFS.AdtFareType = "Refundable";
                                    //    else
                                    //        objFS.AdtFareType = "Non Refundable";
                                    //}
                                    //else
                                    //{ objFS.AdtFareType = "Refundable"; }

                                    for (int mm = 0; mm < gg.adtClass.Count(); mm++)
                                    {
                                        if (gg.adtClass.Skip(mm).First().Element("IndexNum").Value == flightIndex)
                                        {
                                            var adtcabin = gg.adtClass.Skip(mm).First().Elements("BICAry").Elements("BICInfo");
                                            for (int ll = 0; ll < adtcabin.Count(); ll++)
                                            {
                                                var Num = adtcabin.Skip(ll).First().Elements("PsgrDescNumAry").Elements("Num");
                                                if (Num.Count() > 0)
                                                {
                                                    foreach (var pNum in Num)
                                                    {
                                                        if (pNum.Value == "1")
                                                            objFS.AdtRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "2" && searchInputs.Child > 0)
                                                            objFS.ChdRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "2" && searchInputs.Child == 0 && searchInputs.Infant > 0)
                                                            objFS.InfRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "3" && searchInputs.Child > 0 && searchInputs.Infant > 0)
                                                            objFS.InfRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                    }
                                                }
                                                else
                                                {
                                                    if (gg.adtClass.Skip(mm).First().ToString().Contains(flightIndex))
                                                    {
                                                        objFS.AdtRbd = adtcabin.First().Element("BIC").Value;
                                                        objFS.ChdRbd = adtcabin.First().Element("BIC").Value;
                                                        objFS.InfRbd = adtcabin.First().Element("BIC").Value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    try
                                    {
                                        if (gg.adtFarebasis.Count() > l - 1)
                                        {
                                            //objFS.FareRule = objFS.FareRule + gg.AdtRuleInfo.Skip(l - 1).First().Value;
                                            objFS.AdtFarebasis = gg.adtFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                        }
                                        else
                                        {
                                            //objFS.FareRule = objFS.FareRule + gg.AdtRuleInfo.Skip(gg.AdtRuleInfo.Count() - 1).First().Value;
                                            objFS.AdtFarebasis = gg.adtFarebasis.Skip(gg.adtFarebasis.Count() - 1).First().Element("FIC").Value;
                                        }

                                        objFS.FareRule = "";// objFS.FareRule + gg.AdtRuleInfo.First().Value;
                                    }
                                    catch (Exception ex2)
                                    {
                                        throw ex2;
                                    }

                                    #endregion
                                    //Adt Fare Details 
                                    if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                    { objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("BaseFareAmt").First().Value); }
                                    else
                                    {
                                        if (gg.AdtFare.Elements("BaseFareCurrency").First().Value == "INR")
                                            objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("BaseFareAmt").First().Value);
                                        else
                                            objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("EquivAmt").First().Value);
                                    }

                                    objFS.AdtFare = float.Parse(gg.AdtFare.Elements("TotAmt").First().Value);

                                    var adt = from item in gg.AdtFare.Elements("TaxDataAry").Elements("TaxData")
                                              select new
                                              {
                                                  adtAmt = item.Element("Amt").Value,
                                                  adtCon = item.Element("Country").Value
                                              };
                                    //Added by abhilash for AdtQtax
                                    objFS.AdtQ = GetQTax(gg.adtFConstructor.Elements("FareConstructText").Last().Value.ToString().Replace(Utility.Left(depCity, 3), "").Replace(Utility.Left(arrCity, 3), ""));
                                    //End Added by abhilash for AdtQtax
                                    foreach (var adtd in adt)
                                    {
                                        if (adtd.adtCon == "YQ")
                                            objFS.AdtFSur = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "WO")
                                            objFS.AdtWO = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "IN")
                                            objFS.AdtIN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "K3")
                                            objFS.AdtJN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "YM")
                                            objFS.AdtYR = float.Parse(adtd.adtAmt);
                                        else
                                            objFS.AdtOT = objFS.AdtOT + float.Parse(adtd.adtAmt);
                                        //if (adtd.adtCon != "YQ")
                                        objFS.AdtTax = objFS.AdtTax + float.Parse(adtd.adtAmt);

                                    }

                                    // float srvChargeAdt = 0;

                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeAdt = 0; }
                                    #endregion                                   

                                    //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt;
                                    //SMS charge add end


                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1);
                                    //objFS.AdtDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    //objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                    //objFS.AdtSrvTax = float.Parse(STTFTDS["STax"].ToString());
                                    //objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    //objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    //objFS.AdtEduCess = 0;
                                    //objFS.AdtHighEduCess = 0;
                                    //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                    if (searchInputs.Trip.ToString() == JourneyType.D.ToString() && searchInputs.RTF != true)
                                    {
                                   try
                                    {
                                            #region Show hide Net Fare and Commission Calculation-Adult Pax
                                            //if (objFS.Leg == 1)
                                            //{
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector),contx,"");
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {
                                                    objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                    objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                    objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                                    //objFS.AdtDiscount = 0;
                                                    //objFS.AdtCB = 0;
                                                    //objFS.AdtSrvTax = 0;
                                                    //objFS.AdtTF = 0;
                                                    //objFS.AdtTds = 0;
                                                    //objFS.IATAComm = 0;
                                                }
                                                else
                                                {
                                                    objFS.AdtDiscount1 = 0;
                                                    objFS.AdtCB = 0;
                                                    objFS.AdtSrvTax1 = 0;
                                                    objFS.AdtDiscount = 0;
                                                    objFS.AdtTF = 0;
                                                    objFS.AdtTds = 0;
                                                    objFS.IATAComm = 0;
                                                }
                                       //     }
                                            //else
                                            //{
                                            //    objFS.AdtDiscount1 = 0;
                                            //    objFS.AdtCB = 0;
                                            //    objFS.AdtSrvTax1 = 0;
                                            //    objFS.AdtDiscount = 0;
                                            //    objFS.AdtTF = 0;
                                            //    objFS.AdtTds = 0;
                                            //    objFS.IATAComm = 0;
                                            //}
                                            #endregion                                            
                                     }
                                    catch(Exception ex)
                                    {
                                            objFS.AdtDiscount1 = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax1 = 0;
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                            System.IO.File.AppendAllText("C:\\\\CPN_SP\\\\Result_1G_ADTCOM_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + ex.StackTrace.ToString() + ex.Message + Environment.NewLine + Environment.NewLine);
                                    }

                                        //objFS.AdtDiscount = 0;
                                        //objFS.AdtCB = 0;
                                        //objFS.AdtSrvTax = 0;
                                        //objFS.AdtTF = 0;
                                        //objFS.AdtTds = 0;
                                        //objFS.IATAComm = 0;
                                    }
                                    else
                                    {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }

                                    objFS.AdtEduCess = 0;
                                    objFS.AdtHighEduCess = 0;
                                    if (searchInputs.IsCorp == true)
                                    {
                                        try
                                        {
                                            if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                                objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                            }
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                            objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }
                                #endregion

                                #region child
                                objFS.ChdTax = 0;
                                if (searchInputs.Child > 0)
                                {
                                    if (gg.chdfare.Elements("BaseFareAmt").Count() > 0)
                                    {
                                        #region CHDFR
                                        //objFS.ChdfareType = p.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value;
                                        if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                        {
                                            objFS.ChdfareType = "Non Refundable";
                                        }
                                        else
                                        { objFS.ChdfareType = "Refundable"; }

                                        int m = 0;
                                        while (m < (gg.chdRuleInfo.Count()))
                                        {
                                            var chdreader = gg.chdRuleInfo.Skip(m).First().CreateReader();
                                            chdreader.MoveToContent();
                                            objFS.ChdFar = "";// objFS.ChdFar + "<RulesInfo>" + chdreader.ReadInnerXml() + "</RulesInfo>CHDFAR";
                                            m = m + 1;
                                        }

                                        //objFS.ChdCabin = gg.chdSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;

                                        objFS.ChdCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;

                                        try
                                        {
                                            if (gg.chdFarebasis.Count() > l - 1)
                                            {
                                                objFS.ChdFarebasis = gg.chdFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                            }
                                            else
                                            {
                                                objFS.ChdFarebasis = gg.chdFarebasis.Skip(gg.chdFarebasis.Count() - 1).First().Element("FIC").Value;
                                            }
                                            objFS.FareRule = "";// objFS.FareRule + gg.chdRuleInfo.First().Value;
                                        }
                                        catch (Exception ex2)
                                        {
                                            throw ex2;
                                        }
                                        //var chdcabin = from kk in gg.chdClass.Elements("BICInfo")
                                        //               where kk.Elements("BICAry").Any(f => f.Elements("BICInfo").Any(r => r.Elements("PsgrDescNumAry").Any(x => x.Element("Num").Value == "1")))
                                        //               select new
                                        //               {
                                        //                   cabin = kk.Value,

                                        //               };
                                        #endregion
                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        { objFS.ChdBFare = float.Parse(gg.chdfare.Elements("BaseFareAmt").First().Value); }
                                        else
                                        {
                                            if (gg.chdfare.Elements("BaseFareCurrency").First().Value == "INR")
                                                objFS.ChdBFare = float.Parse(gg.chdfare.Elements("BaseFareAmt").First().Value);
                                            else
                                                objFS.ChdBFare = float.Parse(gg.chdfare.Elements("EquivAmt").First().Value);
                                        }

                                        objFS.ChdFare = float.Parse(gg.chdfare.Elements("TotAmt").First().Value);
                                        var chd = from item in gg.chdfare.Elements("TaxDataAry").Elements("TaxData")
                                                  select new
                                                  {
                                                      chdAmt = item.Element("Amt").Value,
                                                      chdCon = item.Element("Country").Value
                                                  };
                                        //Added by abhilash for ChdQtax
                                        objFS.ChdQ = GetQTax(gg.chdFConstructor.Elements("FareConstructText").Last().Value.ToString().Replace(Utility.Left(depCity, 3), "").Replace(Utility.Left(arrCity, 3), ""));
                                        //End Added by abhilash for ChdQtax
                                        foreach (var chdd in chd)
                                        {
                                            if (chdd.chdCon == "YQ")
                                                objFS.ChdFSur = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "WO")
                                                objFS.ChdWO = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "IN")
                                                objFS.ChdIN = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "K3")
                                                objFS.ChdJN = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "YM")
                                                objFS.ChdYR = float.Parse(chdd.chdAmt);
                                            else
                                                objFS.ChdOT = objFS.ChdOT + float.Parse(chdd.chdAmt);
                                            //if (chdd.chdCon != "YQ")
                                            objFS.ChdTax = objFS.ChdTax + float.Parse(chdd.chdAmt);

                                        }

                                        //float srvChargeChd = 0;

                                        #region Get MISC Markup Charges Date 06-03-2018
                                        try
                                        {
                                            srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                        }
                                        catch { srvChargeChd = 0; }
                                        #endregion
                                                                           

                                        //SMS Charge add
                                        objFS.ChdOT = objFS.ChdOT + srvChargeChd;
                                        objFS.ChdTax = objFS.ChdTax + srvChargeChd;
                                        objFS.ChdFare = objFS.ChdFare + srvChargeChd;
                                        //SMS charge add end

                                        objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                        objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                        CommDt.Clear();
                                        STTFTDS.Clear();
                                        //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1);
                                        //objFS.ChdDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        //objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                        //objFS.ChdSrvTax = float.Parse(STTFTDS["STax"].ToString());
                                        //objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                        //objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                        //objFS.ChdEduCess = 0;
                                        //objFS.ChdHighEduCess = 0;
                                        // objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        {
                                            //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1);

                                        try
                                        {
                                                #region Show hide Net Fare and Commission Calculation-Child Pax
                                                if (objFS.Leg == 1)
                                                {
                                                    CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector),contx,"");
                                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                                    {
                                                        objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                        objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                        STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                        objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                        objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                        objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                        objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    }
                                                    else
                                                    {
                                                        objFS.ChdDiscount1 = 0;
                                                        objFS.ChdCB = 0;
                                                        objFS.ChdSrvTax1 = 0;
                                                        objFS.ChdDiscount = 0;
                                                        objFS.ChdTF = 0;
                                                        objFS.ChdTds = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    objFS.ChdDiscount1 = 0;
                                                    objFS.ChdCB = 0;
                                                    objFS.ChdSrvTax1 = 0;
                                                    objFS.ChdDiscount = 0;
                                                    objFS.ChdTF = 0;
                                                    objFS.ChdTds = 0;
                                                }
                                                #endregion                                                
                                         }
                                        catch(Exception ex)
                                        {
                                                objFS.ChdDiscount1 = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax1 = 0;
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                                System.IO.File.AppendAllText("C:\\\\CPN_SP\\\\Result_1G_CHDCOM_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + ex.StackTrace.ToString() + ex.Message + Environment.NewLine + Environment.NewLine);
                                        }

                                            //objFS.ChdDiscount = 0;
                                            //objFS.ChdCB = 0;
                                            //objFS.ChdSrvTax = 0;
                                            //objFS.ChdTF = 0;
                                            //objFS.ChdTds = 0;
                                        }
                                        else
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        if (searchInputs.IsCorp == true)
                                        {
                                            if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                                objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                            }
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                                objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }
                                        objFS.ChdEduCess = 0;
                                        objFS.ChdHighEduCess = 0;
                                    }
                                    else
                                    {
                                        objFS.ChdBFare = 0;
                                        objFS.ChdFare = 0;
                                        objFS.ChdFSur = 0;
                                        objFS.ChdWO = 0;
                                        objFS.ChdIN = 0;
                                        objFS.ChdJN = 0;
                                        objFS.ChdYR = 0;
                                        objFS.ChdOT = 0;
                                        objFS.ChdTax = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                        objFS.ChdEduCess = 0;
                                        objFS.ChdHighEduCess = 0;

                                    }
                                }
                                else
                                {
                                    objFS.ChdBFare = 0;
                                    objFS.ChdFare = 0;
                                    objFS.ChdFSur = 0;
                                    objFS.ChdWO = 0;
                                    objFS.ChdIN = 0;
                                    objFS.ChdJN = 0;
                                    objFS.ChdYR = 0;
                                    objFS.ChdOT = 0;
                                    objFS.ChdTax = 0;
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    //objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                    objFS.ChdEduCess = 0;
                                    objFS.ChdHighEduCess = 0;

                                }

                                #endregion

                                #region Infant
                                objFS.InfTax = 0;
                                if (searchInputs.Infant > 0)
                                {
                                    if (gg.inffare.Elements("BaseFareAmt").Count() > 0)
                                    {
                                        //objFS.InfFareType = p.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value;
                                        if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                        {
                                            objFS.InfFareType = "Non Refundable";
                                        }
                                        else
                                        { objFS.InfFareType = "Refundable"; }

                                        int m = 0;
                                        while (m < (gg.infRuleInfo.Count()))
                                        {
                                            var infreader = gg.infRuleInfo.Skip(m).First().CreateReader();
                                            infreader.MoveToContent();
                                            objFS.InfFar = "";// objFS.InfFar + "<RulesInfo>" + infreader.ReadInnerXml() + "</RulesInfo>INFFAR";
                                            m = m + 1;
                                        }

                                        //objFS.InfCabin = gg.infSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;

                                        objFS.InfCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;
                                        try
                                        {
                                            if (gg.infFarebasis.Count() > l - 1)
                                            {
                                                objFS.InfFarebasis = gg.infFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                            }
                                            else
                                            {
                                                objFS.InfFarebasis = gg.infFarebasis.Skip(gg.infFarebasis.Count() - 1).First().Element("FIC").Value;
                                            }
                                        }
                                        catch (Exception ex2)
                                        { throw ex2; }

                                        objFS.FareRule = "";// objFS.FareRule + gg.infRuleInfo.First().Value;
                                        //var infcabin = p.adtClass.Elements("BICAry").Elements("BICInfo").Where(x => x.Elements("PsgrDescNumAry").Any(kk => kk.Element("Num").Value == "3"));
                                        //objFS.Inf_cabin = infcabin.Elements("BIC").First().Value;
                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        { objFS.InfBfare = float.Parse(gg.inffare.Elements("BaseFareAmt").First().Value); }
                                        else
                                        {
                                            if (gg.inffare.Elements("BaseFareCurrency").First().Value == "INR")
                                                objFS.InfBfare = float.Parse(gg.inffare.Elements("BaseFareAmt").First().Value);
                                            else
                                                objFS.InfBfare = float.Parse(gg.inffare.Elements("EquivAmt").First().Value);
                                        }

                                        objFS.InfFare = float.Parse(gg.inffare.Elements("TotAmt").First().Value);

                                        var inf = from item in gg.inffare.Elements("TaxDataAry").Elements("TaxData")
                                                  select new
                                                  {
                                                      infAmt = item.Element("Amt").Value,
                                                      infCon = item.Element("Country").Value
                                                  };
                                        foreach (var infd in inf)
                                        {
                                            if (infd.infCon == "YQ")
                                                objFS.InfFSur = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "WO")
                                                objFS.InfWO = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "IN")
                                                objFS.InfIN = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "K3")
                                                objFS.InfJN = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "YM")
                                                objFS.InfYR = float.Parse(infd.infAmt);
                                            else
                                                objFS.InfOT = objFS.InfOT + float.Parse(infd.infAmt);
                                            //if (infd.infCon != "YQ")
                                            objFS.InfTax = objFS.InfTax + float.Parse(infd.infAmt);
                                        }

                                        if (searchInputs.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                                objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                    }
                                    else
                                    {
                                        objFS.InfBfare = 0;
                                        objFS.InfFare = 0;
                                        objFS.InfFSur = 0;
                                        objFS.InfWO = 0;
                                        objFS.InfIN = 0;
                                        objFS.InfJN = 0;
                                        objFS.InfYR = 0;
                                        objFS.InfOT = 0;
                                        objFS.InfTax = 0;
                                        objFS.InfDiscount = 0;
                                        objFS.InfCB = 0;
                                        objFS.InfSrvTax = 0;
                                        objFS.InfTF = 0;
                                        objFS.InfTds = 0;
                                        objFS.InfEduCess = 0;
                                        objFS.InfHighEduCess = 0;
                                    }
                                }
                                else
                                {
                                    objFS.InfBfare = 0;
                                    objFS.InfFare = 0;
                                    objFS.InfFSur = 0;
                                    objFS.InfWO = 0;
                                    objFS.InfIN = 0;
                                    objFS.InfJN = 0;
                                    objFS.InfYR = 0;
                                    objFS.InfOT = 0;
                                    objFS.InfTax = 0;
                                    objFS.InfDiscount = 0;
                                    objFS.InfCB = 0;
                                    objFS.InfSrvTax = 0;
                                    objFS.InfTF = 0;
                                    objFS.InfTds = 0;
                                    objFS.InfEduCess = 0;
                                    objFS.InfHighEduCess = 0;
                                }

                                #endregion

                                objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                                objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                                objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                                objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                                objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                                objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                                objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                                objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                                objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                                objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                                objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                    objFS.TotalFare = (objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee) - ((objFS.ADTAdminMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child));
                                else
                                    objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                                objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                                // objFS.OperatingCarrier = p.opreatingcarrier;

                                int hr = int.Parse(p.jrnyTime) / 60;
                                int min = int.Parse(p.jrnyTime) % 60;
                                objFS.TotDur = hr.ToString("00") + ":" + min.ToString("00");

                                int hr1 = int.Parse(p.FltTm) / 60;
                                int min1 = int.Parse(p.FltTm) % 60;
                                objFS.TripCnt = hr1.ToString("00") + ":" + min1.ToString("00");

                                objFS.sno = p.fltNeeded;
                                objFS.Stops = (int.Parse(p.lineNumber.ToString()) + int.Parse(gg.stop.ToString())).ToString() + "-Stop";

                                objFS.TripType = FlightWay;// TType;

                                objFS.Trip = searchInputs.Trip.ToString();
                                objFS.FType = FType;
                                objFS.Flight = flightTip;
                                objFS.RBD = objFS.AdtRbd;
                                string seat = "60";
                                try
                                {

                                    seat = ((from item in p.AvailableSeats.Elements("BICStatus")
                                             where item.Element("BIC").Value == objFS.AdtRbd
                                             select item.Element("Status").Value).ToList())[0].ToString();
                                }
                                catch { }
                                if (Utility.IsNumeric(seat))
                                    objFS.AvailableSeats1 = Convert.ToString(int.Parse(seat));
                                else
                                    objFS.AvailableSeats1 = seat;
                                objFS.Searchvalue = gg.PricingCode;  //14-07-2017  add pricing code (Set deal Code)-Devesh
                                srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                                objFS.OriginalTT = srvCharge;
                                objFS.IsCorp = searchInputs.IsCorp;
                                objFS.Provider = "1G";
                                objFS.SearchId = SearchId;

                                if (!string.IsNullOrEmpty(gg.PricingCode) || CrdType=="CRP")
                                {
                                    objFS.AdtFar = "CRP";
                                }
                                else
                                {
                                    objFS.AdtFar = "NRM";
                                }
                                FlightList.Add(objFS);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            return FlightList[0];
        }

        private FlightSearchResults parseFlightMultiCity(XDocument xDocU, string flightIndex, int l, FlightSearch searchInputs, XDocument xdFare, int lineNumber, string TType, string FType, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, string flightTip, string FlightWay, string depCity, string arrCity, List<MISCCharges> MiscList, string flightSeq, string CrdType, string SearchId)
        {
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            //  string arrCity, depCity = "";
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            HttpContext contx = HttpContext.Current;
            //arrCity = searchInputs.HidTxtDepCity;
            //depCity = searchInputs.HidTxtArrCity;
            #region Select Flihts


            var dump1 = from item in xDocU.Descendants("AirAvail").Descendants("AvailFlt")
                        where item.Element("Index").Value == flightIndex

                        select new
                        {
                            index = "",
                            #region flt list per item
                            fltNeeded = "",// item.Element("OpAirV").Name + ":" + item.Element("OpAirV").Value + "," + item.Element("LinkSellAgrmnt").Name + ":" + item.Element("LinkSellAgrmnt").Value + "," + item.Element("DaysOperates").Name + ":" + item.Element("DaysOperates").Value + "," + item.Element("DaysOperates").Name + ":" + item.Element("DaysOperates").Value + "," + item.Element("ETktEligibility").Name + ":" + item.Element("ETktEligibility").Value + "," + item.Element("AvailSource").Name + ":" + item.Element("AvailSource").Value + "," + item.Element("SegRelatedInfo").Element("BagInfo").Name + ":" + item1.Element("SegRelatedInfo").Element("BagInfo").Value,
                            opreatingcarrier = item.Element("OpAirV").Value,
                            jrnyTime = item.Element("JrnyTm").Value,

                            via1 = int.Parse(item.Element("NumStops").Value) > 0 ? item.Element("FrstDwnlnStp").Value : " ",
                            via2 = int.Parse(item.Element("NumStops").Value) > 1 ? item.Element("LastDwnlnStp").Value : " ",
                            dest = searchInputs.DepartureCity,
                            lineNumber = item.Element("NumStops").Value,
                            src = searchInputs.ArrivalCity,
                            deptime = item.Element("StartTm").Value,
                            arrtime = item.Element("EndTm").Value,
                            conx = item.Element("Conx").Value,
                            AirCode = item.Element("AirV").Value,
                            FltNo = item.Element("FltNum").Value,
                            StartDate = item.Element("StartDt").Value,
                            leg = item.Element("NumStops").Value,
                            //**************
                            OrgDestFrom = searchInputs.DepartureCity,
                            OrgDestTo = searchInputs.ArrivalCity,
                            Adult = searchInputs.Adult,
                            Child = searchInputs.Child,
                            Infant = searchInputs.Infant,
                            TotPax = searchInputs.Adult + searchInputs.Child,
                            DepartureLocation = item.Element("StartAirp").Value,
                            DepartureCityName = item.Element("StartAirp").Value,
                            DepartureAirportName = item.Element("StartAirp").Value,
                            DepartureTerminal = item.Element("StartTerminal").Value,
                            ArrivalLocation = item.Element("EndAirp").Value,
                            ArrivalCityName = item.Element("EndAirp").Value,
                            ArrivalAirportName = item.Element("EndAirp").Value,
                            ArrivalTerminal = item.Element("EndTerminal").Value,
                            DepartureDate = item.Element("StartDt").Value,
                            Departure_Date = item.Element("StartDt").Value,
                            DepartureTime = item.Element("StartTm").Value,
                            ArrivalDate = item.Element("EndDt").Value,
                            Arrival_Date = item.Element("EndDt").Value,
                            MarketingCarrier = item.Element("AirV").Value,
                            eq = item.Element("Equip").Value,
                            FlightIdentification = item.Element("FltNum").Value,
                            // RBD = item.Element("EndDt").Value,
                            AvailableSeats = item.Elements("BICStatusAry"),
                            ValiDatingCarrier = item.Element("AirV").Value,
                            adtRsvnRules = "",//item1.Elements("RsvnRules").Where(x => x.Element("UniqueKey").Value == "0"),
                            FltTm = item.Element("FltTm").Value//added by abhilash
                            #endregion

                        };
            var dump2 = from item1 in xdFare.Descendants("FareInfo")
                        //where item1.Elements("FlightItemRef").Skip(0).First().Elements("FltItemAry").Elements("FltItem").Any(x => x.Element("IndexNum").Value == (i - 1).ToString())
                        select new
                        {
                            #region fltfare list per item
                            FltItem = item1.Elements("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Where(x => x.Element("IndexNum").Value == flightIndex),
                            itemindex = item1.Elements("FlightItemRef"),
                            adtRsvnRules = item1.Elements("RsvnRules").Where(x => x.Element("UniqueKey").Value == "0"),
                            stop = int.Parse(item1.Elements("FlightItemRef").Where(x => x.Element("ODNum").Value == flightTip).Elements("ODNumLegs").First().Value) - 1,
                            adtSegRelatedInfo = item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "1"),
                            chdSegRelatedInfo = item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            infSegRelatedInfo = searchInputs.Child > 0 ? item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("SegRelatedInfo").Where(x => x.Element("UniqueKey").Value == "2"),

                            ItinSegInfo = item1.Elements("ItinSeg"),//.Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 1),
                            //chdItinSegInfo = item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 2),
                            //infItinSegInfo = searchInputs.Child > 0 ? item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 3) : item1.Elements("ItinSeg").Where(x => int.Parse(x.Element("UniqueKey").Value.ToString()) == 2),
                            adtClass = item1.Elements("FlightItemRef").Where(x => x.Element("ODNum").Value == flightTip).Elements("FltItemAry").Elements("FltItem"),
                            chdClass = item1.Descendants("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Elements("BICAry"),
                            infClass = item1.Elements("FlightItemRef").Elements("FltItemAry").Elements("FltItem").Where(y => y.Elements("BICAry").Any(x => x.Elements("BICInfo").Any(m => m.Elements("PsgrDescNumAry").Any(t => t.Element("Num").Value == "3")))),
                            //chdUniqueKey = item1.Elements("PsgrTypes").Any(x => x.Element("PICReq").Value == "CNN") ? item1.Element("GenQuoteDetails").Element("UniqueKey").Value : "0",
                            AdtFare = item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "1"),
                            AdtRuleInfo = item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "1"),
                            chdRuleInfo = item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            infRuleInfo = searchInputs.Child > 0 ? item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("RulesInfo").Where(x => x.Element("UniqueKey").Value == "2"),
                            chdfare = item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "2"),
                            inffare = searchInputs.Child > 0 ? item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "3") : item1.Elements("GenQuoteDetails").Where(x => x.Element("UniqueKey").Value == "2"),

                            adtFarebasis = item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "1" && x.Element("ODNum").Value == flightTip),
                            chdFarebasis = item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "2" && x.Element("ODNum").Value == flightTip),
                            infFarebasis = searchInputs.Child > 0 ? item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "3" && x.Element("ODNum").Value == flightTip) : item1.Elements("FareBasisCodeSummary").Elements("FICAry").Elements("FICInfo").Where(x => x.Element("PsgrDescNum").Value == "2" && x.Element("ODNum").Value == flightTip),
                            infoMsg = item1.Elements("InfoMsg").Where(x => x.Element("MsgNum").Value == "1171"),

                            adtFConstructor = item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0001"),
                            chdFConstructor = item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0002"),
                            infFConstructor = searchInputs.Child > 0 ? item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0003") : item1.Elements("FareConstruction").Where(x => x.Element("UniqueKey").Value == "0002"),
                            PricingCode = item1.Elements("EnhancedPrivateFare").Any() ? item1.Elements("EnhancedPrivateFare").Elements("Acct").Any() ? item1.Elements("EnhancedPrivateFare").Elements("Acct").FirstOrDefault().Value : "" : ""
                            #endregion
                        };

            List<FlightSearchResults> FlightListInbound = new List<FlightSearchResults>();
            foreach (var p in dump1)
            {
                try
                {
                    int mn = 2;
                    bool flag = true;
                    if (flag)
                    {
                        foreach (var gg in dump2)
                        {
                            //mn = mn + 1;
                            if (mn == 2)
                            {
                                FlightSearchResults objFS = new FlightSearchResults();
                                #region Flight Details
                                // objFS.TotDur = "";
                                objFS.getVia = getCityName(p.via1.ToString().Trim(), CityList);
                                if (p.via2.ToString().Trim() != "")
                                    objFS.getVia += getCityName(p.via2.ToString().Trim(), CityList);
                                objFS.DepartureDate = p.DepartureDate;
                                objFS.Departure_Date = Utility.Right(objFS.DepartureDate, 2) + " " + Utility.datecon(Utility.Mid(objFS.DepartureDate, 4, 2));
                                if (p.deptime.Length < 4 && p.deptime.Length == 1)
                                    objFS.DepartureTime = "000" + p.deptime;
                                else if (p.deptime.Length < 4 && p.deptime.Length == 2)
                                    objFS.DepartureTime = "00" + p.deptime;
                                else if (p.deptime.Length < 4 && p.deptime.Length == 3)
                                    objFS.DepartureTime = "0" + p.deptime;
                                else
                                    objFS.DepartureTime = p.deptime;

                                objFS.ArrivalDate = p.ArrivalDate;
                                objFS.Arrival_Date = Utility.Right(objFS.ArrivalDate, 2) + " " + Utility.datecon(Utility.Mid(objFS.ArrivalDate, 4, 2));

                                if (p.arrtime.Length < 4 && p.arrtime.Length == 1)
                                    objFS.ArrivalTime = "000" + p.arrtime;
                                else if (p.arrtime.Length < 4 && p.arrtime.Length == 2)
                                    objFS.ArrivalTime = "00" + p.arrtime;
                                else if (p.arrtime.Length < 4 && p.arrtime.Length == 3)
                                    objFS.ArrivalTime = "0" + p.arrtime;
                                else
                                    objFS.ArrivalTime = p.arrtime;

                                objFS.OrgDestFrom = Utility.Left(depCity, 3);// p.OrgDestFrom;
                                objFS.OrgDestTo = Utility.Left(arrCity, 3);//.OrgDestTo;
                                //if (TType == "R")
                                //    objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                //else
                                //    objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;


                                if (!string.IsNullOrEmpty(searchInputs.DepDate2) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity2) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity2))
                                {
                                    objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + searchInputs.HidTxtArrCity2.Split(',')[0];
                                }
                                if (!string.IsNullOrEmpty(searchInputs.DepDate3) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity3) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity3))
                                {
                                    objFS.Sector += ":" + searchInputs.HidTxtArrCity3.Split(',')[0];
                                }
                                if (!string.IsNullOrEmpty(searchInputs.DepDate4) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity4) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity4))
                                {
                                    objFS.Sector += ":" + searchInputs.HidTxtArrCity4.Split(',')[0];
                                }
                                if (!string.IsNullOrEmpty(searchInputs.DepDate5) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity5) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity5))
                                {
                                    objFS.Sector += ":" + searchInputs.HidTxtArrCity5.Split(',')[0];
                                }
                                if (!string.IsNullOrEmpty(searchInputs.DepDate6) && !string.IsNullOrEmpty(searchInputs.HidTxtDepCity6) && !string.IsNullOrEmpty(searchInputs.HidTxtArrCity6))
                                {
                                    objFS.Sector += ":" + searchInputs.HidTxtArrCity6.Split(',')[0];
                                }




                                objFS.DepAirportCode = p.DepartureAirportName;
                                objFS.DepartureLocation = p.DepartureAirportName;
                                objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);// ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].City;// ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["city"].ToString();
                                objFS.DepartureAirportName = getCityName(objFS.DepartureLocation, CityList);// ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["name"].ToString();
                                objFS.DepartureTerminal = p.DepartureTerminal;
                                objFS.ArrAirportCode = p.ArrivalAirportName;
                                objFS.ArrivalLocation = p.ArrivalAirportName;
                                objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList); //((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].City; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["city"].ToString();
                                objFS.ArrivalAirportName = getCityName(objFS.ArrivalLocation, CityList); //((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName; // ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["name"].ToString();
                                objFS.ArrivalTerminal = p.ArrivalTerminal;
                                objFS.BagInfo = gg.adtSegRelatedInfo.Skip(l - 1).First().Element("BagInfo").Value;
                                //objFS.AvailableSeats1 = p.AvailableSeats;
                                objFS.MarketingCarrier = p.MarketingCarrier;
                                objFS.OperatingCarrier = p.ValiDatingCarrier;
                                try
                                {
                                    if (gg.infoMsg.Count() > 0)
                                        objFS.ValiDatingCarrier = Utility.Right(gg.infoMsg.Skip(0).First().Element("Text").Value, 2);
                                    else
                                        objFS.ValiDatingCarrier = p.ValiDatingCarrier;
                                }
                                catch (Exception ex2)
                                {
                                    objFS.ValiDatingCarrier = p.ValiDatingCarrier;
                                }
                                objFS.FlightIdentification = p.FlightIdentification;
                                objFS.EQ = p.eq;
                                objFS.AirLineName = getAirlineName(objFS.MarketingCarrier, AirList); //((from ar in AirList where ar.AirlineCode == objFS.MarketingCarrier select ar).ToList())[0].AlilineName;// p.MarketingCarrier;
                                objFS.LineNumber = lineNumber;
                                objFS.Leg = l;

                                objFS.Adult = searchInputs.Adult;
                                objFS.Child = searchInputs.Child;
                                objFS.Infant = searchInputs.Infant;
                                objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                                #endregion

                                ////SMS charge calc
                                float srvCharge = 0;                               
                                float srvChargeAdt = 0;
                                float srvChargeChd = 0;
                                // if (searchInputs.Trip.ToString() == "I")
                                //try
                                //{
                                //    srvCharge = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier);//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                //}
                                //catch { srvCharge = 0; } 
                                
                                ////SMS charge calc end
                                #region Adult
                                try
                                {   //objFS.AdtCabin = gg.adtSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;int.Parse(x.Element("UniqueKey").Value.ToString())
                                    objFS.AdtCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;
                                }
                                catch (Exception ex)
                                { }


                                if (gg.AdtFare.Elements("BaseFareAmt").Count() > 0)
                                {
                                    #region ADTFR
                                    int m = 0;
                                    while (m < (gg.AdtRuleInfo.Count()))
                                    {
                                        var adtreader = gg.AdtRuleInfo.Skip(m).First().CreateReader();
                                        adtreader.MoveToContent();
                                        objFS.AdtFar = "";// objFS.AdtFar + "<RulesInfo>" + adtreader.ReadInnerXml() + "</RulesInfo>ADTFAR";
                                        m = m + 1;
                                    }
                                    objFS.AdtFareType = chkFareRule(gg.adtRsvnRules);


                                    //if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                    //{
                                    //    string PenfeeC, PenfeeF, PenfeeI;
                                    //    PenfeeC = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("Cancellation").Value.ToString().Trim();
                                    //    PenfeeF = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("FailConfirmSpace").Value.ToString().Trim();
                                    //    PenfeeI = gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("ItinChg").Value.ToString().Trim();
                                    //    if (PenfeeC == "Y" && PenfeeF == "Y")
                                    //        objFS.AdtFareType = "Refundable";
                                    //    else if (PenfeeC == "N" && PenfeeF == "N" && PenfeeI == "Y")
                                    //        objFS.AdtFareType = "Refundable";
                                    //    else
                                    //        objFS.AdtFareType = "Non Refundable";
                                    //}
                                    //else
                                    //{ objFS.AdtFareType = "Refundable"; }

                                    for (int mm = 0; mm < gg.adtClass.Count(); mm++)
                                    {
                                        if (gg.adtClass.Skip(mm).First().Element("IndexNum").Value == flightIndex)
                                        {
                                            var adtcabin = gg.adtClass.Skip(mm).First().Elements("BICAry").Elements("BICInfo");
                                            for (int ll = 0; ll < adtcabin.Count(); ll++)
                                            {
                                                var Num = adtcabin.Skip(ll).First().Elements("PsgrDescNumAry").Elements("Num");
                                                if (Num.Count() > 0)
                                                {
                                                    foreach (var pNum in Num)
                                                    {
                                                        if (pNum.Value == "1")
                                                            objFS.AdtRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "2" && searchInputs.Child > 0)
                                                            objFS.ChdRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "2" && searchInputs.Child == 0 && searchInputs.Infant > 0)
                                                            objFS.InfRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                        if (pNum.Value == "3" && searchInputs.Child > 0 && searchInputs.Infant > 0)
                                                            objFS.InfRbd = adtcabin.Skip(ll).First().Element("BIC").Value;
                                                    }
                                                }
                                                else
                                                {
                                                    if (gg.adtClass.Skip(mm).First().ToString().Contains(flightIndex))
                                                    {
                                                        objFS.AdtRbd = adtcabin.First().Element("BIC").Value;
                                                        objFS.ChdRbd = adtcabin.First().Element("BIC").Value;
                                                        objFS.InfRbd = adtcabin.First().Element("BIC").Value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    try
                                    {
                                        if (gg.adtFarebasis != null)
                                        {
                                            if (gg.adtFarebasis.Count() > l - 1)
                                            {
                                                //objFS.FareRule = objFS.FareRule + gg.AdtRuleInfo.Skip(l - 1).First().Value;
                                                objFS.AdtFarebasis = gg.adtFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                            }
                                            else
                                            {
                                                //objFS.FareRule = objFS.FareRule + gg.AdtRuleInfo.Skip(gg.AdtRuleInfo.Count() - 1).First().Value;


                                                objFS.AdtFarebasis = gg.adtFarebasis.Skip(gg.adtFarebasis.Count() - 1).First().Element("FIC").Value;
                                            }
                                        }
                                        objFS.FareRule = "";// objFS.FareRule + gg.AdtRuleInfo.First().Value;
                                    }
                                    catch (Exception ex2)
                                    {
                                        //throw ex2;
                                    }

                                    #endregion
                                    //Adt Fare Details 
                                    if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                    { objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("BaseFareAmt").First().Value); }
                                    else
                                    {
                                        if (gg.AdtFare.Elements("BaseFareCurrency").First().Value == "INR")
                                            objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("BaseFareAmt").First().Value);
                                        else
                                            objFS.AdtBfare = float.Parse(gg.AdtFare.Elements("EquivAmt").First().Value);
                                    }

                                    objFS.AdtFare = float.Parse(gg.AdtFare.Elements("TotAmt").First().Value);

                                    var adt = from item in gg.AdtFare.Elements("TaxDataAry").Elements("TaxData")
                                              select new
                                              {
                                                  adtAmt = item.Element("Amt").Value,
                                                  adtCon = item.Element("Country").Value
                                              };
                                    //Added by abhilash for AdtQtax
                                    objFS.AdtQ = GetQTax(gg.adtFConstructor.Elements("FareConstructText").Last().Value.ToString().Replace(Utility.Left(depCity, 3), "").Replace(Utility.Left(arrCity, 3), ""));
                                    //End Added by abhilash for AdtQtax
                                    foreach (var adtd in adt)
                                    {
                                        if (adtd.adtCon == "YQ")
                                            objFS.AdtFSur = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "WO")
                                            objFS.AdtWO = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "IN")
                                            objFS.AdtIN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "K3")
                                            objFS.AdtJN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "YM")
                                            objFS.AdtYR = float.Parse(adtd.adtAmt);
                                        else
                                            objFS.AdtOT = objFS.AdtOT + float.Parse(adtd.adtAmt);
                                        //if (adtd.adtCon != "YQ")
                                        objFS.AdtTax = objFS.AdtTax + float.Parse(adtd.adtAmt);

                                    }

                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeAdt = 0; }
                                    #endregion  

                                    //SMS charge add 
                                    objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                    objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                    objFS.AdtFare = objFS.AdtFare + srvChargeAdt;//srvCharge;
                                    //SMS charge add end


                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1);
                                    //objFS.AdtDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    //objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                    //objFS.AdtSrvTax = float.Parse(STTFTDS["STax"].ToString());
                                    //objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    //objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    //objFS.AdtEduCess = 0;
                                    //objFS.AdtHighEduCess = 0;
                                    //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                    if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                    {

                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, "1",contx,"");
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {
                                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                //objFS.AdtDiscount = 0;
                                                //objFS.AdtCB = 0;
                                                //objFS.AdtSrvTax = 0;
                                                //objFS.AdtTF = 0;
                                                //objFS.AdtTds = 0;
                                                //objFS.IATAComm = 0;
                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }                                            
                                        }
                                        else
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                    }
                                        else
                                        {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                       }
                                        #endregion
                                                                         

                                    objFS.AdtEduCess = 0;
                                    objFS.AdtHighEduCess = 0;
                                    if (searchInputs.IsCorp == true)
                                    {
                                        try
                                        {
                                            if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                                objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                            }
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                            objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }
                                #endregion

                                #region child
                                objFS.ChdTax = 0;
                                if (searchInputs.Child > 0)
                                {
                                    if (gg.chdfare.Elements("BaseFareAmt").Count() > 0)
                                    {
                                        #region CHDFR
                                        //objFS.ChdfareType = p.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value;
                                        if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                        {
                                            objFS.ChdfareType = "Non Refundable";
                                        }
                                        else
                                        { objFS.ChdfareType = "Refundable"; }

                                        int m = 0;
                                        while (m < (gg.chdRuleInfo.Count()))
                                        {
                                            var chdreader = gg.chdRuleInfo.Skip(m).First().CreateReader();
                                            chdreader.MoveToContent();
                                            objFS.ChdFar = "";// objFS.ChdFar + "<RulesInfo>" + chdreader.ReadInnerXml() + "</RulesInfo>CHDFAR";
                                            m = m + 1;
                                        }

                                        //objFS.ChdCabin = gg.chdSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;

                                        objFS.ChdCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;

                                        try
                                        {
                                            if (gg.chdFarebasis.Count() > l - 1)
                                            {
                                                objFS.ChdFarebasis = gg.chdFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                            }
                                            else
                                            {
                                                objFS.ChdFarebasis = gg.chdFarebasis.Skip(gg.chdFarebasis.Count() - 1).First().Element("FIC").Value;
                                            }
                                            objFS.FareRule = "";// objFS.FareRule + gg.chdRuleInfo.First().Value;
                                        }
                                        catch (Exception ex2)
                                        {
                                            // throw ex2;
                                        }
                                        //var chdcabin = from kk in gg.chdClass.Elements("BICInfo")
                                        //               where kk.Elements("BICAry").Any(f => f.Elements("BICInfo").Any(r => r.Elements("PsgrDescNumAry").Any(x => x.Element("Num").Value == "1")))
                                        //               select new
                                        //               {
                                        //                   cabin = kk.Value,

                                        //               };
                                        #endregion
                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        { objFS.ChdBFare = float.Parse(gg.chdfare.Elements("BaseFareAmt").First().Value); }
                                        else
                                        {
                                            if (gg.chdfare.Elements("BaseFareCurrency").First().Value == "INR")
                                                objFS.ChdBFare = float.Parse(gg.chdfare.Elements("BaseFareAmt").First().Value);
                                            else
                                                objFS.ChdBFare = float.Parse(gg.chdfare.Elements("EquivAmt").First().Value);
                                        }

                                        objFS.ChdFare = float.Parse(gg.chdfare.Elements("TotAmt").First().Value);
                                        var chd = from item in gg.chdfare.Elements("TaxDataAry").Elements("TaxData")
                                                  select new
                                                  {
                                                      chdAmt = item.Element("Amt").Value,
                                                      chdCon = item.Element("Country").Value
                                                  };
                                        //Added by abhilash for ChdQtax
                                        objFS.ChdQ = GetQTax(gg.chdFConstructor.Elements("FareConstructText").Last().Value.ToString().Replace(Utility.Left(depCity, 3), "").Replace(Utility.Left(arrCity, 3), ""));
                                        //End Added by abhilash for ChdQtax
                                        foreach (var chdd in chd)
                                        {
                                            if (chdd.chdCon == "YQ")
                                                objFS.ChdFSur = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "WO")
                                                objFS.ChdWO = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "IN")
                                                objFS.ChdIN = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "K3")
                                                objFS.ChdJN = float.Parse(chdd.chdAmt);
                                            else if (chdd.chdCon == "YM")
                                                objFS.ChdYR = float.Parse(chdd.chdAmt);
                                            else
                                                objFS.ChdOT = objFS.ChdOT + float.Parse(chdd.chdAmt);
                                            //if (chdd.chdCon != "YQ")
                                            objFS.ChdTax = objFS.ChdTax + float.Parse(chdd.chdAmt);

                                        }
                                        #region Get MISC Markup Charges Date 06-03-2018
                                        try
                                        {
                                            srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                        }
                                        catch { srvChargeChd = 0; }
                                        #endregion

                                        //SMS Charge add
                                        objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                        objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                        objFS.ChdFare = objFS.ChdFare + srvChargeChd;// srvCharge;
                                        //SMS charge add end

                                        objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                        objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                        CommDt.Clear();
                                        STTFTDS.Clear();
                                        //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1);
                                        //objFS.ChdDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        //objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                        //objFS.ChdSrvTax = float.Parse(STTFTDS["STax"].ToString());
                                        //objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                        //objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                        //objFS.ChdEduCess = 0;
                                        //objFS.ChdHighEduCess = 0;
                                        // objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        {
                                            #region Show hide Net Fare and Commission Calculation-Child Pax
                                            if (objFS.Leg == 1)
                                            {
                                                //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1);
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, "1",contx,"");
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {
                                                    objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                    objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                    objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                }
                                                else
                                                {
                                                    objFS.ChdDiscount = 0;
                                                    objFS.ChdCB = 0;
                                                    objFS.ChdSrvTax = 0;
                                                    objFS.ChdTF = 0;
                                                    objFS.ChdTds = 0;
                                                }

                                                
                                                //objFS.ChdDiscount = 0;
                                                //objFS.ChdCB = 0;
                                                //objFS.ChdSrvTax = 0;
                                                //objFS.ChdTF = 0;
                                                //objFS.ChdTds = 0;
                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }
                                        }
                                            else
                                            {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                            #endregion

                                            
                                        if (searchInputs.IsCorp == true)
                                        {
                                            if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                                objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                            }
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                                objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch(Exception ex) { }
                                        }
                                        objFS.ChdEduCess = 0;
                                        objFS.ChdHighEduCess = 0;
                                    }
                                    else
                                    {
                                        objFS.ChdBFare = 0;
                                        objFS.ChdFare = 0;
                                        objFS.ChdFSur = 0;
                                        objFS.ChdWO = 0;
                                        objFS.ChdIN = 0;
                                        objFS.ChdJN = 0;
                                        objFS.ChdYR = 0;
                                        objFS.ChdOT = 0;
                                        objFS.ChdTax = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                        objFS.ChdEduCess = 0;
                                        objFS.ChdHighEduCess = 0;

                                    }
                                }
                                else
                                {
                                    objFS.ChdBFare = 0;
                                    objFS.ChdFare = 0;
                                    objFS.ChdFSur = 0;
                                    objFS.ChdWO = 0;
                                    objFS.ChdIN = 0;
                                    objFS.ChdJN = 0;
                                    objFS.ChdYR = 0;
                                    objFS.ChdOT = 0;
                                    objFS.ChdTax = 0;
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    //objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                    objFS.ChdEduCess = 0;
                                    objFS.ChdHighEduCess = 0;

                                }

                                #endregion

                                #region Infant
                                objFS.InfTax = 0;
                                if (searchInputs.Infant > 0)
                                {
                                    if (gg.inffare.Elements("BaseFareAmt").Count() > 0)
                                    {
                                        //objFS.InfFareType = p.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value;
                                        if (gg.adtRsvnRules.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim() == "Y")
                                        {
                                            objFS.InfFareType = "Non Refundable";
                                        }
                                        else
                                        { objFS.InfFareType = "Refundable"; }

                                        int m = 0;
                                        while (m < (gg.infRuleInfo.Count()))
                                        {
                                            var infreader = gg.infRuleInfo.Skip(m).First().CreateReader();
                                            infreader.MoveToContent();
                                            objFS.InfFar = "";// objFS.InfFar + "<RulesInfo>" + infreader.ReadInnerXml() + "</RulesInfo>INFFAR";
                                            m = m + 1;
                                        }

                                        //objFS.InfCabin = gg.infSegRelatedInfo.Skip(l - 1).First().Element("FareCabinClsSeg").Value;

                                        objFS.InfCabin = gg.ItinSegInfo.Skip(l - 1).First().Element("BookingCabinClsSeg").Value;
                                        try
                                        {
                                            if (gg.infFarebasis.Count() > l - 1)
                                            {
                                                objFS.InfFarebasis = gg.infFarebasis.Skip(l - 1).First().Element("FIC").Value;
                                            }
                                            else
                                            {
                                                objFS.InfFarebasis = gg.infFarebasis.Skip(gg.infFarebasis.Count() - 1).First().Element("FIC").Value;
                                            }
                                        }
                                        catch (Exception ex2)
                                        { throw ex2; }

                                        objFS.FareRule = "";// objFS.FareRule + gg.infRuleInfo.First().Value;
                                        //var infcabin = p.adtClass.Elements("BICAry").Elements("BICInfo").Where(x => x.Elements("PsgrDescNumAry").Any(kk => kk.Element("Num").Value == "3"));
                                        //objFS.Inf_cabin = infcabin.Elements("BIC").First().Value;
                                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                                        { objFS.InfBfare = float.Parse(gg.inffare.Elements("BaseFareAmt").First().Value); }
                                        else
                                        {
                                            if (gg.inffare.Elements("BaseFareCurrency").First().Value == "INR")
                                                objFS.InfBfare = float.Parse(gg.inffare.Elements("BaseFareAmt").First().Value);
                                            else
                                                objFS.InfBfare = float.Parse(gg.inffare.Elements("EquivAmt").First().Value);
                                        }

                                        objFS.InfFare = float.Parse(gg.inffare.Elements("TotAmt").First().Value);

                                        var inf = from item in gg.inffare.Elements("TaxDataAry").Elements("TaxData")
                                                  select new
                                                  {
                                                      infAmt = item.Element("Amt").Value,
                                                      infCon = item.Element("Country").Value
                                                  };
                                        foreach (var infd in inf)
                                        {
                                            if (infd.infCon == "YQ")
                                                objFS.InfFSur = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "WO")
                                                objFS.InfWO = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "IN")
                                                objFS.InfIN = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "K3")
                                                objFS.InfJN = float.Parse(infd.infAmt);
                                            else if (infd.infCon == "YM")
                                                objFS.InfYR = float.Parse(infd.infAmt);
                                            else
                                                objFS.InfOT = objFS.InfOT + float.Parse(infd.infAmt);
                                            //if (infd.infCon != "YQ")
                                            objFS.InfTax = objFS.InfTax + float.Parse(infd.infAmt);
                                        }

                                        if (searchInputs.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                                objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }

                                    }
                                    else
                                    {
                                        objFS.InfBfare = 0;
                                        objFS.InfFare = 0;
                                        objFS.InfFSur = 0;
                                        objFS.InfWO = 0;
                                        objFS.InfIN = 0;
                                        objFS.InfJN = 0;
                                        objFS.InfYR = 0;
                                        objFS.InfOT = 0;
                                        objFS.InfTax = 0;
                                        objFS.InfDiscount = 0;
                                        objFS.InfCB = 0;
                                        objFS.InfSrvTax = 0;
                                        objFS.InfTF = 0;
                                        objFS.InfTds = 0;
                                        objFS.InfEduCess = 0;
                                        objFS.InfHighEduCess = 0;
                                    }
                                }
                                else
                                {
                                    objFS.InfBfare = 0;
                                    objFS.InfFare = 0;
                                    objFS.InfFSur = 0;
                                    objFS.InfWO = 0;
                                    objFS.InfIN = 0;
                                    objFS.InfJN = 0;
                                    objFS.InfYR = 0;
                                    objFS.InfOT = 0;
                                    objFS.InfTax = 0;
                                    objFS.InfDiscount = 0;
                                    objFS.InfCB = 0;
                                    objFS.InfSrvTax = 0;
                                    objFS.InfTF = 0;
                                    objFS.InfTds = 0;
                                    objFS.InfEduCess = 0;
                                    objFS.InfHighEduCess = 0;
                                }

                                #endregion

                                objFS.Flight = flightSeq;
                                objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                                objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                                objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                                objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                                objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                                objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                                objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                                objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                                objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                                objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                                objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                    objFS.TotalFare = (objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee) - ((objFS.ADTAdminMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child));
                                else
                                    objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                                objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                                // objFS.OperatingCarrier = p.opreatingcarrier;

                                int hr = int.Parse(p.jrnyTime) / 60;
                                int min = int.Parse(p.jrnyTime) % 60;
                                objFS.TotDur = hr.ToString("00") + ":" + min.ToString("00");

                                int hr1 = int.Parse(p.FltTm) / 60;
                                int min1 = int.Parse(p.FltTm) % 60;
                                objFS.TripCnt = hr1.ToString("00") + ":" + min1.ToString("00");

                                objFS.sno = p.fltNeeded;
                                objFS.Stops = (int.Parse(p.lineNumber.ToString()) + int.Parse(gg.stop.ToString())).ToString() + "-Stop";

                                objFS.TripType = "M";// FlightWay;// TType;

                                objFS.Trip = searchInputs.Trip.ToString();
                                objFS.FType = FType;
                                // objFS.Flight = flightTip;
                                objFS.RBD = objFS.AdtRbd;
                                string seat = "60";
                                try
                                {

                                    seat = ((from item in p.AvailableSeats.Elements("BICStatus")
                                             where item.Element("BIC").Value == objFS.AdtRbd
                                             select item.Element("Status").Value).ToList())[0].ToString();
                                }
                                catch { }
                                if (Utility.IsNumeric(seat))
                                    objFS.AvailableSeats1 = Convert.ToString(int.Parse(seat));
                                else
                                    objFS.AvailableSeats1 = seat;
                                objFS.Searchvalue = gg.PricingCode;
                                srvCharge = srvChargeAdt + srvChargeChd; //06-03-2018  Add MISC CHARGES PAX WISE
                                objFS.OriginalTT = srvCharge;
                                objFS.IsCorp = searchInputs.IsCorp;
                                objFS.Provider = "1G";
                                objFS.SearchId = SearchId;
                                if (!string.IsNullOrEmpty(gg.PricingCode) || CrdType=="CRP")
                                {
                                    objFS.AdtFar = "CRP";
                                }
                                else
                                {
                                    objFS.AdtFar = "NRM";
                                }
                                FlightList.Add(objFS);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            return FlightList[0];
        }


        private List<FlightSearchResults> FQCSTransaction(List<FlightSearchResults> FltRes, FlightSearch searchInputs, string ConnectionString, DataSet MrkupDS, List<FltSrvChargeList> SrvchargeList, string RBD, string VC, List<MISCCharges> MiscList, string CrdType)
        {
            float srvCharge = 0;
            List<CredentialList> CrdList;
            CrdList = Data.GetProviderCrd(ConnectionString);
            string url = "", userid = "", Pwd = "", FQCSReqXml = "", FQCSResXml = "";
            int Ln = 0;
            Ln = FltRes[FltRes.Count - 1].LineNumber;
            url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
            userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
            Pwd = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
            FltRequest objFltReq = new FltRequest(searchInputs, CrdList);
            FlightCommonBAL objFlightCommonBAL = new FlightCommonBAL();

            var f = (from ft in FltRes
                     where ft.AdtRbd == RBD && ft.ValiDatingCarrier == VC
                     select ft).ToList();
            IEnumerable distLineNo = f.Select(x => x.LineNumber).Distinct();

            foreach (var item in distLineNo)
            {
                List<FlightSearchResults> FltDataResult = new List<FlightSearchResults>();
                var ft = (from distitem in FltRes
                          where distitem.LineNumber.ToString() == item.ToString()
                          select distitem).ToList();
                for (int i = 0; i < ft.Count(); i++)
                {
                    FlightSearchResults FltData = (FlightSearchResults)ft[0].Clone();
                    FltDataResult.Add(FltData);
                }
                if (FltDataResult.Count == 2)
                {
                    Ln = Ln + 1;
                    List<FlightSearchResults> FltDataRes;
                    FQCSReqXml = objFltReq.FQCSReq(searchInputs, FltDataResult);
                    FQCSResXml = objFlightCommonBAL.PostXml(url, FQCSReqXml, userid, Pwd, "http://webservices.galileo.com/SubmitXml");
                    FltDataRes = GetFareClassic(FQCSResXml, FltDataResult, searchInputs, Ln, ConnectionString, MrkupDS, SrvchargeList, MiscList, CrdType);
                    foreach (FlightSearchResults objFS in FltDataRes)
                    {
                        var a = from b in FltRes
                                where b.FlightIdentification == objFS.FlightIdentification && b.ValiDatingCarrier == objFS.ValiDatingCarrier
                                where b.AdtRbd == objFS.AdtRbd && b.TotalFare == objFS.TotalFare
                                select b;
                        if (a.Count() == 0)
                        {
                            FltRes.Add(objFS);
                        }
                    }
                }
            }
            return FltRes;
        }
        private List<FlightSearchResults> GetFareClassic(string strB, List<FlightSearchResults> objFS1, FlightSearch searchInputs, int Ln, string ConnStr, DataSet MrkupDS, List<FltSrvChargeList> SrvchargeList, List<MISCCharges> MiscList, string CrdType)
        {
            float srvCharge = 0;

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            ////SMS charge calc
            //float srvCharge = 0;
            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS1[0].ValiDatingCarrier, searchInputs.UID);
            ////SMS charge calc end
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            HttpContext contx = HttpContext.Current;
            List<FlightSearchResults> objFS2 = new List<FlightSearchResults>();
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strB);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            var abc = from item1 in xdo.Descendants("FareInfo").Descendants("GenQuoteDetails")
                      where item1.Element("UniqueKey").Value == "1"
                      select new
                      {
                          amnt = item1.Element("TotAmt").Value,
                          qut = item1.Element("QuoteNum").Value
                      };
            var abcd = from item1 in abc
                       orderby item1.amnt
                       select new
                       {
                           amnt = item1.amnt,
                           qut = item1.qut
                       };
            string strQN = "";
            foreach (var data in abcd)
            {
                strQN = data.qut;
                for (int i = 0; i < objFS1.Count(); i++)
                {
                    FlightSearchResults objFS = (FlightSearchResults)objFS1[i].Clone();
                    #region ADULT
                    if (objFS.Adult > 0)
                    {
                        var varAdt = from item1 in xdo.Descendants("FareInfo").Descendants("GenQuoteDetails")
                                     from item2 in xdo.Descendants("FareInfo").Descendants("RulesInfo")
                                     where item1.Element("UniqueKey").Value == "1"
                                     where item2.Element("UniqueKey").Value == "1"
                                     where item1.Element("QuoteNum").Value == strQN.ToString()
                                     where item2.Element("QuoteNum").Value == strQN.ToString()
                                     select new
                                     {
                                         AdtBfare = item1.Element("BaseFareAmt").Value,
                                         adtAmt = item1.Element("TotAmt").Value,
                                         adtFB = item1.Elements("TaxDataAry").Elements("TaxData"),
                                         adtFIC = item2.Element("FIC").Value

                                     };

                        foreach (var adt in varAdt)
                        {
                            objFS.AdtFarebasis = adt.adtFIC;
                            objFS.AdtFare = float.Parse(adt.adtAmt.ToString());
                            objFS.AdtBfare = float.Parse(adt.AdtBfare.ToString());
                            objFS.AdtFSur = 0; objFS.AdtWO = 0; objFS.AdtIN = 0; objFS.AdtJN = 0; objFS.AdtYR = 0;
                            objFS.AdtOT = 0; objFS.AdtTax = 0;
                            foreach (var adtd in adt.adtFB)
                            {

                                if (adtd.Element("Country").Value == "YQ")
                                    objFS.AdtFSur = float.Parse(adtd.Element("Amt").Value);
                                else if (adtd.Element("Country").Value == "WO")
                                    objFS.AdtWO = float.Parse(adtd.Element("Amt").Value);
                                else if (adtd.Element("Country").Value == "IN")
                                    objFS.AdtIN = float.Parse(adtd.Element("Amt").Value);
                                else if (adtd.Element("Country").Value == "K3")
                                    objFS.AdtJN = float.Parse(adtd.Element("Amt").Value);
                                else if (adtd.Element("Country").Value == "YM")
                                    objFS.AdtYR = float.Parse(adtd.Element("Amt").Value);
                                else
                                    objFS.AdtOT = objFS.AdtOT + float.Parse(adtd.Element("Amt").Value);
                                //if (adtd.adtCon != "YQ")
                                objFS.AdtTax = objFS.AdtTax + float.Parse(adtd.Element("Amt").Value);
                            }
                        }
                    }
                    //calc
                    //SMS charge add 
                    objFS.AdtOT = objFS.AdtOT + srvCharge;
                    objFS.AdtTax = objFS.AdtTax + srvCharge;
                    objFS.AdtFare = objFS.AdtFare + srvCharge;
                    //SMS charge add end
                    //Start Calculation Commission,Markup,CB,ServiceTax,TransactionFee,TDS for adult
                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                    objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                    CommDt.Clear();
                    STTFTDS.Clear();

                    #region Show hide Net Fare and Commission Calculation-Adult Pax
                    if (objFS.Leg == 1)
                    {

                        //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1);
                        CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, "1",contx,"");
                        if (CommDt != null && CommDt.Rows.Count > 0)
                        {
                            objFS.AdtDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                            objFS.AdtSrvTax = float.Parse(STTFTDS["STax"].ToString());
                            objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            //Ens Calculation Commission,Markup,CB,ServiceTax,TransactionFee,TDS for adult
                            //Start Corporate Agent(Adult)
                        }
                        else
                        {
                            objFS.AdtDiscount = 0;
                            objFS.AdtCB = 0;
                            objFS.AdtSrvTax = 0;
                            objFS.AdtTF = 0;
                            objFS.AdtTds = 0;
                            objFS.IATAComm = 0;
                        }
                    }
                    else
                    {

                    }
                    #endregion                    
                    if (searchInputs.IsCorp == true)
                    {
                        try
                        {
                            if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                            {
                                objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                            }
                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                            objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        catch { }
                    }
                    //End Corporate Agent(Adult)
                    //calc end
                    #endregion
                    #region CHILD
                    if (objFS.Child > 0)
                    {
                        var varChd = from item1 in xdo.Descendants("FareInfo").Descendants("GenQuoteDetails")
                                     from item2 in xdo.Descendants("FareInfo").Descendants("RulesInfo")
                                     where item1.Element("UniqueKey").Value == "2"
                                     where item2.Element("UniqueKey").Value == "2"
                                     where item1.Element("QuoteNum").Value == strQN.ToString()
                                     where item2.Element("QuoteNum").Value == strQN.ToString()
                                     select new
                                     {
                                         ChdBfare = item1.Element("BaseFareAmt").Value,
                                         chdAmt = item1.Element("TotAmt").Value,
                                         chdFIC = item2.Element("FIC").Value,
                                         chdFB = item1.Elements("TaxDataAry").Elements("TaxData")

                                     };

                        foreach (var chd in varChd)
                        {
                            objFS.ChdFarebasis = chd.chdFIC;
                            objFS.ChdFare = float.Parse(chd.chdAmt.ToString());
                            objFS.ChdBFare = float.Parse(chd.ChdBfare.ToString());
                            objFS.ChdFSur = 0; objFS.ChdWO = 0; objFS.ChdIN = 0; objFS.ChdJN = 0; objFS.ChdYR = 0;
                            objFS.ChdOT = 0; objFS.ChdTax = 0;
                            foreach (var chdd in chd.chdFB)
                            {
                                if (chdd.Element("Country").Value == "YQ")
                                    objFS.ChdFSur = float.Parse(chdd.Element("Amt").Value);
                                else if (chdd.Element("Country").Value == "WO")
                                    objFS.ChdWO = float.Parse(chdd.Element("Amt").Value);
                                else if (chdd.Element("Country").Value == "IN")
                                    objFS.ChdIN = float.Parse(chdd.Element("Amt").Value);
                                else if (chdd.Element("Country").Value == "K3")
                                    objFS.ChdJN = float.Parse(chdd.Element("Amt").Value);
                                else if (chdd.Element("Country").Value == "YM")
                                    objFS.ChdYR = float.Parse(chdd.Element("Amt").Value);
                                else
                                    objFS.ChdOT = objFS.ChdOT + float.Parse(chdd.Element("Amt").Value);
                                //if (chdd.chdCon != "YQ")
                                objFS.ChdTax = objFS.ChdTax + float.Parse(chdd.Element("Amt").Value);

                            }
                        }
                    }

                    //SMS Charge add
                    objFS.ChdOT = objFS.ChdOT + srvCharge;
                    objFS.ChdTax = objFS.ChdTax + srvCharge;
                    objFS.ChdFare = objFS.ChdFare + srvCharge;
                    //SMS charge add end
                    //Start Calculation Commission,Markup,CB,ServiceTax,TransactionFee,TDS for child
                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                    objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                    CommDt.Clear();
                    STTFTDS.Clear();
                    #region Show hide Net Fare and Commission Calculation-Child Pax
                    if (objFS.Leg == 1)
                    {
                        //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1);
                        CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, "1",contx,"");
                        if (CommDt != null && CommDt.Rows.Count > 0)
                        {                            
                            objFS.ChdDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                            objFS.ChdSrvTax = float.Parse(STTFTDS["STax"].ToString());
                            objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                            objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                            //End Calculation Commission,Markup,CB,ServiceTax,TransactionFee,TDS for child
                            //For Corporate Agents(Child)
                        }
                        else
                        {
                            objFS.ChdDiscount = 0;
                            objFS.ChdCB = 0;
                            objFS.ChdSrvTax = 0;
                            objFS.ChdTF = 0;
                            objFS.ChdTds = 0;
                        }                        
                    }
                    else
                    {
                        objFS.ChdDiscount = 0;
                        objFS.ChdCB = 0;
                        objFS.ChdSrvTax = 0;
                        objFS.ChdTF = 0;
                        objFS.ChdTds = 0;
                    }
                    #endregion

                    if (searchInputs.IsCorp == true)
                    {
                        if (searchInputs.Trip.ToString() == JourneyType.I.ToString())
                        {
                            objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                            objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                        }
                        try
                        {
                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                            objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        catch { }
                    }

                    //End Corporate Agent(Child)
                    #endregion
                    #region INFANT
                    if (objFS.Infant > 0)
                    {
                        string strVal = "";
                        if (objFS.Child > 0)
                            strVal = "3";
                        else
                            strVal = "2";

                        var varINF = from item1 in xdo.Descendants("FareInfo").Descendants("GenQuoteDetails")
                                     from item2 in xdo.Descendants("FareInfo").Descendants("RulesInfo")
                                     where item1.Element("UniqueKey").Value == strVal
                                     where item2.Element("UniqueKey").Value == strVal
                                     where item1.Element("QuoteNum").Value == strQN.ToString()
                                     where item2.Element("QuoteNum").Value == strQN.ToString()
                                     select new
                                     {
                                         infBfare = item1.Element("BaseFareAmt").Value,
                                         infAmt = item1.Element("TotAmt").Value,
                                         infFIC = item2.Element("FIC").Value,
                                         infFB = item1.Elements("TaxDataAry").Elements("TaxData")
                                     };

                        foreach (var inf in varINF)
                        {
                            objFS.InfFare = float.Parse(inf.infAmt.ToString());
                            objFS.InfFarebasis = inf.infFIC;
                            objFS.InfBfare = float.Parse(inf.infBfare.ToString());
                            objFS.InfFSur = 0; objFS.InfWO = 0; objFS.InfIN = 0; objFS.InfJN = 0; objFS.InfYR = 0;
                            objFS.InfOT = 0; objFS.InfTax = 0;
                            foreach (var infd in inf.infFB)
                            {
                                if (infd.Element("Country").Value == "YQ")
                                    objFS.InfFSur = float.Parse(infd.Element("Amt").Value);
                                else if (infd.Element("Country").Value == "WO")
                                    objFS.InfWO = float.Parse(infd.Element("Amt").Value);
                                else if (infd.Element("Country").Value == "IN")
                                    objFS.InfIN = float.Parse(infd.Element("Amt").Value);
                                else if (infd.Element("Country").Value == "K3")
                                    objFS.InfJN = float.Parse(infd.Element("Amt").Value);
                                else if (infd.Element("Country").Value == "YM")
                                    objFS.InfYR = float.Parse(infd.Element("Amt").Value);
                                else
                                    objFS.InfOT = objFS.InfOT + float.Parse(infd.Element("Amt").Value);
                                //if (infd.infCon != "YQ")
                                objFS.InfTax = objFS.InfTax + float.Parse(infd.Element("Amt").Value);
                            }
                        }
                    }
                    //End Corporate Agent(Infant)
                    if (searchInputs.IsCorp == true)
                    {
                        try
                        {
                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                            objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        catch { }
                    }
                    //End Corporate Agent(Infant)
                    #endregion

                    objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                    objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                    objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                    objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                    objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                    objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                    objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                    objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                    objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                    objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                    objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                    if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                        objFS.TotalFare = (objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee) - ((objFS.ADTAdminMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child));
                    else
                        objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                    objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                    objFS.LineNumber = Ln;
                    objFS.Searchvalue = "PROMOFARE";
                    objFS2.Add(objFS);
                }

                break;
            }


            return objFS2;
        }

        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }
        private string getAirlineName(string airl, List<AirlineList> AirList)
        {
            string airl1 = "";
            try
            {
                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");


                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }
                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        //private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        //{
        //    DataRow[] airMrkArray;
        //    double mrkamt = 0;
        //    try
        //    {
        //        airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
        //        if (airMrkArray.Length > 0)
        //        {
        //            if (Trip == "I")
        //            {
        //                if (((string)airMrkArray[0]["MarkupType"]) == "P")
        //                {
        //                    mrkamt = Math.Round((fare * ((double)airMrkArray[0]["MarkupType"])) / 100, 0);
        //                }
        //                else if (((string)airMrkArray[0]["MarkupType"]) == "F")
        //                {
        //                    mrkamt = ((double)airMrkArray[0]["MarkupType"]);
        //                }
        //            }
        //            else
        //            {
        //                mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
        //            }
        //        }
        //        else
        //        {
        //            mrkamt = 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mrkamt = 0;
        //    }
        //    return float.Parse(mrkamt.ToString());
        //}

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
           // int IATAComm = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                try
                {
                    List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                    if (StNew.Count > 0)
                    {
                        STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                        TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                        IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                    }
                }
                catch
                { }
                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float GetQTax(string fareConstruct)
        {
            string[] adtQtax;
            float Qt = 0, Roe = 0;
            float QTax = 0;
            try
            {
                adtQtax = Utility.Split(fareConstruct, " ");
                //foreach (var t in adtQtax)
                for (int q = 0; q <= adtQtax.Length - 1; q++)
                {
                    if (adtQtax[q].ToString().StartsWith("Q"))
                    {
                        if (Utility.IsNumeric(adtQtax[q].ToString().Replace("Q", "")))
                        {
                            Qt += float.Parse((adtQtax[q].ToString().Replace("Q", "")));
                        }
                        else
                        {
                            if (adtQtax[q].ToString().Contains("Q"))
                            {
                                if (adtQtax[q].ToString().Length == 1)
                                {
                                    MatchCollection matches = Regex.Matches(adtQtax[q + 1].ToString(), @"[+-]?\d+(\.\d+)?");
                                    if (matches.Count > 0)
                                    {
                                        if (Utility.IsNumeric(matches[0].Value))
                                        {
                                            Qt += float.Parse(matches[0].Value);
                                            //q += 1;
                                        }
                                    }
                                }
                                else
                                {
                                    string[] q1 = Utility.Split(adtQtax[q], "Q");
                                    int i;
                                    for (i = 0; i <= q1.Length - 1; i++)
                                    {
                                        if (Utility.IsNumeric(q1[i].Replace("Q", "")))
                                        {
                                            Qt += float.Parse((q1[i].Replace("Q", "")));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (adtQtax[q].ToString().EndsWith("Q"))
                    {
                        MatchCollection matches = Regex.Matches(adtQtax[q + 1].ToString(), @"[+-]?\d+(\.\d+)?");
                        if (matches.Count > 0)
                        {
                            if (Utility.IsNumeric(matches[0].Value))
                            {
                                Qt += float.Parse(matches[0].Value);
                                //q += 1;
                            }
                        }
                    }
                    else if (adtQtax[q].ToString().StartsWith("ROE"))
                    {
                        if (Utility.IsNumeric(adtQtax[q].ToString().Replace("ROE", "")))
                        {
                            Roe = float.Parse((adtQtax[q].ToString().Replace("ROE", "")));
                        }
                    }
                }
                QTax = Qt * Roe;

            }
            catch (Exception ex)
            {
                QTax = 0;
            }
            return float.Parse(Math.Round(Convert.ToDouble(QTax.ToString()), 0).ToString());
        }

        private string chkFareRule(IEnumerable<XElement> fr)
        {
            string TkNonRef = "", PenfeeC = "", PenfeeF = "", PenfeeI = "", FRtype = "", amt = "", type = "";
            try
            {
                TkNonRef = fr.Elements("PenFeeAry").First().Element("PenFee").Element("TkNonRef").Value.ToString().Trim();
                PenfeeC = fr.Elements("PenFeeAry").First().Element("PenFee").Element("Cancellation").Value.ToString().Trim();
                PenfeeF = fr.Elements("PenFeeAry").First().Element("PenFee").Element("FailConfirmSpace").Value.ToString().Trim();
                PenfeeI = fr.Elements("PenFeeAry").First().Element("PenFee").Element("ItinChg").Value.ToString().Trim();
                amt = fr.Elements("PenFeeAry").First().Element("PenFee").Element("Amt").Value.ToString().Trim();
                type = fr.Elements("PenFeeAry").First().Element("PenFee").Element("Type").Value.ToString().Trim();

                if (TkNonRef == "N")
                    FRtype = "Refundable";
                else if (TkNonRef == "Y" && PenfeeC == "Y" && PenfeeF == "Y")
                    FRtype = "Refundable";
                else if (TkNonRef == "Y" && PenfeeC == "Y" && PenfeeF == "N")
                {
                    if (amt == "100" && type == "P")
                        FRtype = "Non Refundable";
                    else if (type == "D")
                        FRtype = "Refundable";
                    else
                        FRtype = "Non Refundable";
                }
                else if (TkNonRef == "Y" && PenfeeC == "N" && PenfeeI == "Y")
                    FRtype = "Refundable";
                else
                    FRtype = "Non Refundable";
            }
            catch (Exception ex)
            { FRtype = ""; }
            return FRtype;

        }

    }
}
