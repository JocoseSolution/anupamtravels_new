﻿using GALWS.IWEENAirAsia;
using Newtonsoft.Json;
using STD.DAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace STD.BAL
{
    public class FareCrossCheckForAllProvider
    {
        SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        public string ConStr { get; set; }
        public FareCrossCheckForAllProvider()
        { }
        public FareCrossCheckForAllProvider(string connectionString)
        {
            ConStr = connectionString;
        }


        public ArrayList GetFltResult(string userId, string userType, DataSet dsO, DataSet dsR, bool isroundTripNRML, out CacheRereshRespList listFareChange, ArrayList ListData, string UID, string DistrID, string agentType, string TDS, bool isInboundWithLcc)
        {
            ArrayList resultList = new ArrayList();
            listFareChange = new CacheRereshRespList();
            STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL(ConStr);
            List<STD.Shared.UserFlightSearch> FCSHARED = new List<STD.Shared.UserFlightSearch>();
            FCSHARED = FCBAL.GetUserFlightSearch(UID, userType);
            TDS = STD.BAL.Data.Calc_TDS(ConStr, FCSHARED[0].UserId);

            List<List<FlightSearchResults>> list = ConvertObjectToFlightResultList(ListData);
            if (isroundTripNRML)
            {
                if(isInboundWithLcc)
                {
                    ArrayList rListR = new ArrayList();
                    CacheRereshRespList listFareChangeR = new CacheRereshRespList();
                    rListR = GetFltResultOneWay(userId, userType, dsR, isroundTripNRML, out listFareChangeR, list[1], UID, DistrID, agentType, TDS);                     
                    listFareChange.ChangeFareR = listFareChangeR.ChangeFareO;
                }
                else
                {
                    ArrayList rListO = new ArrayList();
                    ArrayList rListR = new ArrayList();
                    CacheRereshRespList listFareChangeO = new CacheRereshRespList();
                    CacheRereshRespList listFareChangeR = new CacheRereshRespList();
                    rListO = GetFltResultOneWay(userId, userType, dsO, isroundTripNRML, out listFareChangeO, list[0], UID, DistrID, agentType, TDS);

                    rListR = GetFltResultOneWay(userId, userType, dsR, isroundTripNRML, out listFareChangeR, list[1], UID, DistrID, agentType, TDS);
                    resultList.Add(rListO.Cast<object>().ToArray());
                    resultList.Add(rListR.Cast<object>().ToArray());
                    listFareChange.ChangeFareO = listFareChangeO.ChangeFareO;
                    listFareChange.ChangeFareR = listFareChangeR.ChangeFareO;
                }
                
            }
            else
            {
                resultList.Add(GetFltResultOneWay(userId, userType, dsO, isroundTripNRML, out listFareChange, list[0], UID, DistrID, agentType, TDS).Cast<object>().ToArray());

            }


            return resultList;

        }


        public List<List<FlightSearchResults>> ConvertObjectToFlightResultList(ArrayList ListData)
        {
            List<List<FlightSearchResults>> result = new List<List<FlightSearchResults>>();

            try
            {
                for (int i = 0; i < ListData.Count; i++)
                {

                    List<FlightSearchResults> OB = new List<FlightSearchResults>();
                    Object[] arr = (Object[])ListData[i];
                    for (int j = 0; j < arr.Count(); j++)
                    {

                        FlightSearchResults fsr = new FlightSearchResults();

                        IDictionary<string, object> d = (IDictionary<string, object>)arr[j];

                        System.Reflection.PropertyInfo[] oProps = null;
                        oProps = ((Type)fsr.GetType()).GetProperties();


                        for (int k = 0; k < oProps.Count(); k++)
                        {
                            try
                            {
                                object a = d[oProps[k].Name];

                                if (oProps[k].PropertyType.FullName == "System.Single")
                                {
                                    oProps[k].SetValue(fsr, float.Parse(d[oProps[k].Name].ToString()), null);

                                }
                                else if (oProps[k].PropertyType.FullName == "System.Int32")
                                {
                                    oProps[k].SetValue(fsr, Convert.ToInt32(d[oProps[k].Name].ToString()), null);

                                }
                                else if (oProps[k].PropertyType.FullName == "System.DateTime")
                                {
                                    oProps[k].SetValue(fsr, Convert.ToString(d[oProps[k].Name]), null);

                                }
                                else if (oProps[k].Name == "depdatelcc" || oProps[k].Name == "arrdatelcc")
                                {
                                    oProps[k].SetValue(fsr, Convert.ToDateTime(d[oProps[k].Name]).ToString("yyyy-MM-ddTHH:mm:ss"), null);
                                }
                                else
                                {
                                    oProps[k].SetValue(fsr, d[oProps[k].Name], null);
                                }



                            }
                            catch (Exception ex1)
                            {

                            }
                        }

                        OB.Add(fsr);


                    }
                    result.Add(OB);

                }
            }
            catch (Exception ex)
            {


            }


            return result;

        }

        public void convertType(object val, string prtyType)
        {


        }

        public ArrayList GetFltResultOneWay(string userId, string userType, DataSet dsO, bool isroundTripNRML, out CacheRereshRespList listFareChange, List<FlightSearchResults> listData, string UID, string DistrID, string agentType, string TDS)
        {
            List<FlightSearchResults> list = listData;
            ArrayList resultList = new ArrayList();
            ArrayList resultListFinal = new ArrayList();
            listFareChange = new CacheRereshRespList();
            CacheRereshResp FareChangeO = new CacheRereshResp();
            FareChangeO.CacheNetFare = float.Parse(dsO.Tables[0].Rows[0]["NetFare"].ToString());
            FareChangeO.CacheTotFare = float.Parse(dsO.Tables[0].Rows[0]["TotFare"].ToString());
            HttpContext contx = HttpContext.Current;
            FlightCommonBAL ObjcBAL = new FlightCommonBAL(ConStr);
            string exep = "";
            string OrderId = Convert.ToString(dsO.Tables[0].Rows[0]["Track_Id"]);


            // code here
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConStr);
            List<FltSrvChargeList> SrvChargeList = Data.GetSrvChargeInfo(dsO.Tables[0].Rows[0]["Trip"].ToString(), ConStr);

            DataTable dtAgentMarkup = new DataTable();
            DataTable dtAdminMarkup = new DataTable();
            //Calculation For AgentMarkUp
            DataSet MarkupDs = new DataSet();
            dtAgentMarkup = Data.GetMarkup(ConStr, UID, DistrID, dsO.Tables[0].Rows[0]["Trip"].ToString(), "TA");
            dtAgentMarkup.TableName = "AgentMarkUp";
            MarkupDs.Tables.Add(dtAgentMarkup);
            //Calculation For AdminMarkUp'
            dtAdminMarkup = Data.GetMarkup(ConStr, UID, DistrID, dsO.Tables[0].Rows[0]["Trip"].ToString(), "AD");
            dtAdminMarkup.TableName = "AdminMarkUp";
            MarkupDs.Tables.Add(dtAdminMarkup);
            float srvCharge = 0;
            List<MISCCharges> MiscList = new List<MISCCharges>();
            try
            {
                MiscList = objFltComm.GetMiscCharges(dsO.Tables[0].Rows[0]["Trip"].ToString(), "ALL", UID, agentType, dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString(), dsO.Tables[0].Rows[0]["OrgDestTo"].ToString());
            }
            catch (Exception ex2)
            {

                // File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", ex2.StackTrace.ToString() + ex2.Message + Environment.NewLine);
            }
            srvCharge = 0; //objFltComm.MISCServiceFee(MiscList, dsO.Tables[0].Rows[0]["ValiDatingCarrier"].ToString());
            string VC = dsO.Tables[0].Rows[0]["ValiDatingCarrier"].ToString();
            if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "TB")
            {
                #region TBO

                DataSet dsCrd = ObjcBAL.GetSearchCredentilas("TB");
                string[] snoArr = Convert.ToString(dsO.Tables[0].Rows[0]["sno"]).Split(':');

                STD.BAL.TBO.TBOBook objBook = new STD.BAL.TBO.TBOBook();

                Dictionary<string, string> log = new Dictionary<string, string>();


                STD.BAL.TBO.FareQuote.FareQuoteResp fResp = new TBO.FareQuote.FareQuoteResp();


                string FLT_STAT = "";
                if (dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["TripType"].ToString().Trim() == "R")
                {
                    FLT_STAT = "RTF";

                }


                if (FLT_STAT == "RTF" && Convert.ToString(dsO.Tables[0].Rows[0]["Trip"]).Trim().ToUpper() == "D")
                {
                    string[] snoArrR = Convert.ToString(dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["sno"]).Split(':');
                    Dictionary<string, string> log1 = new Dictionary<string, string>();
                    fResp = objBook.GetFareQuote(dsCrd, snoArrR[1], snoArr[0] + "," + snoArrR[0], ref log1, ref exep);
                }
                else
                {

                    fResp = objBook.GetFareQuote(dsCrd, snoArr[1], snoArr[0], ref log, ref exep);
                }


                decimal totfare = Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"]);

                if (totfare > 0 && Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare) > 0 && totfare != Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare))
                {


                    foreach (FlightSearchResults fsr in list)
                    {

                        #region Fare
                        float ChargeBU = 0;

                        foreach (var objChargBU in fResp.Response.Results.Fare.ChargeBU)
                        {
                            ChargeBU += float.Parse(objChargBU.value.ToString());

                        }
                        DataTable CommDt = new DataTable();
                        Hashtable STTFTDS = new Hashtable();
                        //FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
                        foreach (var paxFare in fResp.Response.Results.FareBreakdown)
                        {

                            //fsr.AdtAvlStatus = fareInfo.SeatsAvailable.Trim();

                            if (paxFare.PassengerType == 1)
                            {
                                fsr.AdtBfare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                fsr.AdtCabin = "";
                                fsr.AdtFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                fsr.AdtTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) + float.Parse(paxFare.AdditionalTxnFeePub.ToString()) + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                // fsr.AdtRbd = seg.Airline.FareClass.ToString();
                                fsr.AdtFarebasis = fResp.Response.Results.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                fsr.AdtFareType = fResp.Response.Results.IsRefundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //+ srvCharge
                                // fsr.sno = fResp.Response.Results.ResultIndex + ":" + fResp.Response.TraceId + ":" + fResp.Response.Results.IsLCC + ":" + fResp.Response.Results.Source;//fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                fsr.fareBasis = fsr.AdtFarebasis;
                                fsr.FBPaxType = "ADT";


                                fsr.AdtFar = string.IsNullOrEmpty(fResp.Response.Results.AirlineRemark) ? "" : fResp.Response.Results.AirlineRemark.ToLower();

                                if (!string.IsNullOrEmpty(fsr.AdtFar) && fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                {
                                    fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, fsr.Trip.ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, fsr.Trip.ToString());

                                    fsr.AdtDiscount1 = 0;  //-AdtComm  
                                    fsr.AdtCB = 0;
                                    fsr.AdtSrvTax1 = 0;// added TO TABLE
                                    fsr.AdtDiscount = 0;
                                    fsr.AdtEduCess = 0;
                                    fsr.AdtHighEduCess = 0;
                                    fsr.AdtTF = 0;
                                    fsr.AdtTds = 0;
                                }
                                else
                                {
                                    fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());

                                    CommDt.Clear();
                                    CommDt = objFltComm.GetFltComm_Gal(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"]).Trim(), fsr.AdtFarebasis, "", "", fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS.Clear();
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtEduCess = 0;
                                    fsr.AdtHighEduCess = 0;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                }








                            }

                            if (paxFare.PassengerType == 2)
                            {
                                fsr.ChdBFare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                fsr.ChdCabin = "";
                                fsr.ChdFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                fsr.ChdTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) + float.Parse(paxFare.AdditionalTxnFeePub.ToString()) + srvCharge);
                                fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                // fsr.ChdRbd = seg.Airline.FareClass.ToString();
                                fsr.ChdFarebasis = fResp.Response.Results.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); // + srvCharge

                                //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());



                                if (!string.IsNullOrEmpty(fsr.AdtFar) && fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                {
                                    fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, fsr.Trip.ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, fsr.Trip.ToString());
                                    fsr.ChdDiscount1 = 0;  //-AdtComm  
                                    fsr.ChdCB = 0;
                                    fsr.ChdSrvTax1 = 0;// added TO TABLE
                                    fsr.ChdDiscount = 0;
                                    fsr.ChdEduCess = 0;
                                    fsr.ChdHighEduCess = 0;
                                    fsr.ChdTF = 0;
                                    fsr.ChdTds = 0;
                                }
                                else
                                {
                                    fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                    CommDt.Clear();
                                    CommDt = objFltComm.GetFltComm_Gal(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"]).Trim(), fsr.ChdFarebasis, "", "", fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS.Clear();
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                    fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    fsr.ChdEduCess = 0;
                                    fsr.ChdHighEduCess = 0;
                                }


                            }


                            if (paxFare.PassengerType == 3)
                            {
                                fsr.InfBfare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                fsr.InfCabin = "";
                                fsr.InfFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                fsr.InfTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) + float.Parse(paxFare.AdditionalTxnFeePub.ToString()));
                                fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                //fsr.InfRbd = seg.Airline.FareClass.ToString();
                                fsr.InfFarebasis = fResp.Response.Results.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));




                            }





                        }


                        fsr.Searchvalue = JsonConvert.SerializeObject(fResp.Response.Results.Fare) + "itzDanata420" + JsonConvert.SerializeObject(fResp.Response.Results.FareBreakdown);
                        fsr.OriginalTF = float.Parse(fResp.Response.Results.Fare.OfferedFare.ToString());
                        fsr.OriginalTT = srvCharge;
                        fsr.Provider = "TB";

                        // fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                        ////fsr.TotalFare =(float)Math.Ceiling( float.Parse(flt.Fare.PublishedFare.ToString()));
                        ////fsr.TotalFuelSur = (float)Math.Ceiling(float.Parse(flt.Fare.YQTax.ToString()));//(fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                        ////fsr.TotalTax =(float)Math.Ceiling( float.Parse(flt.Fare.Tax.ToString()) + float.Parse(flt.Fare.OtherCharges.ToString()) + float.Parse(flt.Fare.AdditionalTxnFeePub.ToString()));// (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                        ////fsr.OriginalTT = fsr.TotalTax;
                        ////fsr.TotBfare =(float)Math.Ceiling( float.Parse(flt.Fare.BaseFare.ToString()));// (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);


                        fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                        fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                        fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                        fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                        fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                        fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);



                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                        fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));









                        #endregion
                    }


                }


                FareChangeO.NewNetFare = list[0].NetFare;
                FareChangeO.NewTotFare = list[0].TotalFare;
                listFareChange.ChangeFareO = FareChangeO;

                try
                {
                    for (int ii = 0; ii < list.Count; ii++)
                    {
                        PropertyInfo[] infos = list[ii].GetType().GetProperties();

                        Dictionary<string, object> dix = new Dictionary<string, object>();

                        foreach (PropertyInfo info in infos)
                        {
                            try
                            {
                                if (info.Name.Trim() == "ProductDetailQualifier")
                                {
                                    dix.Add(info.Name, "CACHE_REFRESH");
                                }
                                else
                                {
                                    dix.Add(info.Name, info.GetValue(list[ii], null));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        resultList.Add(dix);
                    }
                }
                catch (Exception ex)
                {


                }

                #endregion

            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "YA")
            {
                #region Yatra
                Dictionary<string, string> Log = new Dictionary<string, string>();
                // string exep = "";

                int adultCount = int.Parse(dsO.Tables[0].Rows[0]["Adult"].ToString().Trim());
                int childCount = int.Parse(dsO.Tables[0].Rows[0]["Child"].ToString().Trim());
                int infantCount = int.Parse(dsO.Tables[0].Rows[0]["Infant"].ToString().Trim());

                DataRow[] FltO = dsO.Tables[0].Select("TripType='O'", "counter ASC");
                DataRow[] FltR = dsO.Tables[0].Select("TripType='R'", "counter ASC");
                string ODR = "";
                if (Convert.ToString(dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["TripType"]).Trim().ToUpper() == "R" && Convert.ToString(dsO.Tables[0].Rows[0]["Trip"]).ToUpper().Trim() == "D")
                {
                    ODR = FltR[0]["Searchvalue"].ToString();
                }

                YAAirPrice objAirPrice = new YAAirPrice();
                string airPriceResp = objAirPrice.AirPrice(FltO[0]["Searchvalue"].ToString(), ODR, adultCount, childCount, infantCount, STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, ref Log, ref exep, FltO[0]["Track_id"].ToString(), ConStr, Convert.ToString(dsO.Tables[0].Rows[0]["Trip"]).ToUpper().Trim());

                decimal TotPrice = Math.Floor(objAirPrice.ParseAirPrice(airPriceResp));

                decimal totfare = Math.Floor(Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"]));

                if (totfare > 0 && Convert.ToDecimal(TotPrice) > 0 && totfare != Convert.ToDecimal(TotPrice))
                {
                    List<FlightSearchResults> newList = GetGoAirResultListWithMarkup(ParseAirPrice(airPriceResp, dsO), SrvChargeList, MarkupDs, srvCharge, ConStr, UID, DistrID, agentType, TDS);

                    FareChangeO.NewNetFare = newList[0].NetFare;
                    FareChangeO.NewTotFare = newList[0].TotalFare;
                    listFareChange.ChangeFareO = FareChangeO;

                    try
                    {
                        for (int ii = 0; ii < newList.Count; ii++)
                        {
                            PropertyInfo[] infos = newList[ii].GetType().GetProperties();

                            Dictionary<string, object> dix = new Dictionary<string, object>();

                            foreach (PropertyInfo info in infos)
                            {
                                try
                                {
                                    if (info.Name.Trim() == "ProductDetailQualifier")
                                    {
                                        dix.Add(info.Name, "CACHE_REFRESH");
                                    }
                                    else
                                    {
                                        dix.Add(info.Name, info.GetValue(newList[ii], null));
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            resultList.Add(dix);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                    listFareChange.ChangeFareO = FareChangeO;
                }
                #endregion
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "1GG")
            {
                #region 1GNEW
                string sResponse = "";
                //  float srvCharge = 0;
                float srvChargeAdt = 0;
                float srvChargeChd = 0;
                GALTransanctions obj = new GALTransanctions();
                obj.connectionString = ConStr;


                bool Status = obj.FairAvailablity1G(dsO, ref sResponse);
                if (Status == false)
                {
                    XDocument xmlDoc = XDocument.Parse(sResponse.Replace("xmlns=\"\"", string.Empty));
                    XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

                    XElement FareInfoss = xmlDoc.Element(soapenv + "Envelope").Element(soapenv + "Body").Element("SubmitXmlResponse").Element("SubmitXmlResult").Element("FareQuoteFlightSpecific_23").Element("FareInfo");
                    IEnumerable<XElement> Rulelist = FareInfoss.Elements("RulesInfo");
                    IEnumerable<XElement> FareInfoLIst = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("GenQuoteDetails")
                                                         select item;

                    //var rrList = xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("RulesInfo").ToArray();

                    //IEnumerable<XElement> Rulelist = xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("RulesInfo");
                    //var Ruleinfo = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Elements("FareInfo").Elements("RulesInfo")
                    //                  select new
                    //                  {
                    //                      UniqueKey = int.Parse(item.Element("UniqueKey").Value),
                    //                      FIC = item.Element("FIC").Value,
                    //                  };
                    //IEnumerable<XElement> RuleList = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("RulesInfo")
                    //                                      select item;

                    IEnumerable<XElement> PsgrTypeslist = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("PsgrTypes")
                                                          select item;

                    var FLIGHTSEGMENT = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Elements("FareInfo").Elements("ItinSeg")
                                        select new
                                        {
                                            UniqueKey = item.Element("UniqueKey").Value,
                                            StartPt = item.Element("StartPt").Value,
                                            EndPt = item.Element("EndPt").Value,
                                            AirV = item.Element("AirV").Value,
                                            FltNum = item.Element("FltNum").Value,
                                            BIC = item.Element("BIC").Value,
                                            adtFarebasis = Rulelist.Where(x => x.Element("UniqueKey").Value == "1").ElementAt(0).Element("FIC").Value,
                                           // chdFarebasis =  Rulelist.Where(x => x.Element("UniqueKey").Value == "2").ElementAt(0).Element("FIC").Value
                                            //infFarebasis = list[0].Child > 0 ? Rulelist.Where(x => x.UniqueKey == 2).ToList()[0].FIC : Rulelist.Where(x => x.UniqueKey == 3).ToList()[0].FIC              
                                        };


                    int TotalViaSector = 1;
                    TotalViaSector = list.Distinct().ToList().Count();

                    foreach (FlightSearchResults fsr in list)
                    {
                        // float ChargeBU = 0;
                        //foreach (var objChargBU in fResp.Response.Results.Fare.ChargeBU)
                        // {
                        //ChargeBU += float.Parse(objChargBU.value.ToString());
                        //  }
                        DataTable CommDt = new DataTable();
                        Hashtable STTFTDS = new Hashtable();
                        //FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
                        fsr.AdtFarebasis = FLIGHTSEGMENT.Where(x => x.FltNum == fsr.FlightIdentification).ToList()[0].adtFarebasis;
                        fsr.AdtRbd = FLIGHTSEGMENT.Where(x => x.FltNum == fsr.FlightIdentification).ToList()[0].BIC;
                        fsr.RBD = fsr.AdtRbd;
                        
                        for (int i = 0; i < FareInfoLIst.Count(); i++)
                        {
                            XElement FareInfo = FareInfoLIst.ElementAt(i);// FareInfoLIst.ElementAt(i).Element("GenQuoteDetails");

                            XElement PsgrTypes = PsgrTypeslist.ElementAt(i).Element("PICReq");
                            XElement UniqueKey = PsgrTypeslist.ElementAt(i).Element("UniqueKey");

                            //IEnumerable<XElement> pfare = FareInfoLIst;
                            if (PsgrTypes.Value.ToString().Trim().ToUpper() == "ADT")
                            {
                                foreach (var pxfr in FareInfoLIst.Where(X => int.Parse(X.Element("UniqueKey").Value) == int.Parse(UniqueKey.Value.ToString())))
                                {
                                    if (pxfr.Elements("BaseFareCurrency").First().Value == "INR")
                                        fsr.AdtBfare = float.Parse(pxfr.Elements("BaseFareAmt").First().Value);
                                    else
                                        fsr.AdtBfare = float.Parse(pxfr.Elements("EquivAmt").First().Value);

                                    fsr.AdtFSur = 0;
                                    fsr.AdtYR = 0;
                                    fsr.AdtOT = 0;
                                    fsr.AdtTax = 0;
                                    fsr.AdtWO = 0;
                                    fsr.FBPaxType = "ADT";
                                    fsr.AdtIN = 0;
                                    fsr.AdtJN = 0;
                                    var adt = from item in pxfr.Elements("TaxDataAry").Elements("TaxData")
                                              select new
                                              {
                                                  adtAmt = item.Element("Amt").Value,
                                                  adtCon = item.Element("Country").Value
                                              };

                                    foreach (var adtd in adt)
                                    {
                                        if (adtd.adtCon == "YQ")
                                            fsr.AdtFSur = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "WO")
                                            fsr.AdtWO = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "IN")
                                            fsr.AdtIN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "K3")
                                            fsr.AdtJN = float.Parse(adtd.adtAmt);
                                        else if (adtd.adtCon == "YM")
                                            fsr.AdtYR = float.Parse(adtd.adtAmt);
                                        else
                                            fsr.AdtOT = fsr.AdtOT + float.Parse(adtd.adtAmt);


                                        fsr.AdtTax = fsr.AdtTax + float.Parse(adtd.adtAmt);

                                    }

                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, fsr.AdtFar, Convert.ToString(fsr.AdtBfare), Convert.ToString(fsr.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeAdt = 0; }
                                    #endregion

                                    //SMS charge add 
                                    fsr.AdtOT = fsr.AdtOT + srvChargeAdt;
                                    fsr.AdtTax = fsr.AdtTax + srvChargeAdt;
                                    fsr.AdtFare = fsr.AdtFare + srvChargeAdt;
                                    fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString()));



                                    fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                                   

                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fsr.DepartureDate), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fsr.DepartureDate), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, fsr.FType, Convert.ToString(TotalViaSector), contx, "");
                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                    {
                                        // CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                        fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                        fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS.Clear();
                                        STTFTDS = CalcSrvTaxTFeeTds1G(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, TDS);
                                        fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                        fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                        fsr.AdtEduCess = 0;
                                        fsr.AdtHighEduCess = 0;
                                        fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                        fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    }
                                    else
                                    {
                                        fsr.AdtDiscount1 = 0;
                                        fsr.AdtCB = 0;
                                        fsr.AdtSrvTax1 = 0;
                                        fsr.AdtDiscount = 0;
                                        fsr.AdtEduCess = 0;
                                        fsr.AdtHighEduCess = 0;
                                        fsr.AdtTF = 0;
                                        fsr.AdtTds = 0;
                                    }

                                    if (fsr.IsCorp == true)
                                    {
                                        try
                                        {
                                            if (fsr.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                fsr.AdtBfare = fsr.AdtBfare + fsr.ADTAdminMrk;
                                                fsr.AdtFare = fsr.AdtFare + fsr.ADTAdminMrk;
                                            }
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), fsr.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                            fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }
                            }

                            #region Child
                            else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "CNN")
                            {

                          
                                fsr.ChdFarebasis = Rulelist.Where(x => x.Element("UniqueKey").Value == "2").ElementAt(0).Element("FIC").Value;
                                fsr.ChdRbd = FLIGHTSEGMENT.Where(x => x.FltNum == fsr.FlightIdentification).ToList()[0].BIC;

                                foreach (var pxfr in FareInfoLIst.Where(X => int.Parse(X.Element("UniqueKey").Value) == int.Parse(UniqueKey.Value.ToString())))
                                {
                                    if (pxfr.Elements("BaseFareCurrency").First().Value == "INR")
                                        fsr.ChdBFare = float.Parse(pxfr.Elements("BaseFareAmt").First().Value);
                                    else
                                        fsr.ChdBFare = float.Parse(pxfr.Elements("EquivAmt").First().Value);

                                    fsr.ChdFSur = 0;
                                    fsr.ChdYR = 0;
                                    fsr.ChdOT = 0;
                                    fsr.ChdTax = 0;
                                    fsr.ChdWO = 0;
                                    fsr.ChdIN = 0;
                                    fsr.ChdJN = 0;
                                    var Chd = from item in pxfr.Elements("TaxDataAry").Elements("TaxData")
                                              select new
                                              {
                                                  ChdAmt = item.Element("Amt").Value,
                                                  ChdCon = item.Element("Country").Value
                                              };

                                    foreach (var Chdd in Chd)
                                    {
                                        if (Chdd.ChdCon == "YQ")
                                            fsr.ChdFSur = float.Parse(Chdd.ChdAmt);
                                        else if (Chdd.ChdCon == "WO")
                                            fsr.ChdWO = float.Parse(Chdd.ChdAmt);
                                        else if (Chdd.ChdCon == "IN")
                                            fsr.ChdIN = float.Parse(Chdd.ChdAmt);
                                        else if (Chdd.ChdCon == "K3")
                                            fsr.ChdJN = float.Parse(Chdd.ChdAmt);
                                        else if (Chdd.ChdCon == "YM")
                                            fsr.ChdYR = float.Parse(Chdd.ChdAmt);
                                        else
                                            fsr.ChdOT = fsr.ChdOT + float.Parse(Chdd.ChdAmt);
                                        //if (adtd.adtCon != "YQ")
                                        fsr.ChdTax = fsr.ChdTax + float.Parse(Chdd.ChdAmt);
                                    }
                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, fsr.ValiDatingCarrier, fsr.ChdFar, Convert.ToString(fsr.ChdBFare), Convert.ToString(fsr.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeChd = 0; }
                                    #endregion

                                    //SMS charge add 
                                    fsr.ChdOT = fsr.ChdOT + srvChargeChd;
                                    fsr.ChdTax = fsr.ChdTax + srvChargeChd;
                                    fsr.ChdFare = fsr.ChdFare + srvChargeChd;
                                    fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString()));


                                    fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                   

                                    CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, Convert.ToString(fsr.DepartureDate), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fsr.DepartureDate), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, fsr.FType, Convert.ToString(TotalViaSector),contx,"") ;
                                    if (CommDt != null && CommDt.Rows.Count > 0)
                                    {
                                        // CommDt = objFltComm.GetFltComm_WithouDB(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.OrgDestFrom + "-" + fsr.OrgDestTo, Convert.ToString(fltdt.Rows[0]["DepartureDate"]).Trim(), fsr.fareBasis, fsr.OrgDestFrom, fsr.OrgDestTo, fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", Segment.Count().ToString());
                                        fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                        fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS.Clear();
                                        STTFTDS = CalcSrvTaxTFeeTds1G(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, TDS);
                                        fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                        fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                        fsr.ChdEduCess = 0;
                                        fsr.ChdHighEduCess = 0;
                                        fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                        fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    }
                                    else
                                    {
                                        fsr.ChdDiscount1 = 0;
                                        fsr.ChdCB = 0;
                                        fsr.ChdSrvTax1 = 0;
                                        fsr.ChdDiscount = 0;
                                        fsr.ChdEduCess = 0;
                                        fsr.ChdHighEduCess = 0;
                                        fsr.ChdTF = 0;
                                        fsr.ChdTds = 0;
                                    }

                                    if (fsr.IsCorp == true)
                                    {
                                        try
                                        {
                                            if (fsr.Trip.ToString() == JourneyType.I.ToString())
                                            {
                                                fsr.ChdBFare = fsr.ChdBFare + fsr.CHDAdminMrk;
                                                fsr.ChdFare = fsr.ChdFare + fsr.CHDAdminMrk;
                                            }
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), fsr.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                            fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }
                                }


                            }
                            #endregion

                            #region Inf
                            else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "INF")
                            {
                                string a = "2";
                                if( fsr.Child > 0 && fsr.Infant>0)
                                {
                                    a = "3";
                                }

                                try
                                {
                                    fsr.InfFarebasis = Rulelist.Where(x => x.Element("UniqueKey").Value == a).ElementAt(0).Element("FIC").Value;
                                }
                                catch
                                { }
                                fsr.InfRbd = FLIGHTSEGMENT.Where(x => x.FltNum == fsr.FlightIdentification).ToList()[0].BIC;


                                foreach (var pxfr in FareInfoLIst.Where(X => int.Parse(X.Element("UniqueKey").Value) == int.Parse(UniqueKey.Value.ToString())))
                                {
                                    if (pxfr.Elements("BaseFareCurrency").First().Value == "INR")
                                        fsr.InfBfare = float.Parse(pxfr.Elements("BaseFareAmt").First().Value);
                                    else
                                        fsr.InfBfare = float.Parse(pxfr.Elements("EquivAmt").First().Value);

                                    fsr.InfFSur = 0;
                                    fsr.InfYR = 0;
                                    fsr.InfOT = 0;
                                    fsr.InfTax = 0;
                                    fsr.InfWO = 0;
                                    fsr.InfIN = 0;
                                    fsr.InfJN = 0;
                                    var inf = from item in pxfr.Elements("TaxDataAry").Elements("TaxData")
                                              select new
                                              {
                                                  infAmt = item.Element("Amt").Value,
                                                  infCon = item.Element("Country").Value
                                              };

                                    foreach (var infd in inf)
                                    {
                                        if (infd.infCon == "YQ")
                                            fsr.InfFSur = float.Parse(infd.infAmt);
                                        else if (infd.infCon == "WO")
                                            fsr.InfWO = float.Parse(infd.infAmt);
                                        else if (infd.infCon == "IN")
                                            fsr.InfIN = float.Parse(infd.infAmt);
                                        else if (infd.infCon == "K3")
                                            fsr.InfJN = float.Parse(infd.infAmt);
                                        else if (infd.infCon == "YM")
                                            fsr.InfYR = float.Parse(infd.infAmt);
                                        else
                                            fsr.InfOT = fsr.InfOT + float.Parse(infd.infAmt);
                                        //if (infd.infCon != "YQ")
                                        fsr.InfTax = fsr.InfTax + float.Parse(infd.infAmt);
                                    }

                                    if (fsr.IsCorp == true)
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(agentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), fsr.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                            fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }
                                #endregion


                            }

                        }

                        fsr.Searchvalue = "";//= JsonConvert.SerializeObject(fResp.Response.Results.Fare) + "itzDanata420" + JsonConvert.SerializeObject(fResp.Response.Results.FareBreakdown);
                                             // fsr.OriginalTF = 0;//float.Parse(fResp.Response.Results.Fare.OfferedFare.ToString());
                        fsr.OriginalTT = srvChargeAdt + srvChargeChd;
                        fsr.Provider = "1G";

                        // fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                        ////fsr.TotalFare =(float)Math.Ceiling( float.Parse(flt.Fare.PublishedFare.ToString()));
                        ////fsr.TotalFuelSur = (float)Math.Ceiling(float.Parse(flt.Fare.YQTax.ToString()));//(fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                        ////fsr.TotalTax =(float)Math.Ceiling( float.Parse(flt.Fare.Tax.ToString()) + float.Parse(flt.Fare.OtherCharges.ToString()) + float.Parse(flt.Fare.AdditionalTxnFeePub.ToString()));// (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                        ////fsr.OriginalTT = fsr.TotalTax;
                        ////fsr.TotBfare =(float)Math.Ceiling( float.Parse(flt.Fare.BaseFare.ToString()));// (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);

                        fsr.TotBfare = (fsr.AdtBfare * fsr.Adult) + (fsr.ChdBFare * fsr.Child) + (fsr.InfBfare * fsr.Infant);
                        fsr.TotalTax = (fsr.AdtTax * fsr.Adult) + (fsr.ChdTax * fsr.Child) + (fsr.InfTax * fsr.Infant);
                        fsr.TotalFuelSur = (fsr.AdtFSur * fsr.Adult) + (fsr.ChdFSur * fsr.Child);
                        fsr.TotalFare = (fsr.AdtFare * fsr.Adult) + (fsr.ChdFare * fsr.Child) + (fsr.InfFare * fsr.Infant);
                        fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                        fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child) + (fsr.InfTF * fsr.Infant);
                        fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                        fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                        fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                        fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);
                        if ((fsr.Trip.ToString() == JourneyType.I.ToString()) && (fsr.IsCorp == true))
                            fsr.TotalFare = (fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee) - ((fsr.ADTAdminMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child));
                        else
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                        // objFS.OperatingCarrier = p.opreatingcarrier;

                        #endregion
                    }



                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                    listFareChange.ChangeFareO = FareChangeO;

                    try
                    {
                        for (int ii = 0; ii < list.Count; ii++)
                        {
                            PropertyInfo[] infos = list[ii].GetType().GetProperties();

                            Dictionary<string, object> dix = new Dictionary<string, object>();

                            foreach (PropertyInfo info in infos)
                            {
                                try
                                {
                                    if (info.Name.Trim() == "ProductDetailQualifier")
                                    {
                                        dix.Add(info.Name, "CACHE_REFRESH");
                                    }
                                    else
                                    {
                                        dix.Add(info.Name, info.GetValue(list[ii], null));
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            resultList.Add(dix);
                        }
                    }
                    catch (Exception ex)
                    {


                    }
                }
                else
                {
                    FareChangeO.NewNetFare = float.Parse(dsO.Tables[0].Rows[0]["NetFare"].ToString());
                    FareChangeO.NewTotFare = float.Parse(dsO.Tables[0].Rows[0]["TotFare"].ToString());
                    listFareChange.ChangeFareO = FareChangeO;
                }

               
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "1G")
            {
                #region
                GALTransanctions obj = new GALTransanctions();
                obj.connectionString = ConStr;


                bool status = obj.FairAvailablity(dsO);

                if (status)
                {

                    FareChangeO.NewNetFare = float.Parse(dsO.Tables[0].Rows[0]["NetFare"].ToString());
                    FareChangeO.NewTotFare = float.Parse(dsO.Tables[0].Rows[0]["TotFare"].ToString());

                }
                else
                {
                    //listData[0].

                    //DEL/BOM/26/07/2018/26/07/2018/1/0/0/False/False/


                    string strVc = dsO.Tables[0].Rows[0]["ValidatingCarrier"].ToString().Trim();
                    //string Sector = ""; string Key = "";
                    // Searchdate = Utility.Right(Searchdate, 4) + "-" + Utility.Mid(Searchdate, 3, 2) + "-" + Utility.Left(Searchdate, 2);
                    //  Sector = Convert.ToString(dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString().Trim()) +":"+ Convert.ToString(dsO.Tables[0].Rows[0]["OrgDestTo"].ToString().Trim());
                    // Key = SearchKey(Org, Dest, Depdate, RetDate, Adt, Chd, Inf, GdsRtf, LccRtf, Cabin);

                    string DepDate = "";
                    string RetDate = "";
                    if (dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim().Length > 6)
                    {
                        DepDate = (Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring(6, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring(4, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring(0, 4))))));
                        RetDate = (Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring(6, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring(4, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring(0, 4))))));

                    }
                    else
                    {
                        DepDate = (Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring(0, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring(2, 2) + ("/20" + Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Substring((Convert.ToString(dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim()).Length - 2))))));
                        RetDate = (Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring(0, 2) + ("/" + (Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring(2, 2) + ("/20" + Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Substring((Convert.ToString(dsO.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim()).Length - 2))))));

                    }


                    if (((dsO.Tables[0].Rows[0]["TripType"].ToString().Trim() == "O") && ((dsO.Tables[0].Rows[0]["Trip"].ToString() == "D") && ((isroundTripNRML == true) && (isroundTripNRML == true)))))
                    {
                        SearchKey(dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString().Trim(), dsO.Tables[0].Rows[0]["OrgDestTo"].ToString().Trim(), RetDate, RetDate, Convert.ToInt32(dsO.Tables[0].Rows[0]["Adult"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Child"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Infant"].ToString()), false, false, "", strVc);
                        SearchKey(dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString().Trim(), dsO.Tables[0].Rows[0]["OrgDestTo"].ToString().Trim(), DepDate, DepDate, Convert.ToInt32(dsO.Tables[0].Rows[0]["Adult"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Child"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Infant"].ToString()), false, false, "", strVc);
                    }
                    else
                    {
                        if (dsO.Tables[0].Rows[0]["TripType"].ToString().Trim() == "R" && isroundTripNRML == false)
                        {
                            SearchKey(dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString().Trim(), dsO.Tables[0].Rows[0]["OrgDestTo"].ToString().Trim(), DepDate, RetDate, Convert.ToInt32(dsO.Tables[0].Rows[0]["Adult"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Child"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Infant"].ToString()), true, true, "", strVc);
                        }
                        else
                        {
                            SearchKey(dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString().Trim(), dsO.Tables[0].Rows[0]["OrgDestTo"].ToString().Trim(), DepDate, DepDate, Convert.ToInt32(dsO.Tables[0].Rows[0]["Adult"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Child"].ToString()), Convert.ToInt32(dsO.Tables[0].Rows[0]["Infant"].ToString()), false, false, "", strVc);
                        }
                    }



                    FareChangeO.NewNetFare = -1;
                    FareChangeO.NewTotFare = -1;
                    //FareChangeO..TrackId = "0";
                }

                listFareChange.ChangeFareO = FareChangeO;
                #endregion
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "AK")
            {
                #region Air Asia
                DataSet dsCrd = ObjcBAL.GetSearchCredentilas("AK");

                GALWS.AirAsia.AirAsiaSearchFlight objfarecheck = new GALWS.AirAsia.AirAsiaSearchFlight();
                List<FlightSearchResults> objlistNew = new List<FlightSearchResults>();

                string FLT_STAT = "";
                if (dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["TripType"].ToString().Trim() == "R")
                    FLT_STAT = "RTF";
                objlistNew = objfarecheck.AirAsiaItineraryPrice(dsO, dsCrd, MarkupDs, SrvChargeList, agentType, TDS, ConStr, list, FLT_STAT, MiscList);
                if (objlistNew[0].Searchvalue != list[0].Searchvalue)
                {
                    for (int intCount = 0; intCount < dsO.Tables[0].Rows.Count; intCount++)
                    {
                        dsO.Tables[0].Rows[intCount]["Searchvalue"] = objlistNew[0].Searchvalue;
                    }
                    dsO.Tables[0].AcceptChanges();
                }

                decimal totfare = Math.Round(Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"]), 2);
                decimal OriginalTF = Math.Round(Convert.ToDecimal(objlistNew[0].OriginalTF), 2);
                if (totfare > 0 && OriginalTF > 0 && totfare != OriginalTF)
                {
                    FareChangeO.NewNetFare = (float)objlistNew[0].NetFare;
                    FareChangeO.NewTotFare = (float)objlistNew[0].TotalFare;
                }
                else
                {
                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                    //if (FLT_STAT != "RTF" && dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["TripType"].ToString().Trim() == "R")
                    //{
                    //    FareChangeO.NewNetFare += list[1].NetFare;
                    //    FareChangeO.NewTotFare += list[1].TotalFare;
                    //}
                }
                listFareChange.ChangeFareO = FareChangeO;
                try
                {
                    for (int ii = 0; ii < objlistNew.Count; ii++)
                    {
                        PropertyInfo[] infos = list[ii].GetType().GetProperties();
                        Dictionary<string, object> dix = new Dictionary<string, object>();
                        foreach (PropertyInfo info in infos)
                        {
                            try
                            {
                                if (info.Name.Trim() == "ProductDetailQualifier")
                                {
                                    dix.Add(info.Name, "CACHE_REFRESH");
                                }
                                else
                                {
                                    dix.Add(info.Name, info.GetValue(list[ii], null));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        resultList.Add(dix);
                    }
                }
                catch (Exception ex)
                {

                }

                #endregion
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "TQ")
            {
                #region ITQ
                DataSet dsCrd = ObjcBAL.GetSearchCredentilas("TQ");
                HttpContext ctx = HttpContext.Current;
                GALWS.ITQAirApi.AvailabilityAndPricing objfarecheck = new GALWS.ITQAirApi.AvailabilityAndPricing();
                List<FlightSearchResults> objlistNew = new List<FlightSearchResults>();
                objlistNew = objfarecheck.ITQAirApiItineraryPrice(dsO, dsCrd, MarkupDs, SrvChargeList, agentType, TDS, ConStr, list, MiscList, ctx);

                decimal totfare = Math.Round(Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"]), 2);
                decimal OriginalTF = Math.Round(Convert.ToDecimal(objlistNew[0].OriginalTF), 2);
                if (totfare > 0 && OriginalTF > 0 && totfare != OriginalTF)
                {
                    FareChangeO.NewNetFare = (float)objlistNew[0].NetFare;
                    FareChangeO.NewTotFare = (float)objlistNew[0].TotalFare;
                }
                else
                {
                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                }
                listFareChange.ChangeFareO = FareChangeO;
                try
                {
                    for (int ii = 0; ii < objlistNew.Count; ii++)
                    {
                        PropertyInfo[] infos = list[ii].GetType().GetProperties();
                        Dictionary<string, object> dix = new Dictionary<string, object>();
                        foreach (PropertyInfo info in infos)
                        {
                            try
                            {
                                if (info.Name.Trim() == "ProductDetailQualifier")
                                {
                                    dix.Add(info.Name, "CACHE_REFRESH");
                                }
                                else
                                {
                                    dix.Add(info.Name, info.GetValue(list[ii], null));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        resultList.Add(dix);
                    }
                }
                catch (Exception ex)
                {

                }
                #endregion
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "IW")
            {
                #region IWEEN AIRASIA
                Dictionary<string, string> Log = new Dictionary<string, string>();
                DataSet dsCrd = ObjcBAL.GetSearchCredentilas("IW");
                ArrayList airPriceResp = new ArrayList();
                IweenPricing objAirPrice = new IweenPricing();
                string statusmessage = "";
                string statuscode = "";
                airPriceResp = objAirPrice.GetFareQuote(dsCrd, dsO, ref Log, ref exep, list[0].SearchId, ref statusmessage);
                //string SellReferenceId = "";
                //DataTable Dtnew = dsO.Tables[0];
                //airPriceResp = objAirPrice.ReGetFareQuote(dsCrd, Dtnew, ref Log, ref exep, list[0].SearchId, out SellReferenceId);
                //float TotPrice = (float)Math.Ceiling((Convert.ToDecimal(airPriceResp[0])));
                statuscode = statusmessage.Split(':')[0];
                float totfare = (float)Math.Ceiling((Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"])));

                if (totfare > 0 && statuscode != "200")
                {
                    List<FlightSearchResults> inputList = new List<FlightSearchResults>();
                    List<FlightSearchResults> newList = GetGoAirResultListWithMarkup(inputList, SrvChargeList, MarkupDs, srvCharge, ConStr, UID, DistrID, agentType, TDS);

                    FareChangeO.NewNetFare = newList[0].NetFare;
                    FareChangeO.NewTotFare = newList[0].TotalFare;
                    listFareChange.ChangeFareO = FareChangeO;

                    try
                    {
                        for (int ii = 0; ii < newList.Count; ii++)
                        {
                            PropertyInfo[] infos = newList[ii].GetType().GetProperties();

                            Dictionary<string, object> dix = new Dictionary<string, object>();

                            foreach (PropertyInfo info in infos)
                            {
                                try
                                {
                                    if (info.Name.Trim() == "ProductDetailQualifier")
                                    {
                                        dix.Add(info.Name, "CACHE_REFRESH");
                                    }
                                    else
                                    {
                                        dix.Add(info.Name, info.GetValue(newList[ii], null));
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            resultList.Add(dix);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                    listFareChange.ChangeFareO = FareChangeO;
                }
                #endregion
            }
            else if (dsO.Tables[0].Rows[0]["Provider"].ToString() == "LCC" && VC == "G9")
            {
                #region Air Arabia
                DataSet dsCrd = ObjcBAL.GetSearchCredentilas("G9");

                GALWS.AirArabia.AirArabiaFlightSearch objfarecheck = new GALWS.AirArabia.AirArabiaFlightSearch();
                List<FlightSearchResults> objlistNew = new List<FlightSearchResults>();
                objlistNew = objfarecheck.AirArabiaPricingRQ(dsO, dsCrd, MarkupDs, SrvChargeList, agentType, TDS, ConStr, list, MiscList);

                decimal totfare = Math.Round(Convert.ToDecimal(dsO.Tables[0].Rows[0]["OriginalTF"]), 2);
                decimal OriginalTF = Math.Round(Convert.ToDecimal(objlistNew[0].OriginalTF), 2);
                if (totfare > 0 && OriginalTF > 0 && totfare != OriginalTF)
                {
                    FareChangeO.NewNetFare = (float)objlistNew[0].NetFare;
                    FareChangeO.NewTotFare = (float)objlistNew[0].TotalFare;
                }
                else
                {
                    FareChangeO.NewNetFare = list[0].NetFare;
                    FareChangeO.NewTotFare = list[0].TotalFare;
                }
                listFareChange.ChangeFareO = FareChangeO;
                try
                {
                    for (int ii = 0; ii < objlistNew.Count; ii++)
                    {
                        PropertyInfo[] infos = list[ii].GetType().GetProperties();
                        Dictionary<string, object> dix = new Dictionary<string, object>();
                        foreach (PropertyInfo info in infos)
                        {
                            try
                            {
                                if (info.Name.Trim() == "ProductDetailQualifier")
                                {
                                    dix.Add(info.Name, "CACHE_REFRESH");
                                }
                                else
                                {
                                    dix.Add(info.Name, info.GetValue(list[ii], null));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        resultList.Add(dix);
                    }
                }
                catch (Exception ex)
                {

                }
                #endregion
            }
            else
            {
                FareChangeO.NewNetFare = list[0].NetFare;
                FareChangeO.NewTotFare = list[0].TotalFare;
                listFareChange.ChangeFareO = FareChangeO;
            }
            //resultListFinal.Add(resultList);
            return resultList;
        }
        private string SearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Cabin, string aircode)
        {
            string SKey = "";
            if (GdsRtf == true || LccRtf == true)
            {

                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf.ToString() + "/" + LccRtf.ToString() + "/" + Cabin;
            }
            else
            {
                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf.ToString() + "/" + LccRtf.ToString() + "/" + Cabin;
            }


            deletecache(SKey, aircode);
            return SKey;
        }
        public void deletecache(string skeyvalue, string AirCode)
        {
            try
            {
                int a = 0;
                SqlCommand cmd = new SqlCommand("Sp_DeleteCacheonfailure", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@skeyvalue", skeyvalue.Trim());
                cmd.Parameters.AddWithValue("@AirCode", AirCode.ToUpper().Trim());
                Con.Open();
                a = cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
            }

        }

        public List<FlightSearchResults> ParseAirPrice(string response, DataSet dsO)
        {
            string str = response.Replace("xmlns:soapenv='\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                 .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                 .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                 .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("ns1:", "");

            List<FlightSearchResults> resultList = new List<FlightSearchResults>();

            XDocument xmlDoc = XDocument.Parse(str);

            IEnumerable<XElement> xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirPriceRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
                                                       select el;

            //decimal totalfare = 0;


            for (int ptn = 0; ptn < xlFPricedItinerary.Count(); ptn++)
            {

                IEnumerable<XElement> xlFAirODItnry = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItinerary").Descendants("OriginDestinationOptions").Descendants("OriginDestinationOption");

                IEnumerable<XElement> xlFAirPriceItnry = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo");

                IEnumerable<XElement> xlFAirODItnries = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItinerary");//.Descendants("OriginDestinationOptions").Descendants("OriginDestinationOption");

                #region Origin Destination

                for (int odn = 0; odn < xlFAirODItnry.Count(); odn++)
                {


                    IEnumerable<XElement> segmnt = xlFAirODItnry.ElementAt(odn).Descendants("FlightSegment");


                    for (int s = 0; s < segmnt.Count(); s++)
                    {

                        XElement seg = segmnt.ElementAt(s);

                        FlightSearchResults fsr = new FlightSearchResults();


                        fsr.Leg = s + 1;
                        fsr.LineNumber = ptn + 1;
                        fsr.TotDur = xlFAirODItnry.ElementAt(odn).Elements().FirstOrDefault().Attribute("Duration").Value.ToString();


                        xlFAirODItnries.ToList().ForEach(x => { fsr.Searchvalue = fsr.Searchvalue + x.ToString(); });

                        xlFAirPriceItnry.ToList().ForEach(x => { fsr.sno = fsr.sno + x.ToString(); });

                        fsr.Flight = (odn + 1).ToString();



                        fsr.Stops = (segmnt.Count() - 1).ToString() + "-Stop";


                        fsr.Adult = int.Parse(dsO.Tables[0].Rows[0]["Adult"].ToString().Trim());
                        fsr.Child = int.Parse(dsO.Tables[0].Rows[0]["Child"].ToString().Trim());
                        fsr.Infant = int.Parse(dsO.Tables[0].Rows[0]["Infant"].ToString().Trim());


                        fsr.depdatelcc = seg.Attribute("DepartureDateTime").Value.Trim();
                        fsr.arrdatelcc = seg.Attribute("ArrivalDateTime").Value.Trim();
                        fsr.Departure_Date = Convert.ToDateTime(fsr.depdatelcc).ToString("dd MMM"); ;// legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                        fsr.Arrival_Date = Convert.ToDateTime(fsr.arrdatelcc).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                        fsr.DepartureDate = Convert.ToDateTime(fsr.depdatelcc).ToString("ddMMyy");
                        fsr.ArrivalDate = Convert.ToDateTime(fsr.arrdatelcc).ToString("ddMMyy");


                        fsr.FlightIdentification = seg.Attribute("FlightNumber").Value.Trim();
                        fsr.AirLineName = seg.Element("MarketingAirline").Attribute("Name").Value.Trim();

                        try { fsr.ValiDatingCarrier = seg.Element("ValidatingCarrier").Attribute("Code").Value.Trim(); }
                        catch { fsr.ValiDatingCarrier = seg.Element("MarketingAirline").Attribute("Code").Value.Trim(); }

                        fsr.OperatingCarrier = fsr.ValiDatingCarrier;
                        fsr.MarketingCarrier = seg.Element("MarketingAirline").Attribute("Code").Value.Trim();//seg.Airline.OperatingCarrier.Trim();
                        fsr.fareBasis = seg.Attribute("ResBookDesigCode").Value.Trim();

                        if (fsr.MarketingCarrier.Contains("G8"))
                        {
                            fsr.AdtFar = seg.Element("BookingClassAvail").Attribute("WebFareName").Value.ToString();
                        }
                        else
                        { fsr.AdtFar = ""; }


                        fsr.DepartureLocation = seg.Element("DepartureAirport").Attribute("LocationCode").Value.Trim();
                        fsr.DepartureCityName = seg.Element("DepartureAirport").Attribute("CityName").Value.Trim();
                        fsr.DepartureTime = Convert.ToDateTime(fsr.depdatelcc).ToString("HHmm");
                        fsr.DepartureAirportName = seg.Element("DepartureAirport").Attribute("AirPortName").Value.Trim();
                        try { fsr.DepartureTerminal = seg.Element("DepartureAirport").Attribute("Terminal").Value.Trim(); }
                        catch { }

                        fsr.DepAirportCode = seg.Element("DepartureAirport").Attribute("LocationCode").Value.Trim();

                        fsr.ArrivalLocation = seg.Element("ArrivalAirport").Attribute("LocationCode").Value.Trim();
                        fsr.ArrivalCityName = seg.Element("ArrivalAirport").Attribute("CityName").Value.Trim();
                        fsr.ArrivalTime = Convert.ToDateTime(fsr.arrdatelcc).ToString("HHmm");
                        fsr.ArrivalAirportName = seg.Element("ArrivalAirport").Attribute("AirPortName").Value.Trim();

                        try { fsr.ArrivalTerminal = seg.Element("ArrivalAirport").Attribute("Terminal").Value.Trim(); }
                        catch { }

                        fsr.ArrAirportCode = seg.Element("ArrivalAirport").Attribute("LocationCode").Value.Trim();

                        fsr.Trip = Convert.ToString(dsO.Tables[0].Rows[0]["Trip"]).ToUpper().Trim();
                        fsr.EQ = seg.Element("Equipment").Attribute("AirEquipType").Value.Trim();
                        fsr.Provider = "YA";
                        fsr.AdtFareType = xlFPricedItinerary.ElementAt(ptn).Attribute("FareType").Value.Trim();




                        IEnumerable<XElement> farebrkup = xlFAirPriceItnry.Descendants("PTC_FareBreakdowns").Descendants("PTC_FareBreakdown");


                        fsr.AdtCabin = xlFAirODItnry.ElementAt(odn).Descendants("FormData").Descendants("FareDifference").Descendants("TotalFare").FirstOrDefault().Attribute("Cabin").Value.Trim();
                        fsr.ChdCabin = fsr.AdtCabin;
                        fsr.InfCabin = fsr.AdtCabin;
                        if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                        {

                            fsr.AdtAvlStatus = xlFAirODItnry.ElementAt(odn).Descendants("FormData").Descendants("FBC").FirstOrDefault().Attribute("SeatToSell").Value.Trim();
                            fsr.ChdAvlStatus = fsr.AdtAvlStatus;
                            fsr.InfAvlStatus = fsr.AdtAvlStatus;
                        }
                        else
                        {
                            try { fsr.AdtAvlStatus = xlFPricedItinerary.ElementAt(ptn).Attribute("SeatToSell").Value.Trim(); }
                            catch { fsr.AdtAvlStatus = "9"; }

                            fsr.ChdAvlStatus = fsr.AdtAvlStatus;
                            fsr.InfAvlStatus = fsr.AdtAvlStatus;
                        }



                        #region fare calculation


                        IEnumerable<XElement> xItinTotalFare = xlFAirPriceItnry.Descendants("ItinTotalFare");

                        decimal tFeess = 0;


                        try
                        {
                            fsr.BagInfo = xItinTotalFare.Descendants("FareBaggageAllowance").FirstOrDefault().Attribute("UnitOfMeasureQuantity").Value + " " + xItinTotalFare.Descendants("FareBaggageAllowance").FirstOrDefault().Attribute("UnitOfMeasure").Value;
                        }
                        catch { }
                        fsr.OriginalTF = float.Parse(xItinTotalFare.Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value);




                        float totalfareItn = 0;
                        for (int frb = 0; frb < farebrkup.Count(); frb++)
                        {

                            XElement objfrb = farebrkup.ElementAt(frb);

                            #region Adult
                            if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "ADT")
                            {


                                fsr.AdtBfare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Adult;


                                float AdtTax = 0;
                                float AdtOT = 0;

                                if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                {
                                    fsr.AdtRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                    fsr.AdtFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                }
                                else
                                {
                                    fsr.AdtRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                    try
                                    {
                                        fsr.AdtFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();
                                    }
                                    catch { }

                                }



                                fsr.FBPaxType = "ADT";

                                float AdtWO = 0;
                                float AdtJN = 0;
                                float AdtIN = 0;
                                float AdtFSur = 0;



                                IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                for (int td = 0; td < TaxDetails.Count(); td++)
                                {
                                    XElement taxel = TaxDetails.ElementAt(td);

                                    if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                    {
                                        AdtWO = AdtWO + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                    {
                                        AdtJN = AdtJN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                    {
                                        AdtIN = AdtIN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                    {
                                        AdtFSur = AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else
                                    {
                                        AdtOT = AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }

                                }

                                fsr.AdtWO = AdtWO;
                                fsr.AdtJN = AdtJN;
                                fsr.AdtIN = AdtIN;
                                fsr.AdtFSur = AdtFSur;

                                AdtTax = AdtTax + fsr.AdtWO;
                                AdtTax = AdtTax + fsr.AdtJN;
                                AdtTax = AdtTax + fsr.AdtIN;
                                AdtTax = AdtTax + fsr.AdtFSur;


                                fsr.AdtOT = AdtOT;


                                fsr.AdtOT = (float)Math.Ceiling(fsr.AdtOT + (float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value)));// / searchInput.Adult) + (float.Parse(tFeess.ToString()) / searchInput.Adult);
                                AdtTax = AdtTax + (float)Math.Ceiling(fsr.AdtOT);

                                fsr.AdtTax = AdtTax;

                                fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;


                            }
                            #endregion


                            #region Child
                            if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "CHD")
                            {


                                fsr.ChdBFare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Child;


                                float ChdTax = 0;
                                float ChdOT = 0;
                                if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                {
                                    fsr.ChdRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                    fsr.ChdFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                }
                                else
                                {
                                    fsr.ChdRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();

                                    try
                                    {
                                        fsr.ChdFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();
                                    }
                                    catch { }

                                }
                                fsr.FBPaxType = "Chd";

                                float ChdWO = 0;
                                float ChdJN = 0;
                                float ChdIN = 0;
                                float ChdFSur = 0;


                                IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                for (int td = 0; td < TaxDetails.Count(); td++)
                                {
                                    XElement taxel = TaxDetails.ElementAt(td);

                                    if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                    {
                                        ChdWO = ChdWO + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                    {
                                        ChdJN = ChdJN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                    {
                                        ChdIN = ChdIN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                    {
                                        ChdFSur = ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else
                                    {
                                        ChdOT = ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }

                                }


                                fsr.ChdWO = ChdWO;
                                fsr.ChdJN = ChdJN;
                                fsr.ChdIN = ChdIN;
                                fsr.ChdFSur = ChdFSur;

                                ChdTax = ChdTax + fsr.ChdWO;
                                ChdTax = ChdTax + fsr.ChdJN;
                                ChdTax = ChdTax + fsr.ChdIN;
                                ChdTax = ChdTax + fsr.ChdFSur;

                                fsr.ChdOT = ChdOT;


                                fsr.ChdOT = (float)Math.Ceiling(fsr.ChdOT + float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value));// / searchInput.Adult;
                                ChdTax = ChdTax + (float)Math.Ceiling(fsr.ChdOT);

                                fsr.ChdTax = ChdTax;

                                fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;


                            }



                            #endregion


                            #region Infant
                            if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "INF")
                            {



                                fsr.InfBfare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Infant;

                                float InfTax = 0;
                                float InfOT = 0;

                                if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                {
                                    fsr.InfRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                    fsr.InfFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                }
                                else
                                {
                                    fsr.InfRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();

                                    try
                                    {
                                        fsr.InfFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();

                                    }
                                    catch { }
                                }

                                float InfWO = 0;
                                float InfJN = 0;
                                float InfIN = 0;
                                float InfFSur = 0;


                                IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                for (int td = 0; td < TaxDetails.Count(); td++)
                                {
                                    XElement taxel = TaxDetails.ElementAt(td);

                                    if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                    {
                                        InfWO = InfWO + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                    {
                                        InfJN = InfJN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                    {
                                        InfIN = InfIN + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                    {
                                        InfFSur = InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }
                                    else
                                    {
                                        InfOT = InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                    }

                                }

                                fsr.InfWO = InfWO;
                                fsr.InfJN = InfJN;
                                fsr.InfIN = InfIN;
                                fsr.InfFSur = InfFSur;

                                InfTax = InfTax + fsr.InfWO;
                                InfTax = InfTax + fsr.InfJN;
                                InfTax = InfTax + fsr.InfIN;
                                InfTax = InfTax + fsr.InfFSur;

                                fsr.InfOT = InfOT;

                                fsr.InfOT = (float)Math.Ceiling(fsr.InfOT + float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value));// / searchInput.Adult;
                                InfTax = InfTax + (float)Math.Ceiling(fsr.InfOT);

                                fsr.InfTax = InfTax;

                                fsr.InfFare = fsr.InfBfare + fsr.InfTax;


                            }
                            #endregion

                        }
                        float fareDiff = (float)Math.Ceiling(Convert.ToDecimal((fsr.OriginalTF - totalfareItn) / fsr.Adult));
                        fsr.AdtOT = fsr.AdtOT + fareDiff;
                        fsr.AdtTax = fsr.AdtTax + fareDiff;
                        fsr.AdtFare = fsr.AdtFare + fareDiff;

                        #endregion

                        fsr.Sector = Convert.ToString(dsO.Tables[0].Rows[0]["Sector"]);


                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                        fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));

                        DataRow[] FltO = dsO.Tables[0].Select("TripType='O'", "counter ASC");
                        DataRow[] FltR = dsO.Tables[0].Select("TripType='R'", "counter ASC");

                        if (Convert.ToInt32(fsr.Flight) == 1)
                        {
                            fsr.OrgDestFrom = Convert.ToString(FltO[0]["OrgDestFrom"]); //searchInput.HidTxtDepCity.Split(',')[0];
                            fsr.OrgDestTo = Convert.ToString(FltO[0]["OrgDestTo"]);// searchInput.HidTxtArrCity.Split(',')[0];
                            fsr.TripType = TripType.O.ToString();

                        }
                        else if (Convert.ToInt32(fsr.Flight) == 2)
                        {
                            fsr.OrgDestFrom = Convert.ToString(FltR[0]["OrgDestFrom"]); //searchInput.HidTxtDepCity.Split(',')[0];
                            fsr.OrgDestTo = Convert.ToString(FltR[0]["OrgDestTo"]);
                            fsr.TripType = TripType.R.ToString();

                        }

                        resultList.Add(fsr);
                    }


                }




                #endregion





            }


            return resultList;
        }
        #region Goair RTF
        public List<FlightSearchResults> GetGoAirResultListWithMarkup(List<FlightSearchResults> inputList, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, string ConnStr, string UID, string DistrID, string agentType, string TDS)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<FlightSearchResults> newList = new List<FlightSearchResults>();
            //ArrayList test = (ArrayList)inputList[0];
            ////SMS charge calc
            //float srvCharge = 0;
            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), "G8", searchInputs.UID);
            ////SMS charge calc end
            List<BaggageList> BagInfoList = new List<BaggageList>();
            // BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8");
            for (int i = 0; i < inputList.Count; i++)
            {

                newList.Add(GoAirGetNewObject((FlightSearchResults)inputList[i], SrvChargeList, MarkupDs, srvCharge, objFltComm, UID, DistrID, agentType, TDS));
            }

            return newList;
        }

        public FlightSearchResults GoAirGetNewObject(FlightSearchResults fsr, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, FlightCommonBAL objFltComm, string UID, string DistrID, string agentType, string TDS)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            var objNew = (FlightSearchResults)fsr.Clone();



            #region Fare Calculation

            #region Adult
            if (objNew.Adult > 0)
            {
                //objNew.AdtOT = objNew.AdtTax - objNew.AdtFSur;
                //SMS charge add 
                objNew.AdtOT = objNew.AdtOT + srvCharge;
                objNew.AdtTax = objNew.AdtTax + srvCharge;
                objNew.AdtFare = objNew.AdtFare + srvCharge;
                //SMS charge add end
                if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper() == "N")
                {
                    objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, fsr.Trip.ToString());
                    objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, fsr.Trip.ToString());

                    objNew.AdtDiscount1 = 0;  //-AdtComm  
                    objNew.AdtCB = 0;
                    objNew.AdtSrvTax1 = 0;// added TO TABLE
                    objNew.AdtDiscount = 0;
                    objNew.AdtEduCess = 0;
                    objNew.AdtHighEduCess = 0;
                    objNew.AdtTF = 0;
                    objNew.AdtTds = 0;
                }
                else
                {
                    objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());
                    objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, fsr.Trip.ToString());

                    CommDt.Clear();
                    CommDt = objFltComm.GetFltComm_Gal(agentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, objNew.DepartureDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, objNew.DepartureDate, objNew.AdtFarebasis, "", "", objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "NRM", "");
                    objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                    objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    STTFTDS.Clear();
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, TDS);
                    objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                    objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                    objNew.AdtEduCess = 0;
                    objNew.AdtHighEduCess = 0;
                    objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                    objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                }


            }
            #endregion

            #region Child
            if (objNew.Child > 0)
            {
                // objNew.ChdOT = objNew.ChdTax - objNew.ChdFSur;
                //SMS charge add 
                objNew.ChdOT = objNew.ChdOT + srvCharge;
                objNew.ChdTax = objNew.ChdTax + srvCharge;
                objNew.ChdFare = objNew.ChdFare + srvCharge;
                //SMS charge add end
                if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper() == "N")
                {
                    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.ChdFare, objNew.Trip.ToString());
                    objNew.ChdDiscount1 = 0;  //-AdtComm  
                    objNew.ChdCB = 0;
                    objNew.ChdSrvTax1 = 0;// added TO TABLE
                    objNew.ChdDiscount = 0;
                    objNew.ChdEduCess = 0;
                    objNew.ChdHighEduCess = 0;
                    objNew.ChdTF = 0;
                    objNew.ChdTds = 0;
                }
                else
                {
                    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, objNew.ChdFare, objNew.Trip.ToString());

                    CommDt.Clear();
                    CommDt = objFltComm.GetFltComm_Gal(agentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, objNew.DepartureDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, objNew.DepartureDate, objNew.ChdFarebasis, "", "", objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "NRM", "");
                    objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                    objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    STTFTDS.Clear();
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, TDS);
                    objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                    objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                    objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                    objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                    objNew.ChdEduCess = 0;
                    objNew.ChdHighEduCess = 0;
                }

            }
            #endregion

            #region Infant
            if (objNew.Infant > 0)
            {
                //objNew.InfOT = objNew.InfTax - objNew.InfFSur;

            }

            #endregion

            objNew.STax = (objNew.AdtSrvTax * objNew.Adult) + (objNew.ChdSrvTax * objNew.Child) + (objNew.InfSrvTax * objNew.Infant);
            objNew.TFee = (objNew.AdtTF * objNew.Adult) + (objNew.ChdTF * objNew.Child);// +(objlist.InfTF * objlist.Infant);
            objNew.TotDis = (objNew.AdtDiscount * objNew.Adult) + (objNew.ChdDiscount * objNew.Child);
            objNew.TotCB = (objNew.AdtCB * objNew.Adult) + (objNew.ChdCB * objNew.Child);
            objNew.TotTds = (objNew.AdtTds * objNew.Adult) + (objNew.ChdTds * objNew.Child);// +objFS.InfTds;
            objNew.TotMrkUp = (objNew.ADTAdminMrk * objNew.Adult) + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAdminMrk * objNew.Child) + (objNew.CHDAgentMrk * objNew.Child);
            objNew.TotMgtFee = (objNew.AdtMgtFee * objNew.Adult) + (objNew.ChdMgtFee * objNew.Child) + (objNew.InfMgtFee * objNew.Infant);
            objNew.TotalTax = objNew.TotalTax + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.NetFare = (objNew.TotalFare + objNew.TotTds) - (objNew.TotDis + objNew.TotCB + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAgentMrk * objNew.Child));

            objNew.OriginalTT = srvCharge;
            #endregion


            return objNew;



        }
        #endregion


        private Hashtable CalcSrvTaxTFeeTds1G(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            // int IATAComm = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                try
                {
                    List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                    if (StNew.Count > 0)
                    {
                        STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                        TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                        IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                    }
                }
                catch
                { }
                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }


                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
    }
}