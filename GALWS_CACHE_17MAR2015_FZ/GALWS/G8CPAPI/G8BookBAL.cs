﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;
using System.Xml.Linq;

namespace STD.BAL
{
    public class G8BookBAL
    {

        string SecurityToken;
        string IATANumber;
        string UserName;
        string Password;
        string IP;

        public G8BookBAL(string securityToken, string Iatanumber, string username, string password, string ip)
        {
            SecurityToken = securityToken;
            IATANumber = Iatanumber;
            UserName = username;
            Password = password;
            IP = ip;

        }

        public Dictionary<string, string> BookFlight(G8BookFlightRequest bReq)
        {

            Dictionary<string, string> objDict = new Dictionary<string, string>();
            string exep = "";
            try
            {

                G8TravelAgent objTa = new G8TravelAgent(SecurityToken, IATANumber, UserName, Password, IP);
                G8SvcAndMethodUrls SMUrl = new G8SvcAndMethodUrls();
                G8Reservation objReservation = new G8Reservation(SecurityToken, IATANumber, UserName, Password, IP);

                decimal amount = objReservation.GetSummaryPNR(SMUrl.SummaryPNRUrl, SMUrl.ReservationSvcUrl, bReq, ref objDict, ref exep);
                if (amount <= bReq.paymentDetails.OriginalAmount)
                {
                    decimal processPnrPaymentAmount = objReservation.ProcessPNRPayment(SMUrl.ProcessPNRPaymentUrl, SMUrl.FulfillmentSvcUrl, bReq, ref objDict, ref exep);

                    if (processPnrPaymentAmount == 0)
                    {
                        string res = objReservation.CreatePNR(SMUrl.CreatePNRUrl, SMUrl.ReservationSvcUrl, ref objDict, ref  exep);

                        string str = res.Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "");

                        XDocument xmlDoc = XDocument.Parse(str);

                        // string val = xmlDoc.Element("ReservationBalance").Value;
                        objDict.Add("PNR", xmlDoc.Descendants("ConfirmationNumber").First().Value);
                    }
                    else
                    {
                        exep = exep + "ProcessPnrPayment Amount is not zero";
                    }

                }
                else
                {
                    exep = exep + "Fare Amount difference  in summaryPNR";
                }
            }
            catch (Exception ex)
            {
                exep = exep + " BookFlight Method: " + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            finally
            {
                objDict.Add("EXEP", exep);
            }

            return objDict;
        }



    }
}
