﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelShared
{
    public class HotelComposite
    {
        public HotelSearch HotelSearchDetail { get; set; }
        public List<HotelResult> Hotelresults { get; set; }
        public List<HotelResult> HotelOrgList { get; set; }
    }
    public class RoomComposite
    {
        public SelectedHotel SelectedHotelDetail { get; set; }
        public List<RoomList> RoomDetails { get; set; }
    }
}
