﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using HotelShared;
using HotelDAL;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Data;

namespace HotelBAL
{
    public class TGHotelResponse
    {
        TGRequestXML HtlReq = new TGRequestXML();
        public HotelComposite TGHotelSearchResponse(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelsCombo = new HotelComposite();
            try
            {
                if (SearchDetails.CountryCode == "IN" && SearchDetails.TGUrl !=null && (SearchDetails.TGTrip == SearchDetails.HtlType || SearchDetails.TGTrip =="ALL"))
                {

                    SearchDetails = TGHotels(SearchDetails);
                    obgHotelsCombo.Hotelresults = GetTGResult(SearchDetails.TG_HotelSearchRes, SearchDetails);

                    //XDocument document1;
                    //string XMLFile = "D:\\HotelPriject\\New folder\\B2BHote_RezNext_Expedia\\HotelTGXML\\Home\\TGHotelSearch.xml";
                    //document1 = XDocument.Load(XMLFile);
                    //SearchDetails.TG_HotelSearchRes = document1.ToString();
                    //obgHotelsCombo.Hotelresults = GetTGResult(SearchDetails.TG_HotelSearchRes, SearchDetails);
                }
                else
                {
                    SearchDetails.TG_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                    SearchDetails.TG_HotelSearchReq = "not allow";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.TG_HotelSearchRes });
                    obgHotelsCombo.Hotelresults = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelSearchResponse");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            obgHotelsCombo.HotelSearchDetail = SearchDetails;
            return obgHotelsCombo;
        }
        public RoomComposite TGSelectedHotelResponse(HotelSearch SearchDetails)
        {
            RoomComposite objroomlist = new RoomComposite();
            try
            {
                SearchDetails = TGHotels(SearchDetails);
                objroomlist = GetTGRoomList(SearchDetails.TG_HotelSearchRes, SearchDetails);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGSelectedHotelResponse");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            return objroomlist;
        }
        protected HotelSearch TGHotels(HotelSearch SearchDetails)
        {
            try
            {
                SearchDetails = HtlReq.HotelSearchRequest(SearchDetails);
                SearchDetails.TG_HotelSearchRes = TGPostXml(SearchDetails.TGUrl, SearchDetails.TG_HotelSearchReq);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.TG_HotelSearchRes = ex.Message;
            }
            return SearchDetails;
        }
        protected string TGPostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                HotelDA.InsertHotelErrorLog(WebEx, "");
                WebResponse response = WebEx.Response;
                if (response !=null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "TG", "HotelInsert");
                    Objsendmail.ExceptionEmail(responseMessage);
                }
                else
                    Objsendmail.ExceptionEmail(WebEx.Message);
            }
            catch (Exception exx)
            {
                sbResult.Append(exx.Message);
                HotelDA.InsertHotelErrorLog(exx, "");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, exx.Message, "TG", "HotelInsert");
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                Objsendmail.ExceptionEmail(exx.Message);
            }
            return sbResult.ToString();
        }
        protected List<HotelResult> GetTGResult(string HotelResponse, HotelSearch SearchDetails)
        {
            List<HotelResult> objHotellist = new List<HotelResult>(); HotelDA objhtlDa = new HotelDA();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            try
            {
                if (HotelResponse.Contains("<Success />") || HotelResponse.Contains("<Success/>"))
                {
                    string responses = HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);
  
                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new { BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions") }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        DataTable HotelDt = objhtlDa.GetHotelOverview(SearchDetails.SearchCity, "");
                        int k = 0;
                        if (HotelDt.Rows.Count > 0)
                        {
                        foreach (var hoteldetails in hotelprice)
                        {
                            string HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                            DataRow[] HotelFilterArray = objHtlMrk.FilterHotelDT(HotelDt, HotelCode);
                            if (HotelFilterArray.Length > 0)
                            {
                                var HotelsData = hoteldetails.BasicPropertyInfo.Parent;
                                if (HotelsData.Element("RoomRates").Element("RoomRate") != null && HotelsData.Element("RatePlans").Element("RatePlan") != null)
                                {
                                    try
                                    {
                                        decimal baserate = 0, taxs = 0, disamt = 0, ExtraGuest = 0, TotaldiscAmt = 0;
                                        foreach (var htldtl in HotelsData.Descendants("RoomRates").Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value))
                                        {
                                            foreach (var htl in htldtl.Descendants("Rates"))
                                            {
                                                baserate += Convert.ToDecimal(htl.Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value.Trim());
                                                //if (htl.Element("Rate").Element("Base").Element("Taxes") != null)
                                                //    taxs += Convert.ToDecimal(htl.Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value.Trim());

                                                if (htl.Element("Rate").Element("Discount") != null)
                                                    disamt += Convert.ToDecimal(htl.Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value.Trim());

                                                //if (htl.Element("Rate").Element("AdditionalGuestAmounts") != null)
                                                //{
                                                //    decimal extrarate = 0;
                                                //    foreach (var addigust in htl.Elements("Rate").Elements("AdditionalGuestAmounts").Descendants("AdditionalGuestAmount"))
                                                //    {
                                                //        extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                                                //    }
                                                //    ExtraGuest += extrarate;
                                                //}
                                            }
                                        }
                                        MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (baserate + taxs + ExtraGuest - disamt) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.TG_servicetax);
                                        if (disamt > 0)
                                            TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (baserate + taxs + ExtraGuest) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.TG_servicetax);
                                        HotelRatePlan objRatePlans = new HotelRatePlan();
                                        objRatePlans = RatePlanlist(hoteldetails.BasicPropertyInfo, hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value);
                                      
                                        
                                        // string Reviews;
                                        //if (HotelFilterArray[0]["ReviewRating"].ToString().Trim() != "" && HotelFilterArray[0]["ReviewRating"].ToString().Trim() != null)
                                          //  Reviews = objHtlMrk.SetTripAdvisorRating(Convert.ToDouble(HotelFilterArray[0]["ReviewRating"].ToString().Trim()), SearchDetails.BaseURL) + " (" + HotelFilterArray[0]["ReviewCount"].ToString() + " Reviews)";
                                      //  string htlpaln = DateTime.Now.ToString("hh.mm.ss.ff");
                                        objHotellist.Add(new HotelResult
                                        {
                                            RatePlanCode = hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value,
                                            hotelDiscoutAmt = TotaldiscAmt,
                                            hotelPrice = MarkList.TotelAmt,
                                            AgtMrk = MarkList.AgentMrkAmt,
                                            DiscountMsg = objRatePlans.discountMsg,
                                            inclusions = objRatePlans.inclusions,
                                            RoomTypeCode = objRatePlans.RatePlanCode,
                                            HotelCode = HotelCode,
                                            HotelName = HotelFilterArray[0]["VendorName"].ToString().Trim().Replace("'", ""),
                                            HotelCityCode = SearchDetails.SearchCityCode,
                                            HotelCity = HotelFilterArray[0]["City"].ToString().Trim(),
                                            StarRating = HotelFilterArray[0]["Hotel_Star"].ToString().Trim(),
                                            HotelAddress = HotelFilterArray[0]["Address"].ToString().Trim(),
                                            HotelDescription = "<strong>Description</strong>: " + HotelFilterArray[0]["HotelOverview"].ToString(),
                                            HotelThumbnailImg = HotelFilterArray[0]["ImagePath"].ToString().Trim().Replace("_TN", String.Empty),
                                            Lati_Longi = HotelFilterArray[0]["Latitude"].ToString().Trim() + "##" + HotelFilterArray[0]["Longitude"].ToString().Trim(),
                                            Location = HotelFilterArray[0]["Location"].ToString().Trim(),
                                         
                                            HotelServices = SetHotelService_Image(HotelCode),
                                            ReviewRating =HotelFilterArray[0]["ReviewRating"].ToString().Trim() + "#" + HotelFilterArray[0]["ReviewCount"].ToString(),
                                            Provider="TG", PopulerId = k++
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                        HotelDA.InsertHotelErrorLog(ex, "GetTGResult_List-" + HotelCode);
                                       // objHotellist.Add(new HotelResult { HotelName = "", hotelPrice =0, HtlError = ex.Message });
                                    }
                                }
                            }
                            //else
                            //{
                            //    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found in DB,  Please modify your search.. " });
                            //}
                        }
                        }
                            else
                            {
                                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found in DB,  Please modify your search.. " });
                                int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                            }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found,  Please modify your search.. " });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                    if (HotelResponse.Contains("<Errors>"))
                    {
                        XDocument document = XDocument.Parse(HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                        var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                        if (Hotels_temps.Count > 0)
                        {
                            if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                        }
                    }
                    else
                    {
                         if(HotelResponse.Contains("<p>"))
                         {
                            string Serviceerror = HotelResponse.Substring(HotelResponse.IndexOf("<p>"), HotelResponse.IndexOf("</p>") - HotelResponse.IndexOf("<p>") -2);
                            objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = Serviceerror });
                         }
                         else
                             objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGResult _" + SearchDetails.SearchCity);
                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            return objHotellist;
        }
        protected HotelRatePlan RatePlanlist(XElement hoteldetails, string rateplan)
        {
            HotelRatePlan objRatePlans = new HotelRatePlan();
            try
            {
                foreach (var htldtl in hoteldetails.Parent.Descendants("RatePlans").Elements("RatePlan").Where(x => x.Attribute("RatePlanCode").Value == rateplan))
                {
                    string discount = "";
                    foreach (var htl in htldtl.Descendants("RatePlanDescription").Elements("Text"))
                    {
                        discount += htl.Value;
                    }
                    string inclusions = "";
                    foreach (var htl in htldtl.Descendants("RatePlanInclusions").Descendants("RatePlanInclusionDesciption").Elements("Text"))
                    {
                        inclusions += htl.Value;
                    }
                    objRatePlans.RatePlanCode = htldtl.Attribute("RatePlanCode").Value;
                    objRatePlans.discountMsg = discount;
                    objRatePlans.inclusions = inclusions;
                }
            }
            catch (Exception ex)
            { HotelDA.InsertHotelErrorLog(ex, "RatePlanlist"); }
            return objRatePlans;
        }
        public string TGCancellationPolicy(HotelSearch SearchDetails)
        {
            string Policy = "";
            try
            {
                SearchDetails = TGHotels(SearchDetails);
                if (SearchDetails.TG_HotelSearchRes.Contains("<Success />") || SearchDetails.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    string responses = SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    var hotelPolicy = (from htlPolicy in xmlresp.Descendants("RoomStays").Descendants("RoomStay").Descendants("RatePlans").Descendants("RatePlan")
                                       where htlPolicy.Attribute("RatePlanCode").Value == SearchDetails.HtlRoomCode
                                       select new { RoomName = htlPolicy.Attribute("RatePlanName"), Cancellation = htlPolicy.Element("CancelPenalties").Element("CancelPenalty") }).ToList();

                    if (hotelPolicy.Count > 0)
                    {
                        Policy = "<Div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                        foreach (var hoteldetails in hotelPolicy)
                        {
                            foreach (var hotelpoly in hoteldetails.Cancellation.Descendants("PenaltyDescription").Where(x => x.Attribute("Name").Value != "FREE_CANCELLATION"))
                            {
                                Policy += "<li style='margin:4px 0 4px 0;'>" + hotelpoly.Element("Text").Value + "</li>";
                            }
                        }
                        Policy += "</Div>";
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGCancellationPolicy");
                return ex.Message;
            }
            return Policy;
        }
        protected RoomComposite GetTGRoomList(string HotelResponse, HotelSearch SearchDetails)
        {
            List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
            RoomComposite objRoomDetals = new RoomComposite(); HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA();
            try
            {
                if (HotelResponse.Contains("<Success />") || HotelResponse.Contains("<Success/>"))
                {
                    string responses = HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                          select new
                          {
                              RoomTypes = htlprice.Element("RoomTypes"), RatePlans = htlprice.Element("RatePlans"), RoomRates = htlprice.Element("RoomRates"),
                              BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions")
                          }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        foreach (var hoteldetails in hotelprice)
                        {
                            #region Hotel Details
                            try
                            {
                                HotelDetail.HotelName = hoteldetails.BasicPropertyInfo.Attribute("HotelName").Value;
                                HotelDetail.HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                                HotelDetail.StarRating = hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value;

                                string address = hoteldetails.BasicPropertyInfo.Element("Address").Element("AddressLine").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("StateProv") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("StateProv").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode") != null)
                                    address += " - " + hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value;
                                HotelDetail.HotelAddress = address;

                                try
                                {
                                    HotelDetail.Lati_Longi = "";
                                    if (hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude") != null)
                                        HotelDetail.Lati_Longi = hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude").Value + "," + hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Longitude").Value;

                                    HotelDetail.ThumbnailUrl = "Images/NoImage.jpg";
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Multimedia").Attribute("ThumbnailUrl") != null)
                                        HotelDetail.ThumbnailUrl = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Multimedia").Attribute("ThumbnailUrl").Value.Replace("_TN", String.Empty);

                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("AmenityDescription") != null)
                                        HotelDetail.AmenityDescription = "<strong>DESCRIPTION: </strong>" +  hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("AmenityDescription").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Description") != null)
                                        HotelDetail.HotelDescription = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Description").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Area") != null)
                                        HotelDetail.Location = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Area").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckInTime") != null)
                                        HotelDetail.CheckInTime = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckInTime").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckOutTime") != null)
                                        HotelDetail.CheckOutTime = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckOutTime").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("NumberOfRooms") != null)
                                        HotelDetail.NumberOfRooms = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("NumberOfRooms").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("isFlexibleCheckIn") != null)
                                        HotelDetail.FlexibleCheckIn = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("isFlexibleCheckIn").Value;
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals basic " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                //HotelDetail.ReviewComment = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewComment").Value;
                                //HotelDetail.ReviewPostDate = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewPostDate").Value;
                                //HotelDetail.ReviewRating = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewRating").Value;
                                //HotelDetail.ReviewerName = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewerName").Value;
   
                                string imgdiv = "", Attract = "", RoomFacilty = "", HotelFacilty = "";
                                try
                                {
                                    int im = 0;
                                    foreach (var HtlImg in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("Multimedia").Descendants("ImageJSON").Elements("ImagesList"))
                                    {
                                        im++;
                                        imgdiv += "<img id='img" + im + "' src='" + HtlImg.Element("LargeImageObj").Attribute("url").Value + "' onmouseover='return ShowHtlImg(this);' alt='' title='" + HtlImg.Element("LargeImageObj").Attribute("caption").Value + "' class='imageHtlDetailsshow' />";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Image " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                HotelDetail.HotelImage = imgdiv;
                                // Distance from 
                                foreach (var POI in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("POI").Elements("HotelPOI"))
                                {
                                    Attract += "<div class='check'>  Distance from " + POI.Attribute("POIName").Value + " : Approx. " + POI.Attribute("POIDistance").Value + " km</div>";
                                }
                                HotelDetail.Attraction = Attract;
                                try
                                {
                                    foreach (var Amenities in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("Amenities").Elements("PropertyAmenities"))
                                    {
                                        RoomFacilty += "<div class='check1'>" + Amenities.Attribute("description").Value + "</div>";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Room Facility " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                HotelDetail.HotelAmenities = RoomFacilty;
                                try
                                {
                                    foreach (var Amenities in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("Amenities").Elements("RoomAmenities"))
                                    {
                                        HotelFacilty += "<div class='check1'>" + Amenities.Attribute("description").Value + "</div>";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Hotel Facility " + HotelDetail.HotelName + "" + SearchDetails.SearchCity);
                                }
                                HotelDetail.RoomAmenities = HotelFacilty;
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "Hotel Detals " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                            }
                            #endregion
                            #region Room Details
                            try
                            {
                               // DataTable roomdt = objhtlDa.GetRoomDiscription(HotelDetail.HotelCode);
                                foreach (var rateplane in hoteldetails.RatePlans.Elements("RatePlan"))
                                {
                                    string discountmsg = "",inclusions = "";
                                    foreach (var htl in rateplane.Descendants("RatePlanDescription").Elements("Text"))
                                    {
                                        discountmsg += htl.Value.Trim() + " ";
                                    }
                                    foreach (var htl in rateplane.Descendants("RatePlanInclusions").Descendants("RatePlanInclusionDesciption").Elements("Text"))
                                    {
                                        if (htl.Value.Trim() != "")
                                            inclusions += htl.Value.Trim() + "  ";
                                    }
                                    //Descendants("PenaltyDescription").Where(x => x.Attribute("Name").Value != "FREE_CANCELLATION")
                                    string Policy = "<div>";//<span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                                    foreach (var hotelpoly in rateplane.Descendants("CancelPenalties").Descendants("CancelPenalty").Elements("PenaltyDescription").Where(x => x.Attribute("Name").Value != "FREE_CANCELLATION"))
                                    {
                                        if (hotelpoly.Element("Text").Value.Trim() !="Y")
                                            Policy += "<li style='margin:4px 0 4px 0;'>" + hotelpoly.Element("Text").Value.Trim() + "</li>";
                                    }
                                    RoomRate objRoomRate = new RoomRate();
                                    SearchDetails.StarRating = HotelDetail.StarRating;
                                    objRoomRate = RoomRatelist(hoteldetails.RoomRates, rateplane.Attribute("RatePlanCode").Value, SearchDetails);

                                    RoomType objRoomType = new RoomType();
                                    objRoomType = RoomTypelist(hoteldetails.RoomTypes, objRoomRate.RoomTypeCode);
                                   // DataRow[] HotelFilterArray = objHtlMrk.FilterRoomDT(roomdt, objRoomType.RoomTypeCode);
                                    objRoomList.Add(new RoomList
                                    {
                                        HotelCode = HotelDetail.HotelCode,
                                        RatePlanCode = rateplane.Attribute("RatePlanCode").Value,
                                        RoomTypeCode = objRoomType.RoomTypeCode,
                                        RoomName = rateplane.Attribute("RatePlanName").Value,
                                        discountMsg = discountmsg,
                                        DiscountAMT = objRoomRate.DiscountAMT,
                                        Total_Org_Roomrate = objRoomRate.Total_Org_Roomrate,
                                        TotalRoomrate = objRoomRate.TotalRoomrate,
                                        AdminMarkupPer = objRoomRate.AdminMarkupPer,
                                        AdminMarkupAmt = objRoomRate.AdminMarkupAmt,
                                        AdminMarkupType = objRoomRate.AdminMarkupType,
                                        AgentMarkupPer = objRoomRate.AgentMarkupPer,
                                        AgentMarkupAmt = objRoomRate.AgentMarkupAmt,
                                        AgentMarkupType = objRoomRate.AgentMarkupType,
                                        AgentServiseTaxAmt = objRoomRate.ServiseTaxAmt,
                                        V_ServiseTaxAmt = objRoomRate.V_ServiseTaxAmt,
                                        AmountBeforeTax = objRoomRate.AmountBeforeTax,
                                        Taxes = objRoomRate.Taxes,
                                        MrkTaxes = objRoomRate.MrkTaxes,
                                        ExtraGuest_Charge = objRoomRate.ExtraGuest_Charge,
                                        Smoking = objRoomType.Smoking,
                                        inclusions = inclusions,
                                        CancelationPolicy = Policy + "</div>",
                                        OrgRateBreakups = objRoomRate.Org_RoomrateBreakups,
                                        MrkRateBreakups = objRoomRate.Mrk_RoomrateBreakups,
                                        DiscRoomrateBreakups = objRoomRate.Disc_RoomrateBreakups,
                                        EssentialInformation = "",
                                        RoomDescription = objRoomType.RoomDescription,
                                        Provider = "TG",
                                        RoomImage = objRoomType.RoomImage
                                        //maxAdult = objRoomType.maxAdult,
                                        //maxChild = objRoomType.maxChild,
                                        //maxGuest = objRoomType.maxGuest,
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "ADDRoomList");
                                objRoomList.Add(new RoomList
                                {
                                    TotalRoomrate =0,HtlError = ex.Message
                                });
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        objRoomList.Add(new RoomList
                        {
                            TotalRoomrate = 0,HtlError = "Room Details Not found"
                        });                       
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");

                    string XmlRess = HotelResponse.Replace("xmlns='urn:Hotel_Search' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' PrimaryLangID='en-us' AltLangID='en-us'", "");
                    XDocument document = XDocument.Parse(XmlRess);
                    var Hotels_temps = (from Hotel in document.Descendants("Error") select new { Errormsg = Hotel }).ToList();

                    objRoomList.Add(new RoomList
                    {
                        TotalRoomrate = 0,HtlError = Hotels_temps[0].Errormsg.Value
                    });
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGRoomList");
                objRoomList.Add(new RoomList
                {
                    TotalRoomrate = 0, HtlError = ex.Message
                });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            objRoomDetals.SelectedHotelDetail = HotelDetail;
            objRoomDetals.RoomDetails = objRoomList;
            return objRoomDetals;
        }
        protected RoomType RoomTypelist(XElement hoteldetails, string RoomTypeCode)
        {
            RoomType objRoomType = new RoomType();
            try
            {
                foreach (var htldtl in hoteldetails.Elements("RoomType").Where(x => x.Attribute("RoomTypeCode").Value == RoomTypeCode))
                {
                    objRoomType.RoomTypeName = htldtl.Attribute("RoomType").Value;
                    objRoomType.RoomTypeCode = htldtl.Attribute("RoomTypeCode").Value;
                    objRoomType.Smoking = htldtl.Attribute("NonSmoking").Value;
                    objRoomType.RoomDescription = htldtl.Element("RoomDescription").Element("Text").Value;
                    if (htldtl.Element("RoomDescription").Element("Image")!=null)
                        objRoomType.RoomImage = htldtl.Element("RoomDescription").Element("Image").Value.Replace("_TN", String.Empty);

                    objRoomType.maxAdult = "";// htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxAdult").Value;
                    objRoomType.maxChild = "";//htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxChild").Value;
                    objRoomType.maxGuest = "";//htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxGuest").Value;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomTypelist_" + RoomTypeCode);
            }
            return objRoomType;
        }
        protected RoomRate RoomRatelist(XElement hoteldetails, string RoomPlanCode, HotelSearch SearchDetails)
        {
            RoomRate objRoomRate = new RoomRate(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            MarkupList TaxMarkList = new MarkupList(); MarkupList ExtraMarkList = new MarkupList();
            try
            {
                string Org_RoomRateStr = "", MrkRoomrateStr = "", Disc_RoomrateStr = "", RoomTypeCode = "";
                decimal AmountBeforeTax = 0, MrkTotalPrice = 0, TotaldiscAmt = 0, adminMrkAmt = 0, AgtMrkAmt = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, TotalTaxes = 0,  MrkTaxes = 0, MrkExtraGuest = 0;

                foreach (var roomrate in hoteldetails.Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == RoomPlanCode))
                {
                    decimal Basefare = 0, Taxes = 0, ExtraGuesttax = 0, disamt = 0;
                    RoomTypeCode = roomrate.Attribute("RoomID").Value;
                    Basefare = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value);
                    Taxes = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value);
                    if (roomrate.Element("Rates").Element("Rate").Element("AdditionalGuestAmounts") != null)
                    {
                        decimal extrarate = 0;
                        foreach (var addigust in roomrate.Elements("Rates").Elements("Rate").Elements("AdditionalGuestAmounts").Elements("AdditionalGuestAmount"))
                        {
                            extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                        }
                        ExtraGuesttax = extrarate;
                    }

                    if (roomrate.Element("Rates").Element("Rate").Element("Discount") != null)
                        disamt = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value);

                    Org_RoomRateStr += "Bs:" + Basefare + "-Tx:" + Taxes + "-EG:" + ExtraGuesttax + "-Ds" + disamt + "/";

                    AmountBeforeTax += Basefare - disamt;
                    TotalTaxes += Taxes + ExtraGuesttax;

                    MarkList = objHtlMrk.DesiyaRoomMarkupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (Basefare - disamt), SearchDetails.TG_servicetax, SearchDetails.NoofRoom);
                    TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Taxes, SearchDetails.TG_servicetax);
                    ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ExtraGuesttax, SearchDetails.TG_servicetax);

                    MrkTotalPrice += MarkList.TotelAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                    MrkTaxes += TaxMarkList.TotelAmt;
                    MrkExtraGuest += ExtraMarkList.TotelAmt;
                    adminMrkAmt += MarkList.AdminMrkAmt + TaxMarkList.AdminMrkAmt + ExtraMarkList.AdminMrkAmt;
                    AgtMrkAmt += MarkList.AgentMrkAmt + TaxMarkList.AgentMrkAmt + ExtraMarkList.AgentMrkAmt;
                    SericeTaxAmt += MarkList.AgentServiceTaxAmt + TaxMarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                    VSericeTaxAmt += MarkList.VenderServiceTaxAmt + TaxMarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;
                    MrkRoomrateStr += MarkList.TotelAmt.ToString() + "/";

                    decimal MrkDiscAmt = 0;
                    if (disamt > 0)
                    {
                        MrkDiscAmt = objHtlMrk.DiscountDesiyaRoomMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (Basefare), 0, SearchDetails.NoofRoom);
                        TotaldiscAmt += MrkDiscAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                    }
                    Disc_RoomrateStr += MrkDiscAmt.ToString() + "/";
                }

                objRoomRate.RoomTypeCode = RoomTypeCode;
                objRoomRate.RatePlanCode = RoomPlanCode;

                objRoomRate.Total_Org_Roomrate = (AmountBeforeTax + TotalTaxes);
                objRoomRate.TotalRoomrate = MrkTotalPrice;
                objRoomRate.DiscountAMT = TotaldiscAmt;
                objRoomRate.AmountBeforeTax = AmountBeforeTax;
                objRoomRate.Taxes = TotalTaxes;
                objRoomRate.ExtraGuest_Charge = MrkExtraGuest;
                objRoomRate.MrkTaxes = MrkTaxes;
                objRoomRate.Org_RoomrateBreakups = Org_RoomRateStr;
                objRoomRate.Mrk_RoomrateBreakups = MrkRoomrateStr;
                objRoomRate.Disc_RoomrateBreakups = Disc_RoomrateStr;
                objRoomRate.AdminMarkupPer = MarkList.AdminMrkPercent;
                objRoomRate.AdminMarkupAmt = adminMrkAmt;
                objRoomRate.AdminMarkupType = MarkList.AdminMrkType;
                objRoomRate.AgentMarkupPer = MarkList.AgentMrkPercent;
                objRoomRate.AgentMarkupAmt = AgtMrkAmt;
                objRoomRate.AgentMarkupType = MarkList.AgentMrkType;
                objRoomRate.ServiseTaxAmt = SericeTaxAmt;
                objRoomRate.V_ServiseTaxAmt = VSericeTaxAmt;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomRatelist_" + RoomPlanCode + "_" + SearchDetails.HtlCode);
            }
            return objRoomRate;
        }

        public HotelBooking TGHotelsPreBooking(HotelBooking HotelDetail)
        {
            HotelDetail.ProBookingID = "Not Available";
            try
            {
                if (HotelDetail.TGUrl != null)
                {
                    HotelDetail = HtlReq.ProvosinalBookingRequest(HotelDetail);
                    HotelDetail.ProBookingRes = TGPostXml(HotelDetail.TGUrl, HotelDetail.ProBookingReq);

                    if (HotelDetail.ProBookingRes.Contains("<Success />") || HotelDetail.ProBookingRes.Contains("<Success/>"))
                    {
                        string proresp = HotelDetail.ProBookingRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                        XDocument xmlresp = XDocument.Parse(proresp);

                        var hotelprice = (from htlprice in xmlresp.Descendants("HotelReservation").Descendants("UniqueID")
                                          select new { proid = htlprice.Attribute("ID").Value }).ToList();

                        HotelDetail.ProBookingID = hotelprice[0].proid;
                    }
                    else
                    {
                        HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                    }
                }
                else
                {
                    HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsPreBooking");
                HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
            }
            return HotelDetail;
        }
        public HotelBooking TGHotelsBooking(HotelBooking SearchDetails)
        {
            try
            {
                SearchDetails = HtlReq.BookingRequest(SearchDetails);
                SearchDetails.BookingConfRes = TGPostXml(SearchDetails.TGUrl, SearchDetails.BookingConfReq);

                if (SearchDetails.BookingConfRes.Contains("<Success />") || SearchDetails.BookingConfRes.Contains("<Success/>"))
                {
                    string proresp = SearchDetails.BookingConfRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(proresp);
                    var Bookingss = (from booking in xmlresp.Descendants("HotelReservation")
                                     select new
                                     {
                                         Bookingid = booking.Element("UniqueID"),
                                         Contact = booking.Element("RoomStays").Element("RoomStay").Element("BasicPropertyInfo").Element("ContactNumbers")
                                     }).ToList();

                    foreach (var htls in Bookingss)
                    {
                        SearchDetails.BookingID = htls.Bookingid.Attribute("ID").Value;
                        string contactno = "";
                        foreach (var contactdtl in htls.Contact.Elements("ContactNumber"))
                        {
                            if (contactdtl.Attribute("PhoneNumber").Value != "")
                                contactno += contactdtl.Attribute("CountryAccessCode").Value + " -" + contactdtl.Attribute("AreaCityCode").Value + "  " + contactdtl.Attribute("PhoneNumber").Value + "   /  ";
                        }
                        SearchDetails.HotelContactNo = contactno;
                        SearchDetails.Status = HotelStatus.Confirm.ToString();
                    }
                }
                else
                {
                    SearchDetails.HotelContactNo = "Exception"; SearchDetails.BookingID = ""; 
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsBooking");
                SearchDetails.HotelContactNo = ex.Message;
                SearchDetails.BookingID = "";
            }
            return SearchDetails;
        }

        public HotelCancellation TGHotelsCancelation(HotelCancellation HotelDetail)
        {
            HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
            try
            {
                HotelDetail = HtlReq.CancellationRequest(HotelDetail);
                HotelDetail.BookingCancelRes = TGPostXml(HotelDetail.HotelUrl, HotelDetail.BookingCancelReq);

                if (HotelDetail.BookingCancelRes.Contains("<Success />") || HotelDetail.BookingCancelRes.Contains("<Success/>"))
                {
                    string proresp = HotelDetail.BookingCancelRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(proresp);

                    var hotelcancle = (from htlprice in xmlresp.Descendants("OTA_CancelRS") select new { Cancellation = htlprice}).ToList();
                    foreach (var Hotelcan in hotelcancle)
                    {
                        HotelDetail.CancelStatus = Hotelcan.Cancellation.Attribute("Status").Value;
                        HotelDetail.CancellationID = Hotelcan.Cancellation.Element("CancelInfoRS").Element("UniqueID").Attribute("ID").Value;
                        HotelDetail.CancellationCharge = Convert.ToDecimal(Hotelcan.Cancellation.Element("CancelInfoRS").Element("CancelRules").Element("CancelRule").Attribute("Amount").Value);
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsCancelation");
            }
            return HotelDetail;
        }

        protected string SetHotelService_Image(string HotelCode)
        {
            HotelDA objhtlDa = new HotelDA();
            string InclImg = ""; int i = 0, j = 0, k = 0, l = 0, m = 0, n = 0,p=0,r=0;
            try
            {  
                DataTable HtlServices = objhtlDa.GetHotelServices(HotelCode, "property");
                if (HtlServices.Rows.Count == 0)
                    InclImg = "&nbsp;";
                foreach (DataRow Services in HtlServices.Rows)
                {
                    switch (Services["Amenity_id"].ToString().Trim())
                    {
                        //case "01":
                        //case "344":
                        //    if (n == 0)
                        //    {
                        //        InclImg += "<img src='../Hotel/Images/Facility/travel_desk.png' title='Tagency Help Desk' class='IconImageSize' /><span class='hide'>Travel & Transfers<span>";
                        //        n = 0;
                        //    }
                        //    break;
                        case "344":
                        case "01":
                        case "03":
                        case "04":
                            if (i == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Car rental facilities' class='IconImageSize' /><span class='hide'>Travel & Transfers</span>";
                                i = 1;
                            }
                            break;
                        //case "08":
                        //    InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Banquet hall' class='IconImageSize' />"; Travel & Transfers
                        //    break;
                        case "09":
                            InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' class='IconImageSize' /><span class='hide'>Restaurant/Bar</span>";
                            break;
                        case "11":
                            InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' class='IconImageSize' /><span class='hide'>Beauty Porlour</span>";
                            break;
                        case "22":
                            InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby sitting' class='IconImageSize' /><span class='hide'>Baby Sitting</span>";
                            break;
                        case "08":
                        case "24":
                            if (p == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' class='IconImageSize' /><span class='hide'>Business Facilities</span>";
                                p=1;
                            }
                            break;
                        case "26":
                            InclImg += "<img src='../Hotel/Images/Facility/Phone.png' title='Direct dial phone' class='IconImageSize' /><span class='hide'>Phone</span>";
                            break;
                        case "30":
                        case "32":
                            if (n == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/breakfast.png' title='Tea/Coffee' class='IconImageSize' /><span class='hide'>Tea/Coffee</span>";
                                n = 1;
                            }
                            break;
                        case "31":
                        case "44":
                        case "65":
                            if (j == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' class='IconImageSize' /><span class='hide'>lobby</span>";
                                j = 1;
                            }
                            break;
                        case "40":
                            InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lifts' class='IconImageSize' /><span class='hide'>Lift</span>";
                            break;
                        case "52":
                            InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gym' class='IconImageSize' /><span class='hide'>Gym</span>";
                            break;
                        case "54":
                        case "55":
                        case "56":
                        case "57":
                        case "58":
                            if (k == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/wifi.gif' title='Internet/wifi' class='IconImageSize' /><span class='hide'>Internet/Wi-Fi</span>";
                                k = 1;
                            }
                            break;
                        case "59":
                            InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry services' class='IconImageSize' /><span class='hide'>Laundry Services</span>";
                            break;
                        case "71":
                        case "72":
                        case "73":
                        case "74":
                        case "75":
                            if (l == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' class='IconImageSize' /><span class='hide'>Parking</span>";
                                l = 1;
                            }
                            break;
                        case "88":
                            InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Spa/Massage/Wellness' class='IconImageSize' /><span class='hide'>Spa/Massage/Wellness/Sauna</span>";
                            break;
                        case "122":
                        case "360":
                            if (r == 0)
                            {
                            InclImg += "<img src='../Hotel/Images/Facility/AC.png' title='AC' class='IconImageSize' /><span class='hide'>AC</span>";
                            r=1;
                            }
                                break;
                        case "126":
                        case "325":
                        case "131":
                            if (m == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' class='IconImageSize' /><span class='hide'>TV</span>";
                                m = 1;
                            }
                            break;
                        case "334":
                            InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' class='IconImageSize' /><span class='hide'>Disabled Facilities</span>";
                            break;
                        case "338":
                            InclImg += "<img src='../Hotel/Images/Facility/golf.png' title='Golf' class='IconImageSize' /><span class='hide'>Sports</span>";
                            break;
                        case "345":
                            InclImg = "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' class='IconImageSize' /><span class='hide'>Swimming Pool</span>";
                            break;
                        case "355":
                            InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' class='IconImageSize' /><span class='hide'>Tub Bath</span>";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelService_TG");
            }
            return InclImg;
        }

        public List<HotelResult> Promotion_Hotels(HotelSearch SearchDetails)
        {
            //HotelComposite obgHotelsList = new HotelComposite();
            List<HotelResult> objHotellist = new List<HotelResult>();
            try
            {
                SearchDetails = HtlReq.Promotion_HotelSearchRequest(SearchDetails);
                SearchDetails.TG_HotelSearchRes = TGPostXml(SearchDetails.TGUrl, SearchDetails.TG_HotelSearchReq);
                objHotellist = Promotion_GetTGResult(SearchDetails.TG_HotelSearchRes, SearchDetails);
                //obgHotelsList.HotelSearchDetail = SearchDetails;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "Promotion_Hotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.TG_HotelSearchRes = ex.Message;
                objHotellist.Add(new HotelResult { HtlError = ex.Message });
            }
            return objHotellist;
        }
        protected List<HotelResult> Promotion_GetTGResult(string HotelResponse, HotelSearch SearchDetails)
        {
            List<HotelResult> objHotellist = new List<HotelResult>(); HotelDA objhtlDa = new HotelDA();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            try
            {
                if (HotelResponse.Contains("<Success />") || HotelResponse.Contains("<Success/>"))
                {
                    string responses = HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new { BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions") }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        DataTable HotelDt = objhtlDa.GetHotelOverview(SearchDetails.SearchCity, "");
                        foreach (var hoteldetails in hotelprice)
                        {
                            string HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                            DataRow[] HotelFilterArray = objHtlMrk.FilterHotelDT(HotelDt, HotelCode);
                            if (HotelFilterArray.Length > 0)
                            {
                                var HotelsData = hoteldetails.BasicPropertyInfo.Parent;
                                if (HotelsData.Element("RoomRates").Element("RoomRate") != null && HotelsData.Element("RatePlans").Element("RatePlan") != null)
                                {
                                    try
                                    {
                                        decimal baserate = 0, taxs = 0, disamt = 0, ExtraGuest = 0, TotaldiscAmt = 0;
                                        foreach (var htldtl in HotelsData.Descendants("RoomRates").Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value))
                                        {
                                            foreach (var htl in htldtl.Descendants("Rates"))
                                            {
                                                baserate += Convert.ToDecimal(htl.Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value.Trim());
                                                if (htl.Element("Rate").Element("Base").Element("Taxes") != null)
                                                    taxs += Convert.ToDecimal(htl.Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value.Trim());

                                                if (htl.Element("Rate").Element("Discount") != null)
                                                    disamt += Convert.ToDecimal(htl.Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value.Trim());

                                                if (htl.Element("Rate").Element("AdditionalGuestAmounts") != null)
                                                {
                                                    decimal extrarate = 0;
                                                    foreach (var addigust in htl.Elements("Rate").Elements("AdditionalGuestAmounts").Descendants("AdditionalGuestAmount"))
                                                    {
                                                        extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                                                    }
                                                    ExtraGuest += extrarate;
                                                }
                                            }
                                        }
                                        MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, (baserate + taxs + ExtraGuest - disamt) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.servicetax);
                                        if (disamt > 0)
                                            TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (baserate + taxs + ExtraGuest) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.servicetax);
                                        HotelRatePlan objRatePlans = new HotelRatePlan();
                                        objRatePlans = RatePlanlist(hoteldetails.BasicPropertyInfo, hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value);
                                        objHotellist.Add(new HotelResult
                                        {
                                            RatePlanCode = hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value,
                                            hotelDiscoutAmt = TotaldiscAmt,
                                            hotelPrice = MarkList.TotelAmt - MarkList.AgentMrkAmt,
                                            AgtMrk = 0,
                                            DiscountMsg = objRatePlans.discountMsg,
                                            inclusions = "",
                                            RoomTypeCode = objRatePlans.RatePlanCode,
                                            HotelCode = HotelCode,
                                            HotelName = HotelFilterArray[0]["VendorName"].ToString().Trim().Replace("'", ""),
                                            HotelCityCode = SearchDetails.SearchCityCode,
                                            HotelCity = HotelFilterArray[0]["City"].ToString().Trim(),
                                            StarRating = HotelFilterArray[0]["Hotel_Star"].ToString().Trim(),
                                            HotelAddress = "",
                                            HotelDescription = "",
                                            HotelThumbnailImg = HotelFilterArray[0]["ImagePath"].ToString().Trim(),
                                            Lati_Longi = "",
                                            Location = "",
                                            HotelServices = "",
                                            ReviewRating = "",
                                            Provider = "TG",
                                            PopulerId =0
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                        HotelDA.InsertHotelErrorLog(ex, "GetTGResult_List" + "-" + HotelCode);
                                        objHotellist.Add(new HotelResult { HtlError = ex.Message });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HtlError = "Hotel not found,  Please modify your search.. " });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                    if (HotelResponse.Contains("<Errors>"))
                    {
                        XDocument document = XDocument.Parse(HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                        var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                        if (Hotels_temps.Count > 0)
                        {
                            if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                        }
                    }
                    else
                    {
                        if (HotelResponse.Contains("<p>"))
                        {
                            string Serviceerror = HotelResponse.Substring(HotelResponse.IndexOf("<p>"), HotelResponse.IndexOf("</p>") - HotelResponse.IndexOf("<p>") - 2);
                            objHotellist.Add(new HotelResult { HtlError = Serviceerror });
                        }
                        else
                            objHotellist.Add(new HotelResult { HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGResult" + "_" + SearchDetails.SearchCity);
                objHotellist.Add(new HotelResult { HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            return objHotellist;
        }
    }
}
