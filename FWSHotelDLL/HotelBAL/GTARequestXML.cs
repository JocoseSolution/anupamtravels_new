﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelShared;
using System.Data;
using System.Collections;
namespace HotelBAL
{
  public class GTARequestXML
    {
        public HotelSearch SeachHotelPriceRequestXml(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Country='IN' Currency='USD' Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchHotelPriceRequest>");
                if (SearchDetails.SearchType == "AREA")
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.SearchCityCode + "' DestinationType='area'/>");
                else
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.SearchCityCode + "' DestinationType='city'/>");
                ReqXml.Append("<ImmediateConfirmationOnly />");
                if (!string.IsNullOrEmpty(SearchDetails.HotelName))
                    ReqXml.Append("<ItemName>" + SearchDetails.HotelName + "</ItemName>");

                ReqXml.Append("<PeriodOfStay>");
                ReqXml.Append("<CheckInDate>" + SearchDetails.CheckInDate + "</CheckInDate> <Duration>" + SearchDetails.NoofNight + "</Duration>");
                ReqXml.Append("</PeriodOfStay>");
                ReqXml.Append("<IncludeRecommended/><IncludePriceBreakdown/>");

                ReqXml.Append("<IncludeChargeConditions DateFormatResponse='true' />");
                ReqXml.Append("<Rooms>");
                //set room information
                ReqXml.Append(SetHotelRoomPaxData(SearchDetails, " "));
                ReqXml.Append("</Rooms>");

                if (SearchDetails.StarRating !="0")
                    ReqXml.Append("<StarRating MinimumRating='false'>" + SearchDetails.StarRating + "</StarRating>");

                ReqXml.Append("<OrderBy>pricelowtohigh</OrderBy>");
                ReqXml.Append("</SearchHotelPriceRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SeachHotelPriceRequestXml");
            }
            SearchDetails.GTA_HotelSearchReq = ReqXml.ToString();
            return SearchDetails;
        }
        public string SetHotelRoomPaxData(HotelSearch SearchDetails, string Roomid)
        {
            int Cots = 0, extraRoom = 0; string ExtraRoomType = "";
            StringBuilder RoomXml = new StringBuilder();
            try
            {
                for (int i = 0; i <= SearchDetails.NoofRoom - 1; i++)
                {
                    switch (Convert.ToInt32(SearchDetails.AdtPerRoom[i]))
                    {
                        #region One Adult
                        case 1:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("<Room Code='SB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //checking infant and child both are There or only infant
                                if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                {
                                    RoomXml.Append("<Room Code='TS' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                    Cots += j;
                                }
                                else
                                {
                                    RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' ");
                                    if (j > 0)
                                    {
                                        //Asing No of cots for infant
                                        RoomXml.Append(" NumberOfCots='" + j + "'");
                                        Cots += j;
                                    }
                                    if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - j == 1)
                                    {
                                        RoomXml.Append("><ExtraBeds/>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (SearchDetails.ChdAge[i, ii] > 2)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                        #region Two Adult
                        case 2:
                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' ");
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }

                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    RoomXml.Append(" NumberOfCots='" + j + "'");
                                    Cots += j;
                                }
                                //checking infant and child both are There or only infant
                                if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                {
                                    RoomXml.Append("><ExtraBeds/>");
                                }
                                else
                                {
                                    RoomXml.Append("><ExtraBeds>");
                                    for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                    {
                                        if (SearchDetails.ChdAge[i, ii] > 2)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                    }
                                    RoomXml.Append("</ExtraBeds>");
                                }
                                RoomXml.Append("</Room>");  //extraRoom = extraRoom + 1;
                            }
                            break;
                        #endregion
                        #region Three Adult
                        case 3:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                RoomXml.Append("<Room Code='TR' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    //checking infant and child both are There or only infant
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                    {
                                        RoomXml.Append("<Room Code='TR' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                        Cots += j;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - j == 1)
                                        {
                                            RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                            Cots += j;
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/></Room>");
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                            for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                            {
                                                if (SearchDetails.ChdAge[i, ii] > 2)
                                                {
                                                    RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                                }
                                            }
                                            RoomXml.Append("</ExtraBeds>");
                                            Cots += j;
                                        }
                                    }
                                }
                                else
                                {
                                    bool moreChild = false;
                                    //only adult and  child  are There
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 1))
                                    {
                                        RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1'><ExtraBeds/>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (ii > 1)
                                            {
                                                moreChild = true;
                                                break;
                                            }
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds></Room>");
                                        if (moreChild == true)
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                            for (int ii = 2; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                            RoomXml.Append("</ExtraBeds>");
                                            ExtraRoomType = "Double Bed Room";
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='SB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/>");
                                            ExtraRoomType = "Single Bed Room";
                                        }
                                        extraRoom = extraRoom + 1;
                                    }
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                        #region Four Adult
                        case 4:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                RoomXml.Append("<Room Code='Q'  " + Roomid + "  NumberOfRooms='1'><ExtraBeds/></Room>");
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    //checking infant and child both are There or only infant
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                    {
                                        if (j <= 2)
                                        {
                                            RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                            Cots += j;
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='2'><ExtraBeds/></Room>");
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + (j - 2).ToString() + "'><ExtraBeds/>");
                                            extraRoom = extraRoom + 1;
                                            Cots += 2 + j - 2;
                                            ExtraRoomType = "Double Bed Room";
                                        }
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (SearchDetails.ChdAge[i, ii] > 2)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                        Cots += j;
                                        extraRoom = extraRoom + 1;
                                        ExtraRoomType = "Double Bed Room";
                                    }
                                }
                                else
                                {
                                    //only adult and  child  are There  
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) <= 2))
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            if (ii == 1)
                                            {
                                                break;
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 2; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                    extraRoom = extraRoom + 1;
                                    ExtraRoomType = "Double Bed Room";
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                    }
                }
                SearchDetails.ExtraCot = Cots.ToString();
                SearchDetails.ExtraRoom =  extraRoom;
                SearchDetails.ExtraRoomType = ExtraRoomType;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelRoomPaxData");
            }
            return RoomXml.ToString();
        }
        public HotelSearch SearchItemInformationRequest(HotelSearch SearchDetails, string itemCode, string CityCode)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en' Currency='USD'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchItemInformationRequest ItemType='hotel'>");
                ReqXml.Append("<ItemDestination DestinationType='city' DestinationCode='" + CityCode + "' />");
                //if (SearchDetails.SearchType == "AREA")
                //    ReqXml.Append("<ItemDestination DestinationType='area' DestinationCode='" + CityCode + "' />");
                //else
                //    ReqXml.Append("<ItemDestination DestinationType='city' DestinationCode='" + CityCode + "' />");
                ReqXml.Append("<ItemCode>" + itemCode + "</ItemCode>");
                ReqXml.Append("</SearchItemInformationRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            SearchDetails.GTAItemInformationReq = ReqXml.ToString();
            return SearchDetails;
        }
        public HotelSearch SearchChargeConditionsRequest(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                if (SearchDetails.SearchType == "AREA")
                    ReqXml.Append("<RequestorPreferences Language='en' Currency='USD'>");
                else
                    ReqXml.Append("<RequestorPreferences Country='" + SearchDetails.CountryCode + "' Language='en' Currency='USD'>");

                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchChargeConditionsRequest>");
                ReqXml.Append("<DateFormatResponse/>");
                ReqXml.Append("<ChargeConditionsHotel>");
                ReqXml.Append("<City>" + SearchDetails.HotelCityCode + "</City>");
                ReqXml.Append("<Item>" + SearchDetails.HtlCode + "</Item>");
                ReqXml.Append("<PeriodOfStay>");
                ReqXml.Append("<CheckInDate>" + SearchDetails.CheckInDate + "</CheckInDate> <Duration>" + SearchDetails.NoofNight + "</Duration>");
                ReqXml.Append("</PeriodOfStay>");
                ReqXml.Append("<Rooms>");
                //<Room Code='DB' Id ='001:WIN1:11010:S10825:12604:104013' NumberOfRooms='1' ><ExtraBeds/></Room></Rooms>

                string roomid = " Id = '" + SearchDetails.HtlRoomCode + "'";
                ReqXml.Append(SetHotelRoomPaxData(SearchDetails, roomid));
                ReqXml.Append("</Rooms>");
                ReqXml.Append("</ChargeConditionsHotel>");
                ReqXml.Append("</SearchChargeConditionsRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            SearchDetails.GTAPolicyReq = ReqXml.ToString();
            return SearchDetails;
        }

        public HotelBooking PreBookHotelPriceRequestXml(HotelBooking SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Country='IN' Currency='USD' Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchHotelPriceRequest>");
                if (SearchDetails.SearchType == "AREA")
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.CityCode + "' DestinationType='area'/>");
                else
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.CityCode + "' DestinationType='city'/>");
                //if (SearchDetails.SearchType == "AREA")
                //{
                //    ReqXml.Append("<RequestorPreferences Currency='USD' Language='en'>");
                //    ReqXml.Append(SearchDetails.GTARequestMode);
                //    ReqXml.Append("<SearchHotelPriceRequest>");
                //    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.CityCode + "' DestinationType='area'/>");
                //}
                //else
                //{
                //    ReqXml.Append("<RequestorPreferences Country='" + SearchDetails.CountryCode + "' Currency='USD' Language='en'>");
                //    ReqXml.Append(SearchDetails.GTARequestMode);
                //    ReqXml.Append("<SearchHotelPriceRequest>");
                //    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.CityCode + "' DestinationType='city'/>");
                //}
                ReqXml.Append("<ImmediateConfirmationOnly />");
                ReqXml.Append("<ItemCode>" + SearchDetails.HtlCode + "</ItemCode>");
                ReqXml.Append("<PeriodOfStay>");
                ReqXml.Append("<CheckInDate>" + SearchDetails.CheckInDate + " </CheckInDate> <Duration>" + SearchDetails.NoofNight + "</Duration>");
                ReqXml.Append("</PeriodOfStay><Rooms>");
                //set room information
                string roomid = " Id ='" + SearchDetails.RoomPlanCode + "'";
                ReqXml.Append(SetHotelBookingRoomPaxData(SearchDetails, roomid));
                ReqXml.Append("</Rooms>");
                ReqXml.Append("</SearchHotelPriceRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            SearchDetails.ProBookingReq = ReqXml.ToString();
            return SearchDetails;
        }
        public string SetHotelBookingRoomPaxData(HotelBooking SearchDetails, string Roomid)
        {
            int Cots = 0, extraRoom = 0; string ExtraRoomType = "";
            StringBuilder RoomXml = new StringBuilder();
            try
            {
                for (int i = 0; i <= SearchDetails.NoofRoom - 1; i++)
                {
                    switch (Convert.ToInt32(SearchDetails.AdtPerRoom[i]))
                    {
                        #region One Adult
                        case 1:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("<Room Code='SB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //checking infant and child both are There or only infant
                                if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                {
                                    RoomXml.Append("<Room Code='TS' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                    Cots += j;
                                }
                                else
                                {
                                    RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' ");
                                    if (j > 0)
                                    {
                                        //Asing No of cots for infant
                                        RoomXml.Append(" NumberOfCots='" + j + "'");
                                        Cots += j;
                                    }
                                    if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - j == 1)
                                    {
                                        RoomXml.Append("><ExtraBeds/>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (SearchDetails.ChdAge[i, ii] > 2)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                        #region Two Adult
                        case 2:
                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' ");
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }

                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    RoomXml.Append(" NumberOfCots='" + j + "'");
                                    Cots += j;
                                }
                                //checking infant and child both are There or only infant
                                if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                {
                                    RoomXml.Append("><ExtraBeds/>");
                                }
                                else
                                {
                                    RoomXml.Append("><ExtraBeds>");
                                    for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                    {
                                        if (SearchDetails.ChdAge[i, ii] > 2)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                    }
                                    RoomXml.Append("</ExtraBeds>");
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                        #region Three Adult
                        case 3:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("<Room Code='TR' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    //checking infant and child both are There or only infant
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                    {
                                        RoomXml.Append("<Room Code='TR' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                        Cots += j;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - j == 1)
                                        {
                                            RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                            Cots += j;
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/></Room>");
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                            for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                            {
                                                if (SearchDetails.ChdAge[i, ii] > 2)
                                                {
                                                    RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                                }
                                            }
                                            RoomXml.Append("</ExtraBeds>");
                                            Cots += j;
                                        }
                                    }
                                }
                                else
                                {
                                    bool moreChild = false;
                                    //only adult and  child  are There
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 1))
                                    {
                                        RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1'><ExtraBeds/>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (ii > 1)
                                            {
                                                moreChild = true;
                                                break; 
                                            }
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds></Room>");
                                        if (moreChild == true)
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                            for (int ii = 2; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                            RoomXml.Append("</ExtraBeds>");
                                            ExtraRoomType = "DB";
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='SB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/>");
                                            ExtraRoomType = "SB";
                                        }
                                        extraRoom = extraRoom + 1;
                                    }
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                        #region Four Adult
                        case 4:
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                            {
                                RoomXml.Append("<Room Code='Q'  " + Roomid + "  NumberOfRooms='1'><ExtraBeds/></Room>");
                            }
                            else
                            {
                                int j = 0;
                                //calculation for Extra cots for infant
                                for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                                {
                                    if (SearchDetails.ChdAge[i, l] <= 2)
                                    {
                                        j = j + 1;
                                    }
                                }
                                //Asing No of cots for infant
                                if (j > 0)
                                {
                                    //checking infant and child both are There or only infant
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == j))
                                    {
                                        if (j <= 2)
                                        {
                                            RoomXml.Append("<Room Code='Q' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/>");
                                            Cots += j;
                                        }
                                        else
                                        {
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='2'><ExtraBeds/></Room>");
                                            RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + (j - 2).ToString() + "'><ExtraBeds/>");
                                            extraRoom = extraRoom + 1;
                                            Cots += 2 + j - 2;
                                            ExtraRoomType = "DB";
                                        }
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1' NumberOfCots='" + j + "'><ExtraBeds/></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            if (SearchDetails.ChdAge[i, ii] > 2)
                                            {
                                                RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                        Cots += j;
                                        extraRoom = extraRoom + 1;
                                        ExtraRoomType = "DB";
                                    }
                                }
                                else
                                {
                                    //only adult and  child  are There  
                                    if ((Convert.ToInt32(SearchDetails.ChdPerRoom[i]) <= 2))
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds/></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                    else
                                    {
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 0; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                            if (ii == 1)
                                            {
                                                break;
                                            }
                                        }
                                        RoomXml.Append("</ExtraBeds></Room>");
                                        RoomXml.Append("<Room Code='DB' " + Roomid + " NumberOfRooms='1'><ExtraBeds>");
                                        for (int ii = 2; ii <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; ii++)
                                        {
                                            RoomXml.Append("<Age>" + SearchDetails.ChdAge[i, ii] + "</Age>");
                                        }
                                        RoomXml.Append("</ExtraBeds>");
                                    }
                                    extraRoom = extraRoom + 1;
                                    ExtraRoomType = "DB";
                                }
                                RoomXml.Append("</Room>");
                            }
                            break;
                        #endregion
                    }
                }
                SearchDetails.ExtraCot = Cots.ToString();
               SearchDetails.ExtraRoom = extraRoom;
               // SearchDetails.ExtraRoomType = ExtraRoomType;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            return RoomXml.ToString();
        }
        public HotelBooking GtaHotelBookingReuestXML(HotelBooking SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Country='IN' Currency='USD' Language='en'>");
                //if ((SearchDetails.SearchType == "AREA"))
                //    ReqXml.Append("<RequestorPreferences Currency='USD' Language='en'>");
                //else
                //    ReqXml.Append("<RequestorPreferences Country='" + SearchDetails.CountryCode + "' Currency='USD' Language='en'>");
                ReqXml.Append("<RequestMode>SYNCHRONOUS</RequestMode>");
                ReqXml.Append("</RequestorPreferences>");
                ReqXml.Append("</Source><RequestDetails>");
                ReqXml.Append("<AddBookingRequest Currency='USD'>");
                ReqXml.Append("<BookingName>" + SearchDetails.HotelName + "</BookingName>");
                ReqXml.Append("<BookingReference>" + SearchDetails.Orderid + "</BookingReference>");
                ReqXml.Append("<BookingDepartureDate>" + SearchDetails.CheckInDate + "</BookingDepartureDate> ");
                ReqXml.Append("<PaxNames>");
                try
                {
                    ReqXml.Append("<PaxName PaxId='1'>" + SearchDetails.PGTitle + " " + SearchDetails.PGFirstName + " " + SearchDetails.PGLastName + "</PaxName>");
                    //Additinal Gest 
                    if (SearchDetails.AdditinalGuest != null)
                    {
                        ArrayList list = new ArrayList();
                        list = SearchDetails.AdditinalGuest;
                        for (int i = 0; i <= list.Count - 1; i++)
                        {
                            ArrayList li = new ArrayList();
                            li = (ArrayList)list[i];
                            if (li[3].ToString() == "Adult")
                                ReqXml.Append("<PaxName PaxId='" + (i + 2).ToString() + "'>" + li[0] + " " + li[1] + " " + li[2] + "</PaxName>");
                            else
                                ReqXml.Append("<PaxName PaxId='" + (i + 2).ToString() + "' PaxType='child' ChildAge='" + li[4] + "'>" + li[0] + " " + li[1] + " " + li[2] + "</PaxName>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
                }
                ReqXml.Append("</PaxNames><BookingItems>");
                ReqXml.Append("<BookingItem ItemType='hotel'> <ItemReference>1</ItemReference>");
                ReqXml.Append("<ItemCity Code='" + SearchDetails.CityCode+ "' /> <Item Code='" + SearchDetails.HtlCode + "' />");
                ReqXml.Append("<HotelItem> <PeriodOfStay> <CheckInDate>" + SearchDetails.CheckInDate + " </CheckInDate>");
                ReqXml.Append("<CheckOutDate>" + SearchDetails.CheckOutDate + " </CheckOutDate></PeriodOfStay>");
                ReqXml.Append("<HotelRooms>");
                try
                {
                    int paxid = 1;
                    for (int i = 0; i <= SearchDetails.NoofRoom - 1; i++)
                    {
                        int k = 0;
                        int j = 0;
                        //calculation Number of infant
                        for (int l = 0; l <= Convert.ToInt32(SearchDetails.ChdPerRoom[i]) - 1; l++)
                        {
                            if (SearchDetails.ChdAge[i, l] <= 2)
                            {
                                k = k + 1;
                            }
                            else
                            {
                                j = j + 1;
                            }
                        }
                        switch (Convert.ToInt32(SearchDetails.AdtPerRoom[i]))
                        {
                        #region One Adult
                            case 1:
                                if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                {
                                    ReqXml.Append("<HotelRoom Code='SB' Id='" + SearchDetails.RoomPlanCode + "'>");
                                    ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                    paxid = paxid + 1;
                                }
                                else
                                {
                                    //Asing No of cots for infant
                                    if (k > 0)
                                    {
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == k)
                                        {
                                            ReqXml.Append("<HotelRoom Code='TS' Id='" + SearchDetails.RoomPlanCode + "'  NumberOfCots='" + k + "'>");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            paxid = paxid + 1;
                                        }
                                        else
                                        {
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "' >");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            paxid = paxid + 2;
                                        }
                                    }
                                    else
                                    {
                                        ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ");
                                        if (j == 1)
                                        {
                                            ReqXml.Append(">");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            paxid = paxid + 2;
                                        }
                                        else
                                        {
                                            ReqXml.Append(" ExtraBed='true' NumberOfExtraBeds ='" + (j - 1).ToString() + "'>");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            paxid = paxid + 3;
                                        }
                                    }
                                }
                                ReqXml.Append("</PaxIds></HotelRoom>");
                                break;
                                #endregion
                        #region Two Adult
                            case 2:
                                if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                {
                                    ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "'>");
                                    ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                    paxid = paxid + 2;
                                }
                                else
                                {
                                    ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "'");
                                    if (k > 0)
                                    {
                                        ReqXml.Append(" NumberOfCots='" + k + "'");
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == k)
                                        {
                                            ReqXml.Append(">");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            paxid = paxid + 2;
                                        }
                                        else
                                        {
                                            ReqXml.Append(" ExtraBed='true' NumberOfExtraBeds ='" + j + "' >");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            paxid = paxid + 3;
                                        }
                                    }
                                    else
                                    {
                                        ReqXml.Append(" ExtraBed='true' NumberOfExtraBeds ='" + j + "' >");
                                        ReqXml.Append("<PaxIds>");
                                        ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                        ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                        ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                        paxid = paxid + 3;
                                        if (j == 2)
                                        {
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            paxid = paxid + 1;
                                        }
                                    }
                                }
                                ReqXml.Append("</PaxIds></HotelRoom>");
                                break;
                        #endregion
                            #region Three Adult
                            case 3:
                                if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                {
                                    ReqXml.Append("<HotelRoom Code='TR' Id='" + SearchDetails.RoomPlanCode + "'>");
                                    ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                    paxid = paxid + 3;
                                    ReqXml.Append("</PaxIds></HotelRoom>");
                                }
                                else
                                {
                                    if (k > 0)
                                    {
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == k)
                                        {
                                            ReqXml.Append("<HotelRoom Code='TR' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            paxid = paxid + 3;
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                        else
                                        {
                                            if (j == 1)
                                            {
                                                ReqXml.Append("<HotelRoom Code='Q' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                                ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                                paxid = paxid + 4;
                                            }
                                            else
                                            {
                                                ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                                ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                                ReqXml.Append("</PaxIds></HotelRoom>");
                                                ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='" + j + "'>");
                                                ReqXml.Append("<PaxIds>");
                                                ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 4).ToString() + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + (paxid + 5).ToString() + "</PaxId>");
                                                paxid = paxid + 6;
                                            }
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                    }
                                    else
                                    {
                                        if (j == 1)
                                        {
                                            ReqXml.Append("<HotelRoom Code='Q' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            paxid = paxid + 4;
                                        }
                                        else if (j == 2)
                                        {
                                            ReqXml.Append("<HotelRoom Code='SB' Id='" + SearchDetails.RoomPlanCode + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='2'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 4).ToString() + "</PaxId>");
                                            paxid = paxid + 5;
                                        }
                                        else
                                        {
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='2'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            int lastadult = paxid + 2;
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 4).ToString() + "</PaxId>");
                                            paxid = paxid + 5;
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ");
                                            if (j == 3)
                                            {
                                                ReqXml.Append(">");
                                                ReqXml.Append("<PaxIds><PaxId>" + lastadult + "</PaxId>");
                                                ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                                paxid = paxid + 1;
                                            }
                                            else
                                            {
                                                ReqXml.Append(" ExtraBed='true' NumberOfExtraBeds ='" + (j - 2).ToString() + "'>");
                                                ReqXml.Append("<PaxIds><PaxId>" + lastadult + "</PaxId>");
                                                for (int n = 3; n <= j; n++)
                                                {
                                                    ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                                    paxid = paxid + 1;
                                                }
                                            }
                                            //ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                        ReqXml.Append("</PaxIds></HotelRoom>");
                                    }
                                }
                                break;
                            #endregion
                            #region Four Adult
                            case 4:
                                if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 0)
                                {
                                    ReqXml.Append("<HotelRoom Code='Q' Id='" + SearchDetails.RoomPlanCode + "'>");
                                    ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                    ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                    ReqXml.Append("</PaxIds></HotelRoom>");
                                    paxid = paxid + 4;
                                }
                                else
                                {
                                    if (k > 0)
                                    {
                                        if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == k)
                                        {
                                            ReqXml.Append("<HotelRoom Code='Q' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            paxid = paxid + 4;
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                        else
                                        {
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' NumberOfCots='" + k + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='" + j + "'>");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            paxid = paxid + 4;
                                            for (int n = 1; n <= j; n++)
                                            {
                                                ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                                paxid = paxid + 1;
                                            }
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                    }
                                    else
                                    {
                                        if (j <= 2)
                                        {
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "'>");
                                            ReqXml.Append("<PaxIds>");
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='" + j + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + (paxid + 2).ToString() + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 3).ToString() + "</PaxId>");
                                            paxid = paxid + 4;
                                            for (int n = 1; n <= j; n++)
                                            {
                                                ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                                paxid = paxid + 1;
                                            }
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                        else
                                        {
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='2'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            int lastadult = paxid + 2;
                                            paxid = paxid + 4;
                                            ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + (paxid + 1).ToString() + "</PaxId>");
                                            paxid = paxid + 2;
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                            ReqXml.Append("<HotelRoom Code='DB' Id='" + SearchDetails.RoomPlanCode + "' ExtraBed='true' NumberOfExtraBeds ='" + (j - 2).ToString() + "'>");
                                            ReqXml.Append("<PaxIds><PaxId>" + lastadult + "</PaxId>");
                                            ReqXml.Append("<PaxId>" + lastadult + 1 + "</PaxId>");
                                            for (int n = 3; n <= j; n++)
                                            {
                                                ReqXml.Append("<PaxId>" + paxid + "</PaxId>");
                                                paxid = paxid + 1;
                                            }
                                            ReqXml.Append("</PaxIds></HotelRoom>");
                                        }
                                    }
                                }
                                break;
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    HotelDAL.HotelDA.InsertHotelErrorLog(ex, "GtaHotelBookingReuestXML");
                }
                ReqXml.Append("</HotelRooms></HotelItem></BookingItem></BookingItems></AddBookingRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
                SearchDetails.BookingConfReq = ReqXml.ToString();
            }
            catch (Exception ex)
            {
                SearchDetails.BookingConfReq = "";
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "GtaHotelBookingReuestXML");
            }
            return SearchDetails;
        }
        //Hotel Download Request
        public string ItemInformationDownloadRequest(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append("<RequestMode>SYNCHRONOUS</RequestMode>");
                ReqXml.Append("</RequestorPreferences>");
                ReqXml.Append("</Source><RequestDetails>");
                if (SearchDetails.ReferenceNo == "hotel")
                    ReqXml.Append("<ItemInformationDownloadRequest ItemType='hotel'");
                else if (SearchDetails.ReferenceNo == "sightseeing")
                    ReqXml.Append("<ItemInformationDownloadRequest ItemType='sightseeing'");
                //Full downloads request should not any parameters start from here
                if (SearchDetails.SearchType == "rdbFullDownLoad")
                    ReqXml.Append(" />");
                else if (SearchDetails.SearchType == "rdbYestDownload")
                {
                    //Yesterday downloads request start from here
                    ReqXml.Append(" > <IncrementalDownloads>");
                    ReqXml.Append("</IncrementalDownloads>");
                    ReqXml.Append("</ItemInformationDownloadRequest>");
                    //Yesterday downloads request End here
                }
                else if (SearchDetails.SearchType == "rdbIncDownLoad")
                {
                    //Incremental downloads request start from here
                    ReqXml.Append(" > <IncrementalDownloads>");
                    ReqXml.Append("<FromDate>" + SearchDetails.CheckInDate + "</FromDate>");
                    ReqXml.Append("<ToDate>" + SearchDetails.CheckOutDate + "</ToDate>");
                    ReqXml.Append("</IncrementalDownloads>");
                    ReqXml.Append("</ItemInformationDownloadRequest>");
                    //' Incremental downloads request End here
                }
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "ItemInformationDownloadRequest");
                return "";
            }
            return ReqXml.ToString();
        }

        public HotelSearch SearchBookingItemRequest(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en' Currency='USD'>");
                ReqXml.Append(SearchDetails.GTARequestMode);

                ReqXml.Append("<SearchBookingRequest>");
                if (SearchDetails.Orderid !="")
                    ReqXml.Append("<BookingReference>" + SearchDetails.Orderid + "</BookingReference>");
                else
                {
                    ReqXml.Append("<BookingDateRange DateType='creation'>");
                    ReqXml.Append("<FromDate>" + SearchDetails.CheckInDate + "</FromDate>");
                    ReqXml.Append("<ToDate>" + SearchDetails.CheckOutDate + "</ToDate>");
                    ReqXml.Append("</BookingDateRange>");
                    ReqXml.Append("<BookingStatusCode>" + SearchDetails.Status + "</BookingStatusCode>");
                }
                ReqXml.Append("</SearchBookingRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            SearchDetails.GTA_HotelSearchReq = ReqXml.ToString();
            return SearchDetails;
        }

        public HotelCancellation CancelBookingItemRequest(HotelCancellation SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Source>");
                ReqXml.Append("<RequestorID Client='" + SearchDetails.Can_UserID + "' EMailAddress='" + SearchDetails.Can_PropertyId + "' Password='" + SearchDetails.Can_Password + "'/>");
                ReqXml.Append("<RequestorPreferences Currency='USD' Language='en'>");
                ReqXml.Append("<RequestMode>SYNCHRONOUS</RequestMode></RequestorPreferences></Source><RequestDetails>");
                ReqXml.Append("<CancelBookingRequest>");
                ReqXml.Append("<BookingReference ReferenceSource='api'>" + SearchDetails.BookingID + "</BookingReference>");
                ReqXml.Append("</CancelBookingRequest>");
                ReqXml.Append("</RequestDetails></Request>");
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CancelBookingItemRequest");
            }
            SearchDetails.BookingCancelReq = ReqXml.ToString();
            return SearchDetails;
        }

        public HotelSearch ModifyBookingRequestReduceNoofNights(HotelSearch SearchDetails, string cancelFrom)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en' Currency='USD'>");
                ReqXml.Append(SearchDetails.GTARequestMode);

                ReqXml.Append(" <ModifyBookingItemRequest>");
                ReqXml.Append("<BookingReference ReferenceSource='api'>" + SearchDetails.BookingID + "</BookingReference>");
                ReqXml.Append("<BookingItems>");
                ReqXml.Append("<BookingItem ItemType = 'hotel'>");
                ReqXml.Append("<ItemReference>1</ItemReference>");
                ReqXml.Append("<HotelItem>");
                ReqXml.Append("<PeriodOfStay>");

                string FromDate = "", ToDate = "";
                string[] StartDate = SearchDetails.CheckInDate.Split('/');
                FromDate = StartDate[2] + "/" + StartDate[1] + "/" + StartDate[0] + " " + "12:00:00.000";
                System.DateTime checkinDate = Convert.ToDateTime(FromDate);
                string[] EndDat = SearchDetails.CheckOutDate.Split('/');
                ToDate = EndDat[2] + "/" + EndDat[1] + "/" + EndDat[0];
                System.DateTime checkout = Convert.ToDateTime(ToDate);

                SearchDetails.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                if (cancelFrom == "CheckIN")
                {
                    checkinDate = checkinDate.AddDays(SearchDetails.NoofNight);
                    SearchDetails.CheckInDate = checkinDate.ToString("yyyy-MM-dd");
                    SearchDetails.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                }
                else
                {
                    SearchDetails.CheckInDate = checkinDate.ToString("yyyy-MM-dd");
                    checkout = checkout.AddDays(-(SearchDetails.NoofNight));
                    SearchDetails.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                }
                ReqXml.Append("<CheckInDate>" + SearchDetails.CheckInDate + "</CheckInDate>");
                ReqXml.Append("<CheckOutDate>" + SearchDetails.CheckOutDate + "</CheckOutDate>");
                ReqXml.Append("</PeriodOfStay>");
                ReqXml.Append("</HotelItem>");
                ReqXml.Append("</BookingItem>");
                ReqXml.Append("</BookingItems>");
                ReqXml.Append("</ModifyBookingItemRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            SearchDetails.BookingCancelReq = ReqXml.ToString();
            return SearchDetails;
        }

        public HotelSearch SearchRoomTypeRequestXML(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append("<RequestMode>SYNCHRONOUS</RequestMode>");
                ReqXml.Append("</RequestorPreferences>");
                ReqXml.Append("</Source><RequestDetails>");
                ReqXml.Append("<SearchRoomTypeRequest />");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            return SearchDetails;
        }

        public string Country_Req(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchCountryRequest>");
                ReqXml.Append("<CountryName>INDIA</CountryName>");
                ReqXml.Append("</SearchCountryRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            return ReqXml.ToString();
        }

        public string City_Req(HotelSearch SearchDetails, string CountyCode)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchCityRequest CountryCode='" + CountyCode + "'>");
                ReqXml.Append("</SearchCityRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            return ReqXml.ToString();
        }

        public string Area_Req(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchAreaRequest>");
                ReqXml.Append("<AreaName></AreaName>");
                ReqXml.Append("</SearchAreaRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            return ReqXml.ToString();
        }
        //Shightseeing Download Request
        public string ShightseeingDownloadRequest(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Language='en'>");
                ReqXml.Append("<RequestMode>SYNCHRONOUS</RequestMode>");
                ReqXml.Append("</RequestorPreferences>");
                ReqXml.Append("</Source><RequestDetails>");
                ReqXml.Append("<ItemInformationDownloadRequest ItemType='sightseeing'");
                //Full downloads request should not any parameters start from here
                if (SearchDetails.SearchType == "rdbFullDownLoad")
                    ReqXml.Append(" />");
                else if (SearchDetails.SearchType == "rdbYestDownload")
                {
                    //Yesterday downloads request start from here
                    ReqXml.Append(" > <IncrementalDownloads>");
                    ReqXml.Append("</IncrementalDownloads>");
                    ReqXml.Append("</ItemInformationDownloadRequest>");
                    //Yesterday downloads request End here
                }
                else if (SearchDetails.SearchType == "rdbIncDownLoad")
                {
                    //Incremental downloads request start from here
                    ReqXml.Append(" > <IncrementalDownloads>");
                    ReqXml.Append("<FromDate>" + SearchDetails.CheckInDate + "</FromDate>");
                    ReqXml.Append("<ToDate>" + SearchDetails.CheckOutDate + "</ToDate>");
                    ReqXml.Append("</IncrementalDownloads>");
                    ReqXml.Append("</ItemInformationDownloadRequest>");
                    //Incremental downloads request End here
                }
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
                return "";
            }
            return ReqXml.ToString();
        }

        public HotelSearch Promotion_SeachHotelPriceRequestXml(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(SearchDetails.GTAXmlHeader);
                ReqXml.Append("<RequestorID Client='" + SearchDetails.GTAClintID + "' EMailAddress='" + SearchDetails.GTAEmailAddress + "' Password='" + SearchDetails.GTAPassword + "'/>");
                ReqXml.Append("<RequestorPreferences Country='IN' Currency='USD' Language='en'>");
                ReqXml.Append(SearchDetails.GTARequestMode);
                ReqXml.Append("<SearchHotelPriceRequest>");
                if (SearchDetails.SearchType == "AREA")
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.SearchCityCode + "' DestinationType='area'/>");
                else
                    ReqXml.Append("<ItemDestination  DestinationCode='" + SearchDetails.SearchCityCode + "' DestinationType='city'/>");
                ReqXml.Append("<ImmediateConfirmationOnly />");
                if (!string.IsNullOrEmpty(SearchDetails.HotelName))
                    ReqXml.Append("<ItemName>" + SearchDetails.HotelName + "</ItemName>");

                ReqXml.Append("<PeriodOfStay>");
                ReqXml.Append("<CheckInDate>" + SearchDetails.CheckInDate + "</CheckInDate> <Duration>" + SearchDetails.NoofNight + "</Duration>");
                ReqXml.Append("</PeriodOfStay>");
                ReqXml.Append("<IncludeRecommended/><IncludePriceBreakdown/>");

               // ReqXml.Append("<IncludeChargeConditions DateFormatResponse='true' />");
                ReqXml.Append("<Rooms>");
                //set room information
                ReqXml.Append(SetHotelRoomPaxData(SearchDetails, " "));
                ReqXml.Append("</Rooms>");

                if (SearchDetails.StarRating != "0")
                    ReqXml.Append("<StarRating MinimumRating='false'>" + SearchDetails.StarRating + "</StarRating>");
                ReqXml.Append("<OrderBy>pricelowtohigh</OrderBy>");
                ReqXml.Append("<NumberOfReturnedItems>5</NumberOfReturnedItems>");
                ReqXml.Append("</SearchHotelPriceRequest>");
                ReqXml.Append(SearchDetails.GTAXmlFooter);
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "Promotion_SeachHotelPriceRequestXml");
            }
            SearchDetails.GTA_HotelSearchReq = ReqXml.ToString();
            return SearchDetails;
        }
  }
}
