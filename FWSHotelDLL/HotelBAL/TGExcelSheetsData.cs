﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using System.IO.Compression;
using System.Diagnostics;
using Microsoft.Win32;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using HotelShared;
using HotelDAL;

public class TGExcelSheetsData
{
    SqlConnection contest = new SqlConnection(ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString);
    public TGExcelSheetsData()
    { }
  
    public String DownLoadFile()
    {
        try
        {
            HotelDA objhtlDa = new HotelDA(); HotelBooking objAuthentication = new HotelBooking();
            objAuthentication.Provider = "TGStaticDump";
            objAuthentication = objhtlDa.GetBookingCredentials(objAuthentication, "TEST");
            using (var client = new WebClient())
            {
                string ZipPath = objAuthentication.TGPropertyId;
                client.Credentials = new NetworkCredential(objAuthentication.TGUsername, objAuthentication.TGPassword);
                client.DownloadFile(objAuthentication.TGUrl, ZipPath);
                HotelBAL.HotelAvailabilitySearch objzipfile = new HotelBAL.HotelAvailabilitySearch();
                string unziped = objzipfile.UnZipFile(ZipPath);
                if (unziped == "true")
                    return "File Download Completed. and folder Unziped";
                else
                    return "File Download Completed. Unziped not complete";
            }
        }
        catch (Exception ex)
        {
            HotelDA.InsertHotelErrorLog(ex, "");
            return ex.Message;
        }
    }
    
    public bool InsertExcelRecards(string filepath)
    {
        bool flag = false;
        try
        {
            string connString = "",sheetName = "", extenstion = ".xlsx";
            if (extenstion == ".xlsx")
            {
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;";
            }
            OleDbConnection oledbConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;");
            //OleDbConnection oledbConn = new OleDbConnection(connString);
            oledbConn.Open();
            DataTable dt = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheetName = dt.Rows[i]["TABLE_NAME"].ToString();
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheetName + "]", oledbConn);
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                oleda.SelectCommand = cmd;
                DataSet ds = new DataSet();
                oleda.Fill(ds, sheetName);

                //if (sheetName == "Facilities$" || sheetName == "'Facilities-1$'" || sheetName == "'Facilities-2$'" || sheetName == "'Facilities-3$'" || sheetName == "'Facilities-4$'" || sheetName == "'Facilities-5$'")
                //{
                //   // InsertFacility(ds.Tables[0]);
                //}
               // else if (sheetName == "HotelOverview$")
                 if (sheetName == "HotelOverview$")
                {
                    InsertHotelOverview(ds.Tables[0]);
                }

                //else if (sheetName == "HotelReviews$")
                //{
                // //  InsertHotelReview(ds.Tables[0]);
                //}
                //else if (sheetName == "ImageUrls$" || sheetName == "'ImageUrls-1$'" || sheetName == "'ImageUrls-2$'")
                //{
                //  // InsertImageUrl(ds.Tables[0]);
                //}

                //else if (sheetName == "InAndAround$" || sheetName == "'InAndAround-1$'")
                //{
                // //  InsertInAround(ds.Tables[0]);
                //}
                //else if (sheetName == "RoomDescription$")
                //{
                //  // InsertRoomDescription(ds.Tables[0]);
                //}
                //else if (sheetName == "CityList$")
                //{
                //  // InsertCity(ds.Tables[0]);
                //}
                //else if (sheetName == "POI_Data$")
                //{
                //  // InsertPOIData(ds.Tables[0]);
                //}
            }
           if( dt.Rows.Count >0)
               flag = true;
           oledbConn.Close();
        }
        catch (Exception ex)
        {
            HotelDA.InsertHotelErrorLog(ex, "");
        }
        return flag;
    }

    protected void InsertFacility(DataTable fcilityDt)
    {
        for (int j = 0; j < fcilityDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", fcilityDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", fcilityDt.Rows[j]["Amenity_id"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@AmenityType", fcilityDt.Rows[j]["AmenityType"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Description", fcilityDt.Rows[j]["Description"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@VendorName", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Location", "");
                cmd1.Parameters.AddWithValue("@City", "");
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", "");
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "Facilities");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertHotelOverview(DataTable OverviewDt)
    {
        for (int j = 0; j < OverviewDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", OverviewDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", "");
                cmd1.Parameters.AddWithValue("@Description", "");
                cmd1.Parameters.AddWithValue("@VendorName", OverviewDt.Rows[j]["VendorName"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelClass", OverviewDt.Rows[j]["HotelClass"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Location", OverviewDt.Rows[j]["Location"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@City", OverviewDt.Rows[j]["City"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Country", OverviewDt.Rows[j]["Country"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address1", OverviewDt.Rows[j]["Address1"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address2", OverviewDt.Rows[j]["Address2"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Area", OverviewDt.Rows[j]["Area"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address", OverviewDt.Rows[j]["Address"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelOverview", OverviewDt.Rows[j]["HotelOverview"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@ReviewRating", OverviewDt.Rows[j]["ReviewRating"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@ReviewCount", OverviewDt.Rows[j]["ReviewCount"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Latitude", OverviewDt.Rows[j]["Latitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Longitude", OverviewDt.Rows[j]["Longitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", OverviewDt.Rows[j]["DefaultCheckInTime"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", OverviewDt.Rows[j]["DefaultCheckOutTime"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Hotel_Star", OverviewDt.Rows[j]["Hotel_Star"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupID", OverviewDt.Rows[j]["HotelGroupID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupName", OverviewDt.Rows[j]["HotelGroupName"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@ImagePath", OverviewDt.Rows[j]["ImagePath"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelSearchKey", OverviewDt.Rows[j]["HotelSearchKey"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@REF", "HotelOverview");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertHotelReview(DataTable ReviewDt)
    {
        for (int j = 0; j < ReviewDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", ReviewDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", ReviewDt.Rows[j]["AvgGuestRating"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@AmenityType", ReviewDt.Rows[j]["OverallRating"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Description", ReviewDt.Rows[j]["Comments"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@VendorName", ReviewDt.Rows[j]["RoomQuality"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelClass", ReviewDt.Rows[j]["ServiceQuality"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Location", ReviewDt.Rows[j]["DiningQuality"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@City", ReviewDt.Rows[j]["Consumer_city"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Country", ReviewDt.Rows[j]["Consumer_Country"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address1", ReviewDt.Rows[j]["Customer_name"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address2", ReviewDt.Rows[j]["Post_Date"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Area", ReviewDt.Rows[j]["Cleanliness"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", "");
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "HotelReviews");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertImageUrl(DataTable ImageDt)
    {
        for (int j = 0; j < ImageDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", ImageDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", "");
                cmd1.Parameters.AddWithValue("@Description", "");
                cmd1.Parameters.AddWithValue("@VendorName", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Location", "");
                cmd1.Parameters.AddWithValue("@City", "");
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", ImageDt.Rows[j]["ImageUrl"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelSearchKey", ImageDt.Rows[j]["Content-Title"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@REF", "ImageUrls");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertInAround(DataTable aroundDt)
    {
        for (int j = 0; j < aroundDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", aroundDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", aroundDt.Rows[j]["DistanceInKm"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Description", aroundDt.Rows[j]["NameOfAttraction"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@VendorName", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Location", "");
                cmd1.Parameters.AddWithValue("@City", "");
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", "");
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "InAndAround");

                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertRoomDescription(DataTable RoomDessDt)
    {
        for (int j = 0; j < RoomDessDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", RoomDessDt.Rows[j]["VendorID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Amenity_id", RoomDessDt.Rows[j]["RoomTypeID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@AmenityType", RoomDessDt.Rows[j]["RoomType"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Description", RoomDessDt.Rows[j]["RoomDescription"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@VendorName", RoomDessDt.Rows[j]["Max_Adult_Occupancy"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelClass", RoomDessDt.Rows[j]["Max_Child_Occupancy"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Location", RoomDessDt.Rows[j]["Max_Infant_Occupancy"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@City", RoomDessDt.Rows[j]["Max_Guest_Occupancy"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", RoomDessDt.Rows[j]["ImagePath"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "RoomDescription");

                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertCity(DataTable CityDt)
    {
        for (int j = 0; j < CityDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", "");
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", "");
                cmd1.Parameters.AddWithValue("@Description", "");
                cmd1.Parameters.AddWithValue("@VendorName", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Location", "");
                cmd1.Parameters.AddWithValue("@City", CityDt.Rows[j]["City"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", "");
                cmd1.Parameters.AddWithValue("@Longitude", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", "");
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", "");
                cmd1.Parameters.AddWithValue("@Hotel_Star", "");
                cmd1.Parameters.AddWithValue("@HotelGroupID", "");
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", "");
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "CityList");

                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }
    protected void InsertPOIData(DataTable POIDataDt)
    {
        for (int j = 0; j < POIDataDt.Rows.Count; j++)
        {
            try
            {
                contest.Open();
                SqlCommand cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", "");
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", "");
                cmd1.Parameters.AddWithValue("@Description", "");
                cmd1.Parameters.AddWithValue("@VendorName", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Location", "");
                cmd1.Parameters.AddWithValue("@City", "");
                cmd1.Parameters.AddWithValue("@Country", "");
                cmd1.Parameters.AddWithValue("@Address1", "");
                cmd1.Parameters.AddWithValue("@Address2", "");
                cmd1.Parameters.AddWithValue("@Area", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Latitude", POIDataDt.Rows[j]["latitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Longitude", POIDataDt.Rows[j]["longitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", POIDataDt.Rows[j]["Poi_Id"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", POIDataDt.Rows[j]["Poi_Seo_Id"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Hotel_Star", POIDataDt.Rows[j]["Poi_Name"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupID", POIDataDt.Rows[j]["City_Id"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupName", POIDataDt.Rows[j]["City_Name"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@ImagePath", POIDataDt.Rows[j]["Seo_City_Name"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@REF", "POI_Data");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            finally
            {
                contest.Close();
            }
        }
    }



    public bool InsertExcelRecards_Expedia(string filepath)
    {
        bool flag = false;
        OleDbConnection oledbConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;");
        try
        {
            string sheetName = "";
            oledbConn.Open();
            DataTable dt = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheetName = dt.Rows[i]["TABLE_NAME"].ToString();
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheetName + "]", oledbConn);
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                oleda.SelectCommand = cmd;
                DataSet ds = new DataSet();
                oleda.Fill(ds, sheetName);

                if (sheetName == "ActivePropertyList$")
                {
                    InsertExpediaHotelProperty(ds.Tables[0]);
                }
            }
            if (dt.Rows.Count > 0)
                flag = true;
        }
        catch (Exception ex)
        {
            HotelDA.InsertHotelErrorLog(ex, "");
        }
        finally { oledbConn.Close(); }

        return flag;
    }
    protected void InsertExpediaHotelProperty(DataTable OverviewDt)
    {
        SqlCommand cmd1; contest.Open();
        for (int j = 0; j < OverviewDt.Rows.Count; j++)
        {
            try
            {
                cmd1 = new SqlCommand("SP_INS_HOTEL_EXCEL_RECORD", contest);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@VendorID", OverviewDt.Rows[j]["EANHotelID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@VendorName", OverviewDt.Rows[j]["Name"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Location", OverviewDt.Rows[j]["Location"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@City", OverviewDt.Rows[j]["City"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Country", OverviewDt.Rows[j]["Country"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address1", OverviewDt.Rows[j]["Address1"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Address2", OverviewDt.Rows[j]["Address2"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Area", OverviewDt.Rows[j]["PostalCode"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Latitude", OverviewDt.Rows[j]["Latitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Longitude", OverviewDt.Rows[j]["Longitude"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckInTime", OverviewDt.Rows[j]["CheckInTime"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@DefaultCheckOutTime", OverviewDt.Rows[j]["CheckOutTime"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@Hotel_Star", OverviewDt.Rows[j]["StarRating"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupID", OverviewDt.Rows[j]["ChainCodeID"].ToString().Trim());
                cmd1.Parameters.AddWithValue("@HotelGroupName", "");
                cmd1.Parameters.AddWithValue("@ImagePath", "");
                cmd1.Parameters.AddWithValue("@HotelSearchKey", "");
                cmd1.Parameters.AddWithValue("@HotelClass", "");
                cmd1.Parameters.AddWithValue("@Address", "");
                cmd1.Parameters.AddWithValue("@HotelOverview", "");
                cmd1.Parameters.AddWithValue("@ReviewRating", "");
                cmd1.Parameters.AddWithValue("@ReviewCount", "");
                cmd1.Parameters.AddWithValue("@Amenity_id", "");
                cmd1.Parameters.AddWithValue("@AmenityType", "");
                cmd1.Parameters.AddWithValue("@Description", "");
                cmd1.Parameters.AddWithValue("@REF", "TBL_Htl_HotelOverview");
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
            }
            
        }
        contest.Close(); 
    }

}

