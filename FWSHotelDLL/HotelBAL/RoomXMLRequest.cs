﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using HotelShared;
namespace HotelBAL
{
   public class RoomXMLRequest
    {
        public HotelSearch HotelSearchAvailabilityRequest(HotelSearch HtlSearchQuery)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<AvailabilitySearch><Authority>");
                ReqXml.Append("<Org>" + HtlSearchQuery.RoomXMLOrgID + "</Org>");
                ReqXml.Append("<User>" + HtlSearchQuery.RoomXMLUserID + "</User>");
                ReqXml.Append("<Password>" + HtlSearchQuery.RoomXMLPassword + "</Password>");
                ReqXml.Append("<Currency>INR</Currency>");
                ReqXml.Append("<Version>1.25</Version></Authority>");

                if (HtlSearchQuery.HtlCode == null)
                    ReqXml.Append("<RegionId>" + HtlSearchQuery.RegionId + "</RegionId>");
                else
                    ReqXml.Append("<HotelId>" + HtlSearchQuery.HtlCode + "</HotelId>");
       
                ReqXml.Append("<HotelStayDetails>");
                ReqXml.Append("<ArrivalDate>" + HtlSearchQuery.CheckInDate + "</ArrivalDate>");
                ReqXml.Append("<Nights>"+ HtlSearchQuery.NoofNight +"</Nights>");

                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                { 
                    ReqXml.Append("<Room><Guests>");
                    for(int k = 0; k < Convert.ToInt32(HtlSearchQuery.AdtPerRoom[i]); k++)
                    {  ReqXml.Append("<Adult/>");}
                    if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<Child age='" + HtlSearchQuery.ChdAge[i, j] + "' />");
                        }
                    }
                    ReqXml.Append("</Guests></Room>");
                }
                ReqXml.Append("<Nationality>IN</Nationality>");
                ReqXml.Append("</HotelStayDetails>");
                ReqXml.Append("<HotelSearchCriteria>");
                if(HtlSearchQuery.HotelName != "")
                    ReqXml.Append("<HotelName>" + HtlSearchQuery.HotelName + "</HotelName>");
                else
                {
                    if (HtlSearchQuery.StarRating != "0")
                        ReqXml.Append("<MinStars>" + Convert.ToString(HtlSearchQuery.StarRating) + "</MinStars>");
                    else
                        ReqXml.Append("<MinStars>1</MinStars>");
                }

                ReqXml.Append("<AvailabilityStatus>any</AvailabilityStatus></HotelSearchCriteria>");
                if (HtlSearchQuery.ResultType != null)
                {
                    ReqXml.Append("<DetailLevel>" + HtlSearchQuery.ResultType + "</DetailLevel>");
                   // ReqXml.Append("<CustomDetailLevel>" + HtlSearchQuery.SearchType + ",canxfees</CustomDetailLevel>");
                }
                else
                {
                    ReqXml.Append("<DetailLevel>basic</DetailLevel>");
                   // ReqXml.Append("<CustomDetailLevel>basic,canxfees</CustomDetailLevel>");
                }
                ReqXml.Append("</AvailabilitySearch>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchAvailabilityRequest");
            }
            HtlSearchQuery.RoomXML_HotelSearchReq = ReqXml.ToString();
            return HtlSearchQuery;
        }

        public string BookingRoomXmlRequest(HotelBooking SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<BookingCreate><Authority>");
                ReqXml.Append("<Org>" + SearchDetails.RoomXMLOrgID + "</Org>");
                ReqXml.Append("<User>" + SearchDetails.RoomXMLUserID + "</User>");
                ReqXml.Append("<Password>" + SearchDetails.RoomXMLPassword + "</Password>");
                ReqXml.Append("<Currency>INR</Currency>");
                ReqXml.Append("<Version>1.25</Version></Authority>");
                ReqXml.Append("<QuoteId>" + SearchDetails.RoomPlanCode + "</QuoteId>");
                ReqXml.Append("<HotelStayDetails>");
                ReqXml.Append("<Room><Guests>");
                ReqXml.Append("<Adult id='0' title='" + SearchDetails.PGTitle + "' first='" + SearchDetails.PGFirstName + "' last='" + SearchDetails.PGLastName + "' />");
                bool roomHeade1 = true, roomHeade2 = true, roomHeade3 = true;
                if (SearchDetails.AdditinalGuest != null)
                {
                    ArrayList list = new ArrayList();
                    list = SearchDetails.AdditinalGuest;
                    for (int i = 0; i <= list.Count - 1; i++)
                    {
                        ArrayList li = new ArrayList();
                        li = (ArrayList)list[i];
                        if (Convert.ToInt32(li[5]) > 1 && roomHeade1 == true)
                        {
                            roomHeade1 = false;
                            ReqXml.Append("</Guests></Room>");
                            ReqXml.Append("<Room><Guests>");
                        }
                        if (Convert.ToInt32(li[5]) > 2 && roomHeade2 == true)
                        {
                            roomHeade2 = false;
                            ReqXml.Append("</Guests></Room>");
                            ReqXml.Append("<Room><Guests>");
                        }
                        if (Convert.ToInt32(li[5]) > 3 && roomHeade3 == true)
                        {
                            roomHeade3 = false;
                            ReqXml.Append("</Guests></Room>");
                            ReqXml.Append("<Room><Guests>");
                        }

                        if (li[3].ToString() == "Adult")
                            ReqXml.Append("<Adult id='" + (i + 2).ToString() + "' title='" + li[0] + "' first='" + li[1] + "' last='" + li[2] + "' />");
                        else
                        {
                            if (li[4].ToString() == "0")
                                ReqXml.Append("<Child id='" + (i + 2).ToString() + "' title='" + li[0] + "' first='" + li[1] + "' last='" + li[2] + "'  age='1'/>");
                            else
                                ReqXml.Append("<Child id='" + (i + 2).ToString() + "' title='" + li[0] + "' first='" + li[1] + "' last='" + li[2] + "'  age='" + li[4] + "'/>");
                        }
                    }
                    ReqXml.Append("</Guests></Room>");
                }
                else
                { ReqXml.Append("</Guests></Room>"); }

                ReqXml.Append("<Nationality>IN</Nationality>");
                ReqXml.Append("</HotelStayDetails>");
                ReqXml.Append("<HotelSearchCriteria>");
                ReqXml.Append("<AvailabilityStatus>allocation</AvailabilityStatus>");
                ReqXml.Append("<DetailLevel>basic</DetailLevel>");
                ReqXml.Append("</HotelSearchCriteria>");
                ReqXml.Append("<CommitLevel>" + SearchDetails.BookingType + "</CommitLevel>");
                ReqXml.Append("</BookingCreate>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "PreBookHotelPriceRoomXmlRequest");
            }
            //SearchDetails.ProBookingReq = ReqXml.ToString();
            return ReqXml.ToString();
        }
        public string CancelationPolicyRoomXmlRequest(HotelSearch SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<BookingCreate><Authority>");
                ReqXml.Append("<Org>" + SearchDetails.RoomXMLOrgID + "</Org>");
                ReqXml.Append("<User>" + SearchDetails.RoomXMLUserID + "</User>");
                ReqXml.Append("<Password>" + SearchDetails.RoomXMLPassword + "</Password>");
                ReqXml.Append("<Currency>INR</Currency>");
                ReqXml.Append("<Version>1.25</Version></Authority>");
                ReqXml.Append("<QuoteId>" + SearchDetails.HtlRoomCode + "</QuoteId>");
                ReqXml.Append("<HotelStayDetails>");
                for (int i = 0; i < Convert.ToInt32(SearchDetails.NoofRoom); i++)
                {
                    ReqXml.Append("<Room><Guests>");
                    for (int k = 0; k < Convert.ToInt32(SearchDetails.AdtPerRoom[i]); k++)
                    {
                        ReqXml.Append("<Adult id='" + (i + k).ToString() + "' title='Mr' first='Test' last='" + sterNumToWord(i + k) + "' />");
                    }
                    if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(SearchDetails.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<Child id='" + (SearchDetails.TotAdt + j + i).ToString() + "' title='Mr' first='Test' last='" + sterNumToWord(i + j) + "' age='" + SearchDetails.ChdAge[i, j].ToString() + "' />");
                        }
                    }
                    ReqXml.Append("</Guests></Room>");
                }
                ReqXml.Append("<Nationality>IN</Nationality>");
                ReqXml.Append("</HotelStayDetails>");
                ReqXml.Append("<HotelSearchCriteria>");
                ReqXml.Append("<AvailabilityStatus>allocation</AvailabilityStatus>");
                ReqXml.Append("<DetailLevel>basic</DetailLevel>");
                ReqXml.Append("</HotelSearchCriteria>");
                ReqXml.Append("<CommitLevel>prepare</CommitLevel>");
                ReqXml.Append("</BookingCreate>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CancelationPolicyRoomXmlRequest");
            }
            return ReqXml.ToString(); 
        }
        //public HotelBooking BookingRoomXmlRequest(HotelBooking SearchDetails)
        //{
        //    StringBuilder ReqXml = new StringBuilder();
        //    try
        //    {
        //        ReqXml.Append("<BookingCreate><Authority>");
        //        ReqXml.Append("<Org>" + SearchDetails.RoomXMLOrgID + "</Org>");
        //        ReqXml.Append("<User>" + SearchDetails.RoomXMLUserID + "</User>");
        //        ReqXml.Append("<Password>" + SearchDetails.RoomXMLPassword + "</Password>");
        //        ReqXml.Append("<Currency>INR</Currency>");
        //        ReqXml.Append("<Version>1.25</Version></Authority>");
        //        ReqXml.Append("<QuoteId>" + SearchDetails.RoomPlanCode + "</QuoteId>");
        //        ReqXml.Append("<HotelStayDetails>");
        //        ReqXml.Append("<ArrivalDate>" + SearchDetails.CheckInDate + "</ArrivalDate>");
        //        ReqXml.Append("<Nights>" + SearchDetails.NoofNight + "</Nights>");

        //        for (int i = 0; i < Convert.ToInt32(SearchDetails.NoofRoom); i++)
        //        {
        //            ReqXml.Append("<Room><Guests>");
        //            for (int k = 0; k < Convert.ToInt32(SearchDetails.AdtPerRoom[i]); k++)
        //            {
        //                ReqXml.Append("<Adult>");
        //                ReqXml.Append("<Id>" + k.ToString() + "</Id>");
        //                ReqXml.Append("<Title>" + SearchDetails.PGTitle + "</Title>");
        //                ReqXml.Append("<Forename>" + SearchDetails.PGFirstName + "</Forename>");
        //                ReqXml.Append("<Surname>" + SearchDetails.PGLastName + "</Surname>");
        //                ReqXml.Append("</Adult>");
        //            }
        //            if (Convert.ToInt32(Convert.ToInt32(SearchDetails.ChdPerRoom[i])) > 0)
        //            {
        //                for (int j = 0; j < Convert.ToInt32(SearchDetails.ChdPerRoom[i]); j++)
        //                {
        //                    ReqXml.Append("<Child>");
        //                    ReqXml.Append("<Id>" + j.ToString() + "</Id>");
        //                    ReqXml.Append("<Title>" + SearchDetails.PGTitle + "</Title>");
        //                    ReqXml.Append("<Forename>" + SearchDetails.PGFirstName + "</Forename>");
        //                    ReqXml.Append("<Surname>" + SearchDetails.PGLastName + "</Surname>");
        //                    ReqXml.Append("<Age>" + SearchDetails.ChdAge[i, j].ToString() + "</Age>");
        //                    ReqXml.Append("</Child>");
        //                }
        //            }
        //            ReqXml.Append("<Room><Guests>");
        //        }
        //        ReqXml.Append("<Nationality>IN</Nationality>");
        //        ReqXml.Append("</HotelStayDetails>");
        //        ReqXml.Append("<CommitLevel>" + SearchDetails.Status + "</CommitLevel>");
        //        ReqXml.Append("</BookingCreate>");
        //    }
        //    catch (Exception ex)
        //    {
        //        ReqXml.Append(ex.Message);
        //        HotelDAL.HotelDA.InsertHotelErrorLog(ex, "BookHotelPriceRoomXmlRequest");
        //    }
        //    SearchDetails.ProBookingReq = ReqXml.ToString();
        //    return SearchDetails;
        //}
        public HotelSearch SearchItemInformationRequest(HotelSearch HtlSearchQuery, string itemCode)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<AvailabilitySearch><Authority>");
                ReqXml.Append("<Org>" + HtlSearchQuery.RoomXMLOrgID + "</Org>");
                ReqXml.Append("<User>" + HtlSearchQuery.RoomXMLUserID + "</User>");
                ReqXml.Append("<Password>" + HtlSearchQuery.RoomXMLPassword + "</Password>");
                ReqXml.Append("<Currency>INR</Currency>");
                ReqXml.Append("<Version>1.25</Version></Authority>");
                ReqXml.Append("<HotelId>" + itemCode + "</HotelId>");
                ReqXml.Append("<HotelStayDetails>");
                ReqXml.Append("<ArrivalDate>" + HtlSearchQuery.CheckInDate + "</ArrivalDate>");
                ReqXml.Append("<Nights>1</Nights>");
                ReqXml.Append("<Room><Guests><Adult/></Guests></Room>");
                ReqXml.Append("<Nationality>IN</Nationality>");
                ReqXml.Append("</HotelStayDetails>");
                ReqXml.Append("<HotelSearchCriteria>");
                ReqXml.Append("<AvailabilityStatus>any</AvailabilityStatus></HotelSearchCriteria>");
                ReqXml.Append("<DetailLevel>full</DetailLevel>");
                ReqXml.Append("</AvailabilitySearch>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SearchItemInformationRequest");
            }
            HtlSearchQuery.GTAItemInformationReq = ReqXml.ToString();
            return HtlSearchQuery;
        }
        public string HotelCancellationRequest(HotelCancellation bookingDetails, string CancelType)
        { 
             StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<BookingCancel><Authority>");
                ReqXml.Append("<Org>" + bookingDetails.Can_PropertyId + "</Org>");
                ReqXml.Append("<User>" + bookingDetails.Can_UserID + "</User>");
                ReqXml.Append("<Password>" + bookingDetails.Can_Password + "</Password>");
                ReqXml.Append("<Currency>INR</Currency>");
                ReqXml.Append("<Version>1.25</Version></Authority>");
                ReqXml.Append("<BookingId>" + bookingDetails.BookingID + "</BookingId>");
                ReqXml.Append("<CommitLevel>" + CancelType + "</CommitLevel>");
                ReqXml.Append("</BookingCancel>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelCancellationRequest");
            }
            return ReqXml.ToString();
        }
        protected string sterNumToWord(int num)
        {
            string strword = "test";
            switch (num)
            {
                case 0:
                    strword = "Zero";
                    break;
                case 1:
                    strword = "one";
                    break;
                case 2:
                    strword = "two";
                    break;

                case 3:
                    strword = "three";
                    break;

                case 4:
                    strword = "four";
                    break;

                case 5:
                    strword = "five";
                    break;

                case 6:
                    strword = "six";
                    break;

                case 7:
                    strword = "seven";
                    break;

                case 8:
                    strword = "eight";
                    break;

                case 9:
                    strword = "nine";
                    break;

            }
            return strword;
        }
    }
}
